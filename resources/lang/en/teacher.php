<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'assign' =>'Instructor',
    'assigned' => 'Instructor Assigned',
    'for_assign' => 'Assignable Instructors',
    'assign_success' => 'Assigning Succeed',
    'unassign_success' => 'Unassigning Succeed',

];
