<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'User Manager',
    'create' =>'Create a New User',
    'title' => 'User Name',
    'email'=>'Email',
    'pwd' => 'Password',
    'real_name' => 'Name',
    'role' =>'Role',
    'ranger' => 'Target Users',
    'department' => 'Organization',
    'position' =>'Position',
    'rank' => 'Level',
    'import' =>'User Data Uploader',
    'report' => 'User Information',
    'login_last' => 'Last Access',
    'login_times' => 'Login Times',
    'self_info' => 'My Account',
    'attribute_manage' => 'User Attribute Configuration',
    'new_password' => 'New Password',
    'confirmation_password' => 'Confirm Password',
    'approve' => 'Approver',
    'approve_name' => 'Approver Name',
    'sex'=>'Gender',
    'number' => 'Number of accounts',
    'tel' => 'Telephone',
    'address' => 'Address',
    'created_time' => 'Register Time',
    'weixin_bind' => 'WeiCat Bind',
    'company' => 'Company',
    'company_tag' => 'Please enter your company information',
    'favorite' => 'Favorite Function',


];
