<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'distribute' =>'Assignment',
    'distributed' => 'Assigned Participant(s)',
    'for_distribute' => 'Select Participant(s)',
    'distribute_success' => 'Assigning Succeed',
    'undistribute_success' => 'Remove participant(s) Succeed',
    'overdue'=>'Exipred Participant(s)',
    'redistribute_success' => 'Re-assigning Succeed',
    'type1'=>'Online Course',
    'type2' => 'Exam',
    'type' => 'Training Type',
    'name' => 'Training Title',
'all' => 'All types',

];
