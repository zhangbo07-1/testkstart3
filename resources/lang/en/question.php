<?php

return [

    /*
    |--------------------------------------------------------------------------
    | question Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during question for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'Question Manager',
    'create' => 'Create a New Question',
    'edit'   => 'Edit Question',
    'select' => 'Select Question',
    'radio' => 'Single Choice',
    'mulit' => 'Multiple Choice',
    'false' => 'True Or False',
    'grade'  => 'Rating Question',
    'essay'  => 'Essay',
    'title'  => 'Question',
    'grade_hight' => 'Excellent',
    'grade_low'   => 'Poor',
    'select_title' =>'Choice',
    'priority' => 'Mandatory',
    'true' =>'Correct',
    'unit' => 'Unit',
    'answer' => 'Answer',
    'id'=> 'Question ID',
    'true_select' => 'True Select',
    'answer' => 'Answer',
    'if_true' => 'If True',


];
