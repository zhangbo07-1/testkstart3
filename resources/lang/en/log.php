<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Login Log',
    'email' => 'Mail Log',
    'create' => 'Create a New Material',
    'title' => 'Material Title',
    'file'=>'File Name',
    'size' => 'File Size',
    'login' => 'Login',
    'logout' => 'Log Out',
    'manage' => 'Log Manager',


];
