<?php

return [

    /*
    |--------------------------------------------------------------------------
    | question Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during question for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'Question Pool Manager',
    'create' => 'Add a New Question Pool',
    'name' => 'Question Pool',
    'title' => 'Title',


];
