<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'user' => "Maker",
    'report' => 'Course Report',
    'study_manage' => 'Learning Manager',
    'manage' => 'Course Manager',
    'create' => 'Create a New Course',
    'title' => 'Course Title',
    'name' => 'Course',
    'description' => 'Course Description',
    'id' =>'Course ID',
    'hours' => 'Duration',
    'email' => 'Send E-mail Notification or not',
    'link' => 'Auto Enrollment for New Accounts',
    'link_log' => 'This exam will not be able to do assignment if you select this option.',
    'target' => 'Target Audience ',
    'order' => 'Enforce The Learning Order As Below',
    'order_log' => '（online course->exam->evaluation）',
    'if_open_overdue' => 'Allow To Open The Expired Coursware',
    'upload' => 'Upload Coureware',
    'upload_log' => 'Select a zip file which must be SCORM2004 content package.',
    'ware_name' => 'Course Title',
    'study_times' =>'Total # of Online Launches',
    'online' => 'Online Course',
    'type' => 'Course Type',
    'my_lesson' => 'My Training',
    'lesson' => 'Enrolled Courses',
    'lesson_done' => 'Completed Courses',
    'lesson_overdue' => 'Expired Courses',
    'edit' => 'Course Properties',
    'distribute' => 'Assign Course',
    'update' => 'Update Course',
    'push' => 'Publish Course',
    'preview' => 'Preview Course',
    'exam' => 'Exam',
    'evaluation' => 'Evaluation',
    'delete' => 'Delete',
    'upload_success' => 'The courseware has been uploaded successfully.',
    'change'=> 'Upload this coureware.',
    'push_success' => 'The courseware has been published successfully.',
    'no_ware' => 'No Coureware Exist',
    'send_limit' => 'Auto-Enrollment Target',
    'status' => 'Course Status',
    'create_time'=> 'Create Time',
    'not_set' => 'Not Set',
    'is_set' => 'Set',
    'under_way' => 'In Process',
    'done' => 'Completed',
    'all_done' => 'Completed',
    'not_start' => 'Not Started',
    'overdue' => 'Expired',
    'resource' => 'Material Manager',
    'class' => 'Off-Line Class',
    'class_title' => 'Class Name',
    'class_address' => 'Class Address',
    'class_create' => 'Add Class',
    'class_lesson' => 'Time Space',
    'add_lesson' => 'Add Time Space',
    'delete_lesson' => 'Delete Time Space',
    'class_teacher' => 'Class Teacher',
    'course_survey' => 'Course Survey',
    'class_distribute' => 'Class Distribute',
    'class_late' => 'Be Late Time',
    'enroll_success' => 'Enroll Success',
    'sign_success' => 'Sign Success',
    'lesson_status1' => 'Absenteeism',
    'lesson_status2' => 'Be Late',
    'lesson_status3' => 'Attend',
    'class_select' => 'Chose The Class',

];
