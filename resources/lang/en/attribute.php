<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'User Attribute Configuration',

    'tag_limit' => 'The attribute code contains disallowed characters, only letters/numbers/underline/hyphen allowed',
    'now' => 'Current',
    'tag' => 'Code',
    'attribute_1' => 'User Attribute 1',
    'attribute_2' => 'User Attribute 2',
    'attribute_3' => 'User Attribute 3',
    'attribute_4' => 'User Attribute 4',
    'attribute_5' => 'User Attribute 5',
    'import' => 'User Attribute Loader',
    'limit_now' => 'Currect Criteria',
'title' => 'Title',
    'rename' => 'Rename',
    'title_rename' => 'Rename Title',
    'add' => 'Add',
    'select' => 'Please Select ',
];
