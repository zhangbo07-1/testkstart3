<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'Material Manager',
    'name' => 'Materials',
    'create' => 'Create a New Material',
    'edit' => 'Edit Material',
    'title' => 'Material Title',
    'file'=>'File Name',
    'size' => 'File Size',
    'could_add_continue'=> '，You can continue to upload material.',


];
