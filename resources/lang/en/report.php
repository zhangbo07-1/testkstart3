<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name' => 'Report',
    'course' => 'Learning Report',
    'evaluation' => 'Evaluation Report',
    'user' => 'User Report',
    'question' => 'Test Report',
    'exam' => 'Exam Report',
    'class' => 'Class Report',



];
