<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'Survey Manager',
    'create' => 'Create a New Survey',
    'name' => 'Survey',
    'search' => 'Please enter the survey name you need to search.',
    'start' => 'Start Survey',
    'assign' => 'Assign Survey',
    'assigned'=>'Assigned Survey',
    'for_assign' => 'Assignable Survey',
    'assign_success' => 'Assigning Succeed',
    'deallocate_success' => 'Unassigning Succeed',

    'result' => 'Survey Result',

];
