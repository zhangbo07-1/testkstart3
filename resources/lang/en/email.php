<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'Email Manager',
    'type_1' =>'Course Assignment Notification',
    'type_2' => 'Escalation of Completion Reminder',
    'type_3' => 'Course Completion Notification',
    'type_4' => 'Course Expire Reminder',
    'type_5' => 'Survey Invitation',
    'type_6' => 'Authentication Email',
    'type_7' => 'Resend',
    'type_8' => 'Administrator Approval',
    'type_9' => 'Welcome Email',
    'type_10' => 'Reset Password',
    'type_11' => 'Pending Grading Notification',
    'type_12' => 'graded Notification',


];
