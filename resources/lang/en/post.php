<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'Forum Manager',
    'name' => 'Forum',
    'create' => 'Create a New Forum',
    'select_image' => 'Select Title Image',
    'summary' =>'Summary',
    'comment' => 'Comment',


];
