<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logo' => 'Logo Manager',
    'info' => 'System Information',
    'logo_limit' => 'Please make sure your logo image is png file with 300×100px size,thanks!',
    'logo_now' => 'Current Logo',
    'company' => 'Company Name',
    'phone'  => 'Company Telephone Number',
    'email' => 'Company Email',
    'company_limit' =>'Company name only allowed letter characters.',
    'manage' => 'System Configuration',
    'censor_manage' => 'Verify Manager',
    'if_censor' => 'Administrator approval required or not',
    'need_censor' => 'Yes',
    'not_censor' => 'No',
    'if_validate' => 'Email verification required or not',
    'need_validate' => 'Yes',
    'not_validate' => 'No',

    'if_register' => 'Self-registration allowed or not',
    'need_register' => 'Yes',
    'not_register' => 'No',
    'add_email_suffix' => 'Please enter the mail suffix for login，for example: 163.com ',
    'email_suffix_example' => '163.com',
'email_manage' => 'Email Manage',
    'department' => 'Default Department',
    'login_by_email' => 'Login By Email',


];
