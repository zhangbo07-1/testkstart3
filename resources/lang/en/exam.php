<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'Exam Manager',
    'create' => 'Create a New Exam',
    'edit' => 'Exam Edit',
    'title' => 'Exam Title',
    'description' => 'Exam Description',
    'id' =>'Exam ID',
    'pass_score' => 'Passing Score',
    'email' => 'Send Email Notification',
    'link' => 'Allow link to the coursware',
    'link_log' => 'The exam will not be able to do assignment if you select this option.',
    'assign' =>'Exam',
    'assigned' => 'Exam Assigned',
    'for_assign' => 'Assignable Exam',
    'assign_success' => 'Assigning Succeed',
    'deallocate_success' => 'Unassigning Succeed',
    'combinate' =>'Select Questions',
    'weight' => 'Weighting of Questions',
    'online' => 'Online Exam',
    'marked' => 'Graded Questions',
    'not_marked' => 'Questions Pending Grading',
    'status' => 'Exam Status',
    'preview' => 'Preview',
    'mark' => 'Grading',
    'distribute' => 'Assign Exam',
    'delete' => 'Delete',
    'under_way' => 'In Process',
    'done' => 'Passed',
    'all_done' => 'Reach maximum limit',
    'not_start' => 'Not Started',
    'overdue' => 'Expired',
    'limit_data' => 'Time Limit',
    'type' => 'Exam Type',

];
