<?php

return [

    /*
       |--------------------------------------------------------------------------
       | survey Language Lines
       |--------------------------------------------------------------------------
       |
       | The following language lines are used during survey for various
       | messages that we need to display to the user. You are free to modify
       | these language lines according to your application's requirements.
       |
     */

    'manage' => 'Evaluation Manager',
    'create' => 'Create a New Evaluation',
    'name' => 'Evaluation',
    'start' => 'Start Evaluation',
    'assign' => 'Assign Evaluation',
    'assigned'=>'Assigned Evaluation',
    'for_assign' => 'Assignable Evaluation',
    'assign_success' => 'Assigning Succeed',
    'deallocate_success' => 'Unassigning Succeed',
    'status' => 'Evaluation Status',
    '1'=>'Very Poor',
    '2'=>'Poor',
    '3'=>'Average',
    '4'=>'Good',
    '5'=>'Excellent',



];
