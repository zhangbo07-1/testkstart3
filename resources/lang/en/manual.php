<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'student_help' =>'User Manual',
    'admin_help' => 'Admin Manual',
    'pc' => 'PC',
    'wechat' => 'Wechat',
    'assign_success' => 'Assigning Succeed',
    'unassign_success' => 'Unassigning Succeed',

];
