<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => 'Organization Manager',
    'create' => 'Create a New Organization',
    'rename' => 'Rename',
    'delete' => 'Delete',
    'import' => 'Organization Data Uploader',
    'title' => 'Organization Name',
    'tag' => 'Organization ID',
    'if_all' => 'If Show All Numbers',


];
