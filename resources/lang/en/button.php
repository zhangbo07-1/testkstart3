<?php

return [

    /*
    |--------------------------------------------------------------------------
    | button Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during button for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'add' => 'Add',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'submit' => 'Submit',
    'cancel' => 'Cancel',
    'return' => 'Back',
    'search' => 'Search',
    'range' => 'Select Object',
    'preview'=> 'Preview',
    'push'   => 'Publish',
    'result' => 'Result',
    'add_select' => 'Add Option',
    'delete_select' => 'Delete Option',
    'upload' => 'Upload',
    'download' => 'Download',
    'department' => 'Select Organization',
    'view' => 'View',
    'file' => 'Select File',
    'catalog' => 'Select Catalog',
    'update' => 'Update',
    'example' => 'Export Templete',
    'redistribute'=>'Reassign',
    'mark' => 'Grading',
    'excel' => 'Export Excel',
    'start' => 'Start',
    'record' => 'Record',
    'exam' => 'Exam',
    'evaluation' => 'Eval',
    'relieve' => 'Release Binding',
    'post' => 'Forum',
    'resource' => 'Material',
    'open_all' => 'Expand All',
    'close_all' => 'Collapse All',
    'more' => 'More',
    'send_limit' => 'Select Criteria',
    'add_suffix' => 'Add Email Type',
    'rename' => 'Rename',
    'resend' => 'Resend Email',
    'bind' => 'Bind',
    'close' => 'Close',
    'forward_question' => "Forward Question",
    'next_question' => 'Next Question',
    'exam_QR' => 'Exam QR Code',
    'enroll' => 'Enroll',
    'sign' => 'Sign',
    'manage' => 'Manage',

];
