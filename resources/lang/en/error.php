<?php

return [

    /*
    |--------------------------------------------------------------------------
    | error Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during error for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    '00001' => 'The content cannot be empty.',
    '00002' => 'Please choose the correct answer.',
    '00003' => 'The user name already exist.',
    '00004' => 'You cannot delete the root catalog.',
    '00005' => 'The organization is still contain users, it can not be deleted.',
    '00006' => 'The function can only used for excel document.',
    '00007' => 'Please choose the excel file to upload.',
    '00008' => 'The user name can not be duplicated.',
    '00009' => 'The email address can not be duplicated.',
    '00010' => 'The organization does not exist.',
    '00011' => 'Wrong content format of the imported file.',
    '00012' => 'Fail to import the content, please check the log for details.',
    '00013' => 'The organization ID can not be duplicated.',
    '00014' => 'The uper level organization is not exist.',
    '00015' => 'Successfully created.',
    '00016' => 'Successfully deleted.',
    '00017' => 'Please choose catalog.',
    '00018' => 'The user role does not exist.',
    '00019' => 'Cannot connect the server.',
    '00020' => 'Please choose file to upload or enter the URL.',
    '00021' => 'Fail to save the file.',
    '00022' => 'Please choose catalog.',
    '00023' => 'This course can not be opened, please confirm the format.',
    '00024' => 'You have finished the evaluation.',
    '00025' => 'Please follow the order by course -> exam -> evaluation to finish the training.',
    '00026' => 'Insufficient questions in the exam pool.',
    '00027' => 'Total weight function can not be over 100%.',
    '00028' => 'If the total weight function is less then 100%, the final points will be less than 100.',
    '00029' => 'The email address does not exist',
    '00030' => 'Cannot find course',
    '00031' => 'The course is overdue. Please contact the administrator.',
    '00032' => 'No selected questions for the exam.',
    '00033' => 'You have reached the maximum times of the exam.',
    '00034' => 'There are still users in the position, you can not delete it.',
    '00035' => 'No exist position to delete.',
    '00036' => 'There are still users in the position level , you can not delete it.',
    '00037' => 'No exist position level to delete.',
    '00038' => 'There are still users in the attribute, you can not delete it.',
    '00039' => 'No exist attribute to delete.',
    '00040' => 'Please fill in the missing parts of the imported questions.',
    '00041' => 'Attribute 4 does not exist.',
    '00042' => 'Attribute 5 does not exist.',
    '00043' => 'Attribute 1 does not exist.',
    '00044' => 'Attribute 2 does not exist.',
    '00045' => 'Attribute 3 does not exist.',
    '00046' => 'Please choose the PNG file to upload.',
    '00047' => 'Please make sure your logo image is png file with 300×100px size.',
    '00048' => 'The user name is registered.',
    '00049' => 'The email address is registered.',
    '00050' => 'The user name is a mandatory option.',
    '00051' => 'The email address is a mandatory option.',
    '00052' => 'The registeration is not activated.',
    '00053' => 'Your account is pending for approval by administrator.',
    '00054' => 'Your account is closed.',
    '00055' => 'The approver does not exist.',
    '00056' => 'The attribute name must be unique.',
    '00057' => 'Your broswer can not match the frame!',
    '00058' => 'Please upload the completed form',
    '00059' => 'Do not input @',
    '00060' => 'Can not find this user',
    '00061' => 'Verification Successfully',
    '00062' => 'Registration successfully, please login.',
    '00063' => 'Re-send verification email successfully.',
    '00064' => 'Confirm to delete.',
    '00065' => 'Reach maximum limit.',
    '00066' => 'Please delete from last item.',
    '00067' => 'Wrong email address, please retry.',
    '00068' => 'Wrong password, please retry.',
    '00069' => 'This exam contains questions pending grading, do you want to re-start the exam?',
    '00070' => 'The root of organization must be unique.',
    '00071' => 'Update successfully.',

    '00072' => 'Only the exam of Maximum Number of Attempts set to be 1 can be reviewed correct answer.',
    '00073' => 'The catalog contains courses/exams/materials, cannot be deleted.',
    '00074' => 'This department is system default department ,cannot be deleted',
    '00080' => 'Can not choose this department',
    '00081' => 'The file id missing',
    '00082' => 'The password must include letters and numbers',
    '00083' => 'You have finished the survey.',
    '09998' => 'You have no right to do this',
    '09999' => 'Operation Failed',
    '10000' => 'Operation Succeed',
    '10001' => 'The new password has been sent to your mailbox, please check it.',
    '10002' => 'File uploading succeed.',
    '10003' => 'WeiCat bind success',




];
