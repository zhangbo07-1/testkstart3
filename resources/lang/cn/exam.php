<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '考试管理',
    'create' => '添加新考试',
    'edit' => '考试属性',
    'title' => '考试标题',
    'description' => '考试描述',
    'id' =>'考试编号',
    'pass_score' => '通过分数',
    'email' => '是否发送邮件提醒',
    'link' => '是否允许关联到课程',
    'link_log' => '如果勾选此选项，该考试不能被分配',
    'assign' =>'关联考试',
    'assigned' => '已关联考试',
    'for_assign' => '可选关联',
    'assign_success' => '关联考试成功',
    'deallocate_success' => '取消关联成功',
    'combinate' =>'组卷',
    'weight' => '权重',
    'online' => '在线考试',
    'marked' => '已批阅',
    'not_marked' => '未批阅',
    'status' => '考试状态',
    'limit_data' =>'限定时长',
    'preview' => '预览',
    'mark' => '阅卷',
    'distribute' => '考试分配',
    'delete' => '考试删除',
    'under_way' => '未通过',
    'done' => '已通过',
    'all_done' => '达到次数上限',
    'not_start' => '尚未开始',
    'overdue' => '已过期',
    'type' => '考试类型',

];
