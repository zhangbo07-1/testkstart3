<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '邮件管理',
    'type_1' =>'课程分配提醒',
    'type_2' => '课程催促提醒',
    'type_3' => '课程完成提醒',
    'type_4' => '课程到期提醒',
    'type_5' => '调研问卷邀请',
    'type_6' =>'注册认证邮件',
    'type_7' => '重新发送邮件',
    'type_8' => '审核申请',
    'type_9' => '审核完成通知',
    'type_10' => '密码重置',
    'type_11' => '批阅提醒',
    'type_12' => '批阅完成通知',


];
