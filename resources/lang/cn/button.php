<?php

return [

    /*
    |--------------------------------------------------------------------------
    | button Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during button for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'add' => '添加',
    'edit' => '编辑',
    'delete' => '删除',
    'submit' => '确定',
    'cancel' => '取消',
    'return' => '返回',
    'search' => '搜索',
    'range' => '选择对象',
    'preview'=> '预览',
    'push'   => '发布',
    'result' => '结果',
    'add_select' => '添加选项',
    'delete_select' => '删除选项',
    'upload' => '上传',
    'download' => '下载',
    'department' => '选择部门',
    'view' => '查看',
    'file' => '选择文件',
    'catalog' => '选择目录',
    'update' => '更新',
    'example' => '导出模板',
    'redistribute'=>'重新分配',
    'mark' => '批阅',
    'excel' => '导出Excel',
    'start' => '开始',
    'record' => '记录',
    'exam' => '考试',
    'evaluation' => '评估',
    'relieve' => '解除绑定',
    'post' => '讨论',
    'resource' => '资料',
    'open_all' => '全部展开',
    'close_all' => '全部缩进',
    'more' => '更多',
    'send_limit' => '设定条件',
    'add_suffix' => '添加后缀',
    'rename' => '重命名',
    'resend' => '重新发送验证邮件',
    'bind' => '绑定',
    'close' => '关闭',
    'forward_question' => "上一题",
    'next_question' => '下一题',
    'enroll' => '报名二维码',
    'sign' => '签到二维码',
    'manage' => '管理',
    'exam_QR' => '考试二维码',
    'survey_QR' => '调研二维码',
    'evaluation_QR' => '评估二维码',

];
