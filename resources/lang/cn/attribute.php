<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '用户属性管理',

    'tag_limit' => '仅限字母/数字/下划线/破折号',
    'now' => '现有',
    'tag' => '标记',
    'attribute_1' => '属性1',
    'attribute_2' => '属性2',
    'attribute_3' => '属性3',
    'attribute_4' => '属性4',
    'attribute_5' => '属性5',
    'title' => '标签',
    'import' => '批量导入',
    'limit_now' => '现有条件',
    'rename' => '重命名',
    'title_rename' => '标签重命名',
    'add' => '新增',
    'select' => '请选择',
];
