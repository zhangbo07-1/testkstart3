<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name' => '报表',
    'course' => '培训记录',
    'evaluation' => '评估报表',
    'user' => '用户信息',
    'question' => '题目分析',
    'exam' => '考试记录',
    'class' => '课堂记录',



];
