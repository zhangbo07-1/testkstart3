<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '调研管理',
    'create' => '添加新调研',
    'name' => '调研',
    'search' => '请输入要搜索的调研名',
    'start' => '开始调研',
    'assign' => '关联调研',
    'assigned'=>'已关联调研',
    'for_assign' => '可选调研',
    'assign_success' => '关联调研成功',
    'deallocate_success' => '解除关联成功',

    'result' => '调研结果',

];
