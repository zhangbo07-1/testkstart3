<?php

return [

    /*
    |--------------------------------------------------------------------------
    | question Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during question for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '题库管理',
    'create' => '添加新题库',
    'name' => '题库',
    'title' => '题库名',


];
