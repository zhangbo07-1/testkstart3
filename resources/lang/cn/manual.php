<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'student_help' =>'学员手册',
    'admin_help' => '管理员手册',
    'pc' => 'PC端学习',
    'wechat' => '微信端学习',
    'assign_success' => '关联教师成功',
    'unassign_success' => '取消关联成功',

];
