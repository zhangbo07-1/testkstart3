<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logo' => '图标管理',
    'info' => '系统信息',
    'logo_limit' => '请上传300×100像素的png文件作为系统LOGO，谢谢!',
    'logo_now' => '当前图标',
    'company' => '组织名称',
    'phone'  => '组织电话',
    'email' => '组织邮箱',
    'company_limit' =>'组织名只能由字母组成',
    'manage' => '配置管理',
    'censor_manage' => '审核管理',
    'if_censor' => '是否需要审核',
    'need_censor' => '需要审核',
    'not_censor' => '不需要审核',
    'if_validate' => '是否需要邮件验证',
    'need_validate' => '需要验证',
    'not_validate' => '不需要验证',

    'if_register' => '是否允许用户注册',
    'need_register' => '允许注册',
    'not_register' => '不允许注册',
    'add_email_suffix' => '请添加登录所需邮箱后缀，例如163.com ',
    'email_suffix_example' => '163.com',
    'email_manage' => '邮箱管理',
    'department' => '默认部门',
    'login_by_email' => '是否默认邮箱登录',



];
