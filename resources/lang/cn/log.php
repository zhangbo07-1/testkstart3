<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => '登录日志',
    'email' => '邮件日志',
    'create' => '添加新资料',
    'title' => '资料名',
    'file'=>'文件名',
    'size' => '文件大小',
    'login' => '登入',
    'logout' => '登出',
    'manage' => '日志管理',


];
