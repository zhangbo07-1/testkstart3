<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '用户管理',
    'create' =>'新建用户',
    'title' => '用户名',
    'email'=>'邮箱地址',
    'pwd' => '密码',
    'real_name' => '姓名',
    'role' =>'角色',
    'ranger' => '管理对象',
    'department' => '部门',
    'position' =>'职位',
    'rank' => '职级',
    'import' =>'用户导入',
    'report' => '用户信息',
    'login_last' => '最后访问',
    'login_times' => '登入次数',
    'self_info' => '个人信息',
    'attribute_manage' => '用户属性管理',
    'new_password' => '新密码',
    'confirmation_password' => '确认密码',
    'approve' => '审批人',
    'approve_name' => '审批人姓名',
    'sex'=>'性别',
    'number' => '人数',
    'tel' => '联系电话',
    'address' => '联系地址',
    'created_time' => '注册时间',
    'weixin_bind' => '微信绑定',
    'company' => '公司',
    'company_tag' => '请填写您的工作单位',
    'favorite' => '感兴趣功能',


];
