<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'user' => '创建者',
    'report' => '课程报表',
    'study_manage' => '学习管理',
    'manage' => '课程管理',
    'create' => '添加新课程',
    'title' => '课程标题',
    'name' => '课程',
    'description' => '课程描述',
    'id' =>'课程编号',
    'hours' => '课程时长',
    'email' => '是否发送邮件提醒',
    'link' => '是否分配给新用户',
    'link_log' => '如果勾选此选项，该考试不能被分配',
    'target' => '目标用户',
    'order' => '是否按照以下顺序',
    'order_log' => '（课程->考试->评估）',
    'if_open_overdue' => '是否允许打开过期课程',
    'upload' => '课件上传',
    'upload_log' => '仅支持scorm2004课件包,格式为.zip',
    'ware_name' => '课件名',
    'study_times' =>'学习次数',
    'online' => '在线课程',
    'type' => '课程类型',
    'my_lesson' => '我的培训',
    'lesson' => '进行中课程',
    'lesson_done' => '已完成课程',
    'lesson_overdue' => '已过期课程',
    'edit' => '课程属性',
    'distribute' => '课程分配',
    'update' => '课程更新',
    'push' => '课程发布',
    'preview' => '课程预览',
    'exam' => '关联考试',
    'evaluation' => '关联评估',
    'delete' => '课程删除',
    'upload_success' => '课件上传成功',
    'change'=> '更新该课程',
    'push_success' => '课件发布成功',
    'no_ware' => '尚无课件上传',
    'send_limit' => '分配条件',
    'status' => '课程状态',
    'create_time'=> '注册时间',
    'not_set' => '未设置',
    'is_set' => '已设置',
    'under_way' => '进行中',
    'done' => '已完成',
    'all_done' => '已完成',
    'not_start' => '尚未开始',
    'overdue' => '已过期',
    'resource' => '资料库管理',
    'resource_add' => '关联资料库',
    'class' => '班级管理',
    'class_title' => '班级名',
    'class_address' => '上课地点',
    'class_create' => '新建班级',
    'class_lesson' => '时段',
    'add_lesson' => '增加时段',
    'delete_lesson' => '减少时段',
    'class_teacher' => '授课老师',
    'course_survey' => '课前调研',
    'class_distribute' => '班级分配',
    'class_late' => '允许迟到时间',
    'enroll_success' => '报名成功',
    'sign_success' => '签到成功',
    'lesson_status1' => '缺勤',
    'lesson_status2' => '迟到',
    'lesson_status3' => '已出席',
    'class_select' => '选择班级',

];
