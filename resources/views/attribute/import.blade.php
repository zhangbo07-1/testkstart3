@extends('attribute.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="container">
            <br>
            <br>
            <h3>{{ trans('user.import') }}</h3>
                <div class="row">
                    <div class="col-sm-12 main">
                        <div style="float: right;">
                            {!! Form::open(array('url'=>'users/import','method'=>'POST', 'files'=>true ,'class'=>"form-inline")) !!}
                            <a onclick="document.getElementById('upfile').click();" class="dm3-btn dm3-btn-medium button-large">
                                {{ trans('button.file') }}
                            </a>
                            <div style="display:inline-block;">                                <input type="text" class="form-control"] name="url" id="fileURL">
                            </div>
                            <input type="file" name="file" id="upfile" onchange="document.getElementById('fileURL').value=this.value;" style="display:none">
                            <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                            <a href="{{ URL('users/example') }}" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.example') }}</a>
                            {!! Form::close()!!}
                            {!! errors_for('file', $errors) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 main">
                        <div class="table-responsive">
                            <div class="courseTable text-center">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('import.title') }}</th>
                                            <th>{{ trans('table.create_time') }}</th>
                                            <th>{{ trans('table.user') }}</th>
                                            <th>{{ trans('import.log') }}</th>
                                            <th>{{ trans('table.operation') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($imports as $import)
                                            <tr>
                                                <td class="vertical-align-middle">{{$import->filename}}</td>
                                                <td class="vertical-align-middle">{{$import->created_at}}</td>
                                                <td class="vertical-align-middle">{{$import->user->name}}</td>
                                                <td class="vertical-align-middle">
                                                    <a  onclick="showLog({{$import->id}})" >{{ trans('button.view') }}</a>

                                                </td>
                                                <td class="vertical-align-middle">
                                                    <a href="{{ url('import-log/'.$import->id.'/download') }}" class="dm3-btn dm3-btn-medium button-large">
                                                        <i class="fa fa-download"></i>&nbsp;{{ trans('button.download') }}
                                                    </a>

                                                    <form action="{{ url('import-log/'.$import->id) }}" method="POST" style="display: inline;">
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" onClick="return ifDelete()" class="dm3-btn dm3-btn-medium dm3-btn-red button-large">
                                                            <i class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                             {!! $imports->render() !!}
                        </div>
                    </div>
                </div>
        </div>

    @endsection
