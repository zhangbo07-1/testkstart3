@extends('attribute.partials.template')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-body">
    <br>

                    <div class="row">
                        <div class="col-md-5 col-md-offset-3">
                            @include('layout.message')
                            <form action="{{ URL('attribute/delete-position') }}" method="POST">
                                <input name="_method" type="hidden" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <a>{{ trans('attribute.position_now') }}</a>
                                {!! Form::select('delete_position', $positionSelects,null,['class'=>"form-control"]) !!}
                                <button class="dm3-btn dm3-btn-red button-large">{{ trans('button.delete') }}</button>

                                {!! errors_for('delete_position', $errors) !!}
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-3">
                            <form action="{{ URL('attribute/add-position') }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <br>
                                <a>{{ trans('attribute.position_title') }}</a>
                                <input type="text" name="position" class="form-control" required="required">
                                <a>{{ trans('attribute.position_tag').'('.trans('attribute.tag_limit').')' }}</a>
                                <input type="text" name="tag" class="form-control" style="width:111px;" required="required">
                                {!! errors_for('tag', $errors) !!}
                                <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endsection
