@extends('attribute.partials.template')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    {!! Html::script('assets/js/attribute.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-body">
                    <br>
                    @include('layout.message')


                     <div class="row">
                         <div class="col-md-5 col-md-offset-3">
                              <a>{{ trans('attribute.import')}}
                                  <a data-toggle="tooltip" title="{{ trans('tips.00015') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
                              </a>
                             {!! Form::open(array('url'=>'attribute/fifth-attribute/import','method'=>'POST', 'files'=>true ,'class'=>"form-inline")) !!}
                            <a onclick="document.getElementById('upfile').click();" class="dm3-btn dm3-btn-medium button-large">
                                {{ trans('button.file') }}
                            </a>
                            <div style="display:inline-block;">                                <input type="text" class="form-control"] name="url" id="fileURL">
                            </div>
                            <input type="file" name="file" id="upfile" onchange="document.getElementById('fileURL').value=this.value;" style="display:none">
                            <br>
                            <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                            <a href="{{ URL('attribute/fifth/example') }}" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.example') }}</a>
                            {!! Form::close()!!}
                            {!! errors_for('file', $errors) !!}
                        </div>
                    </div>
                    <br>
                     <div class="row">
                        <div class="col-md-5 col-md-offset-3">
                            <form action="{{ URL('attribute/rename-fifth-attribute') }}" method="POST">
                               
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <a>{{ trans('attribute.title_rename')}}
                                <a data-toggle="tooltip" title="{{ trans('tips.00016') }}"><span class="glyphicon glyphicon-question-sign"></span></a></a>
                                 <input type="text" name="attribute_title" class="form-control" required="required" value="{{$attribute->fifth_title}}">
                                <button class="dm3-btn  button-large">{{ trans('button.submit') }}</button>

                                {!! errors_for('attribute_title', $errors) !!}
                            </form>
                        </div>
                     </div>
                     <br>
    
                    <div class="row">
                        <div class="col-md-5 col-md-offset-3">
                            <form action="{{ URL('attribute/delete-fifth-attribute') }}" method="POST">
                                <input name="_method" type="hidden" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <a>{{ trans('attribute.now').$attribute->fifth_title }}</a>
                                {!! Form::select('delete_attribute', $fifthAttributeSelects,null,['class'=>"form-control",'id'=>"delete_attribute",'onClick'=>'changeRename()']) !!}
                                <button class="dm3-btn dm3-btn-red button-large">{{ trans('button.delete') }}</button>

                                {!! errors_for('delete_attribute', $errors) !!}
                            </form>
                        </div>
                    </div>


                     <div class="row">
                        <div class="col-md-5 col-md-offset-3">
                            <form action="{{ URL('attribute/update-fifth-attribute') }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" id="attribute_id" name="attribute_id" value="">
                                <br>
                                <a>{{ trans('attribute.rename').$attribute->fifth_title }}
                                <a data-toggle="tooltip" title="{{ trans('tips.00017') }}"><span class="glyphicon glyphicon-question-sign"></span></a></a>
                                <input type="text" id="rename_attribute" name="rename_attribute" class="form-control" required="required">
                               
                                <button id="rename_BTN" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.rename') }}</button>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-3">
                            <form action="{{ URL('attribute/add-fifth-attribute') }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <br>
                                <a>{{ trans('attribute.add').$attribute->fifth_title }}
                                <a data-toggle="tooltip" title="{{ trans('tips.00018') }}"><span class="glyphicon glyphicon-question-sign"></span></a></a>
                                <input type="text" name="add_attribute" class="form-control" required="required">
                                
                                <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.add') }}</button>
                            </form>
                        </div>
                    </div>

                   
                </div>
            </div>
        </div>


    @endsection
