@extends('layout.master')
@section('js')
{!! Html::script('assets/js/attribute.min.js') !!}
    @append
@section('content')
    @include('attribute.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection