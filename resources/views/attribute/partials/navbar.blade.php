<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    <li id="first-attributeNav">
                        <a data-toggle="tooltip" title="{{ trans('tips.00014') }}" href="/attribute/first-attribute" data-placement="bottom">
                            <i class="fa fa-pencil"></i><span>{{ trans('attribute.attribute_1') }}
                            </span>
                        </a>
                    </li>

                    <li id="second-attributeNav">
                        <a href="/attribute/second-attribute"  data-toggle="tooltip" data-placement="bottom" title="{{ trans('tips.00014') }}">
                            <i class="fa fa-pencil"></i><span>{{ trans('attribute.attribute_2') }}
                           </span>
                        </a>
                    </li>
                    <li id="third-attributeNav">
                        <a href="/attribute/third-attribute"  data-toggle="tooltip" data-placement="bottom" title="{{ trans('tips.00014') }}">
                            <i class="fa fa-pencil"></i><span>{{ trans('attribute.attribute_3') }}
                           </span>
                        </a>
                    </li>
                     <li id="fourth-attributeNav">
                        <a href="/attribute/fourth-attribute"  data-toggle="tooltip" data-placement="bottom" title="{{ trans('tips.00014') }}">
                            <i class="fa fa-pencil"></i><span>{{ trans('attribute.attribute_4') }}
                           </span>
                        </a>
                     </li>
                      <li id="fifth-attributeNav">
                        <a href="/attribute/fifth-attribute"  data-toggle="tooltip" title="{{ trans('tips.00014') }}">
                            <i class="fa fa-pencil"></i><span>{{ trans('attribute.attribute_5') }}
                           </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
