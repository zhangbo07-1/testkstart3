@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>
            <h3>{{ trans('survey.manage') }}</h3>
            <div class="row">
                <div class="col-sm-12 main">
                    <div class="clearfix">
                        <form class="form-inline" action="/survey">
                            <div style="float: left;">
                                <div class="form-group">
                                    <input type="text" name="findBySurveyname" class="form-control" placeholder="{{ trans('table.input_select_survey') }}">
                                </div>
                                <button type="submit" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.search') }}</button>
                            </div>

                            <div style="float: right;">
                                <a href="{{URL('survey/create')}}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-plus"></i>&nbsp;{{ trans('survey.create') }}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 main">
                    <div class="table-responsive">
                        <div class="courseTable text-center">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{ trans('table.title') }}</th>
                                        <th>{{ trans('table.Qnum') }}</th>
                                        <th>{{ trans('table.pushedOrNot') }}</th>
                                        <th>{{ trans('table.pushed_time') }}</th>
                                        <th>{{ trans('table.operation') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($surveys as $survey)


                                        <tr>
                                            <td class="vertical-align-middle">{{ $survey->title }}</td>
                                            <td class="vertical-align-middle">{{count($survey->items)}}</td>
                                            
                                            <td class="vertical-align-middle">{{$survey->pushed > 0 ? trans('table.yes'): trans('table.no')}}</td>

                                            <td class="vertical-align-middle">{{$survey->pushed > 0 ? $survey->updated_at:"" }}</td>
                                            <td class="vertical-align-middle">

                                                <a href="{{ url('/survey/'.$survey->id.'/edit') }}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-pencil"></i>&nbsp;{{ trans('button.edit') }}</a>
                                                <form action="{{ URL('survey/'.$survey->id) }}" method="POST" style="display: inline;">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button type="submit" onClick="return ifDelete()" class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}</button>

                                                </form>

                                            </td>
                                        </tr><!-- end ngRepeat: trainee in trainees -->
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            {!! $surveys->render() !!}
        </div>
    @endsection


