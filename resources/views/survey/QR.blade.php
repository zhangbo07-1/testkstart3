@extends('layout.base')

@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
@endsection
@section('content')
    <div class="container">
        <br>
        <br>

        <div class="row">
            <div class="col-sm-12 main">
                <div class="text-center">

                    @if(isset($surveyRecord))
                        {!! QrCode::size(500)->color(20,157,210)->generate('http://'.$_SERVER['HTTP_HOST'].'/lessons/'.$course->course_id.'/course-survey/'.$surveyRecord->id.'/answer'); !!}

    <p>{{ trans('button.survey_QR')}}
                    </p>
                        
                    @else
                       {!! QrCode::size(500)->color(20,157,210)->generate('http://'.$_SERVER['HTTP_HOST'].'/lessons/'.$course->course_id.'/evaluation/'.$evaluationRecord->id.'/answer'); !!}

    <p>{{ trans('button.evaluation_QR')}}
                    </p>
                       
                    @endif
                    
             
                </div>

            </div>
            
        </div>
    </div>
@endsection
