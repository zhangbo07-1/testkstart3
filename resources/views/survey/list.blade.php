@extends('layout.master')
@section('content')
    <div class="container">
        <br>
        <br>
        <div class="container">
            <aside class="lessons-container">
                <h3 class="lessons-title">{{ trans('survey.name') }}</h3>
                <ul class="lessons-ul">
                    @include('layout.message')
                    @foreach($surveys as $survey)
                        @if($survey->pushed>0)
                            <li class="row">
                                <div class="col-md-6">
                                    <div class="col-fixed"><span class="icon icon-18"></span></div>
                                    <div class="row row-margin-5px">
                                        <div class="lessons-padding ">
                                            <div><a class="lessons-course-name">{{$survey->title}}</a></div>


                                            <span class="post-date">
                                                {{ trans('table.Qnum') }}:&nbsp;{{count($survey->items)}}&nbsp;&nbsp;
                                                <i class="fa fa-clock-o">&nbsp;{{ trans('table.create_time') }}:&nbsp;</i>{{ date('Y-m-d H:i', strtotime($survey->updated_at)) }}
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="lessons-padding-button">
                                        
                                        <a href="{{ url('survey/'.$survey->id.'/answer/') }}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-edit"></i>&nbsp;{{ trans('table.answer') }}</a>
                                        
                                        
                                    </div>
                                </div>
                            </li>
                        @endif
                    @endforeach
                    
                </ul>
                {!! $surveys->render() !!}
            </aside>
        </div>
@endsection
