@extends('layout.master')
@section('js')
{!! Html::script('assets/js/logs.min.js') !!}
    @append

@section('content')
    @include('logs.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection