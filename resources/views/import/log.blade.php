@extends('layout.base')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
        @section('content')

           <div class="row" style="margin-right:0">
                <div class="col-md-8 col-md-offset-2">
        {!!$import->error!!}

                </div>
           </div>
           <br>
           <br>
            <div align="center">
                <a class="dm3-btn dm3-btn-medium dm3-btn button-large"  onclick="closeWindow()">{{ trans('button.submit') }}</a>
            </div>

        @endsection
