@extends('courses.partials.template')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')

        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('course.upload') }} {{$course->course_id}} {{$course->name}}</h2>
                </div>
            </div>

            <div class="panel panel-announcement">
                {!! Form::open(array('url'=>'/courses/'.$course->id.'/upload','method'=>'POST', 'files'=>true,'class'=>"form-inline")) !!}
                <div class="panel-body">
                    <input type="button" value="{{ trans('button.file') }}" onclick="document.getElementById('upfile').click();" class="dm3-btn dm3-btn-medium button-large"/>
                    <div style="display:inline-block;">
                        <input type="text" name="url" id="fileURL" class="form-control">
                    </div>
                    <input type="file" name="file" id="upfile" onchange="document.getElementById('fileURL').value=this.value;" style="display:none">
                    <br>
                    <button class="dm3-btn dm3-btn-medium button-large" type="submit" onClick="return checkType()">
                        <i class="fa fa-upload"></i>&nbsp;{{ trans('button.upload') }}
                    </button>
                    @include('layout.message')
                    <h4 class="red">{{ trans('course.upload_log') }}</h4>

                    {!! Form::close() !!}
                </div>
            </div>
            <div class="panel">
                <div class="panel-body text-center courseTable">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>{{ trans('course.ware_name') }}</th>
                                <th>{{ trans('table.user') }}</th>
                                <th>{{ trans('table.create_time') }}</th>
                                <th>{{ trans('button.download') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($coursewares as $courseware)
                                <tr>
                                    <td>{{ $courseware->filename }}</td>
                                    <td>{{ $courseware->user->name }}</td>
                                    <td>{{ $courseware->created_at }}</td>
                                    <td><a href="/course-ware/{{$courseware->id }}/download">{{ trans('button.download') }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
<script type="text/javascript">
function checkType()
{
  var file =  document.getElementById("upfile").value;
  if(file.indexOf('.zip') < 0)
  {
      var strHtml = '<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{ trans('error.00023') }}</div>';
document.getElementById("loading").innerHTML = strHtml;
      return false;
  }
    var strHtml = '<p><span style="color:#00b0f0">Loading... ...</span></p>';
  document.getElementById("loading").innerHTML = strHtml;

  $("#need_disable").attr("disabled", "true");
  $('#need_submit').submit();
  return true;



}
 </script>
    @endsection
