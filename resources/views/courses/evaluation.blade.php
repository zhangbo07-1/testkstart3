@extends('courses.partials.template')
@section('js')
    {!! Html::script('assets/bower_components/handlebars/handlebars.min.js') !!}
    {!! Html::script('assets/bower_components/jquery-bootpag/lib/jquery.bootpag.min.js') !!}
    {!! Html::script('assets/bower_components/spin.js/spin.js') !!}
    {!! Html::script('assets/js/course_evaluation.js') !!}
@endsection
@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{ trans('evaluation.assign') }}</h2>
                <div>
                {{ trans('evaluation.assigned') }}:<span id="selectedEvaluationTitle"> {{ isset($evaluationRecord) ? $evaluationRecord->title : trans('table.none') }}</span>
                </div>
                <div class="form-inline">
                    <div class="form-group">
                        <input id="searchWord" class="form-control" type="text" name="findBySurveyName" placeholder="{{ trans('table.input_select_evaluation') }}">
                    </div>
                    <button  id="courseSearchButton" type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}</button>
                </div>
            </div>
        </div>

        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="col-md-12">

                    <h4 class="panel-title">{{ trans('evaluation.for_assign') }}
                        <a  data-toggle="tooltip" title="{{ trans('tips.00032') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
                    </h4>

                    <div class="table-responsive">
                        <div class="courseTable text-center">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{ trans('table.choose') }}</th>
                                        <th>{{ trans('evaluation.name') }}</th>
                                        <th>{{ trans('table.Qnum') }}</th>
                                    </tr>
                                </thead>
                                <tbody id="EvaluationsForm">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="EvaluationsForm-Pagination"></div>
                    <button id="chooseEvaluationButton" class="dm3-btn dm3-btn-medium button-large">
                        <i class="fa fa-thumbs-o-up"></i>&nbsp;{{ trans('button.submit') }}
                    </button>
                    <button id="relieveEvaluationButton" class="dm3-btn dm3-btn-medium dm3-btn-red button-large">
                        <i class="fa fa-trash"></i>&nbsp;{{ trans('button.relieve') }}
                    </button>



                     @if(isset($evaluationRecord))

                        <a class="dm3-btn dm3-btn-medium button-large" href="/course/{{$course->course_id}}/evaluation/{{$evaluationRecord->id}}/result">
                            <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.result') }}</a>
                    @else
                            <a class="dm3-btn dm3-btn-gray button-large" disabled >
                                <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.result') }}</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script id="EvaluationTemplate" type="text/x-handlebars-template">
        @{{#each data}}
        <tr>
            <td><input type="radio" name="evaluationSelect"
                       id="@{{id}}" value="@{{id}}"></td>
            <td>@{{title}}</td>
            <td>@{{items.length}}</td>
        </tr>
        @{{/each}}
    </script>


    <script>
     var course_id = {{$course->id}};
    </script>

@endsection
