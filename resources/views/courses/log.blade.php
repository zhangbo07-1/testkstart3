@extends('layout.master')

@section('content')
    <br>
    <br>

    <div class="container">

        <h1 class="text-center">{{ trans('report.course') }}</h1>
        <div class="row">
            <div class="col-sm-12 main">
                <div align="right">
                    <a class="dm3-btn dm3-btn-medium button-large" href="/lessons/{{$record->course->course_id}}/info" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
                </div>
            </div>
        </div>
        <div class="bar"></div>
        <h4>{{ trans('table.info') }}</h4>
        <div class="row">
            <div class="col-sm-12 main">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td>{{ trans('course.create_time') }}:</td>
                            <td>{{$record->created_at}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('table.start_time') }}:</td>
                            <td>{{$record->started_at}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('table.end_time') }}:</td>
                            @if($record->process >=2)
                                <td>{{$record->end_at}}</td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                        <tr>
                            <td>{{ trans('table.study_span') }}:</td>
                            <td>
                                @if($record->study_span)
                                    {{$record->study_span}}
                                @else
                                    0
                                @endif
                                {{trans('table.minute')}}
                            </td>
                        </tr>
                        <tr>
                            <td>{{ trans('table.study_times') }}:</td>
                            <td>{{$record->study_times}}</td>
                        </tr>
                        @if($record->course->type == 2)
                            <tr>
                                <td>{{ trans('course.course_survey') }}:</td>

                            @if(!$surveyRecord)
                                <td>{{ trans('table.none') }}</td>
                            @elseif(count($surveyResults)<1)
                                <td>{{ trans('table.not_start') }}</td>
                            @else
                                <td>{{ trans('table.done') }}</td>
                            @endif
                            </tr>
                        @endif

                        <tr>
                            <td>{{ trans('course.status') }}:</td>
                            <td>

                                    {{  trans('course.'.$record->status->title)}}
                                {{$record->overdue?'('.trans('table.overdue').')':''}}</td>
                        </tr>
                        <tr>
                            <td>{{ trans('exam.status') }}:</td>
                            @if(!$record->course->examDistribution)
                                <td>{{ trans('table.none') }}</td>
                            @elseif(!$examRecord)
                                <td>{{ trans('exam.not_start') }}</td>
                            @else
                                <td>{{ trans("exam.".$examRecord->status->title) }}</td>
                            @endif
                        </tr>
                        <tr>
                            <td>{{ trans('evaluation.status') }}:</td>
                            @if(!$evaluationRecord)
                                <td>{{ trans('table.none') }}</td>
                            @elseif(count($evaResults)<1)
                                <td>{{ trans('table.not_start') }}</td>
                            @else
                                <td>{{ trans('table.done') }}</td>
                            @endif
                        </tr>
                        <tr>
                            <td>{{ trans('table.score') }}:</td>

                            <td>{{$record->max_score}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        @if($record->course->type == 2)
            @include('class.log')
        @endif


        @include('courses.log_partial')
    </div>



@endsection
