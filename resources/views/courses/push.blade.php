@extends('courses.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{$course->title }}</h2>
                </div>
            </div>
            <div class="panel panel-announcement">
                <div class="panel-body">

                    

                    <form action="{{ URL('courses/'.$course->course_id.'/pushed') }}" method="POST" >
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                @if(isset($ware))
                                {{$ware->user->name}}{{trans('table.in')}}{{$ware->created_at}}{{trans('course.change')}},{{trans('table.wait_push')}}
@elseif($course->type == 2)
    {{trans('record.type3')}}
    @else
        
{{trans('course.no_ware')}}
                                    @endif
                                


                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                @if(isset($ware) || count($course->classes))
                                <button  class="dm3-btn  button-large" type="submit">
                                    <i class="fa fa-save"></i>&nbsp;{{trans('button.push')}}
                                </button>
                                @else

                                <a disable  class="dm3-btn dm3-btn-gray button-large" >
                                    <i class="fa fa-save"></i>&nbsp;{{trans('button.push')}}
                                </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endsection
