@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
{!! Html::script('assets/js/course.min.js') !!}
    @append
@section('content')
    @include('courses.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection