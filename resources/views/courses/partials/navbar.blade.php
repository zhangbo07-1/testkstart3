
<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    <li id="editNav">
                        <a href="/courses/{{$course->course_id}}/edit"><i class="fa fa-home"></i><span>{{ trans('course.edit') }}</span></a>
                    </li>

                    <li id="reportNav">
                        <a href="/courses/{{$course->course_id}}/report"><i class="fa fa-home"></i><span>{{ trans('course.report') }}</span></a>
                    </li>
                    @can('modify',$course)


                    <li id="teachersNav">
                        <a href="/courses/{{$course->course_id}}/teachers">
                            <i class="fa fa-user"></i><span>{{ trans('teacher.assign') }}</span>
                        </a>
                    </li>
                    @if($course->pushed == 1)
                        <li id="pushNav">
                            <a disabled>
                                <i class="fa fa-user gray"></i><span class="gray">{{ trans('course.push') }}</span>
                            </a>
                        </li>

                        @if($course->type == 1)
                        <li id="distributeNav">
                            <a href="/courses/{{$course->course_id}}/distribute">
                                <i class="fa fa-user"></i><span>{{ trans('course.distribute') }}</span>
                            </a>
                        </li>
                        @endif
                    @else
                        <li id="pushNav">
                            <a href="/courses/{{$course->course_id}}/push">
                                <i class="fa fa-refresh"></i><span>{{ trans('course.push') }}</span>
                            </a>
                        </li>
                        @if($course->type == 1)
                        <li id="distributeNav">
                            <a disabled>
                                <i class="fa fa-user gray"></i><span class="gray">{{ trans('course.distribute') }}</span>
                            </a>
                        </li>
                        @endif
                    @endif
                    @endcan
                    @if($course->type == 1)
                    <li id="uploadNav">
                        <a href="/courses/{{$course->course_id}}/upload">
                            <i class="fa fa-refresh"></i><span>{{ trans('course.update') }}</span>
                        </a>
                    </li>
                    <li id="previewNav">
                        <a href="/courses/{{$course->course_id}}/preview" target="_blank">
                            <i class="fa fa-eye"></i><span>{{ trans('course.preview') }}</span>
                        </a>
                    </li>
                    @elseif($course->type == 2)
                    <li id="classNav">
                        <a href="/courses/{{$course->course_id}}/class">
                            <i class="fa fa-group"></i><span>{{ trans('course.class') }}</span>
                        </a>
                    </li>
                    @endif


                    <li id="surveyNav">
                        <a href="/courses/{{$course->course_id}}/survey">
                            <i class="fa fa-check"></i><span>{{ trans('course.course_survey') }}</span>
                        </a>
                    </li>
                    <li id="examNav">
                        <a href="/courses/{{$course->course_id}}/exam">
                            <i class="fa fa-link"></i><span>{{ trans('course.exam') }}</span>
                        </a>
                    </li>
                    <li id="evaluationNav">
                        <a href="/courses/{{$course->course_id}}/evaluation">
                            <i class="fa fa-check"></i><span>{{ trans('course.evaluation') }}</span>
                        </a>
                    </li>

                    
                    @if($course->resource_id)
                    <li id="resourceNav">
                        <a href="/courses/{{$course->course_id}}/resources">
                            <i class="fa fa-file-archive-o"></i><span>{{ trans('course.resource') }}</span>
                        </a>
                    </li>
                    @else
                    <li id="resourceNav">
                        <a href="/courses/{{$course->course_id}}/add_resources">
                            <i class="fa fa-file-archive-o"></i><span>{{ trans('course.resource_add') }}</span>
                        </a>
                    </li>
                    @endif
                    @can('delete',$course)
                    <li id="deleteNav">
                        <a href="/courses/{{$course->course_id}}/delete">
                            <i class="fa fa-trash"></i><span>{{ trans('course.delete') }}</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>
        </div>
    </div>
</div>
