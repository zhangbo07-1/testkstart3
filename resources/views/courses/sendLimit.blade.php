@extends('layout.base')

@section('js')

    {!! Html::script('assets/js/send_limit.js') !!}
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="row">

            <div class="col-sm-6 col-sm-offset-4">


                <a>{{ $attribute->first_title }}</a>
                <div class="form-inline">
                    {!! Form::select('firstAttribute', $firstAttributeSelects,null,['id'=>'attribute1','class'=>"form-control"]) !!}
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="add_attribute1();">{{ trans('button.add_select') }}</button>
                </div>

                <a>{{ trans('attribute.limit_now') }}</a>
                <div class="form-inline">
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="delete_attribute1();">{{ trans('button.delete_select') }}</button>
                    <div id="attribute1_list" style="display:inline-block" >
                    </div>
                </div>
                <br>

                <a>{{ $attribute->second_title }}</a>
                <div class="form-inline">
                    {!! Form::select('secondAttribute', $secondAttributeSelects,null,['id'=>'attribute2','class'=>"form-control"]) !!}
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="add_attribute2();">{{ trans('button.add_select') }}</button>
                </div>

                <a>{{ trans('attribute.limit_now') }}</a>
                <div class="form-inline">
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="delete_attribute2();">{{ trans('button.delete_select') }}</button>
                    <div id="attribute2_list" style="display:inline-block" >
                    </div>
                </div>
<br>
                <a>{{ $attribute->third_title }}</a>
                <div class="form-inline">
                    {!! Form::select('thirdAttribute', $thirdAttributeSelects,null,['id'=>'attribute3','class'=>"form-control"]) !!}
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="add_attribute3();">{{ trans('button.add_select') }}</button>
                </div>


                <a>{{ trans('attribute.limit_now') }}</a>
                <div class="form-inline">
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="delete_attribute3();">{{ trans('button.delete_select') }}</button>
                    <div id="attribute3_list" style="display:inline-block" >
                    </div>
                </div>

<br>
                <a>{{ $attribute->fourth_title }}</a>
                <div class="form-inline">
                    {!! Form::select('fourthAttribute', $fourthAttributeSelects,null,['id'=>'attribute4','class'=>"form-control"]) !!}
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="add_attribute4();">{{ trans('button.add_select') }}</button>
                </div>
                <a>{{ trans('attribute.limit_now') }}</a>
                <div class="form-inline">
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="delete_attribute4();">{{ trans('button.delete_select') }}</button>
                    <div id="attribute4_list" style="display:inline-block" >
                    </div>
                </div>
<br>
                <a>{{ $attribute->fifth_title }}</a>
                <div class="form-inline">
                    {!! Form::select('fifthAttribute', $fifthAttributeSelects,null,['id'=>'attribute5','class'=>"form-control"]) !!}
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="add_attribute5();">{{ trans('button.add_select') }}</button>
                </div>
                <a>{{ trans('attribute.limit_now') }}</a>
                <div class="form-inline">
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="delete_attribute5();">{{ trans('button.delete_select') }}</button>
                    <div id="attribute5_list" style="display:inline-block" >
                    </div>
                </div>


            </div>
        </div>


            <br>
            <br>
            <div class="row">
            <div align="center">
                <a class="dm3-btn dm3-btn-medium dm3-btn button-large" onClick="getSelect()">{{ trans('button.submit') }}</a>
                <a class="dm3-btn dm3-btn-medium dm3-btn-red button-large" onclick="closeWindow()">{{ trans('button.close') }}</a>
<br>
<br>
            </div>
            </div>



<script>
     var is_set = "{{trans('course.is_set')}}";
     var not_set = "{{trans('course.not_set')}}";
</script>
    @endsection
