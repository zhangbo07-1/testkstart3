@extends('courses.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('report.course') }}</h2>
                </div>
            </div>
            <div class="panel panel-announcement">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 main">
                            <form class="form-inline" action="/courses/{{$course->course_id}}/report">
                                <div style="float: left;">
                                     {!! Form::select('class_id', $classes, isset($a['class_id'])?$a['class_id']:null,['class'=>"form-control"]) !!}
                                    <div class="form-group">
                                        <input type="text"  class="form-control" name="findByUserName" placeholder="{{ trans('table.input_select_user') }}" value="{{isset($a['findByUserName'])?$a['findByUserName']:""}}">
                                    </div>
                                    <button type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ trans('button.search') }}</button>
                                </div>
                            </form>
                            <div style="float: right;">
                                <form  class="form-inline" action="/courses/{{$course->course_id}}/report/excel">
                                    <input type="hidden"  name="select" value="{{isset($a)?json_encode($a):""}}"/>
                                    <button type="submit" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.excel') }}</button>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12 main">
                            <div class="table-responsive">
                                <div class="courseTable text-center">
                                    <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:300%;">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>{{ trans('user.real_name') }}</th>
                                                    <th>{{ trans('table.user_name') }}</th>
                                                    <th>{{ trans('table.email') }}</th>
                                                    <th>{{ trans('user.department') }}</th>
                                                    <th>{{ trans('record.name') }}</th>
                                                    <th>{{ trans('record.type') }}</th>
                                                    <th>{{ trans('table.catalog') }}</th>
                                                    <th>{{ trans('table.status') }}</th>
                                                    <th>{{ trans('course.course_survey') }}</th>
                                                    <th>{{ trans('course.status') }}</th>
                                                    <th>{{ trans('exam.status') }}</th>
                                                    <th>{{ trans('evaluation.status') }}</th>
                                                    <th>{{ trans('table.create_time') }}</th>
                                                    <th>{{ trans('table.start_time') }}</th>
                                                    <th>{{ trans('table.end_time') }}</th>
                                                    <th>{{ trans('table.study_times') }}</th>
                                                    <th>{{ trans('table.study_span') }}</th>
                                                    <th>{{ trans('table.course_score') }}</th>
                                                    <th>{{ trans('table.exam_score') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($records as $record)
                                                    @if($record->type == 1 || $record->type == 3)
                                                        <tr>
                                                            <td class="vertical-align-middle">{{ $record->model->user->profile->realname }}</td>
                                                            <td class="vertical-align-middle">{{  $record->model->user->name }}</td>
                                                            <td class="vertical-align-middle">{{  $record->model->user->email }}</td>
                                                            <td class="vertical-align-middle">{{ $record->model->user->department->name }}</td>
                                                            <td class="vertical-align-middle">{{  $record->model->course->title }}</td>
                                                            <td class="vertical-align-middle">{{ trans($record->recordType->title) }}</td>
                                                            <td class="vertical-align-middle">{{  $record->model->course->catalog->name }}</td>
                                                            <td class="vertical-align-middle">
                                                                @if($record->model->process==1)
                                                                    @if(($record->model->course->examDistribution && !empty($examRecord[$record->id])) || (!empty($evaluationRecord[$record->id]) &&  $evaResult[$record->id]>0))
                                                                        {{trans('course.under_way')}}
                                                                    @else
                                                                        {{trans('course.not_start')}}
                                                                    @endif
                                                                @else
                                                                    
                                                                    {{ $record->model->process==3?trans('course.under_way'):trans('course.'.$record->model->status->title)}}
                                                                @endif
                                                                {{$record->model->overdue?'('.trans('table.overdue').')':''}}</td>
                                                                 <td class="vertical-align-middle">
                                                                    @if(empty($surveyRecord[$record->id]))
                                                                        
                                                                    @elseif($surveyResult[$record->id]<1)
                                                                        {{ trans('table.not_start') }}
                                                                    @else
                                                                        {{ trans('table.done') }}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">{{  trans('course.'.$record->model->status->title)}} </td>
                                                                <td class="vertical-align-middle">
                                                                    @if(!$record->model->course->examDistribution)
                                                                        
                                                                    @elseif(!$examRecord[$record->id])
                                                                        {{ trans('exam.not_start') }}
                                                                    @else
                                                                        {{ trans("exam.".$examRecord[$record->id]->status->title) }}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">
                                                                    @if(empty($evaluationRecord[$record->id]))

                                                                    @elseif($evaResult[$record->id]<1)
                                                                        {{ trans('table.not_start') }}
                                                                    @else
                                                                        {{ trans('table.done') }}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">{{  $record->model->created_at }}</td>
                                                                <td class="vertical-align-middle">{{  $record->model->started_at }}</td>
                                                                <td class="vertical-align-middle">
                                                                    @if($record->model->end_at)
                                                                        {{$record->model->end_at}}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">{{ $record->model->study_times}}</td>
                                                                <td class="vertical-align-middle">
                                                                    @if($record->model->study_span)
                                                                        {{$record->model->study_span.trans('table.minute')}}
                                                                    @else
                                                                        0{{ trans('table.minute') }} 
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">{{  $record->model->max_score }}</td>
                                                                <td class="vertical-align-middle">{{  $record->model->exam_max_score }}</td>
                                                               
                                                        </tr>
                                                    @endif
                                                   
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $records->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    @endsection
