@extends('courses.partials.template')
@section('js')
    {!! Html::script('assets/bower_components/handlebars/handlebars.min.js') !!}
    {!! Html::script('assets/bower_components/jquery-bootpag/lib/jquery.bootpag.min.js') !!}
    {!! Html::script('assets/bower_components/spin.js/spin.js') !!}
    {!! Html::script('assets/js/course_exam.js') !!}
@endsection

@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{ trans('exam.assign') }}</h2>
                <div>
                    {{ trans('exam.assigned') }}:<span id="selectedExamTitle">{{ $course->examDistribution ? $course->examDistribution->exam->title : trans('table.none') }} </span>
                </div>
                <div class="form-inline">
                    <div class="form-group">

                        <input id="searchWord" class="form-control" type="text" name="findByUsername" placeholder="{{ trans('table.input_select_exam') }}">

                    </div>
                    <button id="courseSearchButton" type="submit" class="dm3-btn dm3-btn-medium button-large">
                        <i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}
                    </button>
                </div>
            </div>
        </div>

        <div class="panel panel-announcement">

            <div class="panel-body">
                <div class="col-md-12">
                    
                    <h4 class="panel-title">{{ trans('exam.for_assign') }}
                        <a  data-toggle="tooltip" title="{{ trans('tips.00031') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
                    </h4>
                    
                    <div class="table-responsive">
                        <div class="courseTable text-center">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{ trans('table.choose') }}</th>
                                        <th>{{ trans('exam.id') }}</th>
                                        <th>{{ trans('exam.title') }}</th>
                                        <th>{{ trans('exam.description') }}</th>
                                        <th>{{ trans('exam.pass_score') }}</th>
                                    </tr>
                                </thead>
                                <tbody id="ExamsForm">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div id="ExamsForm-Pagination"></div>
                    <button id="chooseExamButton" class="dm3-btn dm3-btn-medium button-large">
                        <i class="fa fa-thumbs-o-up"></i>&nbsp;{{ trans('button.submit') }}
                    </button>
                    <button id="relieveExamButton" class="dm3-btn dm3-btn-medium dm3-btn-red button-large">
                        <i class="fa fa-trash"></i>&nbsp;{{ trans('button.relieve') }}
                    </button>


                    @if($course->examDistribution)

                         <a class="dm3-btn dm3-btn-medium button-large" href="/course/{{$course->course_id}}/exam/{{$course->examDistribution->exam->exam_id}}/QR-code" target="_blank">
                             <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam_QR') }}</a>

                          <a class="dm3-btn dm3-btn-medium button-large" href="/courses/{{$course->course_id}}/exam/{{$course->examDistribution->exam->exam_id}}/result">
                            <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.result') }}</a>
                    @else
                        <a class="dm3-btn dm3-btn-gray button-large" disabled >
                            <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam_QR') }}</a>
                         <a class="dm3-btn dm3-btn-gray button-large" disabled >
                                <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.result') }}</a>
                    @endif

                </div>
            </div>
        </div>
    </div>


    <script id="examTemplate" type="text/x-handlebars-template">
        @{{#each data}}
        <tr>
            <td><input class="" type="radio" name="user[]"
                       id="@{{id}}" value="@{{id}}"></td>
            <td>@{{exam_id}}</td>
            <td>@{{title}}</td>
            <td>@{{description}}</td>
            <td>@{{pass_score}}</td>
        </tr>
        @{{/each}}
    </script>


    <script>
     var course_id = {{$course->id}};
    </script>
@endsection
