@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>

            <h3>{{ trans('course.manage') }}</h3>
            <div class="row">
                <div class="col-sm-12 main">
                    <form class="form-inline" action="/courses">
                        <div style="display:inline-block;">

                            <a  onclick="catalogSelect()" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                        </div>
                        <div style="display:inline-block;" id="catalog_name">{{isset($catalogs)?$catalogs:""}}</div>
                        <input type="hidden" id="catalog_id"  name="catalog_id" value="{{isset($a['catalog_id'])?$a['catalog_id']:""}}"/>
                        <br>
                        <div style="float: left;">
                            <input type="text" name="findByCourseName" class="form-control" placeholder="{{ trans('table.input_select_course') }}" value="{{isset($a['findByCourseName'])?$a['findByCourseName']:""}}">
                            <button type="submit" class="dm3-btn dm3-btn-medium button-large">
                                <i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}
                            </button>
                        </div>
                        <div style="float: right;">
                            <a href="{{URL('courses/create')}}" class="dm3-btn dm3-btn-medium button-large">
                                <i class="fa fa-plus"></i>&nbsp;{{ trans('course.create') }}
                            </a>
                        </div>

                    </form>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12 main">
                    <div class="courseTable">
                        <table class="table table-striped table-bordered text-center">
                            <thead>
                                <tr>
                                    <th>{{ trans('course.id') }}</th>
                                    <th>{{ trans('course.title') }}</th>
                                    <th>{{ trans('course.type') }}</th>
                                    <th>{{ trans('table.operation') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($courses as $course)
                                    <tr>
                                        <td class="vertical-align-middle">{{ $course->course_id }}</td>
                                        <td class="vertical-align-middle">{{ $course->title }}</td>
                                         <td class="vertical-align-middle"> {{$course->type==1?trans('record.type1'):trans('record.type3')}}</td>
                                        <td class="vertical-align-middle">
                                            <a href="{{ url('/courses/'.$course->course_id.'/edit') }}" class="dm3-btn dm3-btn-medium button-large">
                                                <i class="fa fa-pencil"></i>&nbsp;{{ trans('button.edit') }}</a>
                                            @if($course->type == 1)
                                            <a href="{{ url('/courses/'.$course->course_id.'/upload') }}"
                                               class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-refresh"></i>&nbsp;{{ trans('button.update') }}</a>
                                            @else
                                            <a href="{{ url('/courses/'.$course->course_id.'/class') }}"
                                               class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-refresh"></i>&nbsp;{{ trans('button.update') }}</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
{!! $courses->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
                    </div>
                </div>
            </div>

        </div>
    @endsection
