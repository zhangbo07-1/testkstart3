@extends('layout.master')
@section('css')

    {!! Html::style('assets/bower_components/raty/lib/jquery.raty.css') !!}
@endsection

@section('js')
    {!! Html::script('assets/bower_components/raty/lib/jquery.raty.js') !!}
    {!! Html::script('assets/js/jquery_raty.min.js') !!}
    {!! Html::script('assets/js/views/view.operation.js') !!}
@endsection

@section('content')
    <br>
    <br>
    <div class="container">
        <aside class="lessons-container">
            <h3 class="lessons-title">{{ trans('course.lesson') }}</h3>

            <div class="row">
                <div class="col-sm-12 main">
                    <form class="form-inline" action="/lessons">
                        <div style="display:inline-block;">

                            <a onclick="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-file"></i>&nbsp;{{ trans('button.catalog') }}</a>
                        </div>
                        <div style="display:inline-block;" id="catalog_name">{{isset($catalogs)?$catalogs:""}}</div>
                        <input type="hidden" id="catalog_id"  name="catalog_id" value="{{isset($a['catalog_id'])?$a['catalog_id']:""}}"/>
                        <br>
                        <div style="float: left;">

                            {!! Form::select('type', $types, isset($a['type'])?$a['type']:null,['class'=>"form-control",'style'=>"width:126px"]) !!}
                            <div style="display:inline-block;">
                                <input type="text"  class="form-control" name="findByCourseName" placeholder="{{ trans('table.input_select_course') }}" value="{{isset($a['findByCourseName'])?$a['findByCourseName']:""}}">
                            </div>
                            <div style="display:inline-block;">
                                <button type="submit" class="dm3-btn dm3-btn-medium button-large">
                                    <i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <ul class="lessons-ul">
                @include('layout.message')
                @foreach($records as $record)
                    @if($record->type == 1 || $record->type == 3)
                        <li class="row">
                            <div class="col-md-6">
                                @if($record->type == 1)
                                    <div class="col-fixed"><span class="icon icon-0"></span></div>
                                @else
                                    <div class="col-fixed"><span class="icon icon-4"></span></div>
                                @endif
                                    <div class="row row-margin-5px " style="margin-left: 65px;" >
                                        <div class="lessons-padding ">

                                        <div>
                                             
                                            <a  style="color: #149dd2;" class="lessons-course-name"
                                               href="/lessons/{{$record->model->course->course_id}}/info">{{ $record->model->course->title}}</a>
                                             
                                        </div>

                                            <span class="post-date"><i class="fa fa-square orange">&nbsp;</i>{{ $record->model->process == 3 ? trans('course.under_way') : trans('course.'.$record->model->status->title) }}&nbsp;&nbsp;
                                                <i class="fa fa-clock-o">
                                                    &nbsp;</i>{{ trans('table.start_time') }}:{{ $record->model->process>1 ?  date('Y-m-d',strtotime($record->model->started_at)) : trans('table.not_start') }}

                                            </span>

                                            <div class="star-pane" record_id="{{$record->model->id}}"
                                                 score="{{$record->model->star}}">

                                            </div>
                                            
                                            @if ($record->model->course->limitdays != 0)
                                                <div>
                                                    {{ trans('table.cut_off_time') }}: {!! getEndDate($record->model->created_at,$record->model->course->limitdays) !!}</div>

                                            @endif
                                        </div>
                                    </div>
                            </div>
                            <div class="col-md-6">
                                <div class="lessons-padding-button">
                                    @if($record->model->course->type == 1)

                                        <a class="dm3-btn dm3-btn-medium"
                                           onclick="openLessons( '/courses/{{$record->model->course->course_id}}/launch' )" target="_blank">
                                            <i class="fa fa-magic"></i>&nbsp;{{ trans('button.start') }}</a>
                                    @else
                                            <a class="dm3-btn dm3-btn-medium"
                                               href="/lessons/{{$record->model->course->course_id}}/info">
                                                <i class="fa fa-magic"></i>&nbsp;{{ trans('button.start') }}</a>
                                    @endif
                                    


                                    @if($record->model->course->examDistribution)
                                        <a class="dm3-btn dm3-btn-medium" href="/lessons/{{$record->model->course->course_id}}/exam/{{$record->model->course->examDistribution->exam->exam_id}}/answer">
                                            <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam') }}</a>
                                    @else
                                            <a class="dm3-btn dm3-btn-gray" disabled >
                                                <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam') }}</a>
                                    @endif
                                    @if(!empty($evaluationRecords[$record->id]))
                                        <a class="dm3-btn dm3-btn-medium grey"
                                           href="/lessons/{{$record->model->course->course_id}}/evaluation/{{$evaluationRecords[$record->id]->id}}/answer">
                                            <i class="fa fa-dot-circle-o"></i>&nbsp;{{ trans('button.evaluation') }}</a>

                                    @else
                                            <a  class="dm3-btn dm3-btn-gray" disabled >

                                                <i class="fa fa-dot-circle-o"></i>&nbsp;{{ trans('button.evaluation') }}</a>
                                    @endif


                                    <a class="dm3-btn dm3-btn-medium" href="/lessons/{{$record->model->course->course_id}}/info">
                                        <i class="fa fa-folder-o"></i>&nbsp;{{ trans('button.more') }}</a>
                                </div>
                            </div>
                        </li>
                                @endif
                                @if($record->type == 2)
                                    <li class="row">
                                        <div class="col-md-6">
                                            <div class="col-fixed"><span class="icon icon-18"></span></div>
                                            
                                            <div class="row row-margin-5px " style="margin-left: 65px;" >
                                                <div class="lessons-padding ">
                                                    <div><a style="color: #149dd2;" class="lessons-course-name"
                                                            href="{{ '/exam-lessons/'.$record->model->exam->exam_id.'/info' }}">{{ $record->model->exam->title }}</a></div>
                                                    <span class="post-date"><i class="fa fa-square orange">&nbsp;</i>{{ trans('table.under_way') }}&nbsp;&nbsp;
                                                        <i class="fa fa-clock-o">
                                                            &nbsp;</i>{{ trans('table.start_time') }}:
                                                        @if(count($record->model->records))
                                                            {{$record->model->records[0]->created_at}}
                                                        @else
                                                            {{  trans('table.not_start') }}
                                                        @endif
                                                    </span>
                                                    @if ($record->model->exam->limit_days != 0)
                                                        <div>
                                                            {{ trans('table.cut_off_time') }}: {!! getEndDate($record->model->created_at,$record->model->exam->limit_days) !!}</div>

                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="lessons-padding-button">

                                                <a class="dm3-btn dm3-btn-medium" href="{{ '/exam-lessons/'.$record->model->exam->exam_id.'/answer' }}">
                                                    <i class="fa fa-magic"></i>&nbsp;{{ trans('button.start') }}</a>


                                                <a class="dm3-btn  dm3-btn-gray" disabled>
                                                    <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam') }}</a>
                                                <a class="dm3-btn dm3-btn-gray" disabled>
                                                    <i class="fa fa-dot-circle-o"></i>&nbsp;{{ trans('button.evaluation') }}</a>
                                                <a class="dm3-btn dm3-btn-medium" href="/exam-lessons/{{$record->model->exam->exam_id}}/info">                                                                                    <i class="fa fa-folder-o"></i>&nbsp;{{ trans('button.more') }}</a>
                                            </div>
                                        </div>
                                    </li>
                                @endif
                @endforeach
            </ul>
            {!! $records->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
        </aside>
    </div>

@endsection
