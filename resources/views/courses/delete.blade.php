@extends('courses.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{$course->title }}}</h2>
                </div>
            </div>
            <div class="panel panel-announcement">
                <div class="panel-body" style="width: 700px;">

                    

                    <form action="{{ URL('courses/'.$course->id) }}" method="POST" class ="form-horizontal length-750px"">
                        <input name="_method" type="hidden" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group ">
                        <label for="exam_id" align="right" class="col-xs-4 ">{{ trans('course.id') }}</label>

                        <div class="col-xs-8">
                            {{$course->course_id}}
                        </div>
                    </div>


                    <div class="form-group ">
                        <label for="title" align="right" class="col-xs-4">{{ trans('course.title') }}</label>

                        <div class="col-xs-8">
                            {{$course->title}}
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="manage_id" align="right" class="col-xs-4">{{ trans('table.range') }}</label>

                        <div class="col-xs-8">

                            {{$manages}}
                        </div>
                    </div>
                    <div class="form-group"{!! errors_for_tag('catalog_id', $errors) !!}">
                        <label for="catalog_id" align="right" class="col-xs-4">{{ trans('table.catalog') }}</label>

                        <div class="col-xs-8">

                            {{$course->catalog->name}}

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="create_time" align="right" class="col-xs-4">{{ trans('table.create_time') }}</label>

                        <div class="col-xs-8">
                            <div class="div-form-control">{{$course->created_at}}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="update_time" align="right" class="col-xs-4">{{ trans('table.update_time') }}</label>

                        <div class="col-xs-8">
                            <div class="div-form-control">{{$course->created_at}}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="confirm_button" align="right" class="col-xs-4 "></label>

                        <div class="col-xs-8">
                            <button  onClick="return ifDelete()" class="dm3-btn dm3-btn-red button-large" type="submit">
                                <i class="fa fa-check"></i>&nbsp;{{trans('button.delete')}}
                            </button>
                        </div>
                    </div>
                   </form>
                </div>
            </div>
        </div>
    @endsection
