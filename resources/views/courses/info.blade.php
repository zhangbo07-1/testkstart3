@extends('layout.master')

@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
@endsection
@section('content')
    <div class="container">
        <br>
        <br>

        <h3  style="color: #149dd2;">{{$course->title}}</h3>

        <div class="row">
            <div class="col-sm-12 main">
                <div style="float: right;">
                    <a class="dm3-btn dm3-btn-medium button-large" style="margin-bottom:1em;" href="/lessons/{{$course->course_id}}/return" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
                </div>
                <div style="float: left;">
                    @if($course->type == 1)
                        <a class="dm3-btn dm3-btn-medium" onclick="javascript:openLessons( '/courses/{{$course->course_id}}/launch' );" target="_blank">
                            <i class="fa fa-magic"></i>&nbsp;{{ trans('button.start') }}</a>
                    @elseif($course->type == 2)
                            @if($surveyRecord)
                                <a class="dm3-btn dm3-btn-medium" href="/lessons/{{$course->course_id}}/course-survey/{{$surveyRecord->id}}/answer">
                        <i class="fa fa-magic"></i>&nbsp;{{ trans('survey.name') }}</a>
                            @else
                                                                        <a  class="dm3-btn dm3-btn-gray" disabled >

                                    <i class="fa fa-magic"></i>&nbsp;{{ trans('survey.name') }}</a>
                            @endif

                    @endif

                    <a class="dm3-btn dm3-btn-medium" href="/lessons/{{$course->course_id}}/log">
                        <i class="fa fa-folder-o"></i>&nbsp;{{ trans('button.record') }}</a>

                    @if($course->examDistribution)
                        <a class="dm3-btn dm3-btn-medium" href="/lessons/{{$course->course_id}}/exam/{{$course->examDistribution->exam->exam_id}}/answer">
                            <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam') }}</a>
                    @else
                            <a class="dm3-btn dm3-btn-gray" disabled >
                                <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam') }}</a>
                    @endif
                    @if($evaluationRecord)
                        <a class="dm3-btn dm3-btn-medium grey"
                           href="/lessons/{{$course->course_id}}/evaluation/{{$evaluationRecord->id}}/answer">
                            <i class="fa fa-dot-circle-o"></i>&nbsp;{{ trans('button.evaluation') }}</a>

                    @else
                            <a  class="dm3-btn dm3-btn-gray" disabled >

                                <i class="fa fa-dot-circle-o"></i>&nbsp;{{ trans('button.evaluation') }}</a>
                    @endif


                    <a class="dm3-btn dm3-btn-medium" href="/lessons/{{$course->course_id}}/posts">
                        <i class="glyphicon glyphicon-book"></i>&nbsp;{{ trans('button.post') }}</a>
                    @if($course->resource_id)
                        <a class="dm3-btn dm3-btn-medium" href="/lessons/{{$course->course_id}}/resources">
                            <i class="fa fa-file-archive-o"></i>&nbsp;{{ trans('button.resource') }}</a>
                    @else
                            <a  class="dm3-btn dm3-btn-gray" disabled >

                                <i class="fa fa-file-archive-o"></i>&nbsp;{{ trans('button.resource') }}</a>
                    @endif
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 main">
                @include('layout.message')
                <br>
                @if($course->type == 2)
                    @foreach($course->classes as $class)
                        @if(!empty($classRecord[$class->id]))
                            {{ trans('course.class_title') }}  :
                            {{$class->title}}
                            <br>
                            @foreach($class->lessons as $index=>$lesson)
                                {{ trans('course.class_lesson') }}{{$index+1}}: {{$lesson->start_at}} ~ {{$lesson->end_at}}
                                <br>
                                {{ trans('course.class_address') }}  :
                                {{$lesson->address}}
                                <br>
                            @endforeach

                            <br>

                        @endif
                    @endforeach
                @endif
                {{ trans('course.description') }}：

                {{$course->description}}

                <br>
                <br>


                {{ trans('table.catalog') }}：

                {{$course->catalog->name}}

                <br>
                <br>

                {{ trans('course.hours') }}：

                {{$course->hours}}{{ trans('table.hour') }}

                <br>
                <br>

                {{ trans('table.limit_data') }}：

                {{$course->limitdays?$course->limitdays.trans('table.day'):trans('table.no_limit')}}

                <br>
                <br>

                {{ trans('course.target') }}：

                {{$course->target}}
                <br>
                <br>

                {{ trans('table.language') }}：

                {{$course->language->title}}
                <br>
            </div>
        </div>
    </div>
@endsection
