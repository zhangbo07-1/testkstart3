<div class="row">
    <div class="col-sm-12 main">
        <h1 class="text-center">{{ trans('report.exam') }}</h1>

        <div class="table-responsive courseTable">
            <table class="table table-striped">
                <thead>
                    <th>
                        {{ trans('exam.title') }}
                    </th>
                    <th>
                        {{ trans('table.exam_time') }}
                    </th>
                    <th>
                        {{ trans('table.score') }}
                    </th>
                    <th>
                        {{ trans('table.status') }}
                    </th>
                </thead>
                <tbody>
                    @if($examRecord)
                        @foreach($examRecord->results as $examResult)
                            @if($examResult->overed == 1)
                                <tr>
                                    <td><a href="log/{{$examResult->id}}/detailed">{{$examRecord->distribution->exam->title}}</a></td>
                                    <td>{{$examResult->created_at}}</td>
                                    <td>{{$examResult->score}}</td>
                                    @if($examResult->passed)
                                        <td>{{ trans('table.passed') }}</td>
                                    @else
                                        <td>{{ trans('table.not_pass') }}</td>
                                    @endif
                                </tr>
                            @elseif($examResult->overed == 2)
                                <tr>
                                    <td>{{$examRecord->distribution->exam->title}}</td>
                                    <td>{{$examResult->created_at}}</td>
                                    <td></td>

                                    <td>{{ trans('exam.not_marked') }}</td>

                                </tr>
                            @endif
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
