@extends('layout.call')

@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
  <form action="{{ URL('catalogs/'.$node->id.'/edit') }}" method="POST">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">


                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <br>
    @include('layout.message')
                    <a>{{ trans('catalog.title') }}</a>
                    <input type="text" name="name" class="form-control" required="required" value="{{$node->name}}">



            </div>
        </div>

        <br>
        <br>
        <div align="center">
             <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
            <a class="dm3-btn dm3-btn-medium dm3-btn-red button-large"  onclick="closeWindow()">{{ trans('button.cancel') }}</a>
        </div>
 </form>

    @endsection
