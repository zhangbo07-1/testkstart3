@extends('layout.master')
@section('css')
    {!! Html::style('assets/jstree/dist/themes/default/style.min.css') !!}
    @append
    @section('js')

        {!! Html::script('assets/jstree/dist/jstree.js') !!}
        {!! Html::script('assets/js/catalog.js') !!}
        @append
        @section('content')
            <div class="container">
                <br>
                <br>
                <h3>{{ trans('catalog.manage') }}
                <a data-toggle="tooltip" title="{{ trans('tips.00019') }}"><span class="glyphicon glyphicon-question-sign"></span></a></h3>
                <div class="container clearfix">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-inline">
             <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="open_all();"><i class="glyphicon glyphicon-plus"></i>&nbsp;{{ trans('button.open_all') }}</button>
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="close_all();"><i class="glyphicon glyphicon-minus"></i>&nbsp;{{ trans('button.close_all') }}</button>
                                <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="create_node();"><i class="glyphicon glyphicon-asterisk"></i>&nbsp;{{ trans('catalog.create') }}</button>
                                <button type="button"class="dm3-btn dm3-btn-medium button-large" onclick="rename_node();"><i class="glyphicon glyphicon-pencil"></i>&nbsp;{{ trans('catalog.rename') }}</button>
                                <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="delete_node();"><i class="glyphicon glyphicon-remove"></i>&nbsp;{{ trans('catalog.delete') }}</button>
                                <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="search_node();"><i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}</button>
                                <input id="searchWord" type="text" class="form-control"  placeholder="{{ trans('table.input_select') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div id="jstree_div"></div>

                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>


                </div>
                <script>
                                                                                                                                                                  var delete_error = "{{trans('error.00073')}}";
                 var delete_log = "{{trans('error.00064')}}";
                </script>
        @endsection
