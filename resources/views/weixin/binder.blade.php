@extends('layout.base')


@section('navbar')



    <div id="page-toolbar">
        <div class="container clearfix nopadding">
            <div class="toolbar-items" style="display:inline-block;">
                <div class="item">
                    <div class="inner">
                        {!!get_info()!!}
                    </div>
                    
                </div>
            </div>
        </div>
        
    </div>




    <header id="page-header" class="fixed-header">
        
        <div id="page-header-inner">
            <div id="header-container-weixin">
                <div class="container clearfix nopadding">
                    
                    <div id="main-logo">
                        <a href="">
                            <img src="{!!get_logo_url()!!}/img/logo.png" srcset="{!!get_logo_url()!!}/img/logo.png 1x, {!!get_logo_url()!!}/img/logo.png 2x" alt=""> </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    


                    
@endsection





@section('content')

    <div class="container">
        <div class="entry-content">
            <div id="page-title">
                <br>

                <h1 class="entry-title">{{trans('user.weixin_bind')}}</h1>
            </div>
            <div id="auth-forms">
                @include('layout.message')

                {!! Form::open(array('url'=>'weixin/bind/save','method'=>'POST','files'=>true))!!}
                <input type="hidden" name="openid" value="{{$openid }}">


                @if($info->user_email)
                    <div class="form-group {!! errors_for_tag('email', $errors) !!}">
                        <label class="" for="exampleInputAmount">{{trans('table.email')}}</label>
                        
                        {!! errors_for('email', $errors) !!}
                        <input id="loginEmail" style="width:100%;" class="form-control" name="email"  placeholder="{{trans('user.email')}}">

                    </div>
                @else
                    <div class="form-group {!! errors_for_tag('name', $errors) !!}">
                        <label class="" for="exampleInputAmount">{{trans('user.title')}}</label>
                        {!! errors_for('name', $errors) !!}
                        <input id="loginEmail" style="width:100%;" class="form-control" name="name"  placeholder="{{trans('user.title')}}">
                    </div>
                @endif


                <div class="form-group {!! errors_for_tag('password', $errors) !!}">
                    <label>{{trans('table.password')}}</label>
                    {!! Form::password('password', ['class'=> 'form-control','placeholder' => trans('table.password_limit')]) !!}
                    {!! errors_for('password', $errors) !!}
                </div>

                <div>
                    {!! Form::submit(trans('button.bind'),['class' => 'dm3-btn dm3-btn-medium button-large']) !!}
                    @if(get_register())
                        <a class="dm3-btn dm3-btn-medium button-large" href="/register">{{ trans('table.register') }}</a>
                    @endif
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('footer')
    @include('layout.footer')   

@endsection
