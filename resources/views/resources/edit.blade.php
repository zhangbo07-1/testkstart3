@extends('layout.master')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>

            <h3>{{ trans('resource.edit') }}</h3>
            <div class="contentpanel">
                <div class="panel panel-announcement">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-5 col-md-offset-3">
                                <form action="{{ URL('resources-manage/'.$resource->id) }}" method="POST">
                                    <input name="_method" type="hidden" value="PUT">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <br>
                                    <a>{{ trans('resource.title') }}</a>
                                    <input type="text" name="name" class="form-control" required="required" value="{{ $resource->name }}">
                                    {!! errors_for('title', $errors) !!}
                                    <br>
                                    <a>{{ trans('table.range') }}</a>
                                    <br>
                                    <div style="display:inline-block;">
                                        <a  onclick="manageSelect()" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                                    </div>
                                    <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                                    <input type="hidden" id="manage_id"  name="manage_id" value="{{$manageIds}}"/>
                                    {!! errors_for('manage_id', $errors) !!}
                                    <br>
                                    <br>
                                    <a>{{ trans('table.catalog') }}{{ trans('table.must') }}</a>
                                    <br>
                                    <div style="display:inline-block;">

                                        <a  onclick="catalogSelect()" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                                    </div>
                                    <div style="display:inline-block;" id="catalog_name">{{$resource->catalog->name}}</div>
                                    <input type="hidden" id="catalog_id"  name="catalog_id" value="{{$resource->catalog_id}}"/>
                                    {!! errors_for('catalog_id', $errors) !!}

                                    <br>
                                    <br>
                                    <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                                    <a  href="{{ URL('resources-manage') }}" class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    @endsection
