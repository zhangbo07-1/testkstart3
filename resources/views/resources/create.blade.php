@extends('layout.master')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>
            
            <h3>{{ trans('resource.create') }}</h3>
            <div class="entry-content">
                <div class="clearfix">
                    <div align="center">
                        @include('layout.message')
                    </div>
                    {!! Form::open(array('url'=>'resources-manage','method'=>'POST', 'files'=>true,'class'=>"form-horizontal",'id'=>'need_submit')) !!}
                    <input type="hidden" name="area_id" value="{{isset($course)? $course->resource_id:1}}">
                    <input type="hidden" name="course_id" value="{{isset($course)? $course->course_id:''}}">
                    <br>
                    <div class="form-group {!! errors_for_tag('name', $errors) !!}">
                        <label  class="col-xs-5 control-label">{{ trans('resource.title').trans('table.must') }}:</label>
                        <div class="col-xs-7">
                            <div style="display:inline-block;">
                                {!! Form::text("name",null,array('required' => 'required','class'=>"form-control")) !!}
                            </div>
                            {!! errors_for('name', $errors) !!}
                            
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('file', $errors) !!}">
                        <label class="col-xs-5 control-label">{{ trans('table.select_file').trans('table.must') }}:</label>
                        <div  class="col-xs-7">
                            <a onclick="document.getElementById('upfile').click();" class="dm3-btn dm3-btn-medium button-large" style="display:inline-block;">{{ trans('button.file') }}</a>
                            <div>
                                <input type="text" name="url" class="form-control" id="fileURL">
                            </div>
                            <input type="file" name="file" id="upfile" onchange="document.getElementById('fileURL').value=this.value;" style="display:none">
                            {!! errors_for('file', $errors) !!}
                        </div>
                    </div>
                    <div class="form-group {!! errors_for_tag('manage_id', $errors) !!}">
                        <label class="col-xs-5 control-label">{{ trans('table.range').trans('table.must') }}:</label>
                        <div class="col-xs-7">
                            <div style="display:inline-block;">
                                <a  onclick="manageSelect()" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                            </div>
                            <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                            <input type="hidden" id="manage_id"  name="manage_id" value="{{$manageIds}}"/>
                            {!! errors_for('manage_id', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('catalog_id', $errors) !!}">
                        <label class="col-xs-5 control-label">{{ trans('table.catalog').trans('table.must') }}:</label>
                        <div class="col-xs-7">
                            <div style="display:inline-block;">
                                
                                <a  onclick="catalogSelect()" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                            </div>
                            <div style="display:inline-block;" id="catalog_name"></div>
                            <input type="hidden" id="catalog_id"  name="catalog_id" value=""/>
                            {!! errors_for('catalog_id', $errors) !!}
                        </div>
                    </div>
                    
                    <br>
                    <div class="form-group">
                        <label for="confirm_button" class="col-xs-5 control-label"></label>
                        <div class="col-xs-7">
                            
                            <button id="need_disable" class="dm3-btn dm3-btn-medium button-large" onClick="showLoad()" >{{ trans('button.submit') }}</button>
                            @if(isset($course))
                                <a href="{{ URL('courses/'.$course->course_id.'/resources') }}" class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.return') }}</a>
                            @else
                                <a href="{{ URL('resources-manage') }}" class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.return') }}</a>
                            @endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endsection
