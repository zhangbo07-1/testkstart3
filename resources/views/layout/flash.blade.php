@if (Session::has('errors'))
    <div class="alert ">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        {{ $errors->first }}
    </div>
@endif