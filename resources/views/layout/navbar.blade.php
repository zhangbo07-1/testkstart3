<div id="page-toolbar">
    <div class="container clearfix nopadding">
        <div class="toolbar-items" style="display:inline-block;">
            <div class="item">
                <div class="inner">
                    {!!get_info()!!}
                </div>
                
            </div>
        </div>
    </div>
    
</div>



<header id="page-header" class="fixed-header">
    
    <div id="page-header-inner">
        <div id="header-container">
            <div class="container clearfix nopadding">
                
                <div id="main-logo">
                    <a href="">
                        <img src="{!!get_logo_url()!!}/img/logo.png" srcset="{!!get_logo_url()!!}/img/logo.png 1x, {!!get_logo_url()!!}/img/logo.png 2x" alt=""> </a>
                </div>

                <nav id="main-nav">
                    <ul id="menu-main-menu" class="menu">
                        @if (Auth::guest())

                            <li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent">
                                <a href="{{route('login')}}">{{ trans('table.login') }}</a>
                            </li>
                            @if(get_register())
                                <li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent">
                                    <a href="{{route('register')}}">{{ trans('table.register') }}</a>
                                </li>
                            @endif
                            
                        @else
                            <li id="indexLink" class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a href="/">{{ trans('table.home')}}</a>
                            </li>
                            <li id="lessonsLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-768">
                                <a>{{ trans('course.my_lesson') }}</a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-812">
                                        <a href="/lessons">{{ trans('course.lesson') }}</a></li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-785">
                                        <a href="/lessons-done">{{ trans('course.lesson_done') }}</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-785">
                                        <a href="/lessons-overdue">{{ trans('course.lesson_overdue') }}</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="resourcesLink" class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a href="/resources">{{ trans('resource.name') }}</a>
                            </li>
                            <li id="postsLink"
                                class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent">
                                <a href="/posts">{{ trans('post.name') }}</a>
                            </li>

                            @can('manage_report',Auth::user())
                            <li id="reportLink"
                                class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent">
                                <a href="/reports">{{ trans('report.name') }}</a>
                            </li>
                            @endcan
                            @can('manage_lesson',Auth::user())
                            <li id="courseLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-769">
                                <a>{{ trans('course.study_manage') }}</a>
                                <ul class="sub-menu">
                                    
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/courses">{{ trans('course.manage') }}</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/exams">{{ trans('exam.manage') }}</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/question-bank">{{ trans('bank.manage') }}</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/evaluation">{{ trans('evaluation.manage') }}</a>
                                    </li>

                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/course-survey">{{ trans('course.course_survey') }}</a>
                                    </li>
                                </ul>
                            </li>
                            @endcan
                            @can('manage_user',Auth::user())
                            <li id="userManageLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-769">
                                <a>{{ trans('user.manage') }}</a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/users">{{ trans('user.manage') }}</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/organization">{{ trans('organization.manage') }}</a>
                                    </li>
                                    @can('import_users',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/users/import">{{ trans('user.import') }}</a>
                                    </li>
                                    @endcan
                                    @can('import_organization',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/organization/import">{{ trans('organization.import') }}</a>
                                    </li>
                                    @endcan
                                    @can('manage_attribute',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/attribute">{{ trans('attribute.manage') }}</a>
                                    </li>
                                    @endcan
                                </ul>
                            </li>
                            @endcan
                            @can('manage_system',Auth::user())
                            <li id="systemLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-769">
                                <a>{{ trans('table.system_manage') }}</a>
                                <ul class="sub-menu">

                                    @can('manage_catalog',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/catalogs">{{ trans('catalog.manage') }}</a>
                                    </li>
                                    @endcan
                                    @can('manage_message',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/news-manage">{{ trans('news.manage') }}</a>
                                    </li>
                                    @endcan
                                        
                                    @can('manage_survey',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/survey">{{ trans('survey.manage') }}</a>
                                    </li>
                                    @endcan
                                        
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/resources-manage">{{ trans('resource.manage') }}</a>
                                    </li>

                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/posts-manage">{{ trans('post.manage') }}</a>
                                    </li>
                                    @can('manage_emailTemplet',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/email-manage">{{ trans('email.manage') }}</a>
                                    </li>
                                    @endcan

                                    @can('manage_logs',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/log-manage">{{ trans('log.manage') }}</a>
                                    </li>
                                    @endcan
                                    @can('manage_info',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/system-manage">{{ trans('system.manage') }}</a>
                                    </li>
                                    @endcan
                                    
                                </ul>
                            </li>
                            @endcan


                            <li id="profileLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-769">
                                <a href="">{{ Auth::user()->name }}({{Auth::user()->role->name}})</a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/profile">{{ trans('user.self_info') }}</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        @can('student',Auth::user())
                                        <a id="userManualChange" href="/manuals/user/pc">{{ trans('table.help') }}</a>
                        @else
                                        <a href="/manuals/manager">{{ trans('table.help') }}</a>
                                        @endcan

                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <li><a href="{{route('logout')}}">{{ trans('table.logout') }}</a></li>
                                </ul>
                                    </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
