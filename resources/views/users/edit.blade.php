@extends('layout.master')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    <script>
     setRole();
    </script>
    @append
    @section('content')
        <div class="container">
            <br>

            <div class="row">
                <div class="col-md-12" >
                    <div  align="right">
                        <a class="dm3-btn dm3-btn-medium button-large"  href="/users" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" align="center">
                    {!! Form::open(array('url' => 'users/'.$user->id, 'method' => 'put')) !!}
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>{{ trans('user.title') }}:</td>
                                <td>
                                    {!! Form::text('name',$user->name,['class'=>"form-control"]) !!}
                                    {!! errors_for('name', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.email') }}</td>
                                <td>
                                     {!! Form::text('email',$user->email,['class'=>"form-control"]) !!}
                                    {!! errors_for('email', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.role') }}
                                    <a data-toggle="tooltip" title="{{ trans('tips.00007') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
                                    :</td>
                                <td>
                                    {!! Form::select('role_id', $roleSelect, $user->role_id,['id'=>'role_id','onClick'=>'setRole()','class'=>"form-control"]) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('table.status') }}
                                    <a data-toggle="tooltip" title="{{ trans('tips.00008') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
                                    :</td>
                                <td>
                                    {!! Form::select('userStatus', $userStatusSelects, $user->status_id,['class'=>"form-control"]) !!}
                                </td>
                            </tr>
                           
                            <tr>
                                <td>{{ trans('user.real_name') }}:</td>
                                <td> {!! Form::text('realname',$user->profile->realname,['class'=>"form-control"]) !!}
                                    {!! errors_for('realname', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.department') }}
                                    <a data-toggle="tooltip" title="{{ trans('tips.00010') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td>
                                    <div style="display:inline-block;">
                                        <a onclick="departmentSelect()" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.department') }}</a>
                                    </div>
                                    <div style="display:inline-block;" id="department_name">{{$user->department->name}}</div>
                                    <input type="hidden" id="department_id"  name="department_id" value="{{$user->department_id}}"/>
                                    {!! errors_for('department_id', $errors) !!}

                                </td>
                            </tr>

                            @if($user->id!=Auth::user()->id)


                             <tr id="user_ranger" style={{($user->role_id > 1) ? "":"display:none"}}>
                                <td>{{ trans('user.ranger') }}
                                    <a data-toggle="tooltip" title="{{ trans('tips.00009') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td>
                                    <div style="display:inline-block;">
                                        <a  onclick="manageSelect()" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                                    </div>
                                    <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                                    <input type="hidden" id="manage_id"  name="manage_id" value="{{$manageIds}}"/>
                                    {!! errors_for('manage_id', $errors) !!}
                                </td>
                            </tr>

                            @endif
                            <tr>
                                <td>{{ trans('user.approve') }}
                                    <a data-toggle="tooltip" title="{{ trans('tips.00011') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td>
                                    <input type="text" name="approve" value="{{$user->approve?$user->approve->name:""}}" class="form-control">
                                    {!! errors_for('approve', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ trans('user.tel') }}:</td>
                                <td>
                                    <input type="text" name="tel" value="{{$user->profile->tel}}" class="form-control">
                                    {!! errors_for('tel', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ trans('user.address') }}:</td>
                                <td>
                                    <input type="text" name="address" value="{{$user->profile->address}}" class="form-control">
                                    {!! errors_for('address', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->first_title }}
                                     <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute1_id', $firstAttributeSelects, $user->profile->attribute1_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute1_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->second_title }}
                                     <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute2_id', $secondAttributeSelects, $user->profile->attribute2_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute2_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->third_title }}
                                     <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute3_id', $thirdAttributeSelects, $user->profile->attribute3_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute3_id', $errors) !!}
                                </td>
                            </tr>

                            

                              <tr>
                                  <td>{{ $attribute->fourth_title }}
                                       <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute4_id', $fourthAttributeSelects, $user->profile->attribute4_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute4_id', $errors) !!}
                                </td>
                              </tr>

                               <tr>
                                   <td>{{ $attribute->fifth_title }}
                                        <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute5_id', $fifthAttributeSelects, $user->profile->attribute5_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute5_id', $errors) !!}
                                </td>
                            </tr>



                            <tr>
                                <td>{{ trans('table.language') }}:</td>
                                <td>
                                    {!! Form::select('language_id', $languageSelect, $user->language_id,['class'=>"form-control"]) !!}
                                </td>
                            </tr>


                             <tr>
                                <td>{{ trans('user.sex') }}:</td>
                                <td>
                                    {!! Form::select('sex_id', $sexSelects, $user->profile->sex_id,['class'=>"form-control"]) !!}
                                </td>
                            </tr>
                           
                        </tbody>
                    </table>
                    <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" align="center">
                        {!! Form::open(array('url' => 'users/'.$user->id.'/changePassword', 'method' => 'POST')) !!}


                        <table class="table table-user-information">

                            <tr>
                                <td>{{ trans('user.new_password') }}:</td>
                                <td>
                                    {!! Form::password('new_password') !!}
                                    {!! errors_for('new_password', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.confirmation_password') }}:</td>
                                <td>
                                    {!! Form::password('new_password_confirmation') !!}
                                    {!! errors_for('new_password_confirmation', $errors) !!}
                                </td>
                            </tr>
                        </table>
                       <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                        {!! Form::close() !!}
                   
                </div>

            </div>



        </div>

        
    @endsection
