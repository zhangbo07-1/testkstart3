@extends('layout.master')
@section('content')
    <div class="container">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 " align="center">
                    <br>
                    <br>
                    {!! Form::open(array('url' => 'users/'.$user->id, 'method' => 'put')) !!}
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>{{ trans('user.title') }}:</td>
                                <td>{{ $user->name }}</td>
                                 <input type="hidden" name="name" value="{{$user->name}}" >
                            </tr>
                            
                            <tr>
                                <td>{{ trans('user.real_name') }}:</td>
                                <td>
                                    {!! Form::text('realname',$user->profile->realname,['class'=>"form-control"]) !!}
                                    {!! errors_for('realname', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.department') }}:</td>
                                <td>{{ $user->department->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.tel') }}:</td>
                                <td>
                                    <input type="text" name="tel" value="{{$user->profile->tel}}" class="form-control">
                                    {!! errors_for('tel', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ trans('user.address') }}:</td>
                                <td>
                                    <input type="text" name="address" value="{{$user->profile->address}}" class="form-control">
                                    {!! errors_for('address', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->first_title }}:</td>
                                <td> {!! Form::select('attribute1_id', $firstAttributeSelects, $user->profile->attribute1_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute1_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->second_title }}:</td>
                                <td> {!! Form::select('attribute2_id', $secondAttributeSelects, $user->profile->attribute2_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute2_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->third_title }}:</td>
                                <td> {!! Form::select('attribute3_id', $thirdAttributeSelects, $user->profile->attribute3_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute3_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->fourth_title }}:</td>
                                <td> {!! Form::select('attribute4_id', $fourthAttributeSelects, $user->profile->attribute4_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute4_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->fifth_title }}:</td>
                                <td> {!! Form::select('attribute5_id', $fifthAttributeSelects, $user->profile->attribute5_id,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute5_id', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('table.language') }}:</td>
                                <td>
                                    {!! Form::select('language_id', $languageSelect, $user->language_id,['class'=>"form-control"]) !!}
                                </td>
                            </tr>


                            <tr>
                                <td>{{ trans('user.sex') }}:</td>
                                <td>
                                    {!! Form::select('sex_id', $sexSelects, $user->profile->sex_id,['class'=>"form-control"]) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                 <input type="hidden" name="email" value="{{$user->email}}" >
                            </tr>

                        </tbody>
                    </table>
                    <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                    <a onclick="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" align="center">
                    {!! Form::open(array('url' => 'users/'.$user->id.'/changePassword', 'method' => 'POST')) !!}


                    <table class="table table-user-information">

                        <tr>
                            <td>{{ trans('user.new_password') }}:</td>
                            <td>
                                {!! Form::password('new_password') !!}
                                {!! errors_for('new_password', $errors) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>{{ trans('user.confirmation_password') }}:</td>
                            <td>
                                {!! Form::password('new_password_confirmation') !!}
                                {!! errors_for('new_password_confirmation', $errors) !!}
                            </td>
                        </tr>
                    </table>
                    <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
    </div>
@endsection
