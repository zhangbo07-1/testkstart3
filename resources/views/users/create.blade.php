
@extends('layout.master')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>

            <div class="row">
                <div class="col-md-12 ">
                    <div  align="right">
                        <a class="dm3-btn dm3-btn-medium button-large" href="/users" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 " align="center">
                        {!! Form::open(array('url' => 'users', 'method' => 'POST')) !!}
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>{{ trans('user.title') }}:</td>
                                <td>
                                {!! Form::text('name',null,['class'=>"form-control",'required' => 'required']) !!}
                                    {!! errors_for('name', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.email') }}:</td>
<td>
                                {!! Form::text('email',null,['class'=>"form-control",'autocomplete'=>"off",'disableautocomplete']) !!}
                                    {!! errors_for('email', $errors) !!}
                                </td>
                            </tr>


                             <tr>
                                <td>{{ trans('user.new_password') }}:</td>
                                <td>
                                    {!! Form::password('password',['class'=>"form-control",'required' => 'required']) !!}
                                    {!! errors_for('password', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.confirmation_password') }}:</td>
                                <td>
                                    {!! Form::password('password_confirmation',['class'=>"form-control",'required' => 'required']) !!}
                                    {!! errors_for('password_confirmation', $errors) !!}
                                </td>
                            </tr>
                            {{--
                            <tr>
                                <td>{{ trans('user.pwd') }}:</td>
<td>
                                {!! Form::password('password',['class'=>"form-control",'required' => 'required','autocomplete'=>"off",'disableautocomplete']) !!}
                                    {!! errors_for('password', $errors) !!}</td>
                            </tr>
                            --}}
                            <tr>
                                <td>{{ trans('user.real_name') }}:</td>
<td> {!! Form::text('realname',null,['class'=>"form-control",'required' => 'required']) !!}
                                    {!! errors_for('realname', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.role') }}
                                    <a data-toggle="tooltip" title="{{ trans('tips.00007') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
                                    :</td>
                                <td>
                                    {!! Form::select('role_id', $roleSelect,null,['id'=>'role_id','onClick'=>'setRole()','class'=>"form-control"]) !!}
                                    {!! errors_for('role_id', $errors) !!}
                                </td>
                            </tr>

                             <tr>
                                 <td>{{ trans('user.approve') }}
                                     <a data-toggle="tooltip" title="{{ trans('tips.00011') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td>
                                    <input type="text" name="approve" value="" class="form-control">
                                    {!! errors_for('approve', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.department') }}
                                    <a data-toggle="tooltip" title="{{ trans('tips.00010') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td>
                                    <div style="display:inline-block;">
                                        <a  onclick="departmentSelect()"  class="dm3-btn dm3-btn-medium button-large">{{ trans('button.department') }}</a>
                                    </div>
<div style="display:inline-block;" onchange="document.getElementById('manage_name').innerHTML=this.innerHTML;" id="department_name">{{Auth::user()->department->name}}</div>
                                    <input type="hidden" id="department_id"  name="department_id" value="{{Auth::user()->department->id}}"/>
                                    {!! errors_for('department_id', $errors) !!}
                                </td>
                            </tr>
                            <tr id="user_ranger" style="display:none">
                                <td>{{ trans('user.ranger') }}
                                    <a data-toggle="tooltip" title="{{ trans('tips.00009') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td>
                                    <div style="display:inline-block;">
                                        <a  onclick="manageSelect()" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                                    </div>
                                    <div style="display:inline-block;" id="manage_name"></div>
                                    <input type="hidden" id="manage_id"  name="manage_id" value=""/>
                                    {!! errors_for('manage_id', $errors) !!}
                                </td>
                            </tr>
                            
                            <tr>
                                <td>{{ trans('user.tel') }}:</td>
                                <td>
                                    <input type="text" name="tel" value="" class="form-control">
                                    {!! errors_for('tel', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ trans('user.address') }}:</td>
                                <td>
                                    <input type="text" name="address" value="" class="form-control">
                                    {!! errors_for('address', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->first_title }}
                                    <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute1_id', $firstAttributeSelects, null,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute1_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->second_title }}
                                     <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute2_id', $secondAttributeSelects,null,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute2_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->third_title }}
                                     <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute3_id', $thirdAttributeSelects,null,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute3_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->fourth_title }}
                                     <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute4_id', $fourthAttributeSelects,null,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute4_id', $errors) !!}
                                </td>
                            </tr>

                            <tr>
                                <td>{{ $attribute->fifth_title }}
                                     <a data-toggle="tooltip" title="{{ trans('tips.00012') }}"><span class="glyphicon glyphicon-question-sign"></span></a>:</td>
                                <td> {!! Form::select('attribute5_id', $fifthAttributeSelects,null,['class'=>"form-control"]) !!}
                                    {!! errors_for('attribute5_id', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('table.language') }}:</td>
                                <td>
                                    {!! Form::select('language_id', $languageSelect,null,['class'=>"form-control"]) !!}
                                    {!! errors_for('language_id', $errors) !!}
                                </td>
                            </tr>
                             <tr>
                                <td>{{ trans('user.sex') }}:</td>
                                <td>
                                    {!! Form::select('sex_id', $sexSelects,null,['class'=>"form-control"]) !!}
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    
                    <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                    <a  onclick="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endsection
