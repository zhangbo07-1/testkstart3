@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/question.min.js') !!}
@append
@section('content')
    @include('questionBank.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection