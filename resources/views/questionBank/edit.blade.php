@extends('questionBank.partials.template')
@section('css')
    {!! Html::style('assets/jstree/dist/themes/default/style.min.css') !!}
    @append
    @section('js')
        {!! Html::script('assets/js/views/view.operation.js') !!}
        {!! Html::script('assets/jstree/dist/jstree.js') !!}
        @append
        @section('container')
            <div class="contentpanel">
                <div class="panel panel-announcement">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-5 col-md-offset-3">
                                <form action="{{ URL('question-bank/'.$bank->id) }}" method="POST">
                                    <input name="_method" type="hidden" value="PUT">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <br>
                                    <a>{{ trans('bank.title') }}</a>
                                    <input type="text" name="title" class="form-control" required="required" value="{{ $bank->title }}">
                                    {!! errors_for('title', $errors) !!}
                                    <br>
                                    <a>{{ trans('table.range') }}
                                          <a data-toggle="tooltip" title="{{ trans('tips.00045') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
                                    </a>
                                    <br>
                                    <div style="display:inline-block;">
                                        <a  onclick="manageSelect()" class="dm3-btn dm3-btn-medium">{{ trans('button.range') }}</a>
                                    </div>
                                    <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                                    <input type="hidden" id="manage_id"  name="manage_id" value="{{$manageIds}}"/>
                                    {!! errors_for('manage_id', $errors) !!}
                                    <br>
                                    <br>
                                    <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        @endsection
