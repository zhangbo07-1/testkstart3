@extends("exams.partials.template")

@section('container')
    <div class="contentpanel">


        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{$exam->title}}</h2>
            </div>

            
        </div>
        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="row">
                    
                    <div class="col-sm-12 main">
                        <form class="form-inline" action="/exams/{{$exam->exam_id}}/mark">
                            <div class="form-group">
                                {!! Form::select('type', array('1' => trans('exam.not_marked'), '2' => trans('exam.marked')),isset($a['type'])?$a['type']:null,array('class' => 'form-control')) !!}
                                <input type="text"  class="form-control" name="findByUserName" placeholder="{{ trans('table.input_select_user') }}" value="{{isset($a['findByUserName'])?$a['findByUserName']:""}}">
                            </div>
                            <button type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ trans('button.search') }}</button>
                            <a data-toggle="tooltip" data-placement="right"  title="{{ trans('tips.00044') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
                            
                        </form>
                        <div class="table-responsive">
                            <div class="courseTable text-center">
                                <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:100%;">
                                    <table class="table table-striped table-bordered text-center">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('user.title') }}</th>
                                                <th>{{ trans('user.real_name') }}</th>
                                                
                                                <th>{{ trans('question.title') }}</th>
                                                <th>{{ trans('table.operation') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($results as $result)
                                                <tr ng-repeat="trainee in trainees" class="ng-scope">
                                                    <td class="vertical-align-middle">{!! $result->examResult->user->name !!}</td>
                                                    <td class="vertical-align-middle">{!! $result->examResult->user->profile->realname !!}</td>
                                                    <td class="vertical-align-middle">{!! $result->question->title !!}</td>

                                                    <td class="vertical-align-middle">
                                                        @if($result->overed == 0)
                                                            <a href="{{ url('exams/'.$exam->exam_id.'/result/'.$result->id.'/mark') }}"  class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-pencil"></i>&nbsp;{{ trans('button.mark') }}</a>
                                                        @else
                                                            {{trans('exam.marked').'('.$result->score.')'}}
                                                        @endif
                                                    </td>
                                                </tr><!-- end ngRepeat: trainee in trainees -->
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {!! $results->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    
@endsection
