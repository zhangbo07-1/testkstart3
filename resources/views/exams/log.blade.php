@extends('layout.master')

@section('content')
    <br>
    <br>
    <div class="container">
    <h1 class="text-center">{{ trans('report.course') }}</h1>

        <div class="row">
            <div class="col-sm-12 main">
                <div align="right">
                    <a href="/exam-lessons/{{$exam->exam_id}}/info" class="dm3-btn dm3-btn-medium  button-large"><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
                </div>
            </div>
        </div>



        <div class="bar"></div>
        <h4>{{ trans('table.info') }}</h4>
        <div class="row">
            <div class="col-sm-12 main">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td>{{ trans('course.create_time') }}:</td>
                            <td>{{$distribute->created_at}}</td>
                        </tr>
                        @if($examRecord)
                        <tr>
                            <td>{{ trans('table.start_time') }}:</td>
                            <td>{{$examRecord->created_at}}</td>
                        </tr>
                        <tr>
                            <td>{{trans('table.end_time') }}:</td>
                                <td>{{$examRecord->end_at}}</td>
                        </tr>

                        <tr>
                            <td>{{ trans('table.exam_times') }}:</td>
                            <td>{{count($examRecord->results)}}</td>
                        </tr>

                        <tr>
                            <td>{{ trans('exam.status') }}:</td>


                                <td>{{ trans("exam.".$examRecord->status->title) }}</td>

                        </tr>

                        <tr>
                            <td>{{ trans('table.score') }}:</td>

                            <td>{{$examRecord->max_score}}</td>
                        </tr>
                        @else
                        <tr>
                            <td>{{ trans('table.start_time') }}:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>{{trans('table.end_time') }}:</td>
                                <td></td>
                        </tr>

                        <tr>
                            <td>{{ trans('table.exam_times') }}:</td>
                            <td>0</td>
                        </tr>

                        <tr>
                            <td>{{ trans('exam.status') }}:</td>


                                <td>{{ trans('exam.not_start') }}</td>

                        </tr>

                        <tr>
                            <td>{{ trans('table.score') }}:</td>

                            <td></td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
        @include('courses.log_partial')
    </div>
@endsection
