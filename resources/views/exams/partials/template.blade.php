@extends('layout.master')


@section('js')
    {!! Html::script('assets/js/exam.min.js') !!}
@append

@section('content')
    @include('exams.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection