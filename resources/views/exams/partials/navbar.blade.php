<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    <li id="editNav">
                        <a href="/exams/{{$exam->exam_id}}/edit">
                            <i class="fa fa-home"></i><span>{{trans('exam.edit')}}</span></a>
                    </li>

                    <li id="combinateNav">
                        <a href="/exams/{{$exam->exam_id}}/combinate">
                            <i class="fa fa-book"></i><span>{{ trans('exam.combinate') }}</span>
                        </a>
                    </li>
@can('modify',$exam)
                    <li id="teachersNav">
                        <a href="/exams/{{$exam->exam_id}}/teachers">
                            <i class="fa fa-user"></i><span>{{ trans('teacher.assign') }}</span>
                        </a>
                    </li>
@endcan
                    <li id="previewNav">
                        <a href="/exams/{{$exam->exam_id}}/preview">
                            <i class="fa fa-book"></i><span>{{ trans('exam.preview') }}</span>
                        </a>

                    </li>
                    @can('modify',$exam)
                    @if ($exam->if_link)
                        <li id="distributeNav">
                            <a disabled>
                                <i class="fa fa-user gray"></i><span class="gray">{{trans('exam.distribute') }}</span>
                            </a>
                        </li>
                        
                    @else
                        <li id="distributeNav">
                            <a href="/exams/{{$exam->exam_id}}/distribute">
                                <i class="fa fa-user"></i><span>{{trans('exam.distribute') }}</span>
                            </a>
                        </li>
                    @endif
                    @endcan
                    <li id="markNav">
                        <a href="/exams/{{$exam->exam_id}}/mark">
                            <i class="fa fa-user"></i><span>{{ trans('exam.mark') }}</span>
                        </a>
                    </li>
                    @can('delete',$exam)
                    <li id="deleteNav">
                        <a  href="/exams/{{$exam->exam_id}}/delete">
                            <i class="fa fa-trash"></i><span>{{ trans('exam.delete') }}</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>
        </div>
    </div>
</div>
