@extends('layout.master')

@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
@endsection
@section('content')
    <div class="container">
        <br>
        <br>

        <h3>{{$distribution->exam->title}}</h3>

        <div class="row">
            <div class="col-sm-12 main">
                <div style="float: right;">
                    <a class="dm3-btn dm3-btn-medium button-large" style="margin-bottom:1em;" href="/exam-lessons/{{$distribution->id}}/return"  ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
                </div>
                <div style="float: left;">
                    <a class="dm3-btn dm3-btn-medium" href="{{ '/lessons/exam/'.$distribution->id.'/answer' }}">
                        <i class="fa fa-magic"></i>&nbsp;{{ trans('button.start') }}</a>
                    @if($distribution->exam->if_link)
                       
                    @else
                        <a class="dm3-btn dm3-btn-medium" href="/exam-lessons/{{$distribution->exam->exam_id}}/log"> <i class="fa fa-folder-o"></i>&nbsp;{{ trans('button.record') }}</a>
                    @endif
                    
                           
                        
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 main">
                @include('layout.message')

                @if($if_not_marked)
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{trans('error.00069')}}</div>
                @endif
                
                <br>
                {{ trans('exam.description') }}：

                {{$distribution->exam->description}}

                <br>
                <br>


                {{ trans('table.catalog') }}：

                {{$distribution->exam->catalog->name}}

                <br>
                <br>



                {{ trans('table.limit_times') }}：

                {{$distribution->exam->limit_times}}

                <br>
                <br>


                {{ trans('exam.limit_data') }}：
                {{$distribution->exam->exam_time?$distribution->exam->exam_time.trans('table.minute'):trans('table.no_limit')}}

                <br>
                <br>
                {{ trans('exam.pass_score') }}：
                {{$distribution->exam->pass_score}}

                <br>
                <br>
                {{ trans('table.Qnum') }}:
                {{ $Qnum }}


                <br>
            </div>
        </div>
    </div>
@endsection
