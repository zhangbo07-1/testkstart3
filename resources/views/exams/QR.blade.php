@extends('layout.base')

@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
@endsection
@section('content')
    <div class="container">
        <br>
        <br>

        <div class="row">
            <div class="col-sm-12 main">
                <div class="text-center">

                    @if(isset($course))
                        {!! QrCode::size(500)->color(20,157,210)->generate('http://'.$_SERVER['HTTP_HOST'].'/lessons/'.$course->course_id.'/exam/'.$exam->exam_id.'/answer'); !!}
                        
                    @else
                        {!! QrCode::size(500)->color(20,157,210)->generate('http://'.$_SERVER['HTTP_HOST'].'/exam-lessons/'.$exam->exam_id.'/answer'); !!}
                       
                    @endif
                    
             <p>{{ trans('button.exam_QR')}}
                    </p>
                </div>

            </div>
            
        </div>
    </div>
@endsection
