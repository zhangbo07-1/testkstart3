@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
@endsection

@section('content')
    <div class="container">
        <h1 class="text-center">{{ trans('report.exam') }}</h1>

        <h4>{{ trans('exam.title') }}：{{$exam->title}}</h4>

        <div align="right">
            
            
            <a   onclick="getLogUrl()" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.return') }}</a>
        </div>
        <div class="table-responsive courseTable">

            <table class="table table-striped">
                <thead>
                    <th>
                        {{ trans('question.id') }}
                    </th>
                    <th>
                         {{ trans('question.title') }}
                    </th>
                    <th>
                         {{ trans('table.status') }}
                    </th>
                    <th>
                        {{ trans('table.score') }}
                    </th>
                </thead>

                <tbody>

                    @foreach($result->questionResult as $questionResult)
                        <tr>
                            <td><a href="#" onClick="showAnswer({{$result->id}},{{$questionResult->id}})">{{$questionResult->question->question_id}}</a></td>
                            <td>{!! $questionResult->question->title !!}</td>
                            @if($questionResult->question->type == 4)
                                <td>{{ trans('question.essay') }}</td>
                            @elseif($questionResult->if_true == 1)
                                <td><a style="color: #00FF00"><strong>
                                    √
                                </strong></a></td>
                                
                            @else
                                <td><a style="color: #FF0000"><strong>
                                    X
                                </strong></a></td>
                                
                            @endif
                            <td>{{$questionResult->score}}</td>


                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <script type="text/javascript">
         function getLogUrl()
         {
             var url = window.location.pathname;
             var Path = url.split("/");
             url = Path[0]+"/"+Path[1]+"/"+Path[2]+"/"+Path[3];
             window.location.href=url;
         }
        </script>
        
@endsection
