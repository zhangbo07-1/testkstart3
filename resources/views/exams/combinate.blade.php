@extends('exams.partials.template')

@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{$exam->title}}</h2>
            </div>
        </div>

        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="courseTable">
                    <div id="row">
    @include('layout.message')
                        {!! Form::open(array('url'=>'exams/'.$exam->id.'/combinate','method'=>'Post', 'class' => 'form-horizontal')) !!}
                        <div class="col-sm-7 main">
                            <div class="table-responsive">
                                <div class="courseTable text-center">
                                    <table class="table table-striped table-bordered text-center">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('bank.title') }}</th>
                                                <th>{{ trans('table.Qnum') }}</th>
                                                <th>{{ trans('table.select_Qnum') }}</th>
                                                <th>{{ trans('exam.weight') }}
     <i  data-toggle="tooltip" title="{{ trans('tips.00042') }}"><span class="glyphicon glyphicon-question-sign"></span></i>
    </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($exam->combinations as $combination)
                                                <tr>
                                                    <td class="vertical-align-middle">{{$combination->bank->title}}</td>
                                                    <td class="vertical-align-middle">{{count($combination->bank->questions)}}</td>
                                                    <td class="vertical-align-middle">
                                                        <div class="form-inline">
                                                            <div class="input-group">
                                                                <input name="bankSelect{{$combination->bank_id}}"  style="height:36px;line-height:14px; width:80px" type="number" class="form-control" value="{{$combination->numbers}}">
                                                                <div class="input-group-addon">{{ trans('question.unit') }}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="vertical-align-middle">
                                                        <div class="form-inline">
                                                            <div class="input-group">
                                                               <input name="bankWeight{{$combination->bank_id}}" style="height:36px;line-height:14px; width:80px" type="number" class="form-control" type="number" value="{{$combination->weight}}">
                                                                <div class="input-group-addon">%</div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            @endforeach
                                            @foreach($unSelectBanks as $bank)
                                                <tr>
                                                    <td class="vertical-align-middle">{{$bank->title}}</td>
                                                    <td class="vertical-align-middle">{{count($bank->questions)}}</td>
                                                    <td class="vertical-align-middle">
                                                        <div class="form-inline">
                                                            <div class="input-group">
                                                                <input name="bankSelect{{$bank->id}}"  style="height:36px;line-height:14px; width:80px" type="number" class="form-control" value="0">
                                                                <div class="input-group-addon">{{ trans('question.unit') }}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="vertical-align-middle">
                                                        <div class="form-inline">
                                                            <div class="input-group">
                                                                <input name="bankWeight{{$bank->id}}" style="height:36px;line-height:14px; width:80px" type="number" class="form-control" type="number" value="0">
                                                                <div class="input-group-addon">%</div>
                                                            </div>
                                                        </div>
                                                        
                                                    </td>
                                                </tr>
                                            @endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            <button class="dm3-btn dm3-btn-medium button-large" type="submit">
                                <i class="fa fa-check"></i>&nbsp;{{ trans('button.submit') }}
                            </button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    


@endsection
