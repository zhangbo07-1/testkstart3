@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>

            <h3>{{ trans('exam.manage') }}</h3>
            <div class="row">
                <div class="col-sm-12 main">
                    <form class="form-inline" action="/exams">
                        <div style="display:inline-block;">

                            <a  onclick="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                        </div>
                        <div style="display:inline-block;" id="catalog_name">{{isset($catalogs)?$catalogs:""}}</div>
                         <input type="hidden" id="catalog_id"  name="catalog_id" value="{{isset($a['catalog_id'])?$a['catalog_id']:""}}"/>
                        <br>
                        <div style="float: left;">

                            <input type="text" name="findByExamName" class="form-control" placeholder="{{ trans('table.input_select_exam') }}"  value="{{isset($a['findByExamName'])?$a['findByExamName']:""}}">
                            <button type="submit" class="dm3-btn dm3-btn-medium button-large">
                                <i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}
                            </button>
                        </div>

                        <div style="float: right;">
                            <a href="{{URL('exams/create')}}" class="dm3-btn dm3-btn-medium button-large">
                                <i class="fa fa-plus"></i>&nbsp;{{ trans('exam.create') }}</a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 main">
                    <div class="courseTable text-center">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>{{ trans('exam.title') }}</th>
                                    <th>{{ trans('exam.description') }}</th>
                                    <th>{{ trans('table.operation') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($exams as $exam)
                                    <tr>

                                        <td class="vertical-align-middle">{{ $exam->title }}</td>
                                        <td class="vertical-align-middle">{{ $exam->description }}</td>
                                        <td class="vertical-align-middle">
                                            <a href="{{ url('/exams/'.$exam->exam_id.'/edit') }}"
                                               class="dm3-btn dm3-btn-medium button-large"> <i class="fa fa-plus"></i>&nbsp;{{ trans('button.edit') }}</a>


                                            @if($exam->if_link)
                                                 <a class="dm3-btn dm3-btn-gray button-large" disabled >
                                                        <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam_QR') }}</a>

                                            @else

                                                <a class="dm3-btn dm3-btn-medium button-large" href="/exam/{{$exam->exam_id}}/QR-code" target="_blank">
                                                    <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam_QR') }}</a>
                                            @endif
                                        </td>
                                    </tr><!-- end ngRepeat: trainee in trainees -->
                                @endforeach
                            </tbody>
                        </table>
                        {!! $exams->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
                    </div>
                </div>
            </div>

        </div>
    @endsection
