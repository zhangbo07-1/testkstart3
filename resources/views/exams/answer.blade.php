@extends('layout.base')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            {{ trans('question.title') }}:
                <br>
{!!$result->question->title!!}
            <br>
            @if($result->question->type == 4)
                {{ trans('question.answer') }}:{!! $result->answer !!}
            @else
                <table class="no-border">

                    
                    
                    @foreach($result->question->selects as $index=>$select)

                        <tr>
                            <td>
                                
                                @if($select->if_true == 1)
                                    <a style="color: #00FF00"><strong>
                                        √
                                    </strong></a>

                                @else
                                    <a style="color: #FF0000"><strong>
                                        X
                                    </strong></a>                                                            
                                @endif
                                @if($result->question->type == 2)
                                    <input type="checkbox"  {{in_array($select->id,$selectResult) ? "checked='checked'" : ''}} />
                                @else
                                    <input type="radio" {{in_array($select->id,$selectResult) ? "checked='checked'" : ''}}/>
                                @endif
                                
                                {{$select->title}}"
                            </td>
                            
                        </tr>

                    @endforeach
                </table>
            @endif
            <br>
            <br>
            <div align="center">
                {{--
                                                                                                       <a class="dm3-btn dm3-btn-medium  button-large"  href="/exam/{{$examResult->id}}/answer/{{$result->id}}/Forward">{{ trans('button.forward_question') }}</a>
                                                                                                       --}}
                <a class="dm3-btn dm3-btn-medium dm3-btn-red button-large"  onclick="closeWindow()">{{ trans('button.close') }}</a>
                {{--
                <a class="dm3-btn dm3-btn-medium  button-large"  href="/exam/{{$examResult->id}}/answer/{{$result->id}}/Next">{{ trans('button.next_question') }}</a>
                --}}
            </div>
            <br>
        </div>

    @endsection
