@extends('layout.master')
    @section('js')

        {!! Html::script('assets/js/views/view.operation.js') !!}
        @append
@section('content')


    <div class="container">
        <br>
        <br>
        <h3>开始答题</h3>
        <br>
        <div align="right">
            开始倒计时
            <span id="endtime">{{$distribution->exam->exam_time*60}}</span>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel">

                    <div class="panel-body">
                        {!! errors_for('error', $errors) !!}
                        

                        <form id="ExamForm" action="{{ URL('/exams/'.$distribution->id.'/score') }}" method="POST">
                            
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="lists" value="{{ $list }}">
                            
                            @foreach($questions as $index=>$question)
                                <div style="border-top: solid 20px #efefef;">
                                    <h5 style="color: #337ab7;">{{$index+1}}:
                                    </h5>
                                    <h5 style="color: #337ab7;">
                                        {!!$question->title!!}</h5>
                                   @include('questions.select_show')

                                </div>

                            @endforeach
                            
                            
    <button class="dm3-btn dm3-btn-medium button-large" onClick="return checkResult({{count($questions)}})">{{ trans('button.submit') }}</button>
    <a  onclick="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

     var CID = "endtime";
     if(window.CID != null )
     {
         var iTime = document.getElementById(CID).innerText;
         if(iTime != 0)
         {
             var Account;
             setTimeout(function(){$('#ExamForm').submit();},(parseInt(iTime)+1)*1000);
             RemainTime();
         }
     }
     function RemainTime()
     {
         var iHour,iMinute,iSecond;
         var sHour="",sMinute="",sSecond="",sTime="";
         if (iTime >= 0)
         {
             iHour = parseInt((iTime/3600));
             if (iHour > 0)
             {
                 sHour = iHour + "小时";
             }
             iMinute = parseInt((iTime%3600)/60);
             if (iMinute > 0)
             {
                 sMinute = iMinute + "分钟";
             }
             iSecond = parseInt(iTime%60);
             if (iSecond >= 0)
             {
                 sSecond = iSecond + "秒";
             }
             if (sHour=="")
             {
                 sTime="<span style='color:darkorange'>" + sMinute+sSecond + "</font>";
             }
             else
             {
                 sTime=sHour+sMinute+sSecond;
             }
             if(iTime==0)
             {
                 clearTimeout(Account);
                 sTime="<span style='color:green'>考试时间结束！</span>";
             }
             else
             {
                 Account = setTimeout("RemainTime()",1000);
             }
             iTime=iTime-1;
         }
         else
         {
             sTime="<span style='color:red'>考试时间结束！</span>";
         }
         document.getElementById(CID).innerHTML = sTime;
         
     }
    </script>



    
@endsection

