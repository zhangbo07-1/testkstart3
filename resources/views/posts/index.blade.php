@extends('layout.master')
@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
@endsection
    @section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
@endsection
@section('content')
    <section class="section-content">
        <div class="container clearfix">
@if(isset($course))
 <div align="right">
            <a class="dm3-btn dm3-btn-medium button-large" href="/lessons/{{$course->course_id}}/info" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
        </div>
@endif
            <div class="posts-grid posts-grid-3 clearfix">
                <div class="posts-grid posts-grid-3 clearfix">

                    @foreach ($posts as $post)

                        <article class="post-grid ib_educator_course type-ib_educator_course status-publish has-post-thumbnail hentry ib_educator_category-educator">
                            <div class="post-thumb">
                                <img  src="{!!get_url()!!}/posts/{{$post->id}}/title.jpg" style="width:360px;height:224px;"
                                      class="attachment-ib-educator-grid wp-post-image" alt="Business Process"
                                      srcset="{!!get_url()!!}/posts/{{$post->id}}/title.jpg 360w, {!!get_url()!!}/posts/{{$post->id}}/title.jpg 720w"
                                      sizes="(min-width: 768px) 360px, 96vw"/>
                            </div>

                            <div class="post-body">
                                <h2 class="entry-title">
                                    @if(isset($course))
                                        <a href="{{URL("/lessons/".$course->course_id."/posts/".$post->id)}}" rel="bookmark">{{ $post->title }}</a>
                                    @else
                                        <a href="{{URL("/posts/".$post->id)}}" rel="bookmark">{{ $post->title }}</a>
                                    @endif
                                </h2>

                                <div class="content">{{ $post->summary }}</div>

                            </div>

                            <footer class="post-meta">
                                <span class="author vcard" style="display: inline">
                                    <a class="fn n" rel="author">
                                        {{ $post->user->name }}</a>
                                </span>
                                <span class="post-date" style="display: inline">
                                    <time class="entry-date" datetime="2014-10-21T07:07:50+00:00">{{ $post->updated_at }}</time>
                                </span>
                                <span class="comments-link"><a>{{count($post->Comments)}}</a></span>
                            </footer>
                        </article>
                    @endforeach
                </div>
                @can('create_post',Auth::user())
                <div align="right">
                    @if(isset($course))
                        <a href="{{URL('lessons/'.$course->course_id.'/posts/create')}}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-plus"></i>&nbsp;{{ trans('post.create') }}</a>
                    @else

                        
                        <a href="{{URL('posts/create')}}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-plus"></i>&nbsp;{{ trans('post.create') }}</a>
                        
                    @endif
                </div>
                @endcan

                {!! $posts->render() !!}
            </div>
        </div>


    </section>

@endsection
