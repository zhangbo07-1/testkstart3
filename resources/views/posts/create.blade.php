@extends('layout.master')
@section('content')

    @include('UMEditor.head')

    <div class="container">
        <br>
        <br>
        <h3>{{ trans('post.create') }}</h3>
        <div class="row">
            <div class="panel">

                <div class="panel-body">


                    <h5 style="color: #337ab7;">{{ trans('table.title') }}</h5>
                    {!! Form::open(array('url'=>'posts','method'=>'POST','files'=>true,'id'=>'need_submit'))!!}
     <input type="hidden" name="area_id" value="{{isset($course)? $course->area_id:1}}">
     <input type="hidden" name="course_id" value="{{isset($course)? $course->course_id:''}}">
                    <input type="text" name="title" class="form-control" required="required">
                    {!! errors_for('title', $errors) !!}
                    <br>
                    <h5 style="color: #337ab7;">{{ trans('post.select_image') }}</h5>
<div class="form-inline"> 
                     <input type="button" value="{{ trans('button.file') }}" onclick="document.getElementById('upfile').click();" class="dm3-btn dm3-btn-medium button-large"/>
                            <div style="display:inline-block;">
                                <input type="text" name="url" class="form-control" id="fileURL">
                            </div>
                            <input type="file" name="image" id="upfile" onchange="document.getElementById('fileURL').value=this.value;" style="display:none">
                    {!! errors_for('image', $errors) !!}
    </div>
                    <h5 style="color: #337ab7;">{{ trans('post.summary') }}</h5>
                    <textarea name="summary" rows="1" class="form-control" required="required"></textarea>
                    {!! errors_for('summary', $errors) !!}

                    <br>
                    <h5 style="color: #337ab7;">{{ trans('table.text') }}</h5>
                    {!! errors_for('body', $errors) !!}
                    <textarea name="body" class="form-control" style="height:240px;" required="required" id='myEditor'></textarea>
                    <br>
                    <button id="need_disable" class="dm3-btn dm3-btn-medium button-large" onClick="return checkinput()">{{ trans('button.submit') }}</button>
                    <a  onclick="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    <script>
     function checkinput(e)
     {

         if(!UM.getEditor('myEditor').hasContents())
         {
             alert("{{trans('error.00001')}}");
             return false;

         }
         $("#need_disable").attr("disabled", "true");
         $('#need_submit').submit();
         return true;
     }
    </script>
@endsection
