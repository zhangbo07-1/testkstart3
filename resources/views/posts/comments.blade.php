<div class="conmments" style="margin-top: 10px;">
    @include('UMEditor.head')
    @foreach ($post->Comments as $comment)
        <div class="one" style="border-top: solid 20px #efefef; padding: 5px 20px;">
            <div class="content">
                <p style="padding: 20px;">
                    {!! $comment->body !!}
                </p>
            </div>


            <div class="reply" style="text-align: right; padding: 5px;">
                @can('modify',$comment)
                    <form action="{{ URL('/comments/'.$comment->id) }}" method="POST" style="display: inline;">
                        <input name="_method" type="hidden" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" onClick="return ifDelete()" class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.delete') }}</button>
                    </form>
                @endcan
                <footer class="post-meta">
                    <span class="author vcard" style="display: inline">
                        <a class="fn n" rel="author">
                            {{ $comment->user->name }}</a>
                    </span>
                    <span class="post-date" style="display: inline">
                        <time class="entry-date" datetime="2014-10-21T07:07:50+00:00">{{ $comment->updated_at }}</time>
                    </span>

                </footer>


            </div>
        </div>
    @endforeach
</div>
@can('create_comment',Auth::user())
<div id="comments" style="margin-bottom: 100px;">
    <div id="respond" class="comment-respond">
        <h3 id="reply-title" class="comment-reply-title">{{ trans('post.comment') }}<small><a rel="nofollow" id="cancel-comment-reply-link" href="/post-with-floating-images/#respond" style="display:none;">Cancel reply</a></small></h3>
        <form action="{{ URL('/comments') }}" method="post" id="commentform" class="comment-form" novalidate="">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="post_id" value="{{ $post->id }}">
            <textarea id='myEditor' name="body" class="form-control"style="height:240px;" required="required"></textarea>
            <br>
            <input name="submit" type="submit" class="dm3-btn dm3-btn-medium button-large" value="{{ trans('button.submit') }}">
        </form>
    </div><!-- #respond -->
    @endcan
</div>
