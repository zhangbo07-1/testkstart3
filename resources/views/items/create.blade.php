@extends($survey->type == 1?"survey.partials.template":($survey->type == 2?"evaluation.partials.template":"course_survey.partials.template"))
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('question.create') }}</h2>
                </div>
            </div>

            <div class="panel panel-announcement">
                <div class="panel-body">
                    <div class="col-md-10 col-md-offset-1">

                        <a>{{ trans('question.title') }}</a>
                        <form action="{{ URL(($survey->type == 1?'survey/':($survey->type == 2?'evaluation/':'course-survey/')).$survey->id.'/items/') }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <textarea name="title" rows="5" class="form-control" required="required"></textarea>
                            <div id="listNUM">
                                <input type="hidden" id="selectNUM" name="selectNUM"  value="2">
                            </div>
                            <input type="radio" checked="checked" name="Qtype" onClick="changeGrade()" value="grade" />
                            {{ trans('question.grade') }}：
                            <br>
                            <input type="radio" name="Qtype" onClick="changeSelect()" value="select" />
                            {{ trans('question.select') }}：

                            <div id="selectDiv" style="display:none" >
                                @include('items.select_create')
                            </div>
                            <div id="gradeDiv">
                                @include('items.grade_create')
                            </div>
                            <br>
                            <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                            <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                        </form>

                    </div>

                </div>
            </div>
        </div>
        <script>
         var select_title = "{{trans('question.select_title')}}";
         var delete_log = "{{trans('button.delete')}}";
         var add_max = "{{trans('error.00065')}}";
         var delete_limit = "{{trans('error.00066')}}";
        </script>
    @endsection
