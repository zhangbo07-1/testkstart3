@extends('reports.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('report.exam') }}</h2>
                </div>
            </div>
            <div class="panel panel-announcement">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 main">
                            <form class="form-inline" action="/reports/exam">
                                <input type="hidden" id="catalog_id"  name="catalog_id" value="{{isset($a['catalog_id'])?$a['catalog_id']:""}}"/>
                                <a  onclick="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-file"></i>&nbsp;{{ trans('button.catalog') }}</a>
                                <div style="display:inline-block;" id="catalog_name">{{isset($catalogs)?$catalogs:""}}</div>
                                <br>
                                <input type="hidden" id="manage_id" class="form-control"  name="manage_id" value="{{isset($a['manage_id'])?$a['manage_id']:""}}"/>
                                <a  onclick="javascript:manageSelect();" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-user"></i>&nbsp;{{ trans('button.department') }}</a>
                                <div style="display:inline-block;" id="manage_name">{{isset($manages)?$manages:""}}</div>
                                <br>
                                
                                

                                {!! Form::select('attribute1', $firstAttributeSelects,isset($a['attribute1'])?$a['attribute1']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute2', $secondAttributeSelects,isset($a['attribute2'])?$a['attribute2']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute3', $thirdAttributeSelects,isset($a['attribute3'])?$a['attribute3']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute4', $fourthAttributeSelects,isset($a['attribute4'])?$a['attribute4']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute5', $fifthAttributeSelects,isset($a['attribute5'])?$a['attribute5']:null,['class'=>"form-control"]) !!}
                                
                                <br>
                                <div style="float: left;">


                                    

                                    {!! Form::select('userStatus', $userStatusSelects, isset($a['userStatus'])?$a['userStatus']:$status,['class'=>"form-control"]) !!}
                                    <div class="form-group">
                                        <input type="text"  class="form-control" name="findByExamName" placeholder="{{ trans('table.input_select_exam') }}" value="{{isset($a['findByExamName'])?$a['findByExamName']:""}}">
                                    </div>

                                    <div class="form-group">
                                        <input type="text"  class="form-control" name="findByUsername" placeholder="{{ trans('table.input_select_user') }}" value="{{isset($a['findByUsername'])?$a['findByUsername']:""}}">
                                    </div>
                                    <button type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ trans('button.search') }}</button>
                                </div>
                            </form>
                            <div style="float: right;">
                                <form  class="form-inline" action="/reports/exam/excel">
                                    <input type="hidden"  name="select" value="{{isset($a)?json_encode($a):""}}"/>
                                    <button type="submit" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.excel') }}</button>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12 main">
                            <div class="table-responsive">
                                <div class="courseTable text-center">
                                    <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:7000px;">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>{{ trans('user.real_name') }}</th>
                                                    <th>{{ trans('table.user_name') }}</th>
                                                    <th>{{ trans('table.email') }}</th>
                                                    <th>{{ trans('user.department') }}</th>
                                                    <th>{{ $attribute->first_title}}</th>
                                                    <th>{{ $attribute->second_title}}</th>
                                                    <th>{{ $attribute->third_title}}</th>
                                                    <th>{{ $attribute->fourth_title}}</th>
                                                    <th>{{ $attribute->fifth_title}}</th>

                                                    <th>{{ trans('table.status')}}</th>
                                                    <th>{{ trans('exam.title') }}</th>
                                                    <th>{{ trans('table.catalog') }}</th>
                                                    <th>{{ trans('exam.status') }}</th>
                                                    <th>{{ trans('table.create_time') }}</th>
                                                    <th>{{ trans('table.start_time') }}</th>
                                                    <th>{{ trans('table.end_time') }}</th>
                                                    <th>{{ trans('table.exam_times') }}</th>
                                                    <th>{{ trans('table.exam_time') }}</th>
                                                    <th>{{ trans('table.exam_score') }}</th>
                                                    <th>{{ trans('question.id') }}</th>
                                                    <th>{{ trans('question.title') }}</th>
                                                    <th>{{ trans('bank.name') }}</th>
                                                    <th>{{ trans('question.true_select') }}</th>
                                                    <th>{{ trans('question.answer') }}</th>
                                                    <th>{{ trans('question.if_true') }}</th>
                                                    <th>{{ trans('question.select_title') }}1</th>
                                                    <th>{{ trans('question.select_title') }}2</th>
                                                    <th>{{ trans('question.select_title') }}3</th>
                                                    <th>{{ trans('question.select_title') }}4</th>
                                                    <th>{{ trans('question.select_title') }}5</th>
                                                    <th>{{ trans('question.select_title') }}6</th>
                                                    <th>{{ trans('question.select_title') }}7</th>
                                                    <th>{{ trans('question.select_title') }}8</th>
                                                    <th>{{ trans('question.select_title') }}9</th>
                                                    <th>{{ trans('question.select_title') }}10</th>

                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach ($records as $record)
                                                    <tr>
                                                        <td class="vertical-align-middle">{{ $record->user->profile->realname }}</td>
                                                        <td class="vertical-align-middle">{{  $record->user->name }}</td>
                                                        <td class="vertical-align-middle">{{  $record->user->email }}</td>
                                                        <td class="vertical-align-middle">{{ $record->user->department->name }}</td>
                                                        <td class="vertical-align-middle">{{ $record->user->profile->attribute1?$record->model->user->profile->attribute1->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $record->user->profile->attribute2?$record->model->user->profile->attribute2->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $record->user->profile->attribute3?$record->model->user->profile->attribute3->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $record->user->profile->attribute4?$record->model->user->profile->attribute4->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $record->user->profile->attribute5?$record->model->user->profile->attribute5->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $record->user->userStatus->title }}</td>
                                                        <td class="vertical-align-middle">{{ $record->distribution->exam->title }}</td>
                                                        <td class="vertical-align-middle">{{ $record->distribution->exam->catalog->name }}</td>
                                                        <td class="vertical-align-middle"> {{ trans("exam.".$record->status->title) }}</td>
                                                        <td class="vertical-align-middle">{{  $record->distribution->created_at }}</td>
                                                        <td class="vertical-align-middle">{{  $record->created_at }}</td>
                                                        <td class="vertical-align-middle">{{  $record->end_at }}</td>
                                                        <td class="vertical-align-middle">{{  count($record->results) }}</td>
                                                        <td class="vertical-align-middle">{{  isset($examResults[$record->id])?$examResults[$record->id]->created_at:"" }}</td>
                                                        <td class="vertical-align-middle">{{  $record->max_score }}</td>
                                                        @if(isset($examResults[$record->id]))
                                                            @foreach($examResults[$record->id]->questionResult as $index=>$result)
                                                                @if($index > 0)
                                                                    <tr>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                        <td class="vertical-align-middle"></td>
                                                                @endif

                                                                <td class="vertical-align-middle">{{  $result->question->question_id }}</td>
                                                                <td class="vertical-align-middle">{!!  $result->question->title !!}</td>
                                                                <td class="vertical-align-middle">{{  $result->question->bank->title }}</td>
                                                                <td class="vertical-align-middle">{{  $trueSelect[$result->question->id] }}</td>
                                                                <td class="vertical-align-middle">{{  isset($selectResults[$result->id])?$selectResults[$result->id]:"" }} </td>
                                                                @if($result->question->type < 4)
                                                                    <td class="vertical-align-middle">{{  $result->if_true == 1?trans('table.yes'):trans('table.no') }} </td>
                                                                    @foreach($questionSelects[$result->question->id] as $select)
                                                                        <td class="vertical-align-middle">{{  $select}} </td>

                                                                    @endforeach
                                                                    
                                                                @else
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    
                                                                    
                                                                @endif
                                                                    
                                                                    
                                                            @endforeach
                                                            </tr>
                                                        @else
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle"></td>
                                                            <tr>
                                                        @endif
                                                        
                                                @endforeach
                                                
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $records->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    @endsection
