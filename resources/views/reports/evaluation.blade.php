@extends('reports.partials.template')

@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
    <h2 class="panel-title">{{ trans('report.evaluation') }}</h2>
            </div>
        </div>
        <div class="panel panel-announcement">
            <div class="panel-body">
                    <form class="form-inline" action="/reports/evaluation">
                         <div class="form-group">
                             <input type="text" name="findByName" placeholder="{{ trans('table.input_select') }}" value="{{isset($a['findByName'])?$a['findByName']:""}}">
                             </div>
                             <button type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ trans('button.search') }}</button>
                    </form>
                <div class="row">
                    <div class="col-sm-12 main">
                        <div class="table-responsive">
                            <div class="courseTable text-center">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                                                                                                                                   <th>{{ trans('evaluation.name') }}</th>
<th>{{ trans('table.Qnum') }}</th>
<th>{{ trans('table.answer_number') }}</th>
<th>{{ trans('table.operation') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($evaluationRecords as $record)
                                           
                                                <tr>
                                                    <td class="vertical-align-middle">{{$record->title}}</td>
                                                    <td class="vertical-align-middle">
                                                        {{count($record->survey->items)}}
                                                    </td>
                                                    <td class="vertical-align-middle">
                                                        {{count($record->surveyResults)}}
                                                    </td>
                                                   
                                                    <td class="vertical-align-middle">
                                                        @if(count($record->surveyResults))
                                                                                                                                                   <a href="{{ url('reports/evaluation/'.$record->id.'/result') }}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-question-circle"></i>&nbsp;{{ trans('table.result') }}</a>
                                                        @else
                                                            <a disabled="disabled" class="dm3-btn dm3-btn-medium dm3-btn-gray  button-large"><i class="fa fa-question-circle"></i>&nbsp;{{ trans('table.result') }}</a>
                                                        @endif
                                                        
                                                    </td>
                                                </tr>
                                             
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
 {!! $evaluationRecords->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
