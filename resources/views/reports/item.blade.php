@extends(isset($course)?"courses.partials.template":"reports.partials.template")

@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{$record->title}}</h2>
            </div>
        </div>
        <div class="panel panel-announcement">
            <div class="panel-body">

                @if($result['resultNumber'])

                    @if(isset($course))
                        <form class="form-inline" action="result">
                            <div style="float: left;">
                                {!! Form::select('class_id', $classes, isset($a['class_id'])?$a['class_id']:null,['class'=>"form-control"]) !!}
                                
                                <button type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ trans('button.search') }}</button>
                            </div>
                        </form>

                    @endif
                    
                    <div class="clearfix" align="right">
                         <form  class="form-inline" action="/reports/evaluation/{{$record->id}}/result/excel">
                                    <input type="hidden"  name="select" value="{{isset($a)?json_encode($a):""}}"/>
                                    <button type="submit" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.excel') }}</button>
                                    <a class="dm3-btn dm3-btn-medium button-large"  onclick="javascript:history.back(-1)" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
                                </form>
                       
                        
                        
                    </div>


                    <br>
                    <div class="row">
                        <div class="col-sm-12 main">
                            <div class="table-responsive">
                                <div class="courseTable text-center">
                                    <div name="grade">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>{{trans('evaluation.1')}}</th>
                                                    <th>{{trans('evaluation.2')}}</th>
                                                    <th>{{trans('evaluation.3')}}</th>
                                                    <th>{{trans('evaluation.4')}}</th>
                                                    <th>{{trans('evaluation.5')}}</th>
                                                    <th>{{trans('table.avg')}}</th>
                                                    <th>{{trans('table.answer_number')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($record->survey->items as $item)
                                                    @if($item->type ==2)
                                                        
                                                        <tr>
                                                            <td class="vertical-align-middle">{{$item->title}}</td>
                                                            @for($j = 1;$j<=5;$j++)
                                                                <td class="vertical-align-middle">
                                                                    {{ round($result['number'][$item->id][$j]/$result['resultNumber']*100,2)}}%
                                                                </td>

                                                            @endfor
                                                            <td class="vertical-align-middle">{{$result['avg'][$item->id]}}</td>
                                                            <td class="vertical-align-middle">
                                                                {{$result['resultNumber']}}
                                                            </td>
                                                        </tr>
                                                        
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div name="select">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>{{ trans('question.mulit')}}</th>
                                                    <th></th>
                                                    <th>{{trans('table.answer_number')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($record->survey->items as $item)
                                                    @if($item->type == 1)
                                                        <tr>
                                                            <td class="vertical-align-middle">{{$item->title}}</td>
                                                            <td class="vertical-align-middle"></td>
                                                            <td class="vertical-align-middle">
                                                                {{$result['resultNumber']}}
                                                            </td>
                                                        </tr>
                                                        @foreach($item->selects as $select)
                                                            <tr>
                                                                <td class="vertical-align-middle">{{$select->title}}</td>
                                                                <td class="vertical-align-middle">
                                                                    {{ round($result['number'][$item->id][$select->id]/$result['resultNumber']*100,2)}}%
                                                                </td>
                                                                <td class="vertical-align-middle"></td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="clearfix" align="right">                   
                        
                        <a class="dm3-btn dm3-btn-medium button-large"  onclick="javascript:history.back(-1)" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
                        
                    </div>


                    <br>
                    {{ trans('table.no_answer') }}
                @endif
                
            </div>
        </div>
    </div>
    
@endsection
