<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <nav>
                    <ul class="nav nav-pills nav-stacked nav-quirk">
                        <li id="courseNav">
                            <a href="/reports/course"><i class="fa fa-home"></i><span>{{ trans('report.course') }}</span></a>
                        </li>

                        <li id="examNav">
                            <a href="/reports/exam"><i class="fa fa-home"></i><span>{{ trans('report.exam') }}</span></a>
                        </li>
                        @can('manage_user',Auth::user())
                        <li id="userNav">
                            <a href="/reports/user"><i class="fa fa-home"></i><span>{{ trans('report.user') }}</span></a>
                        </li>
                        @endcan

                        <li id="questionNav">
                            <a href="/reports/question"><i class="fa fa-home"></i><span>{{ trans('report.question') }}</span></a>
                        </li>
                        <li id="evaluationNav">
                            <a href="/reports/evaluation"><i class="fa fa-home"></i><span>{{ trans('report.evaluation') }}</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
