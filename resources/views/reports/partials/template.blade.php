@extends('layout.master')

@section('js')
    {!! Html::script('assets/js/report.min.js') !!}
@append

@section('content')
    @include('reports.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection