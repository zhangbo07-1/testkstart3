@extends('reports.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('user.report') }}</h2>
                </div>
            </div>
            <div class="panel panel-announcement">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 main">

                            <form class="form-inline" action="/reports/user">
                                <input type="hidden" id="manage_id" class="form-control"  name="manage_id" value="{{isset($a['manage_id'])?$a['manage_id']:""}}"/>
                                <a  onclick="manageSelect();" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-user"></i>&nbsp;{{ trans('button.department') }}</a>
                                <div style="display:inline-block;" id="manage_name">{{isset($manages)?$manages:""}}</div>
                                <br>

                                {!! Form::select('attribute1', $firstAttributeSelects,isset($a['attribute1'])?$a['attribute1']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute2', $secondAttributeSelects,isset($a['attribute2'])?$a['attribute2']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute3', $thirdAttributeSelects,isset($a['attribute3'])?$a['attribute3']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute4', $fourthAttributeSelects,isset($a['attribute4'])?$a['attribute4']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute5', $fifthAttributeSelects,isset($a['attribute5'])?$a['attribute5']:null,['class'=>"form-control"]) !!}
                                
                                <br>
                                <div style="float: left;">

                                    {!! Form::select('userStatus', $userStatusSelects,isset($a['userStatus'])?$a['userStatus']:$status,['class'=>"form-control"]) !!}

                                    <div class="form-group">
                                        <input type="text"  class="form-control" name="findByUsername" placeholder="{{ trans('table.input_select_user') }}" value="{{isset($a['findByUsername'])?$a['findByUsername']:""}}">
                                    </div>
                                    <button type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ trans('button.search') }}</button>
                                </div>
                            </form>
                            <div style="float: right;">
                                <form  class="form-inline" action="/reports/user/excel">
                                    <input type="hidden"  name="select" value="{{isset($a)?json_encode($a):""}}"/>
                                    <button type="submit" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.excel') }}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 main">
                            <div class="table-responsive">
                                <div class="courseTable text-center">
                                    <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:300%;">
                                        <table id="table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>{{ trans('table.user_name') }}</th>
                                                    <th>{{ trans('table.email') }}</th>
                                                    <th>{{ trans('user.real_name') }}</th>
                                                    <th>{{ trans('user.role') }}</th>
                                                    
                                                    <th>{{ trans('table.status') }}</th>
                                                    <th>{{ trans('user.department') }}</th>
                                                    <th>{{ trans('user.approve') }}</th>
                                                    <th>{{ trans('user.approve_name') }}</th>
                                                    <th>{{ $attribute->first_title}}</th>
                                                    <th>{{ $attribute->second_title}}</th>
                                                    <th>{{ $attribute->third_title}}</th>
                                                    <th>{{ $attribute->fourth_title}}</th>
                                                    <th>{{ $attribute->fifth_title}}</th>
                                                    <th>{{ trans('user.sex')}}</th>

                                                    <th>{{ trans('user.tel')}}</th>
                                                     <th>{{ trans('user.address')}}</th>
                                                    
                                                    <th>{{ trans('table.create_time') }}</th>
                                                    <th>{{ trans('user.login_last') }}</th>
                                                    <th>{{ trans('user.login_times') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($users as $user)
                                                    <tr>
                                                        <td class="vertical-align-middle">{{ $user->name }}</td>
                                                        <td class="vertical-align-middle">{{ $user->email }}</td>
                                                        <td class="vertical-align-middle">{{ $user->profile->realname }}</td>
                                                        <td class="vertical-align-middle">{{$user->role->name}}</td>
                                                        
                                                        <td class="vertical-align-middle">{{$user->userStatus->title}}</td>
                                                        <td class="vertical-align-middle">{{ $user->department->name }}</td>
                                                        <td class="vertical-align-middle">{{ $user->approve?$user->approve->name:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $user->approve?$user->approve->profile->realname:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $user->profile->attribute1?$user->profile->attribute1->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $user->profile->attribute2?$user->profile->attribute2->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $user->profile->attribute3?$user->profile->attribute3->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $user->profile->attribute4?$user->profile->attribute4->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $user->profile->attribute5?$user->profile->attribute5->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $user->profile->sex?trans($user->profile->sex->title):"" }}</td>
                                                        <td class="vertical-align-middle">{{ $user->profile->tel }}</td>
                                                         <td class="vertical-align-middle">{{ $user->profile->address }}</td>
                                                        <td class="vertical-align-middle">{{$user->created_at}}</td>
                                                            <td class="vertical-align-middle">{{ $user->loginMessage?$user->loginMessage->updated_at:"" }}</td>
                                                            <td class="vertical-align-middle">{{ $user->loginMessage?$user->loginMessage->login_times:"" }}</td>
                                                    </tr><!-- end ngRepeat: trainee in trainees -->
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                                {!! $users->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endsection
