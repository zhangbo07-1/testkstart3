@extends('reports.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    {!! Html::script('assets/laydate/laydate.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('report.course') }}</h2>
                </div>
            </div>
            <div class="panel panel-announcement">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 main">
                            <form class="form-inline" action="/reports/course">
                                <input type="hidden" id="catalog_id"  name="catalog_id" value="{{isset($a['catalog_id'])?$a['catalog_id']:""}}"/>
                                <a  onclick="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-file"></i>&nbsp;{{ trans('button.catalog') }}</a>
                                <div style="display:inline-block;" id="catalog_name">{{isset($catalogs)?$catalogs:""}}</div>
                                <br>
                                <input type="hidden" id="manage_id" class="form-control"  name="manage_id" value="{{isset($a['manage_id'])?$a['manage_id']:""}}"/>
                                <a  onclick="javascript:manageSelect();" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-user"></i>&nbsp;{{ trans('button.department') }}</a>
                                <div style="display:inline-block;" id="manage_name">{{isset($manages)?$manages:""}}</div>
                                <br>
                               
                                {!!Form::select('dateLimit', array('0' => trans('table.no_limit'), '1' => trans('table.when_start'), '2'=>trans('table.when_end')),isset($a['dateLimit'])?$a['dateLimit']:null,['class'=>"form-control"])!!}
                                :
                                <input onclick="laydate()" name="startDate" class="form-control" placeholder="{{trans('table.select_time')}}" readonly="readonly" value="{{isset($a['startDate'])?$a['startDate']:""}}" style="width:110px;">
                                ~
                                <input onclick="laydate()" name="endDate"  class="form-control" placeholder="{{trans('table.select_time')}}" readonly="readonly" value="{{isset($a['endDate'])?$a['endDate']:""}}" style="width:110px;">
                                <br>

                                {!! Form::select('attribute1', $firstAttributeSelects,isset($a['attribute1'])?$a['attribute1']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute2', $secondAttributeSelects,isset($a['attribute2'])?$a['attribute2']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute3', $thirdAttributeSelects,isset($a['attribute3'])?$a['attribute3']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute4', $fourthAttributeSelects,isset($a['attribute4'])?$a['attribute4']:null,['class'=>"form-control"]) !!}
                                {!! Form::select('attribute5', $fifthAttributeSelects,isset($a['attribute5'])?$a['attribute5']:null,['class'=>"form-control"]) !!}
                                
                                <br>
                                <div style="float: left;">


                                    {!! Form::select('type', $types, isset($a['type'])?$a['type']:null,['class'=>"form-control"]) !!}

                                    {!! Form::select('userStatus', $userStatusSelects, isset($a['userStatus'])?$a['userStatus']:$status,['class'=>"form-control"]) !!}
                                    <div class="form-group">
                                        <input type="text"  class="form-control" name="findByCourseName" placeholder="{{ trans('table.input_select_course') }}" value="{{isset($a['findByCourseName'])?$a['findByCourseName']:""}}">
                                    </div>

                                    <div class="form-group">
                                        <input type="text"  class="form-control" name="findByUserName" placeholder="{{ trans('table.input_select_user') }}" value="{{isset($a['findByUserName'])?$a['findByUserName']:""}}">
                                    </div>
                                    <button type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ trans('button.search') }}</button>
                                </div>
                            </form>
                            <div style="float: right;">
                                <form  class="form-inline" action="/reports/course/excel">
                                    <input type="hidden"  name="select" value="{{isset($a)?json_encode($a):""}}"/>
                                    <button type="submit" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.excel') }}</button>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12 main">
                            <div class="table-responsive">
                                <div class="courseTable text-center">
                                    <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:300%;">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>{{ trans('user.real_name') }}</th>
                                                    <th>{{ trans('table.user_name') }}</th>
                                                    <th>{{ trans('table.email') }}</th>
                                                    <th>{{ trans('user.department') }}</th>
                                                    <th>{{ trans('record.name') }}</th>
                                                    <th>{{ trans('record.type') }}</th>
                                                    <th>{{ trans('table.catalog') }}</th>
                                                    <th>{{ trans('table.status') }}</th>
                                                    <th>{{ trans('course.course_survey') }}</th>
                                                    <th>{{ trans('course.status') }}</th>
                                                    <th>{{ trans('exam.status') }}</th>
                                                    <th>{{ trans('evaluation.status') }}</th>
                                                    <th>{{ trans('table.create_time') }}</th>
                                                    <th>{{ trans('table.start_time') }}</th>
                                                    <th>{{ trans('table.end_time') }}</th>
                                                    <th>{{ trans('table.study_times') }}</th>
                                                    <th>{{ trans('table.study_span') }}</th>
                                                    <th>{{ trans('table.course_score') }}</th>
                                                    <th>{{ trans('table.exam_score') }}</th>
                                                    <th>{{ $attribute->first_title}}</th>
                                                    <th>{{ $attribute->second_title}}</th>
                                                    <th>{{ $attribute->third_title}}</th>
                                                    <th>{{ $attribute->fourth_title}}</th>
                                                    <th>{{ $attribute->fifth_title}}</th>

                                                     <th>{{ trans('table.status')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($records as $record)
                                                    @if($record->type == 1 || $record->type == 3)
                                                        <tr>
                                                            <td class="vertical-align-middle">{{ $record->model->user->profile->realname }}</td>
                                                            <td class="vertical-align-middle">{{  $record->model->user->name }}</td>
                                                            <td class="vertical-align-middle">{{  $record->model->user->email }}</td>
                                                            <td class="vertical-align-middle">{{ $record->model->user->department->name }}</td>
                                                            <td class="vertical-align-middle">{{  $record->model->course->title }}</td>
                                                            <td class="vertical-align-middle">{{ trans($record->recordType->title) }}</td>
                                                            <td class="vertical-align-middle">{{  $record->model->course->catalog->name }}</td>
                                                            <td class="vertical-align-middle">
                                                                @if($record->model->process==1)
                                                                    @if(($record->model->course->examDistribution && !empty($examRecord[$record->id])) || (!empty($evaluationRecord[$record->id]) &&  $evaResult[$record->id]>0))
                                                                        {{trans('course.under_way')}}
                                                                    @else
                                                                        {{trans('course.not_start')}}
                                                                    @endif
                                                                @elseif($record->model->process < 6)
                                                                    
                                                                {{ $record->model->process==3?trans('course.under_way'):trans('course.'.$record->model->status->title)}}
                                                                @endif
                                                                {{$record->model->overdue?'('.trans('table.overdue').')':''}}</td>
                                                                 <td class="vertical-align-middle">
                                                                    @if(empty($surveyRecord[$record->id]))
                                                                        
                                                                    @elseif($surveyResult[$record->id]<1)
                                                                        {{ trans('table.not_start') }}
                                                                    @else
                                                                        {{ trans('table.done') }}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">{{ $record->model->process < 6? trans('course.'.$record->model->status->title):""}} </td>
                                                                <td class="vertical-align-middle">
                                                                    @if(!$record->model->course->examDistribution)
                                                                        
                                                                    @elseif(!$examRecord[$record->id])
                                                                        {{ trans('exam.not_start') }}
                                                                    @else
                                                                        {{ trans("exam.".$examRecord[$record->id]->status->title) }}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">
                                                                    @if(empty($evaluationRecord[$record->id]))

                                                                    @elseif($evaResult[$record->id]<1)
                                                                        {{ trans('table.not_start') }}
                                                                    @else
                                                                        {{ trans('table.done') }}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">{{  $record->model->created_at }}</td>
                                                                <td class="vertical-align-middle">{{  $record->model->started_at }}</td>
                                                                <td class="vertical-align-middle">
                                                                    @if($record->model->end_at)
                                                                        {{$record->model->end_at}}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">{{ $record->model->study_times}}</td>
                                                                <td class="vertical-align-middle">
                                                                    @if($record->model->study_span)
                                                                        {{$record->model->study_span.trans('table.minute')}}
                                                                    @else
                                                                        0{{ trans('table.minute') }} 
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle">{{  $record->model->max_score }}</td>
                                                                <td class="vertical-align-middle">{{  $record->model->exam_max_score }}</td>
                                                                <td class="vertical-align-middle">{{ $record->model->user->profile->attribute1?$record->model->user->profile->attribute1->title:"" }}</td>
                                                                <td class="vertical-align-middle">{{ $record->model->user->profile->attribute2?$record->model->user->profile->attribute2->title:"" }}</td>
                                                                <td class="vertical-align-middle">{{ $record->model->user->profile->attribute3?$record->model->user->profile->attribute3->title:"" }}</td>
                                                                <td class="vertical-align-middle">{{ $record->model->user->profile->attribute4?$record->model->user->profile->attribute4->title:"" }}</td>
                                                                <td class="vertical-align-middle">{{ $record->model->user->profile->attribute5?$record->model->user->profile->attribute5->title:"" }}</td>
     <td class="vertical-align-middle">{{ $record->model->user->userStatus->title }}</td>                                                   
</tr>
                                                    @endif
                                                    @if($record->type == 2)
                                                        
                                                        @if($record->model->model)
                                                            <tr>
                                                                <td class="vertical-align-middle">{{ $record->model->model->profile->realname }}</td>
                                                                <td class="vertical-align-middle">{{  $record->model->model->name }}</td>
                                                                <td class="vertical-align-middle">{{  $record->model->model->email }}</td>
                                                                <td class="vertical-align-middle">{{ $record->model->model->department->name }}</td>
                                                                
                                                                <td class="vertical-align-middle">{{  $record->model->exam->title }}</td>
                                                                <td class="vertical-align-middle">{{ trans($record->recordType->title) }}</td>
                                                                <td class="vertical-align-middle">{{  $record->model->exam->catalog->name }}</td>
                                                                <td class="vertical-align-middle">
                                                                    @if(count($record->model->records))
                                                                        {{trans("exam.".$record->model->records[0]->status->title)}}
                                                                    @else
                                                                        {{ trans('table.not_start') }}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle"> </td>
                                                                <td class="vertical-align-middle">
                                                                    @if(count($record->model->records))
                                                                        {{trans("exam.".$record->model->records[0]->status->title)}}
                                                                    @else
                                                                        {{ trans('table.not_start') }}
                                                                    @endif
                                                                </td>
                                                                <td class="vertical-align-middle"> </td>
                                                                <td class="vertical-align-middle">{{  $record->model->created_at }}</td>
                                                                @if(count($record->model->records))
                                                                    <td class="vertical-align-middle">{{  $record->model->records[0]->created_at }}</td>
                                                                    <td class="vertical-align-middle">{{  $record->model->records[0]->end_at }}</td>
                                                                    <td class="vertical-align-middle">{{ count($record->model->records[0]->results)}}</td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle">N/A</td>
                                                                    <td class="vertical-align-middle">{{  $record->model->records[0]->max_score }}</td>
                                                                @else
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                    <td class="vertical-align-middle"></td>
                                                                @endif

                                                                 <td class="vertical-align-middle">{{ $record->model->model->profile->attribute1?$record->model->model->profile->attribute1->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $record->model->model->profile->attribute2?$record->model->model->profile->attribute2->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $record->model->model->profile->attribute3?$record->model->model->profile->attribute3->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $record->model->model->profile->attribute4?$record->model->model->profile->attribute4->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{ $record->model->model->profile->attribute5?$record->model->model->profile->attribute5->title:"" }}</td>
                                                        <td class="vertical-align-middle">{{  $record->model->model->userStatus->title }}</td>
                                                            </tr>
                                                        @endif
                                                    @endif
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $records->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    @endsection
