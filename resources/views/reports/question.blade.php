@extends('reports.partials.template')

@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{ trans('report.question') }}</h2>
            </div>
        </div>
        <div class="panel panel-announcement">
            <div class="panel-body">       
                <div class="row">
                    <div class="col-sm-12 main">
                        <div style="float: left;">
                            <form class="form-inline" action="/reports/question">
                                
                                {!! Form::select('bank_id', $bankSelect,isset($a['bank_id'])?$a['bank_id']:null,['class'=>"form-control"]) !!}
                                <button type="submit" class="dm3-btn dm3-btn-medium button-large"></i>&nbsp;{{ trans('button.search') }}</button>
                                
                            </form>
                        </div>
                        
                        
                        <div style="float: right;">
                            <form  class="form-inline" action="/reports/question/excel">
                                <input type="hidden"  name="select" value="{{isset($a)?json_encode($a):""}}"/>
                                <button type="submit" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.excel') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 main">
                        <div class="table-responsive">
                            <div class="courseTable text-center">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('question.title') }}</th>
                                            <th>{{trans('bank.title')}}</th>
                                            <th>{{ trans('table.type') }}</th>
                                            <th>{{ trans('table.answer_number')}}</th>
                                            <th>%{{ trans('table.true')}}</th>
                                            <th>%{{ trans('table.false')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($questions as $question)
                                            <tr>
                                                
                                                <td class="vertical-align-middle">{!! $question->title !!}</td>
                                                <td class="vertical-align-middle">{{ $question->bank->title }}</td>
                                                <td class="vertical-align-middle">
                                                    @if($question->type == 1)
                                                        {{ trans('question.radio') }}
                                                    @elseif($question->type ==2)
                                                        {{ trans('question.mulit') }}
                                                    @elseif($question->type == 3)
                                                        {{ trans('question.false') }}
                                                    @elseif($question->type ==4)
                                                        {{ trans('question.essay') }}
                                                    @else
                                                        {{ trans('table.unknow') }}
                                                    @endif
                                                </td>
                                                <td class="vertical-align-middle">{{ count($question->questionResults) }}</td>
                                                @if($question->type!=4)
                                                    <td class="vertical-align-middle">{{$true[$question->id]}}%</td>
                                                    <td class="vertical-align-middle">{{$false[$question->id]}}%</td>
                                                @else
                                                    <td class="vertical-align-middle"></td>
                                                    <td class="vertical-align-middle"></td>
                                                @endif
                                                
                                            </tr><!-- end ngRepeat: trainee in trainees -->
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                                                                             {!! $questions->appends(['select'=>isset($a)?json_encode($a):""])->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
