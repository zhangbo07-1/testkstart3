@extends('system_info.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append  
    @section('container')
        
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('system.logo') }}</h2>
                </div>
            </div>
            <div class="panel panel-announcement">
                <div class="panel-body">
                    {{ trans('system.logo_limit') }}
                    <br>
                    <label>{{ trans('system.logo_now') }}：</label>

                    <img id="avatar" src="{!!get_logo_url()!!}/img/logo.png" />

                    <br>
                    <br>
                    {!! Form::open(array('url'=>'system-manage/logo/upload','method'=>'POST','files'=>true,'class'=>"form-inline"))!!}

                    
                    <input type="button" value="{{ trans('button.file') }}" onclick="document.getElementById('upfile').click();" class="dm3-btn dm3-btn-medium button-large"/>
                    <div style="display:inline-block;">
                        <input type="text" name="url" class="form-control" id="fileURL">
                    </div>
                    
                    <input type="file" name="image" id="upfile" onchange="document.getElementById('fileURL').value=this.value;" style="display:none">
                    <div id="loading">

                    </div>
                    {!! errors_for('image', $errors) !!}
                    <br>
                    
                    <button class="dm3-btn dm3-btn-medium button-large" onClick="showLoad()">{{ trans('button.submit') }}</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    @endsection
