@extends('layout.master')


@section('js')
{!! Html::script('assets/js/system.min.js') !!}
    @append


@section('content')
    @include('system_info.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection