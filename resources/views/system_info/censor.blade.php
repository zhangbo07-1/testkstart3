@extends('system_info.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append

@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{ trans('system.info') }}</h2>
            </div>
        </div>
        
        

        <div class="panel panel-announcement">
            <br>
            <br>
            @include('layout.message')
            <div class="login-form clearfix">
                <form  class="form-horizontal length-750px" action="{{ URL('system-manage/censor') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                    <div class="form-group">
                        <label  class="col-xs-4 control-label">{{ trans('system.if_register') }}<a data-toggle="tooltip" title="{{ trans('tips.00001') }}"><span class="glyphicon glyphicon-question-sign"></span></a> : </label>
                        @if($info->register == 0)
                            <input type="radio" name="register" checked="checked" value="0" >{{ trans('system.not_register') }}
                            <br>
                            <input type="radio" name="register" value="1" >{{ trans('system.need_register') }}
                        @else
                            <input type="radio" name="register" value="0" >{{ trans('system.not_register') }}
                            <br>
                            <input type="radio" name="register" checked="checked" value="1" >{{ trans('system.need_register') }}
                        @endif
                    </div>



                    <div class="form-group">
                        <label  class="col-xs-4 control-label">{{ trans('system.department') }}<a data-toggle="tooltip" title="{{ trans('tips.00049') }}"><span class="glyphicon glyphicon-question-sign"></span></a> : </label>
                        
                        <input type="hidden" id="department_id" class="form-control"  name="department_id" value="{{$info->department_id}}"/>
                        <a  onclick="departmentSelect()" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-user"></i>&nbsp;{{ trans('button.department') }}</a>
                        <div style="display:inline-block;" id="department_name">{{$info->department->name}}</div>
                        
                    </div>


                    <div class="form-group">
                        <label  class="col-xs-4 control-label">{{ trans('system.if_validate') }}
                            <a data-toggle="tooltip" title="{{ trans('tips.00002') }}"><span class="glyphicon glyphicon-question-sign"></span></a> : 
                        </label>
                        @if($info->validate == 0)
                            <input type="radio" name="validate" checked="checked" value="0" >{{ trans('system.not_validate') }}
                            <br>
                            <input type="radio" name="validate" value="1" >{{ trans('system.need_validate') }}
                        @else
                            <input type="radio" name="validate" value="0" >{{ trans('system.not_validate') }}
                            <br>
                            <input type="radio" name="validate" checked="checked" value="1" >{{ trans('system.need_validate') }}
                        @endif
                    </div>
                    <div class="form-group">
                        <label  class="col-xs-4 control-label">{{ trans('system.if_censor') }}
                            <a data-toggle="tooltip" title="{{ trans('tips.00003') }}"><span class="glyphicon glyphicon-question-sign"></span></a> : 
                        </label>
                        @if($info->censor == 0)
                            <input type="radio" name="censor" checked="checked" value="0" >{{ trans('system.not_censor') }}
                            <br>
                            <input type="radio" name="censor" value="1" >{{ trans('system.need_censor') }}
                        @else
                            <input type="radio" name="censor" value="0" >{{ trans('system.not_censor') }}
                            <br>
                            <input type="radio" name="censor" checked="checked" value="1" >{{ trans('system.need_censor') }}
                        @endif

                    </div>

                    <div class="form-group">
                        <label for="confirm_button" class="col-xs-4 control-label"></label>
                        <div class="col-xs-8">
                            <button class="dm3-btn dm3-btn-medium button-large" type="submit">
                                <i class="fa fa-check"></i>&nbsp;{{ trans('button.submit') }}
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    
@endsection
