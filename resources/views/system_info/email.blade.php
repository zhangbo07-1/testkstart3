@extends('system_info.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append

    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('system.info') }}</h2>
                </div>
            </div>
            <div class="panel panel-announcement">
                <br>
                <br>
                @include('layout.message')
                    <div class="login-form clearfix">
                        <form  class="form-horizontal"  action="{{ URL('system-manage/email') }}" method="POST">
                            <div class="form-group">
                                
                                <label  class="col-xs-4 control-label">{{ trans('system.login_by_email') }}<a data-toggle="tooltip" title="{{ trans('tips.00050') }}"><span class="glyphicon glyphicon-question-sign"></span></a> : </label>
                                @if($info->user_email)
                                    <input type="radio" name="user_email" checked="checked" value="1" >{{ trans('table.yes') }}
                                    <br>
                                    <input type="radio" name="user_email" value="0" >{{ trans('table.no') }}
                                @else
                                    <input type="radio" name="user_email" value="1" >{{ trans('table.yes') }}
                                    <br>
                                    <input type="radio" name="user_email" checked="checked" value="0" >{{ trans('table.no') }}
                                @endif
                            </div>
                            
                            <div class="col-xs-1">
                                
                            </div>
                            {{ trans('system.add_email_suffix')  }}
                            

                            <a data-toggle="tooltip" title="{{ trans('tips.00004') }}"><span class="glyphicon glyphicon-question-sign"></span></a>

                            <br>
                            <br>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">


                            <div id="listNUM">
                                <input type="hidden" id="emailNUM" name="emailNUM" value="{{count($info->emailSuffixes)}}">
                            </div>


                            <div id="emailPlace">
                                
                                @foreach($info->emailSuffixes as $key=>$emailSuffix)
                                    
                                    <div id="emaildiv_{{$key+1}}">
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" name="email{{$key+1}}" class="form-control" required="required" value="{{$emailSuffix->email}}" >
                                                </td>
                                                <td><input type="button" value="{{ trans('button.delete') }}" class="dm3-btn dm3-btn-medium button-large"  onclick="delEmail({{$key+1}})"/></td>
                                            </tr>
                                        </table>
                                    </div>
                                @endforeach
                            </div>

                            
                            
                            <div class="form-group">
                                <label for="confirm_button" class="col-xs-4 control-label"></label>

                                <div class="col-xs-8">
                                    <button class="dm3-btn dm3-btn-medium button-large" type="submit">
                                        <i class="fa fa-check"></i>&nbsp;{{ trans('button.submit') }}
                                        
                                    </button>
                                    <input type="button" class="dm3-btn dm3-btn-medium button-large" id="addEmailBTN" onClick="addEmail()" value="{{ trans('button.add_suffix') }}"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        
        
        <script>
         var delete_log = "{{trans('button.delete')}}";
         var add_max = "{{trans('error.00065')}}";
         var delete_limit = "{{trans('error.00066')}}";
        </script>
    @endsection
