<div class="lesson" style="margin-top: 10px;">


    <div id="lessonPlace">
        @foreach($class->lessons as $index=>$lesson)
            <div id="lessondiv_{{$index+1}}" class="form-group">
                <label for="address" class="col-xs-4 control-label text-right">{{ trans('course.class_lesson') }}{{$index+1}}

                </label>

                <div class="col-xs-8">
                    <input onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="start_time{{$index+1}}" class="form-control" placeholder="{{trans('table.start_time')}}" readonly="readonly" value="{{$lesson->start_at}}">
                    <input onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="end_time{{$index+1}}" class="form-control" placeholder="{{trans('table.end_time')}}" readonly="readonly" value="{{$lesson->end_at}}">
                    <input type="text" name="address{{$index+1}}" class="form-control" placeholder="{{trans('course.class_address')}}" value="{{$lesson->address}}">
                </div>
            </div>
        @endforeach
    </div>

    <div align="right">
        <a onClick="addLesson()" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-plus"></i>&nbsp;{{ trans('course.add_lesson') }}</a>
        <a onClick="delLesson()" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-minus"></i>&nbsp;{{ trans('course.delete_lesson') }}</a>
    </div>


</div>

<script>
 var class_lesson = "{{trans('course.class_lesson')}}";
 var start_time = "{{trans('table.start_time')}}";
 var end_time = "{{trans('table.end_time')}}";
 var class_address = "{{trans('course.class_address')}}";
</script>
