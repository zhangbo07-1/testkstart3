@extends('layout.base')

@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
@endsection
@section('content')
    <div class="container">
        <br>
        <br>

        <div class="row">
            <div class="col-sm-12 main">
                <div class="text-center">
                    {!! QrCode::size(500)->color(20,157,210)->generate('http://'.$_SERVER['HTTP_HOST'].'/courses/'.$course->course_id.'/class/'.$class->id.'/lesson/'.$lesson->id.'/sign'); !!}
             <p>{{ trans('button.sign')}}:
                        {{$course->title}}=>
                        {{$class->title}}=>
                        {{trans('course.class_lesson')}}:
                        {{$lesson->start_at}}~
                        {{$lesson->end_at}}
                    </p>
                </div>

            </div>
            
        </div>
    </div>
@endsection
