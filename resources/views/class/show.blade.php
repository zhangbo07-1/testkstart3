@extends('courses.partials.template')

@section('js')

    {!! Html::script('assets/bower_components/handlebars/handlebars.min.js') !!}
    {!! Html::script('assets/bower_components/jquery-bootpag/lib/jquery.bootpag.min.js') !!}
    {!! Html::script('assets/bower_components/spin.js/spin.js') !!}
    {!! Html::script('assets/js/class_status.js') !!}
    {!! Html::script('assets/js/views/view.operation.js') !!}
    {!! Html::script('assets/laydate/laydate.js') !!}
    @append

    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{$course->title}}</h2>
                </div>
            </div>

            <div class="panel panel-announcement">
                
                
                <div class="courseTable">
                    <div class="panel">
                        
                        <div class="panel-body">
                            <div  align="right">
                                <a class="dm3-btn dm3-btn-medium button-large" href="/courses/{{$course->course_id}}/class" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>

                            </div>

                            {{ trans('course.class_title') }}:{{$class->title}}
                            <br>
                            {{ trans('course.class_teacher') }}:{{$class->teacher}}
                            <table class="table table-striped table-bordered  text-center">
                                <thead>
                                    <tr>
                                        <th>{{ trans('course.class_lesson') }}</th>
                                        <th>{{ trans('table.start_time') }}</th>
                                        <th>{{ trans('table.end_time') }}</th>
                                        <th>{{ trans('course.class_address') }}</th>
                                        <th>{{ trans('table.operation') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($class->lessons as $index=>$lesson)
                                        <tr>
                                            <td class="vertical-align-middle">{{ trans('course.class_lesson') }}{{$index+1}}</td>

                                            <td class="vertical-align-middle">{{ $lesson->start_at }}</td>
                                            <td class="vertical-align-middle">{{ $lesson->end_at }}</td>
                                            <td class="vertical-align-middle">{{ $lesson->address }}</td>
                                            <td class="vertical-align-middle">
                                                <a href="{{ url('/courses/'.$course->course_id.'/class/'.$class->id.'/lesson/'.$lesson->id.'/forsign') }}" class="dm3-btn dm3-btn-medium button-large" target="_blank">
                                                    <i class="fa fa-pencil"></i>&nbsp;{{ trans('button.sign') }}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div>
                                @if($course->pushed)
                                    <a href="/courses/{{$course->course_id}}/class/{{$class->id}}/distribute" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-user"></i>&nbsp;{{ trans('course.class_distribute') }}</a>
                                @else
                                    <a  class="dm3-btn dm3-btn-medium dm3-btn-gray button-large" disabled><i class="fa fa-user"></i>&nbsp;{{ trans('course.class_distribute') }}</a>
                                @endif
                                @if($course->pushed)
                                    <a href="/courses/{{$course->course_id}}/class/{{$class->id}}/forenroll" class="dm3-btn dm3-btn-medium button-large"  target="_blank"><i class="fa fa-user"></i>&nbsp;{{ trans('button.enroll') }}</a>
                                @else
                                    <a  class="dm3-btn dm3-btn-medium dm3-btn-gray button-large" disabled><i class="fa fa-user"></i>&nbsp;{{ trans('button.enroll') }}</a>
                                @endif

                              @if($surveyRecord)

                                    <a class="dm3-btn dm3-btn-medium button-large" href="/course/{{$course->course_id}}/course-survey/{{$surveyRecord->id}}/QR-code" target="_blank">
                                        <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.survey_QR') }}</a>
                                @else
                                        <a class="dm3-btn dm3-btn-gray button-large" disabled >
                                            <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.survey_QR') }}</a>
                                @endif

                                @if($course->examDistribution)

                                    <a class="dm3-btn dm3-btn-medium button-large" href="/course/{{$course->course_id}}/exam/{{$course->examDistribution->exam->exam_id}}/QR-code" target="_blank">
                                        <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam_QR') }}</a>
                                @else
                                        <a class="dm3-btn dm3-btn-gray button-large" disabled >
                                            <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.exam_QR') }}</a>
                                @endif


                                


                                @if($evaluationRecord)

                                    <a class="dm3-btn dm3-btn-medium button-large" href="/course/{{$course->course_id}}/evaluation/{{$evaluationRecord->id}}/QR-code" target="_blank">
                                        <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.evaluation_QR') }}</a>
                                @else
                                        <a class="dm3-btn dm3-btn-gray button-large" disabled >
                                            <i class="fa fa-check-square-o"></i>&nbsp;{{ trans('button.evaluation_QR') }}</a>
                                @endif



                                
                            </div>
                            
                            
                                
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-announcement">
                <div class="courseTable">
                    <div class="panel">
                        
                        <div class="panel-body">
                            <div class="form-inline">
                                <input type="hidden" id="manage_id" class="form-control"  name="manage_id" value=""/>
                                <a  onclick="manageSelect()" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-user"></i>&nbsp;{{ trans('button.department') }}</a>
                                <div style="display:inline-block;" id="manage_name"></div>
                                
                                <br>
                                {{ trans('user.created_time') }}:
                                <input onclick="laydate()" id="startDate" class="form-control" placeholder="{{trans('table.select_time')}}" readonly="readonly" style="width:110px;">
                                ~
                                <input onclick="laydate()" id="endDate"  class="form-control" placeholder="{{trans('table.select_time')}}" readonly="readonly" style="width:110px;" >
                                <br>
                                
                                {!! Form::select('attribute1', $firstAttributeSelects,null,['class'=>"form-control",'id'=>'attribute1']) !!}
                                {!! Form::select('attribute2', $secondAttributeSelects,null,['class'=>"form-control",'id'=>'attribute2']) !!}
                                {!! Form::select('attribute3', $thirdAttributeSelects,null,['class'=>"form-control",'id'=>'attribute3']) !!}
                                {!! Form::select('attribute4', $fourthAttributeSelects,null,['class'=>"form-control",'id'=>'attribute4']) !!}
                                {!! Form::select('attribute5', $fifthAttributeSelects,null,['class'=>"form-control",'id'=>'attribute5']) !!}
                                
                                <br>
                                <input id="searchWord" class="form-control" type="text" name="findByUsername" placeholder="{{ trans('table.input_select') }}">
                                
                                <button id="searchButton" type="submit" class="dm3-btn dm3-btn-medium button-large">
                                    <i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}
                                </button>
                            </div>
                            <table class="table table-striped table-bordered text-center">
                                <thead>
                                    <tr>
                                        <th><input id="classRecordButton" type="checkbox"></th>
                                        <th>{{ trans('user.title') }}</th>
                                        <th>{{ trans('user.real_name') }}</th>
                                        <th>{{ trans('user.email') }}</th>
                                        <th>{{ trans('user.department') }}</th>
                                        <th>{{ trans('course.status') }}</th>

                                        @foreach($class->lessons as $index=>$lesson)
                                            <th>{{ trans('course.class_lesson') }}{{$index+1}}</th>
                                        @endforeach
                                        <th>{{ $attribute->first_title}}</th>
                                        <th>{{ $attribute->second_title}}</th>
                                        <th>{{ $attribute->third_title}}</th>
                                        <th>{{ $attribute->fourth_title}}</th>
                                        <th>{{ $attribute->fifth_title}}</th>
                                    </tr>
                                </thead>
                                <tbody id="classRecordForm">
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-body">
                            <div id="classRecordForm-Pagination"></div>
                            <button id="recordOverSubmit" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-thumbs-o-up"></i>&nbsp;{{ trans('table.done') }}</button>
                            <button id="recordNotOverSubmit" class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i class="fa fa-times"></i>&nbsp;{{ trans('table.not_done') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script id="classRecordTemplate" type="text/x-handlebars-template">
            @{{#each data}}
            <tr>
                <td><input class="classRecord" type="checkbox" name="user[]"
                           id="@{{id}}" value="@{{user.id}}"></td>
                <td>@{{user.name}}</td>
                <td>@{{user.profile.realname}}</td>
                <td>@{{user.email}}</td>
                <td>@{{user.department.name}}</td>
               
                <td>@{{class_status}}</td>
                @{{list_status}}
               
                
                <td>@{{user.profile.attribute1.title}}</td>
                <td>@{{user.profile.attribute2.title}}</td>
                <td>@{{user.profile.attribute3.title}}</td>
                <td>@{{user.profile.attribute4.title}}</td>
                <td>@{{user.profile.attribute5.title}}</td>
            </tr>
            @{{/each}}
        </script>
        <script>
         var course_id = {{$course->id}};
         var class_id = {{$class->id}};
         var class_status1 = "{{trans('table.not_done')}}";
         var class_status2 = "{{trans('table.done')}}";
         var lesson_status1 = "{{trans('course.lesson_status1')}}";
         var lesson_status2 = "{{trans('course.lesson_status2')}}";
         var lesson_status3 = "{{trans('course.lesson_status3')}}";
        </script>
    @endsection
