<div class="row">
    <div class="col-sm-12 main">
        <h1 class="text-center">{{ trans('report.class') }}</h1>

        <div class="table-responsive courseTable">
            <table class="table table-striped text-center">
                <thead>
                    <th>
                        {{ trans('course.class_title') }}
                    </th>
                    <th>
                        {{ trans('table.start_time') }}
                    </th>
                    <th>
                        {{ trans('table.end_time') }}
                    </th>
                    <th>
                        {{ trans('table.status') }}
                    </th>
                </thead>
                <tbody>
                    @foreach($record->course->classes as $class)
                        @if(!empty($classRecord[$class->id]))
                            @foreach($class->lessons as $index=>$lesson)

                                <tr>
                                    <td>{{$class->title}}</td>
                                    <td>{{$lesson->start_at}}</td>
                                    <td>{{$lesson->end_at}}</td>
                                    @if(empty($classSign[$lesson->id]))
                                        <td>{{ trans('course.lesson_status1') }}</td>
                                    @elseif($classSign[$lesson->id] == 1)
                                        <td>{{ trans('course.lesson_status1') }}</td>
                                    @elseif($classSign[$lesson->id] == 2)
                                        <td>{{ trans('course.lesson_status2') }}</td>
                                    @elseif($classSign[$lesson->id] == 3)
                                        <td>{{ trans('course.lesson_status3') }}</td>
                                    @endif





                                </tr>


                            @endforeach
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
