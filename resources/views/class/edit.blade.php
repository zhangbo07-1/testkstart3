@extends('courses.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    {!! Html::script('assets/laydate/laydate.js') !!}
    {!! Html::script('assets/js/course_class.js') !!}
    @append
    @section('content')

        @section('container')
            <div class="contentpanel">
                <div class="panel panel-announcement">
                    <div class="panel-heading">
                        <h2 class="panel-title">{{$course->title }}</h2>
                    </div>
                </div>

                <div class="panel panel-announcement">
                    <br>
                    <br>
                    @include('layout.message')
                    {!! Form::open(array('url'=>'courses/'.$course->course_id.'/class/'.$class->id,'method'=>'PUT', 'class' => 'form-horizontal length-750px')) !!}

                    <input type="hidden" name="course_id" value="{{isset($course)? $course->id:''}}">
                     <input type="hidden" id="lessonNUM" name="lessonNUM" value="{{count($class->lessons)}}">

                    <div class="form-group {!! errors_for_tag('title', $errors) !!}">
                        <label for="title" class="col-xs-4 control-label">{{ trans('course.class_title') }}</label>

                        <div class="col-xs-8">
                            {!! Form::text('title', $class->title, ['class'=>"form-control","required"=>"required"]) !!}
                            {!! errors_for('title', $errors) !!}
                        </div>
                    </div>
                     <div class="form-group {!! errors_for_tag('teacher', $errors) !!}">
                        <label for="teacher" class="col-xs-4 control-label">{{ trans('course.class_teacher') }}</label>

                        <div class="col-xs-8">
                            {!! Form::text('teacher', $class->teacher, ['class'=>"form-control","required"=>"required"]) !!}
                            {!! errors_for('teacher', $errors) !!}
                        </div>
                    </div>


                    <div class="form-group {!! errors_for_tag('late', $errors) !!}">
                        <label for="late" class="col-xs-4 control-label">{{ trans('course.class_late') }}

                        </label>

                        <div class="col-xs-8">
                            <div class="form-inline margin-left-10px">
                                <div class="form-group width-130px">
                                    <div class="input-group">
                                        {!! Form::text("late",$class->late,array('class' => 'form-control text-right')) !!}
                                        <div class="input-group-addon">{{ trans('table.minute') }}</div>
                                    </div>
                                </div>
                            </div>
                            {!! errors_for('minute', $errors) !!}
                        </div>
                    </div>


                    @include('class.lesson_edit')


                    <div class="form-group">
                        <label for="confirm_button" class="col-xs-4 control-label"></label>

                        <div class="col-xs-8">
                            <button class="dm3-btn dm3-btn-medium button-large" type="submit">
                                <i class="fa fa-check"></i>&nbsp;{{trans('button.submit')}}
                            </button>

                            <a  href="/courses/{{$course->course_id}}/class" class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i class="fa fa-reply"></i>&nbsp;{{ trans('button.cancel') }}</a>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        @endsection
