@extends('courses.partials.template')

@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{$course->title}}</h2>
            </div>
        </div>

        <div class="panel panel-announcement">
            <div class="courseTable">
                <div class="panel">
                    <div class="panel-body text-center">
                        <div align="right">
                            <a href="{{URL('courses/'.$course->course_id.'/class/create')}}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-plus"></i>&nbsp;{{ trans('course.class_create') }}</a>
                        </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>{{ trans('course.class_title') }}</th>
                                    <th>{{ trans('course.class_teacher') }}</th>
                                    <th>{{ trans('course.class_lesson') }}</th>
                                    <th>{{ trans('table.operation') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($classes as $class)
                                    <tr>

                                        <td class="vertical-align-middle">{{ $class->title }}</td>

                                        <td class="vertical-align-middle">{{ $class->teacher }}</td>
                                        <td class="vertical-align-middle">{{ count($class->lessons) }}</td>

                                        <td class="vertical-align-middle">
                                            <a href="{{ url('/courses/'.$course->course_id.'/class/'.$class->id.'/edit') }}" class="dm3-btn dm3-btn-medium button-large">
                                                <i class="fa fa-pencil"></i>&nbsp;{{ trans('button.edit') }}</a>
                                            <a href="{{ url('/courses/'.$course->course_id.'/class/'.$class->id) }}" class="dm3-btn dm3-btn-medium button-large">
                                                <i class="fa fa-eye"></i>&nbsp;{{ trans('button.manage') }}</a>
                                            <form action="{{ URL('/courses/'.$course->course_id.'/class/'.$class->id) }}" method="POST" style="display: inline;">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" onClick="return ifDelete()" class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}</button>

                                            </form>
                                        </td>
                                    </tr>

                                @endforeach

                            </tbody>
                        </table>
                        {!! $classes->render() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>

    <script>
     var course_id = {{$course->id}};
    </script>
@endsection
