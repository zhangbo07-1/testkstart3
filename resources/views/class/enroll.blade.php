@extends('layout.base')

@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
@endsection
@section('content')
    <div class="container">
        <br>
        <br>

        <div class="row">
            <div class="col-sm-12 main">
                <div class="text-center">
                    {!! QrCode::size(500)->color(20,157,210)->generate('http://'.$_SERVER['HTTP_HOST'].'/courses/'.$course->course_id.'/class/'.$class->id.'/enroll'); !!}
                    <p>{{ trans('button.enroll')}}:
                        {{$course->title}}=>
                        {{$class->title}}
                    </p>
                </div>

            </div>
            
        </div>
    </div>
@endsection
