<div class="lesson" style="margin-top: 10px;">

    <div >
        <div class="form-group">
            <label for="address" class="col-xs-4 control-label text-right">{{ trans('course.class_lesson') }}1

            </label>

            <div class="col-xs-8">
                <input onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="start_time1" class="form-control" placeholder="{{trans('table.start_time')}}" readonly="readonly">
                <input onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="end_time1" class="form-control" placeholder="{{trans('table.end_time')}}" readonly="readonly">
                {!! Form::text("address1",null,array('class' => 'form-control', 'placeholder'=>trans('course.class_address'))) !!}
            </div>
        </div>
    </div>
    <div id="lessonPlace">
    </div>

    <div align="right">
        <a onClick="addLesson()" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-plus"></i>&nbsp;{{ trans('course.add_lesson') }}</a>
        <a onClick="delLesson()" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-minus"></i>&nbsp;{{ trans('course.delete_lesson') }}</a>
    </div>


</div>

<script>
     var class_lesson = "{{trans('course.class_lesson')}}";
var start_time = "{{trans('table.start_time')}}";
var end_time = "{{trans('table.end_time')}}";
 var class_address = "{{trans('course.class_address')}}";
</script>
