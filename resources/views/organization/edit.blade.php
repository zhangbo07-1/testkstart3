@extends('layout.call')

@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <form action="{{ URL('organization/'.$node->id.'/edit') }}" method="POST">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    @if($ability)

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <br>
                        @include('layout.message')
                        <a>{{ trans('organization.title') }}</a>
                        <input type="text" name="name" class="form-control" required="required" value="{{$node->name}}">
                        <a>{{ trans('organization.tag') }}</a>
                        <input type="text" name="tag" class="form-control" required="required" value="{{$node->tag}}">
                        {!! errors_for('tag', $errors) !!}
                    @else
                        <br>
                        <br>
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{ trans('error.09998') }}</div>

                    @endif

                </div>
            </div>

            <br>
            <br>
            <div align="center">
                @if($ability)
                    <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                @endif

                <a class="dm3-btn dm3-btn-medium dm3-btn-red button-large"  onclick="closeWindow()">{{ trans('button.cancel') }}</a>
            </div>
        </form>

    @endsection
