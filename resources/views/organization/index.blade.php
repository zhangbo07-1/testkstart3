@extends('layout.master')
@section('css')
    {!! Html::style('assets/jstree/dist/themes/default/style.min.css') !!}
    @append
    @section('js')

        {!! Html::script('assets/jstree/dist/jstree.js') !!}
        {!! Html::script('assets/js/organization.js') !!}
        @append
        @section('content')
            <div class="container">
                <br>
                <br>
                <h3>{{ trans('organization.manage') }}
             <a data-toggle="tooltip" title="{{ trans('tips.00013') }}"><span class="glyphicon glyphicon-question-sign"></span></a></h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-inline">
                            <div style="float: left;">
             <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="open_all();"><i class="glyphicon glyphicon-plus"></i>&nbsp;{{ trans('button.open_all') }}</button>
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="close_all();"><i class="glyphicon glyphicon-minus"></i>&nbsp;{{ trans('button.close_all') }}</button>
                                <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="create_node();"><i class="glyphicon glyphicon-asterisk"></i>{{ trans('organization.create') }}</button>
                                <button type="button"class="dm3-btn dm3-btn-medium button-large" onclick="rename_node();"><i class="glyphicon glyphicon-pencil"></i>{{ trans('organization.rename') }}</button>
                                <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="delete_node();"><i class="glyphicon glyphicon-remove"></i>{{ trans('organization.delete') }}</button>
                                <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="search_node();"><i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}</button>
                                <input id="searchWord" type="text" class="form-control"  placeholder="{{ trans('table.input_select') }}">
                            </div>
                            <div style="float: right;">
                                 <a  href="{{URL('organization/excel')}}" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.excel') }}</a>

                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <div id="jstree_div"></div>
                        <br>
                        <div style="float: left;">
                            <label for="if_all">{{ trans('organization.if_all') }}</label>
                            <input id="if_all" type="checkbox" onClick="selectAll()" >
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>


            </div>

                    <script>
                     var delete_error = "{{trans('error.00005')}}";
                     var delete_log = "{{trans('error.00064')}}";
                    </script>
        @endsection
