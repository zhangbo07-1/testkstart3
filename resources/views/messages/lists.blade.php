@extends('layout.master')
@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
    @endsection
@section('content')
    <br>
    <br>
    <div class="container">
        <aside class="lessons-container">
        <h3 class="lessons-title">{{ trans('news.name') }}</h3>
            <ul class="lessons-ul">
                @foreach($messages as $message)
                    <li class="row">
                        <div class="col-md-6">
                            <div class="col-fixed"><span class="icon icon-34"></span></div>
                            <div class="row row-margin-5px">
                                <div class="lessons-padding ">
                                    <div><a class="lessons-course-name" href="{{URL("/news/".$message->id)}}" >{{$message->title}}</a></div>

                                    <footer class="post-meta">
                                        
                                        <span class="post-date" style="display: inline">
                                            <time class="entry-date" datetime="2014-10-21T07:07:50+00:00">{{ $message->updated_at }}</time>
                                        </span>

                                    </footer>

                                </div>
                            </div>
                        </div>

                    </li>
                @endforeach
            </ul>
        </aside>
        {!! $messages->render() !!}
    </div>
@endsection
