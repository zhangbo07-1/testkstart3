@extends('layout.master')
@section('content')
    @include('UMEditor.head')

    <div class="container">
        <br>
        <br>
        <h3>{{ trans('news.create') }}</h3>
        <div class="row">
                <div class="panel">

                    <div class="panel-heading"></div>
                    <div class="panel-body">

                        <h5 style="color: #337ab7;">{{ trans('table.title') }}</h5>
                        <form action="{{ URL('news-manage') }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" name="title" class="form-control" required="required">
                            {!! errors_for('title', $errors) !!}

                            <br>
                            <h5 style="color: #337ab7;">{{ trans('table.text') }}</h5>
                            {!! errors_for('body', $errors) !!}
                            <textarea id='myEditor' name="body" class="form-control" style="height:240px;" required="required"></textarea>
                            <br>
                            <button class="dm3-btn dm3-btn-medium button-large" onClick="checkinput()">{{ trans('button.submit') }}</button>
                            <a  onclick="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                        </form>

                    </div>
                </div>
            </div>
    </div>
    <script>
     function checkinput()
     {
         var tag = false;
         if(!UM.getEditor('myEditor').hasContents())
         {
             alert("{{ trans('error.text_can_not_empty') }}");

         }

         //
         //var checkText = $("input[name='title']").val();
         //if ( checkText == "" || checkText == null ){
         //   alert("标题不能为空");

         // }
         /// return tag;

     }
    </script>
@endsection
