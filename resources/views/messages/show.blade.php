@extends('layout.master')
@section('content')
    <div class="container">
        <section class="section-content">
            <div class="container clearfix">
                <div  align="right">
                    <a class="dm3-btn dm3-btn-medium button-large" onClick="returnurl()" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
                </div>
                <div>

                    <h1 class="entry-title" style=" text-align:center;">{{ $message->title }}</h1>

                    <div class="entry-content">
                        <br>
                        <br>
                        <div> {!! $message->body !!}</div>
                    </div>

                    <footer class="post-meta">
                        <span class="post-date">
                            <time class="entry-date" datetime="2014-10-21T07:07:50+00:00">{{ $message->updated_at }}</time>
                        </span>
                    </footer>


                    <div id="comments" class="comments-area">

                    </div>


                </div>
            </div>
        </section>

    </div>
     <script>
     function returnurl()
     {

         var url = document.referrer;
         if((url.indexOf("manage")>-1))
         {
             window.location.href="/news-manage";
         }
         else
         {
             window.location.href="/news/lists";
             }

     }
    </script>
@endsection
