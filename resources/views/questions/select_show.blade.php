<div class="selects" style="margin-top: 10px;">
    @if($question->type ==4)
        <textarea  name="{{$question->id}}" id="text{{$index}}" rows="3" class="form-control" required="required"></textarea>
    @else
        @foreach($question->selects as $select)
            @if($question->type == 2)
                <input type="checkbox" id = "select{{$index}}"  name="{{$question->id}}[]" value="{{$select->id}}" />
            @else
                <input type="radio" id = "select{{$index}}" name="{{$question->id}}" value="{{$select->id}}" />
            @endif
            {{$select->title}}
            <br>
            <br>

        @endforeach
    @endif

</div>
