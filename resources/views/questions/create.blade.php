@extends('questionBank.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @include('UMEditor.head')
    @section('container')


        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('question.create') }}
         <a data-toggle="tooltip" title="{{ trans('tips.00048') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
    </h2>
                </div>
            </div>
            <div class="panel panel-announcement">
                <div class="panel-body">

                    <div class="col-md-10 col-md-offset-1">
                       
                        <form action="{{ URL('/question-bank/'.$bank->id.'/questions/') }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">


                            <a>{{ trans('question.id') }}</a>
                            <input type="text" class="form-control" id="question_id" name="question_id"  value="">
                             {!! errors_for('question_id', $errors) !!}
                             <a>{{ trans('question.title') }}</a>
                            <textarea name="title" rows="5" class="form-control" required="required" id='myEditor' style="height:240px;"></textarea>
                            {{ trans('question.priority') }}：&nbsp;
                            <input type="radio"  name="priority" value="1" />
                            {{ trans('table.yes') }}&nbsp;&nbsp;

                            <input type="radio" checked="checked" name="priority" value="0" />
                            {{ trans('table.no') }}
                            <br>
                            <input type="radio" checked="checked" onClick="changeRadio()" name="Qtype" value="radio" />
                            {{ trans('question.radio') }}

                            <input type="radio" onClick="changeMultiple()" name="Qtype" value="multiple" />
                            {{ trans('question.mulit') }}

                            <input type="radio" onClick="changeFalse()" name="Qtype" value="false" />
                            {{ trans('question.false') }}
                            <input type="radio" onClick="changeEssay()" name="Qtype" value="essay" />
                            {{ trans('question.essay') }}

                            <div id="listNUM">
                                <input type="hidden" id="radioNUM" name="radioNUM" value="2">
                                <input type="hidden" id="mulitNUM" name="mulitNUM" value="2">
                            </div>

                            <div  style="margin-top:10px;" id="radioDiv">
                                @include('questions.radio_create')
                            </div>

                            <div  style="margin-top:10px;display:none" id="multipleDiv">
                                @include('questions.mulit_create')
                            </div>
                            <div  style="margin-top:10px;display:none" id="falseDiv">
                                @include('questions.false_create')
                            </div>

                            <button class="dm3-btn dm3-btn-medium button-large" onClick="return checkbutton()" >{{ trans('button.submit') }}</button>
                            <a  onclick="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                        </form>



                    </div>
                </div>
            </div>
        </div>

<script>
     var select_title = "{{trans('question.select_title')}}";
var delete_log = "{{trans('button.delete')}}";
var add_max = "{{trans('error.00065')}}";
var delete_limit = "{{trans('error.00066')}}";
var true_log = "{{trans('question.true')}}";
</script>



    @endsection
