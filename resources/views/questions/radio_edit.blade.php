<div  style="margin-top:10px;" id="radioDiv">
    <div id="radioPlace">
        @foreach($question->selects as $index=>$select )
            <div id="radiodiv_{{$index+1}}">
                <a> {{ trans('question.select_title') }}{{$index+1}}</a>
                <br>
                <div class="row" >
                <div class="col-md-1" style="padding-left: 0px;">
                    <input  type="radio" name="radioture" {{$select->if_true == 1 ? "checked='checked'" : ''}} value="{{$index+1}}" >{{ trans('question.true') }}
                </div>
                @if($index<2)
                    <div class="col-md-11">
                        <input type="text" name="radioselect{{$index+1}}" class="form-control" required="required" value="{{$select->title}}">
                    </div>
                @else
                    <div class="col-md-10">
                        <input type="text" name="radioselect{{$index+1}}" class="form-control" required="required" value="{{$select->title}}">
                    </div>
                    <div class="col-md-1">
                        <input type="button" value="{{ trans('button.delete') }}" class="dm3-btn dm3-btn-medium"  onclick="delRadio({{$index+1}})"/>
                    </div>
                @endif
                </div>
            </div>
        @endforeach
    </div>

    <div align="right">
        <input type="button" class="dm3-btn dm3-btn-medium button-large" id="addRadioBTN" onClick="addRadio()" value="{{ trans('button.add_select') }}"/>
    </div>
</div>
