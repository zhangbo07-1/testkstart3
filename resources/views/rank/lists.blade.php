@extends('layout.master')
@section('content')
    <br>
    <br>
    <div class="container">
        <aside class="lessons-container">
            <h3 class="lessons-title">{{ trans('table.rank') }}</h3>
            <ul class="lessons-ul">
                @foreach($ranks as $rank)
                    <li class="row">
                        <div class="col-md-6">
                            <div class="col-fixed"><span class="icon icon-10"></span></div>
                            <div class="row row-margin-5px">
                                <div class="lessons-padding ">
                                    <div><a class="lessons-course-name" >{{$rank->user->profile->realname}}</a></div>

                                    <footer class="post-meta">
                                        <span class="post-date" style="display: inline">
                                            {{ trans('user.title') }}:&nbsp;{{$rank->user->name}}&nbsp;&nbsp;
                                            <i class="fa fa-clock-o">&nbsp; {{trans('table.study_span')}}:&nbsp;</i>{{ $rank->study_span.trans('table.minute') }}
                                        </span>

                                    </footer>

                                </div>
                            </div>
                        </div>

                    </li>
                @endforeach
            </ul>
        </aside>
        {!! $ranks->render() !!}
    </div>
@endsection
