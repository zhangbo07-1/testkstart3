<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link media="all" type="text/css" rel="stylesheet" href="{{ elixir("assets/css/style.css") }}">
    {!! Html::style('assets/css/font-awesome.css') !!}
    {!! Html::style('assets/css/owl.carousel.css') !!}
    {!! Html::style('assets/css/owl.theme.css') !!}
    {!! Html::style('assets/css/owl.transitions.css') !!}
    {!! Html::style('assets/css/editor-style.css') !!}
    {!! Html::style('assets/css/woocommerce.css') !!}
    {!! Html::style('assets/css/magnific-popup.css') !!}
    {!! Html::style('assets/css/flexslider.css') !!}
    {!! Html::style('assets/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css') !!}
    {!! Html::style('css/animate.css') !!}
    {!! Html::style('assets/bower_components/fontawesome/css/font-awesome.css') !!}
    {!! Html::style('css/custom.css') !!}
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
    @yield('css')
    <link media="all" type="text/css" rel="stylesheet" href="{{ elixir("assets/css/app.css") }}">
</head>
<body>


<div class="container">
    <h1><strong>如何注册</strong></h1>
    <br>
        <p>如果已获得用户名和密码，请输入用户名和密码登入系统。</p>
        <p>如若还未有用户名和密码，可点击注册按钮。</p>
        <img class="photo" src="/img/manual/registration/image001.png">
        <p><strong>注册账号：</strong></p>
        <p>从PC的网页浏览器或者手机的网页浏览器中打开站点地址（eg: abc.knowsurface.com）；</p>
        <p>点击右上角“注册”按钮；</p>
        <p>填写用户名、邮箱、密码；</p>
         <img class="photo" src="/img/manual/registration/image002.png">
        <p>点击“注册”按钮；</p>
        <p> 如果站点需要验证邮箱，则从注册的邮箱中收到一封验证邮件；</p>
        <p> 根据邮件中的提示，点击验证链接；</p>
        <img class="photo" src="/img/manual/registration/image003.png">
        <p>提示验证成功。</p>
        <p>如若站点的注册需要审批，则需等待管理员审批后才可登入系统。</p>
        <p>如若站点的注册不需要审批，在邮件验证通过后，即可登入系统；</p>
        <p><strong>用户手册</strong>在主页 右上角点击用户名 使用帮助</p>
        <br>
        <p><strong>通过微信注册，并绑定微信账号：</strong></p>
        <p>在微信公众账号搜索“知识平面”，添加公众号（请搜索您企业实际的公众号名称）</p>
        <img class="photo" src="/img/manual/registration/image005.png">
        <p>从微信公众账号中点击菜单按钮打开站点（eg: abc.knowsurface.com）；</p>
        <p>点“注册”按钮；</p>
        <p>填写用户名、邮箱、密码；</p>
        <p>点击“注册”按钮；</p>
        <p>如果站点需要验证邮箱，则从注册的邮箱中收到一封验证邮件；</p>
        <p>根据邮件中的提示，点击验证链接；</p>
        <p>提示验证成功。</p>
        <p>如若站点的注册需要审批，则需等待管理员审批后才可登入系统。</p>
        <p>如若站点的注册不需要审批，在邮件验证通过后，即可登入系统；</p>


        <br>
        <p>如果微信公众账号是企业号或服务号，登入时点击绑定，输入注册时填写的邮箱和密码，成功绑定。</p>
        <p>如果微信公众账号是订阅号，登入时点击“记住我”。</p>

        <br>
        <p><strong>用户手册</strong>请点击右上角</p>
        <img class="photo" src="/img/manual/registration/image006.png">
        <p>按钮 点击用户名 使用帮助</p>
        <br>
        <p>祝您学习愉快！</p>

    {{-- <a class="dm3-btn dm3-btn-medium button-large" href="/register">返回注册</a>--}}
    <br>
</div>

{!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}
{{--{!! Html::script('assets/js/bootstrap.min.js') !!}--}}

{!! Html::script('assets/vendor/jquery.js') !!}
{{--{!! Html::script('assets/js/plugins.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.easing.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.appear.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.cookie.js') !!}--}}
{{--{!! Html::script('assets/vendor/bootstrap.js') !!}--}}
{{--{!! Html::script('assets/vendor/twitterjs/twitter.js') !!}--}}
{{--{!! Html::script('assets/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js') !!}--}}
{{--{!! Html::script('assets/vendor/rs-plugin/js/jquery.themepunch.revolution.js') !!}--}}
{{--{!! Html::script('assets/vendor/owl-carousel/owl.carousel.js') !!}--}}
{{--{!! Html::script('assets/vendor/circle-flip-slideshow/js/jquery.flipshow.js') !!}--}}
{{--{!! Html::script('assets/vendor/magnific-popup/magnific-popup.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.validate.js') !!}--}}
{{--{!! Html::script('assets/js/views/view.home.js') !!}--}}
{{--{!! Html::script('assets/js/theme.js') !!}--}}
{{--{!! Html::script('assets/js/custom.js') !!}--}}
{{--{!! Html::script('/js/main.min.js') !!}--}}
{!! Html::script('assets/js/modernizr.js') !!}
{!! Html::script('assets/js/base.js') !!}
{!! Html::script('assets/js/bootstrap.min.js') !!}
{!! Html::script('assets/js/jquery.flexslider.js') !!}
{!! Html::script('assets/js/jquery.magnific-popup.js') !!}
{!! Html::script('assets/js/jquery.validate.js') !!}
{!! Html::script('assets/js/owl.carousel.js') !!}
{!! Html::script('assets/js/main.js') !!}
{{--{!! Html::script('assets/js/theme.js') !!}--}}
{!! Html::script('assets/js/views/view.contact.advanced.js') !!}
{!! Html::script('assets/js/views/view.contact.js') !!}
{!! Html::script('assets/js/views/view.home.js') !!}
{!! Html::script('assets/bower_components/jquery-ui/jquery-ui.min.js') !!}
{!! Html::script('assets/bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js') !!}
@yield('subTemplateJs')
@yield('js')
{!! Html::script('assets/js/main.min.js') !!}

</body>
</html>
