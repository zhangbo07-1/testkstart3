@extends('manual.container')

@section('navbar')
    @include('manual.manager.navbar')
@endsection

@section('content')

    <h2>管理员手册</h2>

    <br>

 <img class="photo" src="/img/manual/manager/image001.png" alt="">
    <br><br><h3><strong>一、搭建你的ks系统</strong></h3>

<div id="nav1" class="entry-content">


    <br><h4><strong>第一步、如何上传logo</strong></h4>
    <div id="nav1subnav1" class="entry-content">
        <p>点击“系统管理-配置管理-图标管理管理”，点击“选择图片”从您的电脑中选择合适的logo，点击“开始上传”</p>
        <img class="photo" src="/img/manual/manager/image003.png" alt=""><br>
    </div>
    <br><h4><strong>第二步、如何更改系统设置</strong></h4>
    <div id="nav1subnav2" class="entry-content">
        <p>点击“系统管理-配置管理-系统信息”，修改企业信息并点击“确认”</p>
        <img class="photo" src="/img/manual/manager/image005.png" alt=""><br>
    </div>

    <br><h4><strong>第三步、如何设定验证</strong></h4>
    <div id="nav1subnav3" class="entry-content">
        <p>点击“系统管理-配置管理-审核管理”，以确认新注册的用户是否要经过邮件验证，或是管理员审核才能被激活。</p>
        <img class="photo" src="/img/manual/manager/image007.png" alt="">
    </div>
    <br><h4><strong>第四步、如何锁定邮箱</strong></h4>
    <div id="nav1subnav3" class="entry-content">
        <p>您可以锁定用户登入的邮箱为统一的后缀，比如公司邮箱后缀，那么必须是拥有此邮箱后缀的帐号才是有效的帐号。</p>
        <img class="photo" src="/img/manual/manager/image009.png" alt=""><br>
         <img class="photo" src="/img/manual/manager/image011.png" alt="">
    </div>


</div>
<br><br><h3><strong>二、管理你的用户账号</strong></h3>

<div id="nav2" class="entry-content">
    <br><h4><strong>第一步、如何新建组织结构/导入组织结构</strong></h4>
    <div id="nav2subnav1" class="entry-content">
        <p>添加组织结构有两种方式，一种是手动新建，另一种是以表格的形式导入。手动新建适用于单个或者少量的组织添加，表格导入适用于大量的组织添加。</p>
        <ol>
            <br><li><strong>新建组织结构</strong></li>
            <p>点击“用户管理-组织架构管理-新建组织”，在弹出的页面中填写组织结构名和组织ID，页面提示“操作成功”，就表示已经成功添加。</p>
            <img class="photo" src="/img/manual/manager/image013.png" alt=""><br>
            <br><li><strong>导入组织结构</strong></li>
            <p>在导入前需要填写组织结构表格，表格中字段至少包括“组织ID”、“组织名称”、“上级组织ID”、“操作”等。</p>
            <p>点击“用户管理-组织结构导入”，点击“选择文件”，选择填好组织结构信息的表格，点击“确定”进行上传。</p>
            <img class="photo" src="/img/manual/manager/image015.png" alt=""><br>

        </ol>
    </div>
    <br><h4><strong>第二步、如何管理组织结构</strong></h4>
    <div id="nav2subnav2" class="entry-content">
        <p>点击“用户管理-组织结构管理”，可以对已有的组织结构进行展开、关闭、新建、重命名、删除、搜索等操作。</p>
        <img class="photo" src="/img/manual/manager/image017.png" alt=""><br>
    </div>

    <br><h4><strong>第三步、如何新建用户/导入用户</strong></h4>
    <div id="nav2subnav3" class="entry-content">
        <p>添加新用户有两种方式，一种是手动新建，另一种是以表格的形式导入。手动新建适用于单个或者少量的用户添加，表格导入适用于大量的用户添加。</p>
        <ol>
            <br><li><strong>新建用户</strong></li>
            <p>点击“用户管理-用户管理-新建用户”，填写用户名、邮箱地址、密码、姓名、角色等信息，并点击“确定”。可以看到新增的用户出现在所有用户列表中，表示新用户添加成功。</p>
            <img class="photo" src="/img/manual/manager/image019.png" alt=""><br>

            <br><li><strong>导入用户</strong></li>
            <p>在导入前需要填写用户导入表格，表格中字段至少包括“操作”、“用户名”、“邮箱”、“密码”、“姓名”、“角色”、“组织”等。</p>
            <p>点击“用户管理-用户导入”，点击“选择文件”，选择填好用户信息的表格，点击“确定”进行上传。页面提示“操作成功”，表示表格导入成功。</p>
            <img class="photo" src="/img/manual/manager/image021.png" alt=""><br>
        </ol>
    </div>

    <br><h4><strong>第四步、如何管理用户信息</strong></h4>
    <div id="nav2subnav4" class="entry-content">
        <p>点击“用户管理-用户管理”，通过搜索功能找到需要修改信息的用户，点击右侧的“编辑”按钮，可以对用户的角色、状态、部门、密码等信息进行修改。如需关闭用户，则将用户状态改为“关闭”，已关闭的帐号无法再登入系统，但所有学习记录将被保留。</p>
        <img class="photo" src="/img/manual/manager/image023.png" alt=""><br>
        <p>如要修改用户密码，输入“新密码”和“确认密码”。</p>
        <img class="photo" src="/img/manual/manager/image025.png" alt=""><br>
    </div>
    
    <br><h4><strong>第五步、如何管理用户属性</strong></h4>
    <div id="nav2subnav5" class="entry-content">
        <p>点击“用户管理-用户属性管理”，可以对每个属性进行属性编辑。用户属性支持通过表格导入至系统。</p>
        <p>用户自定义属性可作为用户的标签，与组织架类似，可作为用户搜索条件的标签。通常可设置的用户属性有：职位、职级、区域等。</p>
        <strong>注意:</strong>
        <p>如果用户导入表中包含用户属性，则必须在系统中先建立或导入这些属性，否则用户导入会出错。</p>
        <img class="photo" src="/img/manual/manager/image027.png" alt=""><br>
    </div>
</div>

<br><br><h3><strong>三、管理你的课程</strong></h3>

<div id="nav3" class="entry-content">
    <br><h4><strong>第一步、如何创建目录</strong></h4>
    <div id="nav3subnav1" class="entry-content">
        <ol>
            <br><li><strong>创建目录</strong></li>
            <p>点击“系统管理-目录管理-新建目录”，输入目录名称即可</p>
            <img class="photo" src="/img/manual/manager/image029.png" alt=""><br>
            <br><li><strong>管理目录</strong></li>
            <p>点击“系统管理-目录管理”，可以对已有目录进行展开、关闭、重命名、删除、搜索等操作</p>
            <img class="photo" src="/img/manual/manager/image031.png" alt=""><br>
        </ol>
    </div>
    <br><h4><strong>第二步、如何创建并上传课程</strong></h4>
    <div id="nav3subnav2" class="entry-content">
        <ol>
            <br><li><strong>创建课程</strong></li>
            <p>点击“学习管理-课程管理-添加新课程”，填写以下信息。</p>
            <img class="photo" src="/img/manual/manager/image033.png" alt=""><br>
            <img class="photo" src="/img/manual/manager/image035.png" alt=""><br>
            <strong>注意：</strong>
            <ul>
                <li>“课程编号”为必填项，且必须为字母或数字，接受中横杠、下划线，不能包含空格和中文；</li>
                <li> “过期天数”指课程被分配后，天数达到过期天数后，用户还没有完成课程学习，那么该课程就算过期，会被列在用户的“我的培训-已过期课程”列表中，过期课程可设定为不允许再次打开学习，但可以由管理员再次分配；</li>
                <li>“用户对象”是指这个课程可被哪些用户学习，默认情况下，用户对象将继承课件创建者所在的组织架构。即，课件创建者所在的组织架构以及以下层级的所有用户，都默认拥有学习此课程的权限。课程创建者有权限更改用户对象；</li>
                <li>“目录”是指此课程的目录归类；</li>
                <li>“是否允许打开过期课程”默认是勾选状态，即过期的课程可以再次打开，如果不勾选，表示课程一旦过期，用户无法打开该课程；</li>
                <li>“是否按照以下顺序”默认是不勾选，表示如果课程中关联了考试和评估，用户只要完成了课程、考试、评估，课程状态就会变为“已完成”；如勾选，则用户需要严格按照课程-考试-评估的顺序进行学习。</li>
                <li>“是否分配给新用户”默认不勾选，如果勾选，系统中的新用户会被自动分配该课程，并收到自动分配的系统邮件。</li>
            </ul>
            <strong>注意：</strong>
            <p>可以根据属性选择满足条件的新用户来设定自动分配。设定后，加入系统的新用户凡是符合设定的条件，都将在第一次登入系统时被自动分配课程。</p>
            <img class="photo" src="/img/manual/manager/image037.png" alt=""><br>

            <br><li><strong>上传课件</strong></li>
            <p>填好课程属性的相关信息后，点击“确定”，进行课程上传。点击“选择文件”选择您的课程包。</p>
            <img class="photo" src="/img/manual/manager/image039.png" alt=""><br>
            <strong>注意：</strong>
            <p>仅支持scorm2004课件包，格式为.zip。选择课程包后，点击“上传”。上传时请不要离开页面直至页面中提示“课件上传成功”，此时课程上传完成。</p>
            <br><li><strong>更新课件</strong></li>
            <p>如需更新课件，点击“学习管理-课程管理”，点击课程后的“编辑”按钮。点击“课程更新”，并上传课程包进行内容更新。</p>
            <strong>注意：</strong>
            <p>仅支持scorm2004课件包，格式为.zip。课程更新后，当前正在学习该课程的用户，将需要从头开始学习。</p>
            <img class="photo" src="/img/manual/manager/image041.png" alt=""><br>
            <p>上传完成后，点击“课程预览”，可以预览课件。</p>
        </ol>
    </div>

     <br><h4><strong>第三步、如何发布课程</strong></h4>
    <div id="nav3subnav3" class="entry-content">
        <p>课件上传成功后，点击“发布课程”，对课程进行发布。</p>
        <strong>注意：</strong>
        <p>未发布的课程无法进行分配；</p>
        <p>老师没有发布课程的权限。</p>
        <img class="photo" src="/img/manual/manager/image043.png" alt=""><br>
    </div>

    <br><h4><strong>第四步、如何分配课程</strong></h4>
    <div id="nav3subnav4" class="entry-content">
        <p>点击“学习管理-课程管理”，点击课程后的“编辑”按钮。</p>
        <img class="photo" src="/img/manual/manager/image045.png" alt=""><br>
        <p>点击“课程分配”，勾选需要分配的目标用户。你可以通过输入关键词搜索用户，关键词包含用户名、姓名、email、微信号、部门、职位等所有用户信息中的关键词。选定用户后，点击“确认”，课程分配成功。</p>
        <strong>注意：</strong>
        <p>未发布的课程不能分配。</p>
        <img class="photo" src="/img/manual/manager/image047.png" alt=""><br>
        <p>如果需要批量的将课程退出用户学习列表，则同样在此处，在“已分配学员”列表中，勾选用户并点击“删除”按钮，即可批量退课。课程已过期的学员，也可以被再次分配该课程。在已过期学员列表中，勾选需要再次分配课程的学员，点击下方的“重新分配”按钮。该课程将再次出现在“我的培训-进行中课程”列表里。</p>
        <img class="photo" src="/img/manual/manager/image049.png" alt=""><br>
    </div>

    <br><h4><strong>第五步、如何关联教师</strong></h4>
    <div id="nav3subnav5" class="entry-content">
        <p>点击“学习管理-课程管理”，在课程右侧点击“编辑”，点击“关联教师”。在可关联教师列表中勾选需要关联的教师。教师可以对自己已关联的课程进行管理，但无权限发布课程和删除课程。</p>
        <img class="photo" src="/img/manual/manager/image051.png" alt=""><br>
    </div>
</div>

<br><br><h3><strong>四、创建你的考试</strong></h3>

<div id="nav4" class="entry-content">
    <br><h4><strong>第一步、如何新建题库</strong></h4>
    <div id="nav4subnav1" class="entry-content">
        <ol>
            <br><li><strong>新建题库</strong></li>
            <p>点击“学习管理-题库管理-添加新题库”。</p>
            <p>输入题库名并选择对象，点击“确定”</p>
            <img class="photo" src="/img/manual/manager/image053.png" alt=""><br>
            <br><li><strong>编辑/删除题库</strong></li>
            <p>点击“学习管理-题库题库”；</p>
            <p>点击题库后的“编辑”按钮可编辑题库名和用户对象；</p>
            <p>点击题库后的“删除”按钮，删除题库。</p>
            <img class="photo" src="/img/manual/manager/image055.png" alt=""><br>
            <br><li><strong>添加考试题目</strong></li>
            <p>点击“学习管理-题库管理”，点击题库后的“编辑”，点击“题目管理-添加新题目”</p>
            <img class="photo" src="/img/manual/manager/image057.png" alt=""><br>
            <p>输入题干内容，以及选项内容：</p>
            <img class="photo" src="/img/manual/manager/image059.png" alt=""><br>
            <p>系统目前支持单项选择题，多项选择题，以及是非题，请勾选题型后再输入题目内容；</p>
            <p>如果该题是强制题，在组卷时希望强制被抽到，请在是否强制处勾选“是”；</p>
            <p>如果系统给予的答案选项不够，请点击“添加选项”；</p>
            <img class="photo" src="/img/manual/manager/image061.png" alt=""><br>
            <br><li><strong>编辑/删除考试题目</strong></li>
            <p>点击“学习管理-题库管理”；</p>
            <p>点击题库的“编辑”；</p>
            <p>点击“题目管理”按钮；</p>
            <p>点击题目后的“编辑”按钮可编辑题目内容；</p>
            <p>点击题目后的“删除”按钮可删除题目。</p>
            <img class="photo" src="/img/manual/manager/image063.png" alt=""><br>
    </div>

    <br><h4><strong>第二步、如何新建考试及组卷</strong></h4>
    <div id="nav4subnav2" class="entry-content">
        <p>点击“学习管理-考试管理-添加新考试”。</p>
        <img class="photo" src="/img/manual/manager/image065.png" alt=""><br>
        <p>填写以下内容：</p>
        <img class="photo" src="/img/manual/manager/image067.png" alt=""><br>
        <img class="photo" src="/img/manual/manager/image069.png" alt=""><br>
        <strong>注意：</strong>
        <ul>
            <li>“限定时长”指考试的限制时间，到达限制时间后如未答完题，考试将自动关闭。</li>
            <li>“限定次数”指该考试允许用户参加次数；</li>
            <li>“通过分数”指该考试通过的分数，用户考试分数等于或者高于通过分数，则用户通过该考试；若用户考试分数低于通过分数，则用户没有通过该考试。</li>
            <li>“用户对象”是指这个考试可被哪些用户参与，默认情况下，用户对象将继承考试创建者所在的组织架构。即，考试创建者所在的组织架构以及以下层级的所有用户，都默认拥有参与此考试的权限。考试创建者有权限更改用户对象；</li>
            <li>“目录”是指此考试的目录归类；</li>
            <li>“是否允许关联到课程”默认是勾选状态，允许关联到课程的考试将不能被分配，不勾选的话才可以被分配。原因是考试不能被多次利用，已关联到课程的考试，不允许单独作为考试被分配，同样的，已被关联的考试不能关联到另一门课程里。</li>
        </ul>
        <p>点击“组卷”，选择题库中的题目，并给与权重，系统将按照权重来计算总分。</p>
        <p>如下图举例，计分为：领导力培训抽取1题，将得40分；产品知识抽取2题，每题30分。</p>
        <img class="photo" src="/img/manual/manager/image071.png" alt=""><br>
        <p>点击“预览”，可以预览考试题目</p>
        <img class="photo" src="/img/manual/manager/image073.png" alt=""><br>
    </div>

     <br><h4><strong>第三步、如何分配/关联考试</strong></h4>
     <div id="nav4subnav3" class="entry-content">
         <ol>
             <br><li><strong>分配考试</strong></li>
             <p>点击“学习管理-考试管理”，点击考试后的“编辑”按钮。</p>
             <p>点击“考试分配”，勾选需要分配的目标用户。你可以通过输入关键词搜索用户，关键词包含用户名、姓名、email、微信号、部门、职位等所有用户信息中的关键词。选定用户后，点击“确认”，考试分配成功。</p>
             <img class="photo" src="/img/manual/manager/image075.png" alt=""><br>
             <p>如果需要批量的将考试退出用户学习列表，则同样在此处，在“已分配学员”列表中，勾选用户并点击“删除”按钮，即可批量退出考试。</p>
             <br><li><strong>关联考试</strong></li>
             <p>如果课程被关联了考试，则用户必须完成课程，并通过考试，整个课程才算完成。</p>
             <p>要关联考试，请点击“学习管理-课程管理”</p>
             <p>点击课程后的“编辑”按钮</p>
             <p>点击“关联考试”</p>
             <p>搜索需要关联的考试，并点击“确定”</p>
             <img class="photo" src="/img/manual/manager/image077.png" alt=""><br>
     </div>

     <br><h4><strong>第四步、如何关联教师</strong></h4>
     <div id="nav4subnav4" class="entry-content">
         <p>点击“学习管理-考试管理”，在考试右侧点击“编辑”，点击“关联教师”。在可关联教师列表中勾选需要关联的教师。</p>
         <p>教师可以对自己已关联的考试进行编辑、阅卷等操作。</p>
         <img class="photo" src="/img/manual/manager/image079.png" alt=""><br>
     </div>

     <br><h4><strong>第五步、如何阅卷</strong></h4>
     <div id="nav4subnav5" class="entry-content">
         <p>如果考试中带有主观题，考试的创建者，或是被关联的教师，都有阅卷此考试的权限。</p>
         <p>点击“阅卷”按钮，可看到所有用户待批阅的题目，点击批阅进行阅卷</p>
         <img class="photo" src="/img/manual/manager/image081.png" alt=""><br>
         <p>阅卷时，将可分为0-5分的选项，阅卷者可根据用户的答题情况进行评分。系统将根据试卷设置的权重以及主观题的评分选项，得出最终的总分。</p>
         <img class="photo" src="/img/manual/manager/image083.png" alt=""><br>
         <p>如下图举例，如果领导力培训中的题目均为主观题，计分为：领导力培训抽取1题，得40分（如果主观题批阅中选择为5分，则用户此题得40分，如果主观题批阅中选择为2分，则用户此题得16分，以此类推）；产品知识抽取2题，每题30分。</p>
         <img class="photo" src="/img/manual/manager/image071.png" alt=""><br>
     </div>
</div>


<br><br><h3><strong>五、丰富你的学习项目试</strong></h3>

<div id="nav5" class="entry-content">
    <br><h4><strong>第一步、如何新建课后评估</strong></h4>
    <div id="nav5subnav1" class="entry-content">
        <ol>
            <br><li><strong>新建评估</strong></li>
            <p>点击“学习管理-评估管理-添加新评估”</p>
            <img class="photo" src="/img/manual/manager/image086.png" alt=""><br>
            <p>填写以下内容</p>
            <img class="photo" src="/img/manual/manager/image088.png" alt=""><br>
            <p>“用户对象”是指这个评估可被哪些用户参与，默认情况下，用户对象将继承评估创建者所在的组织架构。即，评估创建者所在的组织架构以及以下层级的所有用户，都默认拥有参与此评估的权限。评估创建者有权限更改用户对象；</p>
            <br><li><strong>添加评估题目</strong></li>
            <p>点击“学习管理-评估管理”</p>
            <p>点击评估后的“编辑”</p>
            <p>点击“题目管理-添加新题目”</p>
            <img class="photo" src="/img/manual/manager/image090.png" alt=""><br>
            <p>填写题目内容，点击“确定”，添加题目完成。</p>
            <img class="photo" src="/img/manual/manager/image092.png" alt=""><br>
            <br><li><strong>关联课后评估</strong></li>
            <p>如果课程被关联了评估，则用户必须完成评估，整个课程才算完成。</p>
            <p>要关联评估，请点击“学习管理-课程管理”</p>
            <p>点击课程后的“编辑”按钮</p>
            <p>点击“关联评估”</p>
            <p>搜索需要关联的评估，并点击“确定”</p>
            <img class="photo" src="/img/manual/manager/image094.png" alt=""><br>
        </ol>
    </div>

    <br><h4><strong>第二步、如何新建调研</strong></h4>
    <div id="nav5subnav2" class="entry-content">
        <ol>
            <br><li><strong>新建调研</strong></li>
            <p>点击“系统管理-调研管理-添加新调研”，</p>
            <img class="photo" src="/img/manual/manager/image096.png" alt=""><br>
            <p>填写以下内容：</p>
            <img class="photo" src="/img/manual/manager/image098.png" alt=""><br>
            <p>“用户对象”是指这个调研可被哪些用户参与，默认情况下，用户对象将继承调研创建者所在的组织架构。即，调研创建者所在的组织架构以及以下层级的所有用户，都默认拥有参与此调研的权限。调研创建者有权限更改用户对象；</p>
            <br><li><strong>添加调研题目</strong></li>
            <p>点击“系统管理-调研管理”</p>
            <p>点击调研后的“编辑”</p>
            <p>点击“题目管理-添加新题目”</p>
            <img class="photo" src="/img/manual/manager/image100.png" alt=""><br>
            <p>填写题目内容，点击“确定”，添加题目完成。</p>
            <img class="photo" src="/img/manual/manager/image102.png" alt=""><br>
        </ol>
    </div>

    <br><h4><strong>第三步、如何上传资料库内容</strong></h4>
    <div id="nav5subnav3" class="entry-content">
        <p>点击“系统管理-资料库管理”</p>
        <p>点击“添加新资料”</p>
        <p>资料可以从本机上传，也可以是网络资源，如为网络资源，则请输入URL网络链接；</p>
        <p>“用户对象”是指这个资料可被哪些用户下载，默认情况下，用户对象将继承资料上传者所在的组织架构。即，资料上传者所在的组织架构以及以下层级的所有用户，都默认拥有阅读此资料的权限。资料上传者有权限更改用户对象；</p>
        <img class="photo" src="/img/manual/manager/image104.png" alt=""><br>
    </div>
    
    
</div>

<br><br><h3><strong>六、创建你的报表</strong></h3>

<div id="nav6" class="entry-content">
    <br><h4><strong>第一步、选择报表类型</strong></h4>
    <div id="nav6subnav1" class="entry-content">
        <p><strong>用户信息</strong>：列出目前系统中的所有用户帐号的信息，可根据部门、职位、状态、属性等条件过滤用户，并可将最终结果导出为excel报表；</p>
        <p><strong>培训记录</strong>：列出目前系统中所有用户的所有学习记录，可根据不同课程、不同时间进行过滤选择，并可将结果导出为excel报表；</p>
        <p><strong>题目分析</strong>：查看所有题目的正确率；</p>
        <p><strong>评估报表</strong>：查看课程评估的统计情况；</p>
    </div>

    <br><h4><strong>第二步：选择筛选条件</strong></h4>
    <div id="nav6subnav2" class="entry-content">
    </div>
    <br><h4><strong>第三步：点击“导出EXCEL”按钮</strong></h4>
    <div id="nav6subnav3" class="entry-content">
        <img class="photo" src="/img/manual/manager/image106.png" alt=""><br>
    </div>
    
</div>


    <div class="dm3-divider-dotted"></div>




@endsection
