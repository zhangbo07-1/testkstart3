<ul class="list-unstyled" >
    <li><a href="/manuals/manager"><h3>管理员手册</h3></a></li>
    <li>
        <ul class="list-unstyled">
            <li><a href="/manuals/manager#nav1">一、搭建你的ks系统</a></li>
            <ul class="list-unstyled">
                <li><a href="/manuals/manager#nav1subnav1">第一步、如何上传logo</a></li>
                <li><a href="/manuals/manager#nav1subnav2">第二步、如何更改系统设置</a></li>
                <li><a href="/manuals/manager#nav1subnav3">第三步、如何设定验证</a></li>
                <li><a href="/manuals/manager#nav1subnav4">第四步、如何锁定邮箱</a></li>
            </ul>


            <li><a href="/manuals/manager#nav2">二、管理你的用户账号</a></li>
            <ul class="list-unstyled">
                <li><a href="/manuals/manager#nav2subnav1">第一步、如何新建组织结构/导入组织结构</a></li>
                <li><a href="/manuals/manager#nav2subnav2">第二步、如何管理组织结构</a></li>
                <li><a href="/manuals/manager#nav2subnav3">第三步、如何新建用户/导入用户</a></li>
                <li><a href="/manuals/manager#nav2subnav4">第四步、如何管理用户信息</a></li>
                <li><a href="/manuals/manager#nav2subnav5">第五步、如何管理用户属性</a></li>
            </ul>

            <li><a href="/manuals/manager#nav3">三、管理你的课程</a></li>
            <ul class="list-unstyled">
                <li><a href="/manuals/manager#nav3subnav1">第一步、如何创建目录</a></li>
                <li><a href="/manuals/manager#nav3subnav2">第二步、如何创建并上传课程</a></li>
                <li><a href="/manuals/manager#nav3subnav3">第三步、如何发布课程</a></li>
                <li><a href="/manuals/manager#nav3subnav4">第四步、如何分配课程</a></li>
                <li><a href="/manuals/manager#nav3subnav5">第五步、如何关联教师</a></li>
            </ul>

            <li><a href="/manuals/manager#nav4">四、创建你的考试</a></li>
            <ul class="list-unstyled">
                <li><a href="/manuals/manager#nav4subnav1">第一步、如何新建题库</a></li>
                <li><a href="/manuals/manager#nav4subnav2">第二步、如何新建考试及组卷</a></li>
                <li><a href="/manuals/manager#nav4subnav3">第三步、如何分配/关联考试</a></li>
                <li><a href="/manuals/manager#nav4subnav4">第四步、如何关联教师</a></li>
                <li><a href="/manuals/manager#nav4subnav5">第五步、如何阅卷</a></li>
            </ul>

             <li><a href="/manuals/manager#nav5">五、丰富你的学习项目试</a></li>
            <ul class="list-unstyled">
                <li><a href="/manuals/manager#nav5subnav1">第一步、如何新建课后评估</a></li>
                <li><a href="/manuals/manager#nav5subnav2">第二步、如何新建调研</a></li>
                <li><a href="/manuals/manager#nav5subnav3">第三步、如何上传资料库内容</a></li>
            </ul>

            <li><a href="/manuals/manager#nav6">六、创建你的报表</a></li>
            <ul class="list-unstyled">
                <li><a href="/manuals/manager#nav6subnav1">第一步、选择报表类型</a></li>
                <li><a href="/manuals/manager#nav6subnav2">第二步：选择筛选条件</a></li>
                <li><a href="/manuals/manager#nav6subnav3">第三步：点击“导出EXCEL”按钮</a></li>
            </ul>
            
        </ul>
    </li>
</ul>
