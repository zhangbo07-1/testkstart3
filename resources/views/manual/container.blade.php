<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}"/>
        <link media="all" type="text/css" rel="stylesheet" href="{{ elixir("assets/css/style.css") }}">
        {!! Html::style('assets/css/font-awesome.css') !!}
        {!! Html::style('assets/css/owl.carousel.css') !!}
        {!! Html::style('assets/css/owl.theme.css') !!}
        {!! Html::style('assets/css/owl.transitions.css') !!}
        {!! Html::style('assets/css/editor-style.css') !!}
        {!! Html::style('assets/css/woocommerce.css') !!}
        {!! Html::style('assets/css/magnific-popup.css') !!}
        {!! Html::style('assets/css/flexslider.css') !!}
        {!! Html::style('assets/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css') !!}
        {!! Html::style('css/animate.css') !!}
        {!! Html::style('assets/bower_components/fontawesome/css/font-awesome.css') !!}
        {!! Html::style('css/custom.css') !!}
        {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        @yield('css')
        <link media="all" type="text/css" rel="stylesheet" href="{{ elixir("assets/css/app.css") }}">


    </head>
    <body>


        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">知识平面-KnowSurface</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="/manuals/user/pc" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">学员手册 <span class="caret"></span></a>
                             <ul class="dropdown-menu">
                              <li><a href="/manuals/user/pc">电脑端学习</a></li>
                              <li role="separator" class="divider"></li>
                               <li><a href="/manuals/user/wechat">微信端学习</a></li>
                             </ul>
                             
                        </li>
                    </ul>
                    @can('student',Auth::user())
        @else
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="/manuals/manager" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">管理员手册 <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                               <li><a href="/manuals/manager#nav1">一、搭建你的ks系统</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/manuals/manager#nav2">二、管理你的用户账号</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/manuals/manager#nav3">三、管理你的课程</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/manuals/manager#nav4">四、创建你的考试</a></li>
                                 <li role="separator" class="divider"></li>
                                 <li><a href="/manuals/manager#nav5">五、丰富你的学习项目试</a></li>
                                  <li role="separator" class="divider"></li>
                                <li><a href="/manuals/manager#nav6">六、创建你的报表</a></li>
                                                        </ul>
                        </li>
                    </ul>
                    @endcan
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/logout">{{ trans('table.logout') }}</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/">{{ trans('table.home')}}</a></li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </nav>


        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    @yield('navbar')
                </div>
                <div class="col-sm-9 ">
                    @yield('content')
                </div>
            </div>
        </div>

        {!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}
        {{--{!! Html::script('assets/js/bootstrap.min.js') !!}--}}

        {!! Html::script('assets/vendor/jquery.js') !!}
        {{--{!! Html::script('assets/js/plugins.js') !!}--}}
        {{--{!! Html::script('assets/vendor/jquery.easing.js') !!}--}}
        {{--{!! Html::script('assets/vendor/jquery.appear.js') !!}--}}
        {{--{!! Html::script('assets/vendor/jquery.cookie.js') !!}--}}
        {{--{!! Html::script('assets/vendor/bootstrap.js') !!}--}}
        {{--{!! Html::script('assets/vendor/twitterjs/twitter.js') !!}--}}
        {{--{!! Html::script('assets/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js') !!}--}}
        {{--{!! Html::script('assets/vendor/rs-plugin/js/jquery.themepunch.revolution.js') !!}--}}
        {{--{!! Html::script('assets/vendor/owl-carousel/owl.carousel.js') !!}--}}
        {{--{!! Html::script('assets/vendor/circle-flip-slideshow/js/jquery.flipshow.js') !!}--}}
        {{--{!! Html::script('assets/vendor/magnific-popup/magnific-popup.js') !!}--}}
        {{--{!! Html::script('assets/vendor/jquery.validate.js') !!}--}}
        {{--{!! Html::script('assets/js/views/view.home.js') !!}--}}
        {{--{!! Html::script('assets/js/theme.js') !!}--}}
        {{--{!! Html::script('assets/js/custom.js') !!}--}}
        {{--{!! Html::script('/js/main.min.js') !!}--}}
        {!! Html::script('assets/js/modernizr.js') !!}
        {!! Html::script('assets/js/base.js') !!}
        {!! Html::script('assets/js/bootstrap.min.js') !!}
        {!! Html::script('assets/js/jquery.flexslider.js') !!}
        {!! Html::script('assets/js/jquery.magnific-popup.js') !!}
        {!! Html::script('assets/js/jquery.validate.js') !!}
        {!! Html::script('assets/js/owl.carousel.js') !!}
        {!! Html::script('assets/js/main.js') !!}
        {{--{!! Html::script('assets/js/theme.js') !!}--}}
        {!! Html::script('assets/js/views/view.contact.advanced.js') !!}
        {!! Html::script('assets/js/views/view.contact.js') !!}
        {!! Html::script('assets/js/views/view.home.js') !!}
        {!! Html::script('assets/bower_components/jquery-ui/jquery-ui.min.js') !!}
        {!! Html::script('assets/bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js') !!}
        @yield('subTemplateJs')
        @yield('js')
        {!! Html::script('assets/js/main.min.js') !!}

    </body>
</html>

