<ul class="list-unstyled">
    <li><a href="/manuals/user/pc"><h3>电脑端学习</h3></a></li>
    <li>
        <ul class="list-unstyled">
            <li><a href="/manuals/user/pc#nav1">第一步：浏览主页</a></li>
            <li><a href="/manuals/user/pc#nav2">第二步：完善你的信息</a></li>
            <li><a href="/manuals/user/pc#nav3">第三步：开始你的学习</a></li>
            <li><a href="/manuals/user/pc#nav4">第四步：参与讨论</a></li>
            <li><a href="/manuals/user/pc#nav5">第五步 下载资料</a></li>
        </ul>
    </li>
    <li><a href="/manuals/user/wechat"><h3>微信端学习</h3></a></li>
    <li>
        <ul class="list-unstyled">
            <li><a href="/manuals/user/wechat#nav1">第一步：浏览主页</a></li>
            <li><a href="/manuals/user/wechat#nav2">第二步：完善你的信息</a></li>
            <li><a href="/manuals/user/wechat#nav3">第三步：开始你的学习</a></li>
            <li><a href="/manuals/user/wechat#nav4">第四步：参与讨论</a></li>
            <li><a href="/manuals/user/wechat#nav5">第五步：下载资料</a></li>
        </ul>
    </li>
</ul>

