@extends('manual.container')

@section('navbar')
    @include('manual.user.navbar')
@endsection

@section('content')

    <h2><strong>电脑端学习</strong></h2>

    <br>
    <img class="photo" src="/img/manual/user/image001.png" alt=""><br>
    <br>
    <br>
    <div id="nav1" class="entry-content">
        <h4><strong>第一步：浏览主页</strong></h4>
        <ol>
    <br>
            <li><strong>使用帮助</strong></li>
            <p>你可以随时点击右上方用户名菜单下的“使用帮助”，查看系统使用教程。</p>
            <img class="photo" src="/img/manual/user/image003.png" alt=""><br>
    <br>
            <li><strong>系统主页</strong></li>
            <p>在系统主页上，你可以：</p>
            <p>浏览新闻：点击新闻标题即可浏览新闻内容。点击新闻下方的“更多”按钮，进入所有新闻列表；</p>
            <p>查看排行：查看到目前课程完成率最高的排行名单；</p>
            <p>参与系统调研问卷：点击调研标题即可进入调研。点击调研下方的“更多”按钮，进入所有调研列表。</p>
            <img class="photo" src="/img/manual/user/image005.png" alt=""><br>
        </ol>
    </div>
<br>
    <div id="nav2" class="entry-content">
        <h4><strong>第二步：完善你的信息</strong></h4>
        <p>登录系统后，点击屏幕右上角用户名下的“个人信息”按钮，在出现的页面中完善个人信息然后点击“确定”按钮。</p>
        <img class="photo" src="/img/manual/user/image007.png" alt=""><br>
        <p>你也可以在此修改密码</p>
        <img class="photo" src="/img/manual/user/image009.png" alt=""><br>
    </div>
<br>
    <div id="nav3" class="entry-content">
        <h4><strong>第三步：开始你的学习</strong></h4>
        <p>我的培训中包括进行中课程、已完成课程、已过期课程。</p>
        <p>已过期课程是指：设定了截止日期的课程，在截止日期到达时你还未完成该课程，则该课程会被列入已过期列表。</p>
        <img class="photo" src="/img/manual/user/image011.png" alt=""><br>
        <ol>
    <br>
            <li><strong>如何启动课程</strong></li>
            <p>点击“我的培训-进行中课程”，列表中是所有需要完成的课程。</p>
            <img class="photo" src="/img/manual/user/image013.png" alt=""><br>
            <p>点击课程右边的“开始”按钮，进行课程学习。注意，请浏览课程的所有页面，如果课程中有考题，请答完所有题目。</p>
            <img class="photo" src="/img/manual/user/image015.png" alt=""><br>
    <br>
            <li><strong>如何进行课程关联的考试</strong></li>
            <p>点击“我的培训-进行中课程”，如果课程右侧的“考试”按钮高亮，说明此课程被关联了考试。点击“考试”开始答题。如果课程和考试关联，则必须全部都完成，整个课程才会被记录已完成。</p>
            <img class="photo" src="/img/manual/user/image017.png" alt=""><br>
            <p>考试也可以不与课程关联，是一个独立的在线考试。点击“我的培训-进行中可曾”，点击考试右侧的开始按钮。</p>
            <img class="photo" src="/img/manual/user/image019.png" alt=""><br>
    <br>
            <li><strong>如何启动课后评估</strong></li>
            <p>点击“我的培训-进行中课程”，如果“评估”按钮高亮，说明此课程被关联了评估。点击“评估”开始答题。如果课程和评估关联，则必须全部都完成，整个课程才会被记录已完成。</p>
            <img class="photo" src="/img/manual/user/image021.png" alt=""><br>
    <br>
            <li><strong>如何查看学习记录</strong></li>
            <p>点击“我的培训-进行中课程”，点击课程右侧的更多按钮，点击“记录”按钮，查看学习记录。如果该课程有考试关联，则记录中将显示课程记录以及考试记录。如果无考试关联，将只显示课程记录。</p>
            <img class="photo" src="/img/manual/user/image023.png" alt=""><br>
    <br>
            <li><strong>如何参与课程讨论</strong></li>
            <p>点击“我的培训-进行中课程”，点击课后右侧的更多按钮，点击“讨论区”按钮，点击“新建讨论”输入标题和内容，并点击“发表文档”。</p>
            <img class="photo" src="/img/manual/user/image025.png" alt=""><br>
        </ol>
    </div>
    <br>
    <div id="nav4" class="entry-content">
        <h4><strong>第四步：参与讨论</strong></h4>
        <p>点击系统主页上主菜单的“讨论区”，点击“新建讨论”，输入标题和内容，并点击“发表文档”</p>
        <img class="photo" src="/img/manual/user/image027.png" alt=""><br>
        <img class="photo" src="/img/manual/user/image029.png" alt=""><br>
    </div>
<br>
    <div id="nav5" class="entry-content">
        <h4><strong>第五步 下载资料</strong></h4>
        <p>点击“资料库”，通过选择目录来搜索资料并进行下载。</p>
        <img class="photo" src="/img/manual/user/image031.png" alt=""><br>
    </div>



    <div class="dm3-divider-dotted"></div>


@endsection
