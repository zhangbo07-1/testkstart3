@extends('layout.master')


@section('content')
    <div class="container">
        <div class="entry-content">
            <div id="page-title">
                <br>

                <h1 class="entry-title">{{trans('table.password_reset')}}</h1>
            </div>
            <div id="auth-forms">
                @include('layout.message')
                {!! Form::open(array('url' => 'password-reset', 'method' => 'POST')) !!}
                <div class="form-group {!! errors_for_tag('email', $errors) !!}">
                    <label class="" for="email">{{trans('table.email')}}</label>
                    
                    <input id="loginEmail" style="width:100%;" class="form-control" name="email"  placeholder="{{trans('user.email')}}">
                    {!! errors_for('email', $errors) !!}
                </div>

                <div>
                    {!! Form::submit(trans('table.password_reset'),['class' => 'dm3-btn dm3-btn-medium button-large']) !!}
                    
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! Html::script('assets/js/auth.min.js') !!}
@endsection
