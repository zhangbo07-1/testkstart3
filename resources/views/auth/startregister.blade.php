@extends('layout.master')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
@section('content')
    <div class="container">
        <div class="entry-content">
            <div id="page-title">
                <br>

                <h1 class="entry-title">{{ trans('table.register') }}</h1>
            </div>
            <div id="auth-forms">

                {!! Form::open(['id' => 'registerForm', 'route' => 'register', 'class' => 'form']) !!}

                <div class="form-group  {!! errors_for_tag('name', $errors) !!}">
                    <label>{{ trans('user.title').trans('table.must') }}</label>
                    <input type="text" class="form-control" name="name" value=""  placeholder="{{ trans('user.title') }}">
                    {!! errors_for('name', $errors) !!}
                </div>



                <div class="form-group  {!! errors_for_tag('realname', $errors) !!}">
                    <label>{{ trans('user.real_name').trans('table.must') }}</label>
                    <input type="text" class="form-control" name="realname" value=""  placeholder="{{ trans('user.real_name') }}">
                    {!! errors_for('realname', $errors) !!}
                </div>

                <div class="form-group  {!! errors_for_tag('email', $errors) !!}">
                    <label class="" for="exampleInputAmount">{{ trans('table.email').trans('table.must')  }}</label>
                    <input id="registerEmail" class="form-control" name="email" type="text" placeholder="{{ trans('user.email') }}">
                    {!! errors_for('email', $errors) !!}
                </div>

                <div class="form-group  {!! errors_for_tag('tel', $errors) !!}">
                    <label>{{ trans('user.tel') }}</label>
                    <input type="text" class="form-control" name="tel" value=""  placeholder="{{ trans('user.tel') }}">
                    {!! errors_for('tel', $errors) !!}
                </div>

                <div class="form-group  {!! errors_for_tag('address', $errors) !!}">
                    <label>{{ trans('user.address') }}</label>
                    <input type="text" class="form-control" name="address" value=""  placeholder="{{ trans('user.address') }}">
                    {!! errors_for('address', $errors) !!}
                </div>
                <div class="form-group  {!! errors_for_tag('company', $errors) !!}">
                    <label>{{ trans('user.company').trans('table.must')}}</label>
                    <input type="text" class="form-control" name="company" value=""  placeholder="{{ trans('user.company_tag') }}">
                    {!! errors_for('company', $errors) !!}
                </div>

                <div class="form-group  {!! errors_for_tag('favorite', $errors) !!}">
                    <label>{{ trans('user.favorite') }}</label>
                    <input type="text" class="form-control" name="favorite" value="" placeholder="{{ trans('user.favorite') }}">
                    {!! errors_for('favorite', $errors) !!}
                </div>


                <div class="form-group {!! errors_for_tag('password', $errors) !!}">
                    <label>{{ trans('user.pwd').trans('table.must')  }}</label>
                    {!! Form::password('password', ['class'=> 'form-control','placeholder'=>trans('table.password_limit')]) !!}
                    {!! errors_for('password', $errors) !!}
                </div>

                <div class="form-group {!! errors_for_tag('password', $errors) !!}">
                    <label>{{ trans('user.confirmation_password').trans('table.must')  }}</label>
                    {!! Form::password('password_confirmation', ['class'=> 'form-control','placeholder'=>trans('table.password_limit')]) !!}
                </div>

                <div>
                    {!! Form::submit(trans('table.register'),['class' => 'dm3-btn dm3-btn-medium button-large']) !!}

                    <a href="/manuals/registration" target="_blank">{{ trans('table.any_question') }}</a>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! Html::script('assets/js/auth.min.js') !!}
@endsection


