@extends('layout.master')


@section('content')
    <div class="container">
        <div class="entry-content">
            <div id="page-title">
                <br>

                <h1 class="entry-title">{{trans('table.login')}}</h1>
            </div>
            <div id="auth-forms">
    @include('layout.message')

                {!! Form::open(['id' => 'loginForm' ,'route' => 'login', 'class' => 'input']) !!}
                <div>



                    @if($info->user_email)
                         <label class="" for="exampleInputAmount">{{trans('table.email')}}</label>
                        @if(count($emailSuffixes))

                            <div class="input-group">
                                <input class="form-control"  id="loginEmailPrefix" type="text" value="" placeholder="{{trans('user.email')}}">
                                <span class="input-group-addon">@</span>
                                {!! Form::select('emailType',$emailSuffixes ,null,array('id' => 'loginEmailDomain','class'=>'form-control no-radius-left-top')) !!}
                            </div>
                            {!! errors_for('email', $errors) !!}
                            <input id="loginEmail" class="form-control" name="email" type="hidden" value="">
                        @else
                            {!! errors_for('email', $errors) !!}
                            <input id="loginEmail" style="width:100%;" class="form-control" name="email"  placeholder="{{trans('user.email')}}">
                        @endif
                    @else
                         <label class="" for="exampleInputAmount">{{trans('user.title')}}</label>
                         {!! errors_for('name', $errors) !!}
                            <input id="loginEmail" style="width:100%;" class="form-control" name="name"  placeholder="{{trans('user.title')}}">
                    @endif

                    {{--
                    <input id="loginEmail" style="width:100%;" class="form-control" name="email"  placeholder="{{trans('user.email')}}">
                    {!! errors_for('email', $errors) !!}
                    --}}
                </div>


                <div>
                    <label>{{trans('table.password')}}</label>
                    {!! Form::password('password', ['class'=> 'form-control','placeholder' => trans('table.password_limit')]) !!}
                    {!! errors_for('password', $errors) !!}
                </div>

                <div class="checkbox">
                    <label><input name="remember" type="checkbox"> {{trans('table.remember_me')}}</label>
                </div>
                <div>
                    {!! Form::submit(trans('table.login'),['class' => 'dm3-btn dm3-btn-medium button-large']) !!}
                    <a class="dm3-btn dm3-btn-medium button-large" href="/password-reset">{{trans('table.password_reset')}}</a>
                    @if(get_register())
                        <a class="dm3-btn dm3-btn-medium button-large" href="/register">{{ trans('table.register') }}</a>
                    @endif
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! Html::script('assets/js/auth.min.js') !!}
@endsection
