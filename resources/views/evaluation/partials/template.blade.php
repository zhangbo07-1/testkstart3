@extends('layout.master')
@section('js')
{!! Html::script('assets/js/survey.min.js') !!}
    @append
@section('content')
    @include('evaluation.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection