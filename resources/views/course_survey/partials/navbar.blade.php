<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    <li id="editNav">
                        <a href="/course-survey/{{$survey->id}}/edit">
                            <i class="fa fa-home"></i><span>{{ trans('survey.manage') }}</span>
                        </a>
                    </li>
                    <li id="itemsNav">
                        <a href="/course-survey/{{$survey->id}}/items">
                            <i class="fa fa-pencil"></i><span>{{ trans('question.manage') }}</span>
                        </a>
                    </li>
                    <li id="messageNav">
                        <a href="/course-survey/{{$survey->id}}">
                            <i class="fa fa-eye"></i><span>{{ trans('button.preview') }}</span>
                        </a>
                    </li>
                    @if($survey->pushed == 0)
                        <li id="pushNav">
                            <a href="/survey/{{$survey->id}}/push">
                                <i class="fa fa-save"></i><span>{{ trans('button.push') }}</span>
                            </a>
                        </li>
                    @endif

                    @if(count($survey->surveyResults))

                        <li id="resultNav">
                            <a href="/course-survey/{{$survey->id}}/result">
                                <i class="fa fa-question-circle"></i><span>{{ trans('button.result') }}</span>
                            </a>
                        </li>

                    @endif
                    <li id="messageNav">
                        <a href="/course-survey">
                            <i class="fa fa-reply"></i><span>{{ trans('button.return') }}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
