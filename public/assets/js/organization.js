var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
var node_id;
var if_all;
$('#jstree_div').jstree({
    'core' : {
	'data' : {
	    'url': "/organization/show?operation=get_node",
            'type': 'POST',
            'dataType': 'JSON',
	    'data' : function (node) {
                return {  '_token': CSRF_TOKEN,'id' : node.id ,'NUM' : true,'Status' : if_all};
            }
	},

	'check_callback' : function(o, n, p, i, m) {
	    if(m && m.dnd && m.pos !== 'i') { return false; }
	    return true;
	},
	'themes' : {
	    'responsive' : false,
	    'variant' : 'middle',
	    'stripes' : true
	}
    },
    'types' : {
        'default' : {
            "icon" : "glyphicon glyphicon-user"
        }
    },
    'unique' : {
	'duplicate' : function (name, counter) {
	    return name + ' ' + counter;
	}
    },

    'plugins' : ['state','types','unique','search','wholerow']
});
$('#jstree_div').on('create_node.jstree', function (e, data)
    {
        $.ajax({
            url: "/organization/show?operation=create_node",
            type: 'POST',
            data:
            {
                _token: CSRF_TOKEN,
                type: data.node.type,
                id:  data.node.parent,
                text: data.node.text
            },
            dataType: 'JSON',
            success: function (d)
            {
                data.instance.set_id(data.node, d.id);
            }
        });
    })
		.on('rename_node.jstree', function (e, data)
                    {
		        $.ajax({
                            url: "/organization/show?operation=rename_node",
                            type: 'POST',
                            data:
                            {
                                _token: CSRF_TOKEN,
                                id:  data.node.id,
                                text: data.text
                            },
                            dataType: 'JSON',
                            success: function (d)
                            {
                                data.instance.set_id(data.node, d.id);
                            }
                        });

                        
		    })
    
                .on('delete_node.jstree', function (e, data)
                    {
                        $.ajax({
                            url: "/organization/show?operation=delete_node",
                            type: 'POST',
                            data:
                            {
                                _token: CSRF_TOKEN,
                                id:  data.node.id
                            },
                            dataType: 'JSON',
                            success: function (d)
                            {
                                if(d.status != 'OK')
                                {
                                    data.instance.refresh();
                                    showSuccessMessage(d.error);
                                }
                            }
                        });
                    });
;


var to = false;

function find_node()
{
    
    if(to)
    {
        clearTimeout(to);
    }
    to = setTimeout(function ()
        {
            var v = $('#searchWord').val();
            $('#jstree_div').jstree(true).search(v);
        }, 1000);
}

function search_node()
{
    
    $('#jstree_div').jstree(true).load_all(find_node());
    
    
}

function selectAll()
{
    
    if( $("input[id='if_all']:checkbox").is(':checked'))
    {
        if_all = 0;

    }
    else
        if_all = 1;
    $('#jstree_div').jstree(true).refresh();
    
}

function creatNode(o)
{
    var iWidth=800; //弹出窗口的宽度;
    var iHeight=600; //弹出窗口的高度;
    var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
    var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;
    //设置模式窗口的一些状态值
    var windowStatus = "height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft;
    //在模式窗口中打开的页面
    var url = "/organization/"+o+"/create";
    //将模式窗口返回的值临时保存
    window.open(url,"noMenuWindowName",windowStatus);

}

function editNode(o)
{
    var iWidth=800; //弹出窗口的宽度;
    var iHeight=600; //弹出窗口的高度;
    var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
    var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;
    //设置模式窗口的一些状态值
    var windowStatus = "height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft;
    //在模式窗口中打开的页面
    var url = "/organization/"+o+"/edit";
    //将模式窗口返回的值临时保存
    window.open(url,"noMenuWindowName",windowStatus);

}

function renameNode(o)
{
    var iWidth=800; //弹出窗口的宽度;
    var iHeight=600; //弹出窗口的高度;
    var iTop = (window.screen.availHeight-30-iHeight)/2; //获得窗口的垂直位置;
    var iLeft = (window.screen.availWidth-10-iWidth)/2; //获得窗口的水平位置;
    //设置模式窗口的一些状态值
    var windowStatus = "height="+iHeight+", width="+iWidth+", top="+iTop+", left="+iLeft;
    //在模式窗口中打开的页面
    var url = "/organization/"+o+"/edit";
    //将模式窗口返回的值临时保存
    window.open(url,"noMenuWindowName",windowStatus);

}
function create_node()
{
    var ref = $('#jstree_div').jstree(true),
        sel = ref.get_selected();
    if(!sel.length)
    {
        return false;
    }
    sel = sel[0];
    creatNode(sel);
};
function rename_node()
{
    var ref = $('#jstree_div').jstree(true),
        sel = ref.get_selected();
    if(!sel.length)
    {
        return false;
    }
    sel = sel[0];
    editNode(sel);
};
function delete_node()
{
    var ref = $('#jstree_div').jstree(true),
        sel = ref.get_selected(true);
    if(!sel.length)
    {
        return false;
    }
    if(sel[0].text.indexOf('(0)') < 0)
    {
        showSuccessMessage(delete_error);
        return false;
    }
    
    if(!confirm(delete_log)) {

        return false;
    }
    ref.delete_node(sel);
};


function open_all()
{
    var ref = $('#jstree_div').jstree(true);
    ref.open_all();
};
function close_all()
{
    var ref = $('#jstree_div').jstree(true);
    ref.close_all();
};
