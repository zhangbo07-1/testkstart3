var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
var SurveyTemplate = Handlebars.compile($("#SurveyTemplate").html());

function getSurveys(userType, url) {
    var url = '/api/course-survey/distribute';
  $.get(url, {course_id:course_id,search_word: $("#searchWord").val()},
        function (responseData) {
            console.log(responseData);
            $("#SurveysForm").html(SurveyTemplate(responseData));
            if (responseData.last_page != 1) {
                $('#SurveysForm-Pagination').bootpag({
                    total: responseData.last_page,
                    page: responseData.current_page,
                    maxVisible: 10,
                    leaps: true,
                    firstLastUse: false,
                    wrapClass: 'pagination',
                    activeClass: 'active',
                    disabledClass: 'disabled',
                    nextClass: 'next',
                    prevClass: 'prev',
                    lastClass: 'last',
                    firstClass: 'first'
                }).on("page", function (event, num) {
                    var visit_url = url + "/?page= " + num;
                    $.get(visit_url, {search_word: $("#searchWord").val()},
                        function (responseData) {
                            console.log(SurveyTemplate(responseData));
                            $("#SurveysForm").html(SurveyTemplate(responseData));
                        });
                });
            }
        });
}




function setSurvey(id) {

    $.ajax({
        url: '/course-survey/'+id+'/distribute',
        type: 'POST',
        data: {
            _token: CSRF_TOKEN,
            survey_id: id,
            course_id: course_id
        },
        dataType: 'JSON',
        success: function (data) {
            if (data.result == true) {
              showSuccessMessage(data.message);
                getSurveys();
                $("#selectedSurveyTitle").html(data.record_title);
            }
        }
    });
}

function deleteSurvey() {

    $.ajax({
        url: '/course-survey/deallocate',
        type: 'POST',
        data: {
            _token: CSRF_TOKEN,
            course_id: course_id
        },
        dataType: 'JSON',
        success: function (data) {
            if (data.result == true) {
              showSuccessMessage(data.message);
                getSurveys();
                $("#selectedSurveyTitle").html(data.record_title);
            }
        }
    });
}


function getChooseResult() {
    var selectedVal = "";
    var selected = $("#SurveysForm input[type='radio']:checked");
    if (selected.length > 0) {
        selectedVal = selected.val();
        console.log(selectedVal);
        setSurvey(selectedVal);
    }
}

$(document).ready(function () {
    getSurveys();
    $("#courseSearchButton").click(function () {
        getSurveys();
    })

    $("#chooseSurveyButton").click(function () {
        getChooseResult();
    })
  $("#relieveSurveyButton").click(function () {
        deleteSurvey();
    })
})

$('#searchWord').keypress(function (e) {
    var key = e.which;
    if (key == 13)  // the enter key code
    {
        getSurveys();
        return false;
    }
});


//# sourceMappingURL=course_survey.min.js.map
