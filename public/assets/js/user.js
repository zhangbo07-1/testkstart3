var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
var node_id;
$('#jstree_div').jstree({
    'core' : {
	'data' : {
	    'url': "/organization/show?operation=get_node",
            'type': 'GET',
            'dataType': 'JSON',
	    'data' : function (node) {
                return {  '_token': CSRF_TOKEN,'id' : node.id };
            }
	},
        

	'check_callback' : function(o, n, p, i, m) {
	    if(m && m.dnd && m.pos !== 'i') { return false; }
	    return true;
	},
	'themes' : {
	    'responsive' : false,
	    'variant' : 'small',
	    'stripes' : true
	}
    },					
    'unique' : {
	'duplicate' : function (name, counter) {
	    return name + ' ' + counter;
	}
    },

  'plugins' : ['state','sort','types','unique','search','wholerow']
})
  .on('select_node.jstree', function (e, data) {
    $("#usersForm-Pagination").html('');
                    if(data && data.selected && data.selected.length)
                    {
                         $.ajax({
                        url: "/users/department",
                        type: 'GET',
                        data: {
                            _token: CSRF_TOKEN,
                            id:  data.node.id
                        },
                        dataType: 'JSON',
                             success: function (usersData) {
                               $("#usersForm").html(Handlebars.compile($("#userTemplate").html())(usersData));
                               
                               if (usersData.last_page != 1) {
                                 $('#usersForm-Pagination').bootpag({
                                   total: usersData.last_page,
                                   page: usersData.current_page,
                                   maxVisible: 10,
                                   leaps: true,
                                   firstLastUse:false,
                                   wrapClass: 'pagination',
                                   activeClass: 'active',
                                   disabledClass: 'disabled',
                                   nextClass: 'next',
                                   prevClass: 'prev',
                                   lastClass: 'last',
                                   firstClass: 'first'
                                 }).on("page", function (event, num) {
                                   var visit_url = "/users/department/?page= " + num;
                                   $.get(visit_url, {id: data.node.id},
                                         function (usersData) {
                                           $("#usersForm").html(Handlebars.compile($("#userTemplate").html())(usersData));
                                         });
                                 });
                               }
                        }
                    });
                    }
    });

