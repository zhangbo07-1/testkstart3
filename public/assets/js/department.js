var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
var department_id =  window.opener.document.getElementById("department_id").value;
var node_id;
$('#jstree_department_div').jstree({
    'core' : {
        "multiple":false,
        'data' : {
            'url': "/organization/show?operation=get_node",
            'type': 'POST',
            'dataType': 'JSON',
            'data' : function (node) {
                return {  '_token': CSRF_TOKEN,'id' : node.id ,'manage' :department_id};
            }
        },

        'check_callback' : function(o, n, p, i, m) {
            if(m && m.dnd && m.pos !== 'i') { return false; }
            return true;
        },
        'themes' : {
            'responsive' : false,
            'variant' : 'middle',
            'stripes' : true
        }
    },
    'types' : {
        'default' : {
            "icon" : "glyphicon glyphicon-user"
        }
    },
    "checkbox" : {
        "keep_selected_style" : false,
        "tree_state" : false
    },
    
  'plugins' : ['types','search','checkbox','wholerow']
});


function getSelect()
{
  var names = "";
  var list = "";
    var nodes = $("#jstree_department_div").jstree("get_top_checked");
    $.each(nodes, function(i, n) {
        var node = $('#jstree_department_div').jstree("get_node", n);
      names += node.text+",";
      list+= n;

    });
  $.ajax({
        url: "/organization/check",
    type: 'POST',
    data: {
      nodes: list,
      _token: CSRF_TOKEN
      
    },
        dataType: 'JSON',
    success: function (data) { 
            if (data.status == false) {
              showSuccessMessage(data.message);
            }
      else
      {
        window.opener.document.getElementById("department_id").value = nodes;
  window.opener.document.getElementById("department_name").innerHTML = names;
  if( window.opener.document.getElementById("manage_name"))
  {
    window.opener.document.getElementById("manage_name").innerHTML = names;
  }
         if( window.opener.document.getElementById("manage_id"))
        {
          window.opener.document.getElementById("manage_id").value = nodes;
        }
  window.opener = null;
   window.open("about:blank","_self").close();
      }
        }
  })
 
   // window.close();
}
var to = false;
function find_node()
{
     
    if(to)
    {
        clearTimeout(to);
    }
    to = setTimeout(function ()
        {
            var v = $('#searchWord').val();
            $('#jstree_department_div').jstree(true).search(v);
        }, 1000);
}
function search_node()
{
  
  $('#jstree_department_div').jstree(true).load_all(find_node());
  
 
}


function open_all()
{
  var ref = $('#jstree_department_div').jstree(true);
  ref.open_all();
}
function close_all()
{
  var ref = $('#jstree_department_div').jstree(true);
  ref.close_all();
}
