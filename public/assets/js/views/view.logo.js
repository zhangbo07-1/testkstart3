$field = $("input[type='file']");
$field.uploadify({//配置uploadify
                  'buttonText': '选择图片',  //选择按钮显示的字符
                  'swf'       : '/uploadify/uploadify.swf', //swf文件的位置
                  'uploader'  : '/uploadify/upload.php', //上传的接收者
                  'cancelImg' : '/uploadify/uploadify-cancel.png',
                  'folder'    : '/uploads',//上传图片的存放地址
                  'auto'      : false,    //选择图片后是否自动上传
                  'multi'     : false,   //是否允许同时选择多个(false一次只允许选中一张图片)
                  'method'    : 'post',
                  'queueSizeLimit' : 1,//最多能选择加入的文件数量
                  'fileTypeExts': '*.png', //允许的后缀
                  'fileTypeDesc': 'Image Files', //允许的格式，详见文档
                  'height'	   : 20,
                  'width'	   : 120,
                  'fileSizeLimit' : '1024KB',
                  'onUploadSuccess' : function(file, data, response) {
		      var msg = $.parseJSON(data);
		      if( msg.result_code == 1 ){
			  $("#img").val( msg.result_des );
			  $("#target").attr("src",msg.result_des);
			  $(".preview").attr("src",msg.result_des);
			  $('#target').Jcrop({
                              // start off with jcrop-light class
                              bgOpacity: 0.5,
                              bgColor: 'white',
                              addClass: 'jcrop-light',
                              onSelect: updateCoords,
                              aspectRatio:'6.8'
                          },function(){
                              api = this;
                              api.setSelect([130,65,130+191,65+28]);
                              api.setOptions({ bgFade: true });
                              api.ui.selection.addClass('jcrop-selection');
                          });
			  $(".imgchoose").css('display','block');
			  $("#avatar_submit").css('display','block');
		      } else {
			alert(msg.result_code);
		      }
		  },
		  'onClearQueue' : function(queueItemCount) {
                      alert( $('#img1') );
                  },
		  'onCancel' : function(file) {
                      alert('The file ' + file.name + ' was cancelled.');
                  }
});
//头像裁剪
var jcrop_api, boundx, boundy;

function updateCoords(c)
{
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
};
function checkCoords()
{
    if (parseInt($('#w').val())) return true;
    alert('请选择图片上合适的区域');
    return false;
};


$("#avatar_submit").click(function(){
    var img = $("#img").val();
    var x = $("#x").val();
    var y = $("#y").val();
    var w = $("#w").val();
    var h = $("#h").val();
    if( checkCoords() ){
	$.ajax({
	    type: "POST",
	    url: "../../uploadify/resize.php",
	    data: {"img":img,"x":x,"y":y,"w":w,"h":h},
	    dataType: "json",
	    success: function(msg){
		if( msg.result_code == 1 ){
		    $('html,body').animate({scrollTop:$('#avatar').offset().top-150},1000,'swing',function(){
location.reload() ;
			$('#avatar_msg').show();

		    });
		} else {
		    alert(msg.result_code);
		}
	    }
	});
    }
});

