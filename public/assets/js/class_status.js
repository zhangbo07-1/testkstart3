function checkButton() {
    $("#classRecordButton").change(function () {
        if ($(this).is(":checked")) {
            $("input.classRecord:checkbox:not(:checked)").prop('checked', true);
        } else {
            $("input.classRecord:checkbox:checked").prop('checked', false);
        }
    });


}
var distribute = {
  course_id: course_id,
  class_id: class_id,
  classRecordUrl: '/api/classes/'+class_id+'/record',
    classRecordTemplate: Handlebars.compile($("#classRecordTemplate").html())
}

function getUsers(userType, url) {
    url = typeof url !== 'undefined' ? url : distribute[userType + "Url"];
  $.get(url, {findByUsername: $("#searchWord").val(),attribute1: $("#attribute1").val(), attribute2: $("#attribute2").val(),attribute3: $("#attribute3").val(),attribute4: $("#attribute4").val(),attribute5: $("#attribute5").val(), manage_id:$("#manage_id").val(),startRegister:$("#startDate").val(), endRegister:$("#endDate").val(),course_id: distribute.course_id},
        function (responseData) {
          $("#" + userType + "Form").html(distribute[userType + 'Template'](responseData));
            if (responseData.last_page != 1) {
                $('#' + userType + 'Form-Pagination').bootpag({
                    total: responseData.last_page,
                    page: responseData.current_page,
                    maxVisible: 10,
                    leaps: true,
                    firstLastUse: false,
                    wrapClass: 'pagination',
                    activeClass: 'active',
                    disabledClass: 'disabled',
                    nextClass: 'next',
                    prevClass: 'prev',
                    lastClass: 'last',
                    firstClass: 'first'
                }).on("page", function (event, num) {
                    var visit_url = url + "/?page= " + num;
                    $.get(visit_url, {search_word: $("#searchWord").val(), course_id: distribute.course_id},
                        function (responseData) {
                            $("#" + userType + "Form").html(distribute[userType + "Template"](responseData));
                        });
                });
            }
        });
}



function refreshUsers() {
    initialAllUsersTable();
    getUsers("classRecord");
}


$("#searchButton").click(function () {
    refreshUsers();
})

$(document).ready(function () {
    getUsers("classRecord");
    checkButton();
})

var opts = {
    lines: 15 // The number of lines to draw
    , length: 28 // The length of each line
    , width: 14 // The line thickness
    , radius: 42 // The radius of the inner circle
    , scale: 1 // Scales overall size of the spinner
    , corners: 1 // Corner roundness (0..1)
    , color: '#000' // #rgb or #rrggbb or array of colors
    , opacity: 0.25 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 60 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '50%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'absolute' // Element positioning
}
var spinner = new Spinner(opts).spin();

function initCheckButtons(){
    $("#classRecordButton").prop('checked',false);
}

function initialAllUsersTable() {
    $("#classRecordForm").html(spinner.el);
    $("#classRecordForm-Pagination").html('');
}


var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
function classOver() {
    var selected = [];
    var is_all = false;

    if ($('input#classRecordButton').is(':checked')) {
        is_all = true;
    } else {
        $('#classRecordForm input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
    }
    
    $.ajax({
        url: "/api/course/"+course_id+"/classes/"+class_id+"/beOver",
        type: 'POST',
        data: {
          _token: CSRF_TOKEN,
            findByUsername: $("#searchWord").val(),
            attribute1: $("#attribute1").val(),
            attribute2: $("#attribute2").val(),
            attribute3: $("#attribute3").val(),
            attribute4: $("#attribute4").val(),
            attribute5: $("#attribute5").val(),
            manage_id:$("#manage_id").val(),
            startRegister:$("#startDate").val(),
            endRegister:$("#endDate").val(),
            user_id: selected,
          course_id: course_id,
          class_id:class_id,
            is_all: is_all
        },
        dataType: 'JSON',
        success: function (data) {
            if (data.result == true) {
                initCheckButtons();
                showSuccessMessage(data.message);
                refreshUsers();
            }
        }
    })
}

function classNotOver() {
    var selected = [];
    var is_all = false;

    if ($('input#classRecordButton').is(':checked')) {
        is_all = true;
    } else {
        $('#classRecordForm input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
    }

    selected.push($(this).attr('value'));

    $.ajax({
         url: "/api/course/"+course_id+"/classes/"+class_id+"/beNotOver",
        type: 'post',
        data: {
            _token: CSRF_TOKEN,
            findByUsername: $("#searchWord").val(),
            attribute1: $("#attribute1").val(),
            attribute2: $("#attribute2").val(),
            attribute3: $("#attribute3").val(),
            attribute4: $("#attribute4").val(),
            attribute5: $("#attribute5").val(),
            manage_id:$("#manage_id").val(),
            startRegister:$("#startDate").val(),
            endRegister:$("#endDate").val(),
            user_id: selected,
            course_id: course_id,
            is_all: is_all
        },
        dataType: 'JSON',
        success: function (data) {

            if(data.result == true)
            {
                initCheckButtons();
                showSuccessMessage(data.message);
                refreshUsers();
            }

        }
    })
}


$("#recordOverSubmit").click(function () {
  classOver();
  refreshUsers();
})

$("#recordNotOverSubmit").click(function () {
  classNotOver();
  refreshUsers();
})


$('#searchWord').keypress(function (e) {
    var key = e.which;
    if (key == 13)  // the enter key code
    {
        refreshUsers();
        return false;
    }
});
//# sourceMappingURL=distribute.js.map


Handlebars.registerHelper('class_status', function() {
  return new Handlebars.SafeString(
    this.process == 1 ?class_status1:class_status2);
  
});
Handlebars.registerHelper('lesson_status', function() {
  if(this.process == 3)
    return new Handlebars.SafeString(lesson_status3);
  else if(this.process == 2)
    return new Handlebars.SafeString(lesson_status2);
  else
    return new Handlebars.SafeString(lesson_status1);
  
});



Handlebars.registerHelper('list_status', function(items, options) {
  var out = "";
  if(this.class_signs.length == 0)
  {
    for (var i=0;i<this.course_class.lessons.length;i++)
    {
      out +="<td></td>";
      
    }
   
  }
  else
  {
    for (var i=0;i<this.course_class.lessons.length;i++)
    {
      var mun = 0;
      for (var j=0;j<this.class_signs.length;j++)
      {
        if(this.course_class.lessons[i].id == this.class_signs[j].lesson_id)
        {
          mun ++;
          if(this.class_signs[j].process == 3)
            out +="<td>"+lesson_status3+"</td>";
          else if(this.class_signs[j].process == 2)
            out +="<td>"+lesson_status2+"</td>";
          else
            out +="<td>"+lesson_status1+"</td>";
        }
      }
      if(mun == 0)
        out +="<td></td>";
      
    }
  }
  return new Handlebars.SafeString(out);
 
}); 
