
var LMSDebug={Start:function(){this.IsOn=true
if(window.console&&console.log)for(var iM=0;iM<this.Queue.length;iM++)console.log(this.Queue[iM])
return"[Dbg on]"},Errors:function(){var sErr=LMSInterface.API.GetLastError(),sErrStr="Unknown error",sErrDiag="No diagnostic"
if(sErr!=0){try{sErrStr=LMSInterface.API.GetErrorString(sErr);}catch(e){}
try{sErrDiag=LMSInterface.API.GetDiagnostic(sErr);}catch(e){}
this.Log("**Error**","LMS error details: #"+sErr+", "+sErrStr+", "+sErrDiag)}
return false},Log:function(op,msg,err){var sMsg="["+op+"] "+msg
if(this.IsOn){if(window.console&&console.log)console.log(sMsg)}else{this.Queue.push(sMsg)}
if(err)this.Errors()},Queue:[]}
var LMSInterface={API:null,Status:"unknown",StartTime:new Date().getTime(),Init:function(){var oWin=top,oAPIWin=false
while(true){var oAPIWin=this.SearchAPI(oWin)
if(!oAPIWin&&oWin.opener){try{oWin=oWin.opener.top
oWin.opener}catch(oErr){break}}
else break}
this.API=oAPIWin.API_1484_11||false
this.Track=oConfig.firstChild.getAttribute("tracking")=="true"&&location.href.indexOf("http")==0
if(this.API){LMSDebug.Log("Info","SCORM 2004 API found, calling initialize")
try{var bInit=this.API.Initialize("")
LMSDebug.Log("Initialize","Result: "+bInit,true)
this.RegisterVariableInterface()
window.API_1484_11={GetLastError:function(){return LMSInterface.API.LastError();},GetErrorString:function(sErr){return LMSInterface.API.ErrorDesc(sErr);},GetDiagnostic:function(sErr){return LMSInterface.API.ErrorDiag(sErr);},GetValue:function(sProp){return LMSInterface.API.Get(sProp);},SetValue:function(sProp,sValue){return LMSInterface.API.Set(sProp,sValue);},Initialize:function(){return true;},Terminate:function(){return true;}}
try{if(parent&&!parent.API_1484_11)parent.API_1484_11=window.API_1484_11;}catch(e){}}catch(oErr){LMSDebug.Log("Error","Initialize threw an exception, turning off SCORM")
alert("A student tracking interface was detected (SCORM), but did not properly respond. This session will not be tracked. Please contact an administrator.")
this.API=null}}else{LMSDebug.Log("Info","SCORM API not found")}
if(this.API||this.Track){this.SetDefaults()
onbeforeunload=onunload=this.Dispose}},SearchAPI:function(oWin){try{if(oWin.API_1484_11)return oWin;}catch(oErr){}
try{for(var iFrame=0;iFrame<oWin.frames.length;iFrame++){try{var oFrame=this.SearchAPI(oWin.frames[iFrame])
if(oFrame)return oFrame}catch(oErr){}}}catch(oErr){}
return false},RegisterVariableInterface:function(){VarInterface.Types.Add("Scorm",LMSInterface.GetVariable)
VarInterface.Add("first name","Custom",function(){return getNamePart("f","cmi.learner_name");})
VarInterface.Add("last name","Custom",function(){return getNamePart("l","cmi.learner_name");})
VarInterface.Add("middle name","Custom",function(){return getNamePart("m","cmi.learner_name");})
VarInterface.Add("total hours","Custom",function(){return getTimePart("h","cmi.total_time",LMSInterface.GetSessionTime())})
VarInterface.Add("total minutes","Custom",function(){return getTimePart("m","cmi.total_time",LMSInterface.GetSessionTime())})
VarInterface.Add("current hours","Custom",function(){return getTimePart("h",LMSInterface.GetSessionTime())})
VarInterface.Add("current minutes","Custom",function(){return getTimePart("m",LMSInterface.GetSessionTime())})
VarInterface.Add("global score","Scorm","cmi.score.raw")
VarInterface.Add("status","Scorm","cmi.completion_status")
function getNamePart(sPart,sVarName){var sName=LMSInterface.GetVariable(sVarName)
if(sName!=null){if(/^(\S+?),\s*(\S+)(?:$|\s+(\S+)$)/.test(sName)){var pattern={l:1,f:2,m:3}
return RegExp["$"+pattern[sPart]]||null}
if(/^(\S+)\s+(\S+)(?:$|\s+(\S+)$)/.test(sName)){var pattern={l:RegExp.$3?3:2,f:1,m:RegExp.$3?2:-1}
return RegExp["$"+pattern[sPart]]||null}}
return null}
function getTimePart(sIndex,sVarName,sAdd){var sTime=LMSInterface.GetVariable(sVarName)
if(sTime&&sTime.length>2&&/^P(?:0Y)?(?:0M)?(?:(\d+)D)?(?:T(?:(\d+)H)?(?:(\d+)M)?(?:(\d+(?:\.\d+)?)S)?)?$/i.test(sTime)){var iHours=+RegExp.$2
iHours+=RegExp.$1*24
var iMins=+RegExp.$3
iMins+=Math.round(RegExp.$4/60)
if(sAdd&&/^PT(\d+)H(\d+)M$/i.test(sAdd)){iHours+=+RegExp.$1
iMins+=+RegExp.$2
while(iMins>59){iHours++
iMins-=60}}
return sIndex=="h"?iHours:iMins}
return null}},GetSessionTime:function(){var iTimeSpent=(new Date().getTime()-LMSInterface.StartTime)/1000/60,iHours=Math.floor(iTimeSpent/60),iMinutes=Math.floor(iTimeSpent%60)
if(iHours<10)iHours="0"+iHours
if(iMinutes<10)iMinutes="0"+iMinutes
return"PT"+iHours+"H"+iMinutes+"M"},FormatDate:function(oDate){var prePad=oDate.getFullYear()+"-"+(oDate.getMonth()+1)+"-"+oDate.getDate()+"T"+oDate.getHours()+":"+oDate.getMinutes()+":"+oDate.getSeconds()
return prePad.replace(/(\D)(\d)(?=\D|$)/g,"$10$2")},ResetObjective:function(oTestNode){if(!this.API)return null
var iIdx=oTestNode.getAttribute("scormIndex")
if(iIdx!=null){var sObv="cmi.objectives."+iIdx
this.SetVariable(sObv+".score.raw","0")
this.SetVariable(sObv+".score.scaled","0")
this.SetVariable(sObv+".success_status","unknown")}},GetIndexById:function(sBag,sId){sBag+="."
var iCount=this.GetVariable(sBag+"_count")
if(iCount===null)return null
for(var iItem=0;iItem<iCount;iItem++){var sCurId=this.GetVariable(sBag+iItem+".id")
if(sCurId==sId)return iItem
if(sCurId=="")this.FilterId.unique=true}
return iCount},RegisterBookmark:function(sData,sLocation){if(!this.API)return null
this.SetVariable("cmi.location",sLocation)
this.SetVariable("cmi.suspend_data",sData)},GetBookmark:function(){if(!this.API)return null
return{xml:this.GetVariable("cmi.suspend_data"),location:this.GetVariable("cmi.location")}},RegisterObjective:function(oTestNode){if(!this.API)return null
var iIdx=this.GetIndexById("cmi.objectives",this.FilterId(oTestNode))
if(iIdx==null)return null
var sObv="cmi.objectives."+iIdx
this.SetVariable(sObv+".id",this.FilterId(oTestNode))
this.SetVariable(sObv+".description",(oTestNode.getAttribute("title")||"").slice(0,255))
this.SetVariable(sObv+".score.raw",oTestNode.getAttribute("totalscore"))
this.SetVariable(sObv+".score.scaled",getFixed(oTestNode.getAttribute("totalscore")/100,3).toString())
this.SetVariable(sObv+".score.max","100")
this.SetVariable(sObv+".score.min","0")
this.SetVariable(sObv+".success_status",oTestNode.getAttribute("status"))
oTestNode.setAttribute("scormIndex",iIdx)},ResetInteraction:function(oNavNode){if(!this.API)return null
var iIdx=oNavNode.getAttribute("scormIndex")
if(iIdx!=null){var sIrn="cmi.interactions."+iIdx
this.SetVariable(sIrn+".result","neutral")}},GetInteractionIndex:function(oNavNode,iEl){if(!this.ActivityIndex)this.ActivityIndex=this.GetVariable("cmi.interactions._count")||0
var sIndex=oNavNode.getAttribute("scormIndex")||"",aIndex=sIndex.split(",")
if(!aIndex[iEl]){aIndex[iEl]=this.ActivityIndex
oNavNode.setAttribute("scormIndex",aIndex.join(","))
this.ActivityIndex++}
return aIndex[iEl]},PreRegisterInteraction:function(oNavNode,iEl,iTotalEl,sType,sCorrect,bUnscored,oStartTime,sDesc){if(!this.API||this.GetVariable("cmi.interactions._count")==null)return null
var iIdx=this.GetInteractionIndex(oNavNode,iEl),sIrn="cmi.interactions."+iIdx
this.SetVariable(sIrn+".id",this.FilterId(oNavNode,iEl))
this.SetVariable(sIrn+".timestamp",this.FormatDate(oStartTime))
this.SetVariable(sIrn+".description",(sDesc||oNavNode.getAttribute("title")||"").slice(0,255))
if(oNavNode.parentNode.tagName=="section"&&!bUnscored){this.SetVariable(sIrn+".objectives.0.id",this.FilterId(oNavNode.parentNode.parentNode))
this.SetVariable(sIrn+".weighting",getFixed(oNavNode.getAttribute("maxScore")/iTotalEl,5))}else{this.SetVariable(sIrn+".weighting","0")}
var oTypes={dragdrop:"matching",fillblank:"fill-in",multichoice:"choice",sort:"sequencing",matching:"matching",rating:"numeric",text:"other"}
this.SetVariable(sIrn+".type",oTypes[sType]||"other")
if(sCorrect!=undefined)this.SetVariable(sIrn+".correct_responses.0.pattern",sCorrect)
if(!this.InteractionTimes)this.InteractionTimes={}
if(!this.InteractionTimes[iIdx])this.InteractionTimes[iIdx]=oStartTime.getTime()},InteractionAnswered:function(oNavNode,iEl,sAnswer,oTimeStamp,iTotal,iCorrect){if(!this.API)return null
var iIdx=this.GetInteractionIndex(oNavNode,iEl),sIrn="cmi.interactions."+iIdx,iStartTime=this.InteractionTimes[iIdx]
if(iStartTime){delete this.InteractionTimes[iIdx]
this.SetVariable(sIrn+".latency","PT"+getFixed((oTimeStamp.getTime()-new Date(+iStartTime).getTime())/1000,2)+"S")}
if(iTotal!=undefined&&iCorrect!=undefined){var sResult="incorrect"
if(iCorrect>0){sResult=iTotal==iCorrect?"correct":Math.round((iCorrect/iTotal)*100)}
this.SetVariable(sIrn+".result",sResult||"neutral")}
if(sAnswer!=undefined)this.SetVariable(sIrn+".learner_response",sAnswer)},FilterId:function(oNavNode,iIndex){var sId=oNavNode.getAttribute("scormId")
if(!sId){var sId="urn:composica:UID-"+oNavNode.getAttribute("id").replace(/[^a-zA-Z0-9]/g,"-")
if(arguments.callee.unique)sId+="-"+new Date().getTime().toString(36)
oNavNode.setAttribute("scormId",sId)}
return sId+"-"+(iIndex||0)},SetDefaults:function(){this.SetVariable("cmi.score.max","100")
this.SetVariable("cmi.score.min","0")
var oPrj=oConfig.firstChild
if(oPrj.getAttribute("restoreLMS")=="true"||oPrj.getAttribute("restoreUserdata")=="true"){this.SetVariable("cmi.exit","suspend")
this.SetVariable("adl.nav.request","suspendAll")}
var sStatus=this.GetVariable("cmi.completion_status")
if(!sStatus||sStatus=="not attempted"||sStatus=="unknown"){this.SetVariable("cmi.completion_status","incomplete")}},GetVariable:function(sVarName){if(!LMSInterface.API)return null
var sValue=LMSInterface.API.GetValue(sVarName)
LMSDebug.Log("GetValue",sVarName+" ("+sValue+")",true)
return LMSInterface.API.GetLastError()==0?sValue:null},SetVariable:function(sVarName,sValue){if(!LMSInterface.API)return null
var bSuccess=LMSInterface.API.SetValue(sVarName,sValue+"")
LMSDebug.Log("SetValue",sVarName+" = "+sValue+" ("+bSuccess+")",true)
return bSuccess&&LMSInterface.API.GetLastError()==0},GetUserId:function(){return this.GetVariable("cmi.learner_id")},Dispose:function(){if(LMSInterface.disposed)return
if(!LMSInterface.API&&!LMSInterface.Track)return null
if(finishPage()!==false)evaluatePage(oResource,null,null,true)
LMSInterface.disposed=true
LMSInterface.SetVariable("cmi.session_time","PT"+getFixed((new Date().getTime()-LMSInterface.StartTime)/1000,2)+"S")
LMSInterface.SetVariable("cmi.progress_measure",getFixed(VarInterface.Get("total done")/VarInterface.Get("total pages"),3).toString())
var oTests=oConfig.selectNodes("//structure//test"),sCompleteMode=oConfig.firstChild.getAttribute("completionCondition")||"viewed",sSuccessMode=oConfig.firstChild.getAttribute("successCondition")||"passed",iSum=0,iCount=0,bComplete="incomplete",bPass="passed",bAllVisited=VarInterface.Get("total done")>=VarInterface.Get("total pages"),Track={}
for(var iTest=0;iTest<oTests.length;iTest++){if(oTests[iTest].getAttribute("globalScore")!="false"){iCount++
iSum+=oTests[iTest].getAttribute("totalscore")*1
if(oTests[iTest].getAttribute("status")!="passed")bPass="failed"}}
if(iCount){LMSInterface.SetVariable("cmi.score.raw",getFixed(iSum/iCount).toString())
LMSInterface.SetVariable("cmi.score.scaled",getFixed(iSum/iCount/100,3).toString())
if(bPass=="passed"){if(sSuccessMode!="viewed+passed"||bAllVisited)LMSInterface.SetVariable("cmi.success_status",bPass)}else{LMSInterface.SetVariable("cmi.success_status",bPass)}
if(LMSInterface.Track){Track.score=Math.round(iSum/iCount).toString()}}
if((sCompleteMode=="none")||(sCompleteMode=="viewed"&&bAllVisited)||(sCompleteMode=="passed"&&bPass=="passed")||(sCompleteMode=="viewed+passed"&&bAllVisited&&bPass=="passed")){bComplete="completed"
LMSInterface.SetVariable("cmi.completion_status","completed")
LMSInterface.SetVariable("cmi.success_status",bPass)
LMSInterface.SetVariable("cmi.exit","normal")
if(!bSolo)LMSInterface.SetVariable("adl.nav.request","exitAll")}
if(LMSInterface.Track){Track.student=VarInterface.Get("composica_student_id")||""
Track.start=new Date(LMSInterface.StartTime).toString()
Track.end=new Date().toString()
Track.status=iCount?bPass:bComplete
var sMode=oConfig.firstChild.getAttribute("courseMode"),sOrigId=oConfig.firstChild.getAttribute("publicId"),sPost=""
for(var sKey in Track)sPost+=sKey+"="+encodeURIComponent(Track[sKey])+"&"
httpLoader("POST",sMode=="web"?"../../../Track/record.asp?p="+encodeURIComponent(sOrigId):"Runtime/Track/record.asp",function(oXH){if(oXH.responseText!="SUCCESS"){alert("The server could not track your learning performance:\r\n\r\n"+oXH.responseText)}},sPost,true)}
if(LMSInterface.API){LMSDebug.Log("Info","Done, calling commit and terminate")
var bCmt=LMSInterface.API.Commit("")
LMSDebug.Log("Commit","Result: "+bCmt,true)
var bFin=LMSInterface.API.Terminate("")
LMSDebug.Log("Terminate","Result: "+bFin,true)}}}