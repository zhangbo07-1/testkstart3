var loginSubmit=document.getElementById("loginSubmit"),oBlogsIndexHolder=document.getElementById("oBlogsIndexHolder"),oModerate=document.getElementById("oModerate"),oTrack=document.getElementById("oTrack"),oTrackForm=document.getElementById("oTrackForm"),oMain=document.getElementById("oMain")
if(location.href.indexOf("http")==0||navigator.platform.substr(0,3)!="Win"||isTestSuite()){httpLoader("GET",cacheURL("Content/"+GET_PROJECT_STATE+"?readonly=1&project="+encodeURIComponent(document.ProjectId)),projectLoaded)}else{showError("请使用 <em>run.hta</em> 从文件系统开始这个课程</p><p><em>run.html</em> 应该只能通过web服务器或非Windows平台使用。")}
function showError(sMsg){document.body.className="loaded"
document.getElementById("loginHolder").innerHTML="<p>"+sMsg+"</p>"}
function isTestSuite(){try{if(top.opener&&top.opener.location.pathname.toLowerCase().indexOf("/testsuite/")>-1)return true}catch(e){return false;}}
function projectLoaded(oXH){var oXML
if(oXH.status==200||oXH.status==0){oXML=oXH.responseXML
if(oXML&&!oXML.firstChild)oXML.loadXML(oXH.responseText)}
if(oXML&&oXML.firstChild){oXML=oXML.firstChild
try{top.document.title=oXML.getAttribute("title");}catch(oErr){}
var bAutoOpen=oXML.getAttribute("manualStart")!="true",winType=oXML.getAttribute("windowType")||"",bMobile="ontouchstart"in window
if(oXML.getAttribute("description"))courseDesc.style.display="block"
loginSubmit.winHeight=oXML.getAttribute("windowHeight")||600
loginSubmit.winWidth=oXML.getAttribute("windowWidth")||800
loginSubmit.winResizable=oXML.getAttribute("windowResizable")=="true"?"1":"0"
loginSubmit.winMax=winType=="max"
loginSubmit.noWin=winType.indexOf("current")>-1||bMobile
loginSubmit.onclick=openProject
var sBackend=oXML.getAttribute("runtimeBackend")
if(sBackend&&oXML.getAttribute("blogIndex")){var oScript=document.createElement("script")
oScript.setAttribute("type","text/javascript")
oScript.setAttribute("src",sBackend+"launch.js")
backendFound.xml=oXML
document.body.appendChild(oScript)}
if(oXML.getAttribute("tracking")=="true"){oTrack.style.display="block"
oTrack.onclick=tracking
if(oXML.getAttribute("courseMode")=="web"){oTrackForm.action="../../Track/report.asp?p="+encodeURIComponent(oXML.getAttribute("publicId"))}
if(/[?&]track($|&)/i.test(location.search)){tracking()
bAutoOpen=false}else{if(bAutoOpen)openProject()}}else{if(bAutoOpen)openProject()}
if(!loginSubmit.noWin||!bAutoOpen)document.body.className="loaded"}else if(window.chrome&&location.protocol=="file:"&&!oXH.responseText){showError("由于Chrome浏览器的安全性限制，这个课程无法正常播放。请换一个浏览器尝试。")}else{alert("课程读取失败，请稍后重试")}
oXML=null}
function backendFound(){var oXML=backendFound.xml
oBlogsIndexHolder.style.display="block"
if(oModerate&&oXML.getAttribute("hasModeration")=="true"){var sBackend=oXML.getAttribute("runtimeBackend"),sURL=sBackend+"mod.asp?i="+encodeURIComponent(oXML.getAttribute("publicId"))
if(/[?&]moderate($|&)/i.test(location.search)){location.href=sURL
return}else oModerate.innerHTML='<a href="'+sURL.replace(/\"/g,"")+'" target="_blank">管理索引</a>'
oModerate.style.display="block"}
backendFound.xml=null}
function tracking(){oMain.style.display="none"
oTrackForm.style.display="block"
oTrack.style.display="none"}
function validate(form){if(!form.password.value){alert("密码是必须的！")
return false}
return true}
function openProject(){if(loginSubmit.noWin){location.replace("Content/course.html"+location.search)}else{var iWidth=loginSubmit.winWidth*1,iHeight=loginSubmit.winHeight*1
if(isClosed(openProject.win)){var iDefaultCaption=25
if(/Windows NT (\d+(\.\d+)?)/.test(navigator.userAgent)&&+RegExp.$1>=5.1)iDefaultCaption=30
var sAdd
if(loginSubmit.winMax)sAdd="fullscreen=1,top=0,left=0,width="+screen.availWidth+",height="+screen.availHeight
else sAdd="top="+Math.round((screen.availHeight-iHeight)/2-iDefaultCaption)+",left="+Math.round((screen.availWidth-iWidth)/2-6)+",width="+iWidth+",height="+iHeight
openProject.win=window.open("Content/course.html"+location.search,"courseWin"+new Date().getTime(),sAdd+",location=0,resizable="+loginSubmit.winResizable+",status=0,scrollbars=1")
if(openProject.win&&loginSubmit.winMax)try{openProject.win.resizeTo(screen.availWidth,screen.availHeight);}catch(e){}}else{openProject.win.focus()}}}
function isClosed(oWin){return!oWin||oWin.closed||oWin.innerHeight===0||oWin.outerHeight===0||!oWin.document||!oWin.document.documentElement}