onerror=function(sMsg,sURL,iLine){var bDbg=true
bDbg=false
if(!sMsg.srcElement&&sMsg.indexOf("_isOpaque")<0){if(bDbg||(sProjectId&&sProjectId!="Res")){alert("Error: "+sMsg+"\r\nFile: "+sURL+"\r\nLine: "+iLine+"\r\nLocation: "+location.href)
return false}
return true}}
httpLoader.cache={}
function httpLoader(sMethod,sURL,fExecDone,sPostData,bSync,bSkipCache){var oCache=httpLoader.cache[sURL]
if(oCache){if(fExecDone)fExecDone({readyState:4,status:oCache.status,responseText:oCache.text})
return}
var oXH=getXMLHTTP(),bDone=false
oXH.open(sMethod,sURL,!bSync)
oXH.setRequestHeader("Content-type","application/x-www-form-urlencoded")
if(!bSync)oXH.onreadystatechange=invokeDone
try{oXH.send(sPostData)}catch(oErr){handleException()}
if(bSync)invokeDone()
function invokeDone(){if(!bDone&&oXH.readyState==4){if(oXH.responseXML&&oXH.responseXML.__proto__&&window.XMLDocument&&oXH.responseXML.__proto__!=XMLDocument.prototype){var oXML=getXMLDocument()
oXML.loadXML(oXH.responseText)
oXH={readyState:4,status:oXH.status,responseText:oXH.responseText,responseXML:oXML}}
if(fExecDone)fExecDone(oXH)
if(!bSkipCache)httpLoader.cache[sURL]={status:oXH.status,text:oXH.responseText}
bDone=true
oXH=null
fExecDone=null}}
function handleException(){if(fExecDone)fExecDone({readyState:4,status:404,responseText:""})
bDone=true
oXH=null
fExecDone=null}}
function getXMLHTTP(){var xmlVersions=["MSXML2.XMLHTTP.3.0","MSXML2.XMLHTTP","Microsoft.XMLHTTP"]
for(var i=0;i<xmlVersions.length;i++)try{return new ActiveXObject(xmlVersions[i]);}catch(e){}
return null}
function getXMLDocument(){var xmlVersions=["MSXML2.DOMDocument.3.0","MSXML2.DOMDocument","Microsoft.XMLDOM"]
for(var i=0;i<xmlVersions.length;i++)try{return new ActiveXObject(xmlVersions[i]);}catch(e){}
return null}
function scriptLoader(sURL,fExecDone,fExecFail){var oWin=window,oDoc=oWin.document,iUID=(scriptLoader.uid||0)+1,sID="scrldr_"+iUID,oScript=oDoc.createElement("script"),bLoaded
scriptLoader.uid=iUID
oWin[sID]=function(){bLoaded=true
var bSuccess=Array.prototype.shift.call(arguments)
if(bSuccess){fExecDone.apply(oWin,arguments)}else{alert(arguments[0])}
setTimeout(cleanUp,50)}
oScript.setAttribute("type","text/javascript")
oScript.onreadystatechange=function(){if(oScript.readyState=="loaded"){setTimeout(verifyLoad,1000);}}
oScript.onerror=verifyLoad
oScript.setAttribute("src",sURL+(sURL.indexOf("?")>-1?"&":"?")+"f="+sID)
oDoc.body.appendChild(oScript)
function verifyLoad(){if(!bLoaded){if(fExecFail)fExecFail()
cleanUp()}}
function cleanUp(){window[sID]=null
if(oScript&&oScript.parentNode){oScript.parentNode.removeChild(oScript)}}}
var VarInterface={Init:function(oDoc){this.Types.Add("Direct",function(sParam){return sParam;})
this.Types.Add("Custom",function(fParam){return fParam();})
oDoc.body.isVarSupported=function(sVarName){return VarInterface.Variables[sVarName]!==undefined}
oDoc.body.getVarValue=function(sVarName){return VarInterface.Get(sVarName)}},Variables:{},Types:{Add:function(sType,fImplementation){this[sType]=fImplementation;}},Add:function(sVar,sType,vParam){this.Variables[sVar]={Param:vParam,Type:sType}},Get:function(sVar){return this.Types[this.Variables[sVar].Type](this.Variables[sVar].Param)}}
;/*@cc_on @*/
function switchEmbeds(oEl,sState,bKill){document.body.fireEvent("ondisplaychange",{element:oEl,state:sState,kill:bKill})
var aNavs=getTag(oEl).toLowerCase()=="navbutton"?[oEl]:getByTagAll(oEl,"navbutton")
for(var iNav=0;iNav<aNavs.length;iNav++){var oNav=aNavs[iNav]
if(sState!="off"&&oNav.getAttribute("trigger")=="reveal"&&getCurrentDisplay(oNav)!="none")try{return oNav.revealed();}catch(e){}}
var aPlayers=getTag(oEl).toLowerCase()=="mplayer"?[oEl]:getByTagAll(oEl,"mplayer")
for(var iPlayer=0;iPlayer<aPlayers.length;iPlayer++){if(sState=="off"&&bKill)try{aPlayers[iPlayer].reset(false);}catch(e){}
else if(sState=="off")try{aPlayers[iPlayer].stop();}catch(e){}
else try{if(aPlayers[iPlayer].getAttribute("deferStart")=="true"&&getCurrentDisplay(aPlayers[iPlayer])!="none")aPlayers[iPlayer].play()}catch(e){}}
var aEmbeds=getTag(oEl)=="EMBED"?[oEl]:getByTagAll(oEl,"embed"),aStates={on:["Play","PlayMovie"],off:["Stop","StopMovie","StopPlay","Rewind"]}[sState]
;/*@if(!@_jscript)@*/ //Avoid calling Rewind when forcing kill(emulation only)
if(bKill&&sState=="off")aStates.length--
;/*@end @*/
for(var iEmbed=0;iEmbed<aEmbeds.length;iEmbed++){var oEmbed=aEmbeds[iEmbed],sParentTag=getTag(oEmbed.parentNode).toLowerCase(),bSuccess=false
if(sParentTag=="mpstage"||sParentTag=="stage")continue
if(sState=="off"||oEmbed.parentNode.currentStyle.display!="none"){for(var iCall=0;iCall<aStates.length;iCall++){try{oEmbed[aStates[iCall]]()
bSuccess=true}catch(e){}}}
;/*@if(!@_jscript)@*/ //(emulation only)
if(bKill){oEmbed.style.display="none"
oEmbed.offsetWidth}
else if(!bSuccess&&oEmbed.getAttribute("comptype")=="media"){oEmbed.autoStart=sState=="on"?"1":"0"
setTimeout(function(){oEmbed.style.display="none"
oEmbed.offsetWidth
oEmbed.style.display=""},0)}
;/*@end @*/}}
function getPrecision(iNum){return Math.floor(100000*iNum)/100000}
function getFixed(iNum,iPrec){iPrec=iPrec?Math.pow(10,iPrec):100
return Math.round(iPrec*iNum)/iPrec}
function getDuration(sDur){if(/^(-?\d+(?:\.\d+)?)(ms|sec|min)?$/.test(sDur)){var iDur=+RegExp.$1
if(RegExp.$2=="sec")iDur*=1000
if(RegExp.$2=="min")iDur*=60000
return iDur}
return null}
String.prototype.toRX=function(){return this.replace(/[\$\(\)\*\+\.\[\]\?\\\^\{\}\|]/g,"\\$&")}
String.prototype.escapeHTML=function(){return this.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/\"/g,"&quot;").replace(/\'/g,"&#39;")}
if(typeof(cacheURL)=="undefined"){var cacheURL=function(sURL){var sPath=sURL.slice(0,sURL.indexOf("?")),sSearch=sURL.slice(sURL.indexOf("?"))
return sPath+sSearch.replace(/\.\./g,"~")}}
function addLink(sId,sHref){var sLink='<link rel="stylesheet" id="'+sId+'" type="text/css"'
if(sHref)return document.write(sLink+' href="'+sHref+'">')
;/*@if(@_jscript)return document.write(sLink+' href="">');/*@end @*/
return document.write(sLink+">")}
function createCustom(sTag){var oEl=document.createElement("span")
oEl.className="tag--"+sTag
if(window.onaftercreatecustom)window.onaftercreatecustom(oEl,sTag)
return oEl}
function getTag(oEl){var sClass=oEl.className
if(sClass&&/\btag--(\S+)\b/.test(sClass))return RegExp.$1
return oEl.nodeName}
function getByTagAll(oContext,sTag){sTag=sTag.toLowerCase()
if(sTag=="*"){return oContext.getElementsByTagName("*")}else if(oContext.querySelectorAll){return oContext.querySelectorAll(sTag+", span[class~=tag--"+sTag+"]")}else{var aEls=oContext.getElementsByTagName("*"),aRes=[]
for(var iE=0;iE<aEls.length;iE++){var oEl=aEls[iE]
if(getTag(oEl).toLowerCase()==sTag)aRes.push(oEl)}
return aRes}}
function getByTagOne(oContext,sTag){sTag=sTag.toLowerCase()
if(oContext.querySelector){return oContext.querySelector(sTag+", span[class~=tag--"+sTag+"]")}else{var aEls=oContext.getElementsByTagName("*")
for(var iE=0;iE<aEls.length;iE++){var oEl=aEls[iE]
if(getTag(oEl).toLowerCase()==sTag)return oEl}
return null}}
function getChildByTagAll(oContext,sTag){sTag=sTag.toLowerCase()
if(oContext.querySelectorAll){oContext.setAttribute("__templocator","a")
var aRes=oContext.querySelectorAll("*[__templocator=a] > "+sTag+", *[__templocator=a] > span[class~=tag--"+sTag+"]")
oContext.removeAttribute("__templocator")
return aRes}else{var aNodes=oContext.childNodes,aRes=[]
for(var iN=0;iN<aNodes.length;iN++){var oNode=aNodes[iN]
if(oNode.nodeType==1&&getTag(oNode).toLowerCase()==sTag)aRes.push(oNode)}
return aRes}}
function getChildByTagOne(oContext,sTag){sTag=sTag.toLowerCase()
if(oContext.querySelector){oContext.setAttribute("__templocator","a")
var oRes=oContext.querySelector("*[__templocator=a] > "+sTag+", *[__templocator=a] > span[class~=tag--"+sTag+"]")
oContext.removeAttribute("__templocator")
return oRes}else{var aNodes=oContext.childNodes
for(var iN=0;iN<aNodes.length;iN++){var oNode=aNodes[iN]
if(oNode.nodeType==1&&getTag(oNode).toLowerCase()==sTag)return oNode}
return null}}
function getCurrentDisplay(oEl){var oNone=oEl
while(oNone&&(!oNone.currentStyle||oNone.currentStyle.display!="none"))
oNone=oNone.parentElement
return oNone?"none":oEl.currentStyle&&oEl.currentStyle.display||""}