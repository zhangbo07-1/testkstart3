function EmuBehavior(xArg){this.elemQueue=[]
this.pendCount=0
if(typeof xArg=="string"){this.status="uninitialized"
this.parsed=false
this.file=xArg
this.loadFromFile()}else{this.status="complete"
this.parsed=false
for(var sProp in xArg){this[sProp]=xArg[sProp]}}}
EmuBehavior.readyState="uninitialized"
EmuBehavior.TagFiles={}
EmuBehavior.Cache={}
EmuBehavior.errorReport=function(sError){if(window.onerror)
window.onerror(sError+"\n\n","emubehavior.js","")
if(window.console)
console.log("[EmuBehavior] "+sError)}
EmuBehavior.loadBehaviorList=function(sFile){EmuBehavior.readyState="loading"
httpLoader("GET",sFile,function(oXH){if(oXH.status==200||oXH.status==0){var oXML=oXH.responseXML,aBehaviors
if(!oXML)oXML=getXMLDocument()
if(!oXML.documentElement)oXML.loadXML(oXH.responseText)
aBehaviors=oXH.responseXML.selectNodes('/behaviors/behavior')
for(var iB=aBehaviors.length;iB--;){var oB=aBehaviors[iB],sTag=oB.getAttribute("tag"),sSrc=oB.getAttribute("src"),aEls
EmuBehavior.TagFiles[sTag]=sSrc
aEls=getByTagAll(document,sTag)
for(var iE=0;iE<aEls.length;iE++){EmuBehavior.loadBehavior(aEls[iE],sSrc)}}
EmuBehavior.readyState="complete"}else{EmuBehavior.errorReport("Unable to load behaviors list")}})}
EmuBehavior.loadBehavior=function(oElem,sFile){if(oElem.behaviorFiles){if(oElem.behaviorFiles[sFile])return oElem.emuReadyState="complete"}else{oElem.behaviorFiles={}
oElem.behaviorFiles[sFile]=true}
var oEmu=EmuBehavior.Cache[sFile]
if(!oEmu){if(sFile.charAt(0)=="#"){EmuBehavior.errorReport("Non-supported built-in behavior.")
return}
EmuBehavior.Cache[sFile]=oEmu=new EmuBehavior(sFile)}
oEmu.attachToElement(oElem)}
EmuBehavior.scanForBehaviors=function(oContainer,bIncludeContainer){oContainer.offsetWidth
for(var sTag in EmuBehavior.TagFiles){var sSrc=EmuBehavior.TagFiles[sTag],aEls=getByTagAll(oContainer,sTag)
if(bIncludeContainer&&getTag(oContainer).toLowerCase()==sTag)
oContainer.emuReadyState="loading"
for(var iE=0;iE<aEls.length;iE++){aEls[iE].emuReadyState="loading"}}
for(var sTag in EmuBehavior.TagFiles){var sSrc=EmuBehavior.TagFiles[sTag],aEls=getByTagAll(oContainer,sTag)
if(bIncludeContainer&&getTag(oContainer).toLowerCase()==sTag)
EmuBehavior.loadBehavior(oContainer,sSrc)
for(var iE=0;iE<aEls.length;iE++){EmuBehavior.loadBehavior(aEls[iE],sSrc)}}}
EmuBehavior.prototype={loadFromFile:function(){this.status="loading"
httpLoader("GET",this.file,Delegate.create(this,"parseFile"))},parseFile:function(oXH){if(oXH.status!=200&&oXH.status!=0){EmuBehavior.errorReport("HTC not found: '"+this.file+"'")
return}
var oXMLDoc=oXH.responseXML,oXMLComponent=oXMLDoc&&oXMLDoc.documentElement,aScripts=this.scripts=[],aMethods=this.methods=[],aEvents=this.events=[]
if(!oXMLComponent&&oXH.responseText){if(!oXMLDoc)oXMLDoc=getXMLDocument()
oXMLDoc.loadXML(oXH.responseText)
oXMLComponent=oXMLDoc.documentElement}
if(!oXMLComponent||oXMLComponent.nodeName!="component"){var oError=oXMLDoc&&oXMLDoc.parseError
if(oError&&oError.errorCode!=0){EmuBehavior.errorReport("Parse error in file '"+this.file+"' line "+oError.line+" char "+oError.linepos+":\n"+oError.reason+"\n"+oError.srcText)}else{EmuBehavior.errorReport("Parse error in file '"+this.file+"':\n\n"+(oXMLComponent.textContent||""))}
return}
aScripts.push("function createEventObject() { return {}; }")
aScripts.push("var element=arguments[0];")
var aScriptNodes=oXMLComponent.selectNodes("script")
for(iS=0;iS<aScriptNodes.length;iS++){var oScript=aScriptNodes[iS],sSrc=oScript.getAttribute("src"),sScript=oScript.firstChild&&oScript.firstChild.nodeValue||""
aScripts.push(sScript)
if(sSrc){this.loadScript(sSrc,aScripts.length-1)}}
var aMethodNodes=oXMLComponent.selectNodes("method")
for(var iM=0;iM<aMethodNodes.length;iM++){var oMethod=aMethodNodes[iM],sName=oMethod.getAttribute("name")
if(sName){aScripts.push("this.__method_"+sName+"="+sName+";")
aMethods.push(sName)}}
var aEventNodes=oXMLComponent.selectNodes("event")
for(var iE=0;iE<aEventNodes.length;iE++){var oEvent=aEventNodes[iE],sName=oEvent.getAttribute("name"),sId=oEvent.getAttribute("id")
if(sName&&sId){aScripts.push("var "+sId+"=new EmuEvent(element,\""+sName+"\");")
aEvents.push(sName)}}
var aAttachNodes=oXMLComponent.selectNodes("attach")
for(var iA=0;iA<aAttachNodes.length;iA++){var oAttach=aAttachNodes[iA],sEvent=oAttach.getAttribute("event"),sFor=oAttach.getAttribute("for"),sOnEvent=oAttach.getAttribute("onevent")
if(sEvent&&sFor&&sOnEvent){aScripts.push(sFor+".attachEvent(\""+sEvent+"\",function (event) { "+sOnEvent+" });")}}
if(this.pendCount==0){this.createBehavior()}
this.parsed=true},loadScript:function(sSrc,iIdx){this.pendCount++
httpLoader("GET",sSrc,Delegate.create(this,"parseScript",sSrc,iIdx))},parseScript:function(oXH,sSrc,iIdx){if(oXH.status!=200&&oXH.status!=0){EmuBehavior.errorReport("JS not found: '"+sSrc+"' while processing: '"+this.file+"'")
return}
this.scripts[iIdx]=oXH.responseText
this.pendCount--
if(this.pendCount==0&&this.parsed){this.createBehavior()}},createBehavior:function(){var sScript=this.scripts.join("\n")
try{var oClass=this.func=new Function(sScript)}catch(oErr){this.handleError(oErr,"creation")
return}
this.status="complete"
while(this.elemQueue.length>0){this.attachToElement(this.elemQueue.pop())}},attachToElement:function(oElem){if(!oElem.ownerDocument||!oElem.ownerDocument.documentElement||!oElem.ownerDocument.documentElement.contains(oElem)){return setTimeout(Delegate.create(this,"attachToElement",oElem),10)}
if(this.status=="complete"){var aMethods=this.methods,aEvents=this.events,oDispatcher=oElem.dispatcher
if(!oDispatcher){oDispatcher=new ManualEventDispatcher(oElem)
oDispatcher.addCustomEvent("oncontentready")
oDispatcher.addCustomEvent("onemureadystatechange")
oDispatcher.addCustomEvent("ondocumentready")
oElem.dispatcher=oDispatcher}
try{var oInstance=new this.func(oElem)}catch(oErr){this.handleError(oErr,"instantiation")
return}
if(aMethods)for(var iM=0;iM<aMethods.length;iM++){var sMethod=aMethods[iM]
oElem[sMethod]=oInstance["__method_"+sMethod]}
if(aEvents)for(var iE=0;iE<aEvents.length;iE++){var sEvent=aEvents[iE]
oDispatcher.addCustomEvent(sEvent)}
try{oElem.fireEvent("oncontentready")
oElem.emuReadyState="complete"
oElem.fireEvent("onemureadystatechange")
oElem.fireEvent("ondocumentready")}catch(oErr){this.handleError(oErr,"initialization")}}else{oElem.emuReadyState="loading"
this.elemQueue.push(oElem)}},handleError:function(oErr,sEvent){try{var iLineOffset=251,iLine=oErr.lineNumber-iLineOffset,aLines=this.scripts&&this.scripts.join("\n").split(/\n/)
EmuBehavior.errorReport("Error on "+sEvent+" of '"+this.file+"': "+oErr.message+
(aLines&&iLine<aLines.length?"\n\n\t"+aLines[iLine].replace(/^\s+|\s+$/g,""):""))}catch(oErr){EmuBehavior.errorReport("Error on error reporting: "+sEvent)}}}
EmuBehavior.Cache["#default#userData"]=new EmuBehavior({file:"#default#userData",func:function(oElem){var oXML,sBase,sProtocol=location.protocol,sPort=location.port,sPath=location.pathname,iSep=sPath.lastIndexOf("/")
if(!window.localStorage||!Object.defineProperty&&!oElem.__defineGetter__)
return oElem.addBehavior("#default#userData")
if(!sPort)sPort=location.protocol=="https:"?"443":"80"
sBase=sProtocol+sPort+sPath.substr(0,iSep)+"/"
function initXML(){oXML=getXMLDocument()
oXML.appendChild(oXML.createElement("ROOTSTUB"))}
initXML()
oElem.load=function(sStoreName){try{if(!sStoreName)return
var sXML=localStorage[sBase+sStoreName]
if(sXML)oXML.loadXML(sXML)
else initXML()}catch(oErr){initXML()}}
oElem.save=function(sStoreName){try{if(!sStoreName)return
localStorage[sBase+sStoreName]=oXML.xml}catch(oErr){}}
function XMLDocument_get(){return oXML;}
function XMLDocument_set(oVal){oXML=oVal;}
if(oElem.__defineGetter__&&oElem.__defineSetter__){oElem.__defineGetter__("XMLDocument",XMLDocument_get)
oElem.__defineSetter__("XMLDocument",XMLDocument_set)}else{Object.defineProperty(oElem,"XMLDocument",{get:XMLDocument_get,set:XMLDocument_set})}}})
function EmuEvent(oElem,sEvent){this.fire=function(oEvent){return oElem.fireEvent(sEvent,oEvent)}}
Delegate={create:function(oObj,sFunc){var aDelegArgs=Array.prototype.slice.call(arguments,2)
return function(){var aArgs=Array.prototype.slice.call(arguments,0).concat(aDelegArgs)
return oObj[sFunc].apply(oObj,aArgs)}}}
function ManualEventDispatcher(oElem){this.customEvents={}
this.elem=oElem
this.nativeAttachEvent=oElem.attachEvent
this.nativeDetachEvent=oElem.detachEvent
this.nativeFireEvent=oElem.fireEvent
oElem.attachEvent=Delegate.create(this,"attachEvent")
oElem.detachEvent=Delegate.create(this,"detachEvent")
oElem.fireEvent=Delegate.create(this,"fireEvent")
oElem.clearListeners=Delegate.create(this,"clearListeners")}
ManualEventDispatcher.prototype={attachEvent:function(sEvent,fHandler){var aHandlers=this.customEvents[sEvent]
if(aHandlers){aHandlers.push(fHandler)
return true}else{return this.nativeAttachEvent.apply(this.elem,arguments)}},detachEvent:function(sEvent,fHandler){var aHandlers=this.customEvents[sEvent]
if(aHandlers){for(var iH=0;iH<aHandlers.length;iH++){if(aHandlers[iH]==fHandler){aHandlers.splice(iH,1)
break}}}else{return this.nativeDetachEvent.apply(this.elem,arguments)}},fireEvent:function(sEvent,oEvent){var aHandlers=this.customEvents[sEvent]
if(aHandlers){var bReturn=true
try{if(!this.elem.ownerDocument.body.contains(this.elem))return true;}catch(e){return true;}
aHandlers=aHandlers.concat()
if(typeof this.elem[sEvent]=="function"){if(!this.fireEventHandler(sEvent,oEvent,this.elem[sEvent]))bReturn=false}
for(var iH=0;iH<aHandlers.length;iH++){if(!this.fireEventHandler(sEvent,oEvent,aHandlers[iH]))bReturn=false}
return bReturn}else{return this.nativeFireEvent.apply(this.elem,arguments)}},fireEventHandler:function(sEvent,oEvent,fHandler){var oLocalEvent={},bLocalRet=null
if(oEvent)for(var sKey in oEvent)if(Object.prototype.hasOwnProperty.call(oEvent,sKey))oLocalEvent[sKey]=oEvent[sKey]
oLocalEvent.srcElement=this.elem
oLocalEvent.type=sEvent.slice(2)
bLocalRet=fHandler.call(this.elem,oLocalEvent)
return bLocalRet===false||oLocalEvent.returnValue===false?false:true},addCustomEvent:function(sEvent){this.customEvents[sEvent]=[]},clearListeners:function(sEvent){var aHandlers=this.customEvents[sEvent]
if(aHandlers){aHandlers.length=0}}}
window.onaftercreatecustom=function(oEl,sTag){var sFile=EmuBehavior.TagFiles[sTag]
if(sFile)EmuBehavior.loadBehavior(oEl,sFile)};