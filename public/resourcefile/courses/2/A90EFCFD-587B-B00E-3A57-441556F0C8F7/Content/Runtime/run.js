/*@cc_on @*/
var oBaseTheme,oTheme,oGameStyle,oCustomRules,oTopLevelLayer,oContentArea,oDocContent,oMenuContent,oMasterContent,oLoader,sProjectId,defWidth,defHeight,oResource,sCurrentTheme="@NONE",oPersist={},oStatus={},oKeyCombos={},oPreviewArgs,oPopupArgs,oExternalConfig,oConfig,sTopBaseStyle,bSolo,bSnapshot,bStatic,bMobile="ontouchstart"in window
try{document.execCommand("BackgroundImageCache",false,true)
oPreviewArgs=top.opener.previewArgs}
catch(oErr){}
try{oExternalConfig=frameElement.config}catch(e){}
var oRequest={}
oRequest._raw=location.search.slice(1).split("&")
for(var iQS=oRequest._raw.length;iQS--;){var aPair=oRequest._raw[iQS].split("=")
oRequest[aPair[0]]=decodeURIComponent(aPair[1])}
onload=function(){sProjectId=document.ProjectId||oRequest.project
if(oPopupArgs){sProjectId=oPopupArgs.project
oResource=oPopupArgs.resource
VarInterface=oPopupArgs.win.VarInterface
LMSInterface=oPopupArgs.win.LMSInterface
oExternalConfig=oPopupArgs.config}
if(oRequest.htaMode&&!window.htaReady)return setTimeout(arguments.callee,50)
oBaseTheme=document.getElementById("oBaseTheme")
oTheme=document.getElementById("oTheme")
oGameStyle=document.getElementById("oGameStyle")
oCustomRules=document.getElementById("oCustomRules")
oTopLevelLayer=document.getElementById("oTopLevelLayer")
oContentArea=document.getElementById("oContentArea")
oDocContent=document.getElementById("oDocContent")
oMenuContent=document.getElementById("oMenuContent")
oMasterContent=document.getElementById("oMasterContent")
oLoader=document.getElementById("oLoader")
initOffsetFixer()
EmuBehavior.loadBehaviorList("Runtime/behaviors.xml")
if(!oPopupArgs)EmuBehavior.loadBehavior(oLoader,"#default#userData")
EmuBehavior.loadBehavior(document.body,"Runtime/runtimeEvents.xml")
VarInterface.Init(document)
setInterval(function(){try{document.body.offsetWidth;}catch(e){}},50)
oBaseTheme.href=cacheURL(GENERIC_REDIRECTOR+"?cache=1&t=res&readonly=1&project="+encodeURIComponent(sProjectId)+"&resource="+encodeURIComponent("../../Themes/baseTheme.css"))
oCustomRules.href=cacheURL(GET_CUSTOM_STYLESHEET+"?project="+encodeURIComponent(sProjectId));(function(){if(document.body.emuReadyState=="complete"&&EmuBehavior.readyState=="complete"&&oBaseTheme.styleSheet&&oCustomRules.styleSheet){initLoad()}else{setTimeout(arguments.callee,30)}})()}
function initLoad(){bSolo=oRequest.mode=="solo"
bSnapshot=oRequest.mode=="snapshot"
bStatic=bSnapshot
if(bSnapshot){var oCover=document.body.appendChild(createCustom("transalpha"))
oCover.appendChild(createCustom("transalphainner"))
oCover.style.zIndex=1
window.alert=function(){}
document.attachEvent("onbeforeactivate",function(event){return event.returnValue=false;})}
if(bMobile)Classes.add(document.body,"mobile-device")
loadProject(sProjectId,oRequest)}
function haltLoad(sMsg){var oWin=window
oWin.document.body.innerHTML='<div class="failMsg">'+sMsg+'</div>'
window.onerror=oWin.onerror=function(){return true;}}
function loadProject(sId,oDefaults){if(oExternalConfig){handlePrjLoad({status:200,responseXML:oExternalConfig})}else{httpLoader("GET",cacheURL(GET_PROJECT_STATE+"?wizard="+(oDefaults.wizard||"")+"&readonly=1&project="+encodeURIComponent(sId)),handlePrjLoad)}
function handlePrjLoad(oXH){if(oXH.status==200||oXH.status==0){var oXML=oXH.responseXML
if(!oXML.firstChild)oXML.loadXML(oXH.responseText)
oConfig=oXML
var oProject=oXML.firstChild
if(oProject){if(!oPopupArgs){if(!bSnapshot)LMSInterface.Init()
if(bSolo||bSnapshot)oProject.removeAttribute("restoreUserdata")
if(sProjectId=="Res"&&!oDefaults.skipRestore){var bPersist=State.restore(oXML,oDefaults)}
if(bSolo&&!oDefaults.resource)oDefaults.resource=oDefaults.root}
var oStruct=oXML.selectSingleNode("//structure")
oProject=oConfig.firstChild
oXML=null
if(!bPersist&&!bSnapshot&&!oPopupArgs)prepareTests(oStruct,oDefaults.resource)
document.title=oProject.getAttribute("title")
if(oDefaults.resource){oResource=oConfig.selectSingleNode(".//*[@id=\""+oDefaults.resource+"\"]")
if(oResource){if(oDefaults.theme)oResource.setAttribute("theme",oDefaults.theme)
if(oDefaults.master)oResource.setAttribute("master",oDefaults.master)
if(oDefaults.menu)oResource.setAttribute("menu",oDefaults.menu)
if(bSolo&&!bPersist){var oRoot=oStruct.selectSingleNode(".//*[@id=\""+oDefaults.root+"\"]")
if(oRoot){var oKeep=oRoot.cloneNode(!oDefaults.standalone)
while(oStruct.hasChildNodes())oStruct.removeChild(oStruct.firstChild)
oStruct.appendChild(oKeep)
oResource=oStruct.selectSingleNode(".//*[@id=\""+oDefaults.resource+"\"]")}}}
if(bPersist&&oDefaults.history)History.load(oDefaults.history)}
if(oDefaults.view){var oViews=oPreviewArgs&&oPreviewArgs.views||oConfig.selectSingleNode("/project/views"),oView=oViews&&oViews.selectSingleNode('view[@id="'+oDefaults.view+'"]'),oProps=oView&&oView.selectSingleNode("props"),aExclude=oView&&oView.selectNodes("exclude/@res")
if(oProps){for(var iAttr=oProps.attributes.length;iAttr--;){var oProp=oProps.attributes[iAttr],sPropName=oProp.nodeName,sPropVal=oProp.nodeValue
oProject.setAttribute(sPropName,sPropVal)}}
if(aExclude)for(var iEx=aExclude.length;iEx--;){var sRes=aExclude[iEx].nodeValue,oRes=oStruct.selectSingleNode('.//*[@id="'+sRes+'"]')
if(oRes)oRes.parentNode.removeChild(oRes)}}
defWidth=oPopupArgs&&+oPopupArgs.width||+oProject.getAttribute("windowWidth")
defHeight=oPopupArgs&&+oPopupArgs.height||+oProject.getAttribute("windowHeight")
oTopLevelLayer.style.width=defWidth+"px"
oTopLevelLayer.style.height=defHeight+"px"
oTopLevelLayer.style.overflow="hidden"
sTopBaseStyle=oTopLevelLayer.style.cssText
if(bMobile)createNarrationHolder()
if(!oResource)oResource=oStruct.firstChild
if(oResource){var oNavId=bSnapshot?oResource:getNavNode(oResource)
if(!oNavId)oNavId=getNavNode(oResource,"previous")
if(oNavId!==oResource)oResource=oNavId
if(oResource){if(!oPopupArgs){var oNum=getNavNode(oStruct.firstChild,null,null,true),iTotal=0,iChapTotal
while(oNum){iTotal++
oNum.setAttribute("pageNum",iTotal)
iChapTotal=+oNum.parentNode.getAttribute("chapPageTotal")+1
oNum.parentNode.setAttribute("chapPageTotal",iChapTotal)
oNum.setAttribute("chapPageNum",iChapTotal)
oNum=getNavNode(oNum,"next",null,true)}
oProject.setAttribute("totalPages",iTotal)
VarInterface.Add("project title","Custom",function(){return oConfig.documentElement.getAttribute("title");})
VarInterface.Add("location title","Custom",function(){return oResource.getAttribute("title");})
VarInterface.Add("parent title","Custom",function(){var oParent=oResource.parentNode
return oParent&&oParent.nodeName!="config"&&oParent.nodeName!="structure"&&oParent.getAttribute("title")||""})
VarInterface.Add("total pages","Direct",iTotal)
VarInterface.Add("page number","Custom",function(){return oResource.getAttribute("pageNum");})
VarInterface.Add("total done","Custom",function(){return oStruct.selectNodes(".//*[@done=\"true\" and (@skip=\"false\" or not(@skip))]").length;})
VarInterface.Add("percent done","Custom",function(){return Math.round(VarInterface.Get("total done")/VarInterface.Get("total pages")*100)+"%";})
VarInterface.Add("chapter page number","Custom",function(){return oResource.getAttribute("chapPageNum");})
VarInterface.Add("chapter total pages","Custom",function(){return oResource.parentNode.getAttribute("chapPageTotal");})
VarInterface.Add("chapter total done","Custom",function(){return oResource.parentNode.selectNodes("*[@done=\"true\" and (@skip=\"false\" or not(@skip))]").length;})
VarInterface.Add("current global score","Custom",function(){var aTests=oStruct.selectNodes('.//test[not(@globalScore) or @globalScore="true"]')
if(aTests.length>0){var iSum=0
for(var iTest=0;iTest<aTests.length;iTest++)iSum+=aTests[iTest].getAttribute("totalscore")*1
return getFixed(iSum/aTests.length)||0}
return 100})
VarInterface.Add("composica_student_id","Custom",function(){return oProject.getAttribute("composicaStudentId");})
VarInterface.Add("date eu","Custom",function(){var oD=new Date();return oD.getDate()+"/"+(oD.getMonth()+1)+"/"+oD.getFullYear();})
VarInterface.Add("date us","Custom",function(){var oD=new Date();return(oD.getMonth()+1)+"/"+oD.getDate()+"/"+oD.getFullYear();})
if(bSnapshot){VarInterface.Add("total done","Custom",function(){return VarInterface.Get("page number");})
VarInterface.Add("chapter total done","Custom",function(){return VarInterface.Get("chapter page number");})}
oStruct.setAttribute("visited","true")}
try{var bScreenReaderPopup=oPopupArgs&&parent.bReaderPopup;}catch(e){}
if(bScreenReaderPopup){CacheManager.disable()
return}else{var oCacher=CacheManager
if(oCacher.isCapable(oProject)){document.body.attachEvent("onnavigate",function(){var oLastNode=oResource
for(var iId=0;iId<oCacher.cacheAhead;iId++){oLastNode=getNavNode(oLastNode,"next",null,true)
if(oLastNode){oCacher.cache(oLastNode.getAttribute("id"))
oCacher.cache(findAttribute("master",oLastNode))
oCacher.cache(findAttribute("menu",oLastNode))}else break}})
oCacher.init([oResource.getAttribute("id"),findAttribute("master",oResource),findAttribute("menu",oResource)],function(){loadResource(oResource);})
return}else{oCacher.disable()
return loadResource(oResource)}}}}
return haltLoad(bSolo?"":"该课程为包含任何可视化内容")}}
return haltLoad("课程读取失败，请稍后重试")}}
function prepareTests(oStruct,forceIncludeId){var aTests=oStruct.selectNodes(".//test"),sIncludeTestId,sIncludeSectId
if(!!forceIncludeId){var oInclude=oStruct.selectSingleNode("//*[@id=\""+forceIncludeId+"\"]")
if(!oInclude||oInclude.parentNode.nodeName!="section"&&oInclude.nodeName!="section"){forceIncludeId=null}else{sIncludeTestId=oInclude.nodeName=="section"?oInclude.parentNode.getAttribute("id"):oInclude.parentNode.parentNode.getAttribute("id")}}
for(var iTest=0;iTest<aTests.length;iTest++){var oTest=aTests[iTest],sMode=oTest.getAttribute("testMode")
if(oTest.getAttribute("testMode")=="globalbank"){var iCurLen=oTest.selectNodes(".//page").length,sLen=oTest.getAttribute("questNumber"),iGlobalLen=!sLen||sLen=="All"?iCurLen:+sLen
if(iCurLen>iGlobalLen){var aSects=oTest.childNodes,iSectLen=aSects.length,iTargetLen=0,aVSects=[]
for(var iS=0;iS<iSectLen;iS++){aVSects[iS]={current:+aSects[iS].childNodes.length,target:0}}
var iSect=-1
if(!!forceIncludeId&&sIncludeTestId==oTest.getAttribute("id")){var sIncSectId=oInclude.parentNode.getAttribute("id")
for(var iS=0;iS<iSectLen;iS++){if(aSects[iS].getAttribute("id")==sIncSectId){iSect=iS
break}}}
if(iSect==-1){iSect=Math.floor(Math.random()*iSectLen)}
while(iTargetLen<iGlobalLen){oVSect=aVSects[iSect]
if(oVSect.current){oVSect.current--
oVSect.target++
iTargetLen++}
iSect=(iSect+1)%iSectLen}
for(var iSect=0;iSect<iSectLen;iSect++){aSects[iSect].setAttribute("questNumber",aVSects[iSect].target)}
oTest.setAttribute("testMode","bank")}}
if(oTest.getAttribute("testMode")=="bank"){var aSects=oTest.childNodes
for(var iSect=0;iSect<aSects.length;iSect++){var oSect=aSects[iSect],iCurLen=oSect.childNodes.length,sLen=oSect.getAttribute("questNumber"),iDesiredLen=!sLen||sLen=="All"?iCurLen:+sLen
while(iCurLen>iDesiredLen){var oSelNode=oSect.childNodes[Math.floor(Math.random()*iCurLen)]
if(oSelNode.getAttribute("id")!=forceIncludeId){oSect.removeChild(oSelNode)
iCurLen--}}}}
if(oTest.getAttribute("scoreMethod")=="normal"||oTest.getAttribute("scoreMethod")==null){var aQuests=oTest.selectNodes(".//page"),aSects=oTest.childNodes,iQuestLen=aQuests.length
if(iQuestLen>0){var iScore=getPrecision(100/iQuestLen)
for(var iQst=0;iQst<iQuestLen;iQst++)aQuests[iQst].setAttribute("maxScore",iScore)
for(var iSect=0;iSect<aSects.length;iSect++){var iWeight=aSects[iSect].childNodes.length*iScore
aSects[iSect].setAttribute("weight",iWeight)}}}else if(oTest.getAttribute("scoreMethod")=="sect"){var aSects=oTest.childNodes,iAutoSect=0,iNoneSect=0,iDiff,iConfigScore=0
for(var iSect=0;iSect<aSects.length;iSect++){var iWeight=aSects[iSect].getAttribute("weight")
if(iWeight=="Auto"||iWeight==null){iAutoSect++}else if(iWeight=="None"){aSects[iSect].setAttribute("weight","0")
iNoneSect++}else{iConfigScore+=iWeight*1||0}}
if(iConfigScore<100){if(iAutoSect==0){iDiff=getPrecision((100-iConfigScore)/(aSects.length-iNoneSect))
for(var iSect=0;iSect<aSects.length;iSect++){var iW=aSects[iSect].getAttribute("weight")
if(iW!=="0"){aSects[iSect].setAttribute("weight",+iW+iDiff)}}}else{iDiff=getPrecision((100-iConfigScore)/iAutoSect)}}
for(var iSect=0;iSect<aSects.length;iSect++){var oSect=aSects[iSect],aQuests=oSect.selectNodes(".//page"),iQuestLen=aQuests.length
if(iQuestLen>0){var iScore=oSect.getAttribute("weight")
if(iScore=="Auto"||iScore==null){iScore=iDiff
oSect.setAttribute("weight",iScore)}
iScore=getPrecision(iScore/iQuestLen)
for(var iQst=0;iQst<iQuestLen;iQst++){var oQuestNode=aQuests[iQst]
oQuestNode.setAttribute("maxScore",iScore)
oQuestNode.setAttribute("sectType",oSect.getAttribute("sectType")||"question")}}}}
var sTL=oTest.getAttribute("timeLimit")
if(sTL){var aSects=oTest.childNodes,sPeriod=oTest.getAttribute("limitPeriod")
if(sTL=="limited"||sTL=="sect"){for(var iSect=0;iSect<aSects.length;iSect++){var oSect=aSects[iSect],sSectMode=oSect.getAttribute("timeLimit")
if(sSectMode=="global"){if((!oTest.getAttribute("sectionOrder")||oTest.getAttribute("sectionOrder")=="normal")&&(!oTest.getAttribute("questionOrder")||oTest.getAttribute("questionOrder")=="normal")){oSect.setAttribute("globalTime",(oSect.getAttribute("limitPeriod")||"10min"))}}else{var aQuests=oSect.selectNodes(".//page"),iQuestLen=aQuests.length,sSectPeriod=sTL=="sect"?(sSectMode=="limited"?(oSect.getAttribute("limitPeriod")||"5sec"):0):(sPeriod||"5sec")
if(sSectPeriod!=0){for(var iQst=0;iQst<iQuestLen;iQst++){aQuests[iQst].setAttribute("limitPeriod",sSectPeriod)}}}}}
if(sTL=="global"){oTest.setAttribute("globalTime",(sPeriod||"10min"))}}
if(oTest.getAttribute("questionOrder")=="rand"){if(sIncludeTestId&&oTest.getAttribute("id")==sIncludeTestId&&oInclude.nodeName=="section"&&oTest.childNodes.length>1){oTest.insertBefore(oInclude,oTest.firstChild)}
for(var iSect=0;iSect<aSects.length;iSect++){var oSect=aSects[iSect],aQuests=oSect.selectNodes(".//page"),iQuestLen=aQuests.length
oSect.setAttribute("vtitle",oSect.getAttribute("title"))
for(var iQst=0;iQst<iQuestLen;iQst++){aQuests[iQst].setAttribute("vsectindex",iSect)}}
oTest.setAttribute("vsections","true")
for(var iSc=oTest.childNodes.length-1;iSc>0;iSc--){var oS=oTest.childNodes[iSc]
while(oS.hasChildNodes()){oTest.firstChild.appendChild(oS.firstChild)}
oS.setAttribute("skip","hide")}
if(oTest.firstChild.getAttribute("skip")!="hide"){oTest.firstChild.setAttribute("skip","true")}
oTest.firstChild.setAttribute("questionOrder","rand")
oTest.setAttribute("questionOrder","sect")}
if(oTest.getAttribute("questionOrder")=="sect"){var aSects=oTest.childNodes
for(var iSect=0;iSect<aSects.length;iSect++){var oSect=aSects[iSect]
if(oSect.getAttribute("questionOrder")=="rand"){var oNewSect=oSect.cloneNode(false)
while(oSect.hasChildNodes()){oNewSect.appendChild(oSect.childNodes[Math.floor(Math.random()*oSect.childNodes.length)])}
oTest.replaceChild(oNewSect,oSect)}}}
if(oTest.getAttribute("sectionOrder")=="rand"&&oTest.getAttribute("vsections")!="true"){var oNewTest=oTest.cloneNode(false)
while(oTest.hasChildNodes()){oNewTest.appendChild(oTest.childNodes[Math.floor(Math.random()*oTest.childNodes.length)])}
oTest.parentNode.replaceChild(oNewTest,oTest)}}}
function loadResource(oResNode,bExcludeLayers){var sId=oResNode.getAttribute("id"),sTheme=findAttribute("theme",oResNode,null,true),sThemeURL=null,sMaster=findAttribute("master",oResNode),sMenu=findAttribute("menu",oResNode),iPending=1,oResults={}
oStatus.loading=true
if(!sMaster||sMaster.charAt(0)=="@"&&sMaster.slice(0,4)!="@NEW")sMaster="@"
if(!sMenu||sMenu.charAt(0)=="@"&&sMenu.slice(0,4)!="@NEW")sMenu="@"
document.body.clearListeners("onstatuschange")
document.body.clearListeners("onvarchange")
document.body.clearListeners("oninitpage")
document.body.clearListeners("onfinishpage")
document.body.clearListeners("onlockchange")
document.body.clearListeners("onbeforepersist")
document.body.clearListeners("onnewcontent")
document.body.clearListeners("ondisplaychange")
document.body.clearRehash(oDocContent)
clearTimeout(mediaMonitor.timer)
mediaMonitor.media=[]
oResource=oResNode
oTopLevelLayer.style.cssText=sTopBaseStyle
if(window.oGameStyle&&!oGameStyle.disabled){oDocContent.innerHTML=""
oGameStyle.disabled=true}
if(sTheme&&sTheme!=sCurrentTheme){if(sTheme.charAt(0)!="@"){sThemeURL=getThemeURL(sTheme)
iPending++
httpLoader("GET",sThemeURL,allDone)}}
if(!bExcludeLayers){iPending++
loadLayer("master",oMasterContent,sMaster,layerDone)
iPending++
loadMenuLayer()}
function loadMenuLayer(){if(sMenu!="@"){var oLayerNode=oConfig.selectSingleNode(".//*[@id=\""+sMenu+"\"]")
if(oLayerNode){var sApply=oLayerNode.getAttribute("dynApply")
if(sApply=="all"||(sApply=="chapter"&&oResNode.nodeName!="page")||(sApply=="page"&&oResNode.nodeName=="page")){oResults.dynPosition=oLayerNode.getAttribute("dynPosition")||"below"
loadLayer("menu",oMenuContent,sMenu,layerDone)
return}}}
loadLayer("menu",oMenuContent,"@",layerDone)}
iPending++
if(oPersist[sId]){resourceDone(null,oPersist[sId])}else{var sLoadPrjId=oResNode.getAttribute("from")||sProjectId,sLoadDocId=oResNode.getAttribute("copyof")||sId
httpLoader("GET",cacheURL(GET_PROJECT_RESOURCE+"?readonly=1&project="+encodeURIComponent(sLoadPrjId)+"&resource="+encodeURIComponent(sLoadDocId)),resourceDone)}
function layerDone(sLayerName,sStyle,sContent){oResults[sLayerName]={content:sContent,style:sStyle}
allDone()}
function resourceDone(oXH,oPersistDoc){var sContent="",sStyle=""
if(oPersistDoc){sContent=oPersistDoc.content
sStyle=oPersistDoc.globalStyle}else if(oXH&&(oXH.status==200||oXH.status==0)){sContent=oXH.responseText.replace(/<!-- (.*) -->\r?\n/,"")
sStyle=RegExp.$1}
oResults.doc={content:sContent,style:sStyle}
allDone()}
allDone()
function allDone(){if(--iPending>0)return
if(sTheme&&sTheme!=sCurrentTheme)
loadTheme(sTheme,sThemeURL)
setLayerContent(oMasterContent,oResults.master.style,oResults.master.content)
if(oResults.dynPosition&&Classes.get(oMenuContent)!=oResults.dynPosition)
Classes.set(oMenuContent,oResults.dynPosition)
setLayerContent(oMenuContent,oResults.menu.style,oResults.menu.content)
setContent(oResults.doc.content,oResults.doc.style)
var oMasterNode=sMaster&&sMaster!="@"&&oConfig.selectSingleNode('.//*[@id="'+sMaster+'"]'),sDevice=oResource&&oResource.getAttribute("device")||oMasterNode&&oMasterNode.getAttribute("device")||oConfig.firstChild.getAttribute("device")||"desktop",bMobile=sDevice!="desktop"
if(bMobile!=Classes.has(oTopLevelLayer,"mobile"))
Classes[bMobile?"add":"remove"](oTopLevelLayer,"mobile")
scanKeyCombos()
if(!oPopupArgs)
initPage(oResNode)
document.body.fireEvent("onnavigate")
oResNode=null}}
function setContent(sContent,sGlobalStyle){if(sGlobalStyle!=null){if(sGlobalStyle!=oDocContent.style.cssText)oDocContent.style.cssText=sGlobalStyle
oDocContent.globalStyle=sGlobalStyle
var aProps=sGlobalStyle.match(/(?:background|cursor)[^;]*/ig)
if(aProps)oTopLevelLayer.style.cssText+=";"+aProps.join(";")}
switchEmbeds(oDocContent,"off",true)
oDocContent.innerHTML=sContent
setAreaVisibility(oDocContent)
fixBGPositionBug(oDocContent)
EmuBehavior.scanForBehaviors(oDocContent)
mediaMonitorScan(oDocContent)}
function getThemeURL(sTheme){return cacheURL(GENERIC_REDIRECTOR+"?cache=1&t=res&readonly=1&project="+encodeURIComponent(sProjectId)+"&resource="+encodeURIComponent(sTheme))}
function loadTheme(sTheme,sThemeURL){if(sTheme.charAt(0)!="@"){if(oTheme.disabled)oTheme.disabled=false
oTheme.href=sThemeURL||getThemeURL(sTheme)}else if(!oTheme.disabled){oTheme.disabled=true}
sCurrentTheme=sTheme}
function loadLayer(sLayerName,oLayer,sId,fCallback){var sLastId=oLayer.current
if(sLastId!=sId){oLayer.current=sId
document.body.clearRehash(oLayer)
if(sId=="@"){fCallback(sLayerName,"","")}else{var oLayerNode=oConfig.selectSingleNode('//*[@id="'+sId+'"]'),sLoadPrjId=oLayerNode&&oLayerNode.getAttribute("from")||sProjectId,sLoadDocId=oLayerNode&&oLayerNode.getAttribute("copyof")||sId
httpLoader("GET",cacheURL(GET_PROJECT_RESOURCE+"?readonly=1&project="+encodeURIComponent(sLoadPrjId)+"&resource="+encodeURIComponent(sLoadDocId)),function(oXH){if(oXH.status==200||oXH.status==0){var sText=oXH.responseText.replace(/<!-- (.*) -->\r?\n/,"")
fCallback(sLayerName,RegExp.$1,sText)}else{fCallback(sLayerName,"","")}})}}else{fCallback(sLayerName,oLayer.cachedStyle,null)
document.body.fireRehash(oLayer)}}
function setLayerContent(oLayer,sStyle,sContent){if(oLayer.style.cssText!=sStyle)oLayer.style.cssText=sStyle
if(sStyle){var aProps=sStyle.match(/(?:background|cursor)[^;]*/ig)
if(aProps)oTopLevelLayer.style.cssText+=";"+aProps.join(";")}
oLayer.cachedStyle=sStyle
if(sContent!==null){switchEmbeds(oLayer,"off",true)
oDocContent.innerHTML=""
oLayer.innerHTML=sContent
fixBGPositionBug(oLayer)
EmuBehavior.scanForBehaviors(oLayer)
mediaMonitorScan(oLayer)}
setAreaVisibility(oLayer)}
function mediaMonitorScan(oLayer){var oEmbeds=getByTagAll(oLayer,"embed")
var bShowFlash=false
for(var iEmbed=0;iEmbed<oEmbeds.length;iEmbed++){var oEmbed=oEmbeds[iEmbed]
if(!oEmbed.getAttribute("comptype")&&oEmbed.src&&oEmbed.src.slice(-4).toLowerCase()==".swf")oEmbed.setAttribute("comptype","flash")
if(!bShowFlash&&oEmbed.getAttribute("comptype")=="flash"&&getTag(oEmbed.parentNode)!="flashape")bShowFlash=true}
for(var iEmbed=0;iEmbed<oEmbeds.length;iEmbed++){var oEmbed=oEmbeds[iEmbed]
if(oEmbed.getAttribute("comptype")=="media"||(oEmbed.getAttribute("comptype")=="flash"&&!bShowFlash))mediaMonitorCollect(oEmbed)
if((bShowFlash||oEmbed.getAttribute("comptype")!="flash")&&!Classes.has(oEmbed,"showembed"))Classes.add(oEmbed,"showembed")}
mediaMonitor()}
function mediaMonitorCollect(oEmbed){var aEmbeds=mediaMonitor.media
for(var iE=aEmbeds.length;iE--;)
if(aEmbeds[iE]==oEmbed)
return
aEmbeds.push(oEmbed)}
mediaMonitor.media=[]
function mediaMonitor(){var aEmbeds=mediaMonitor.media
clearTimeout(mediaMonitor.timer)
for(var iEmbed=aEmbeds.length;iEmbed--;){var oEmbed=aEmbeds[iEmbed]
if(oEmbed.getAttribute("comptype")=="media"){if(oEmbed.HasError&&oEmbed.Duration==0){if(oEmbed.ReadyState==4){oEmbed.Open(decodeURIComponent(oEmbed.getAttribute("filename")))}}else if(oEmbed.ReadyState==4){aEmbeds.splice(iEmbed,1)
var sSrc=oEmbed.getAttribute("fileName")
if(sSrc&&oEmbed.Filename!=sSrc)oEmbed.Open(sSrc)}}
if(oEmbed.getAttribute("comptype")=="flash"){try{if(oEmbed.PercentLoaded()>1){Classes.add(oEmbed,"showembed")
aEmbeds.splice(iEmbed,1)}}catch(e){}}}
if(aEmbeds.length)mediaMonitor.timer=setTimeout(mediaMonitor,250)}
function setAreaVisibility(oEl){;/*@if(@_jscript)
var oParent=oEl,oEl=oEl.firstChild,sOrig=oParent.innerHTML
oParent.style.visibility="visible"
while(oEl){if(oEl.parentNode!=oParent){oParent.innerHTML=sOrig
return}
if(oEl.nodeType==1){var bSkip=false
if(!oEl.currentStyle.hasLayout){if(getTag(oEl)=="UL"){var oWrapper=createCustom("ulwrapper")
oWrapper.style.zoom=1
oWrapper.style.visibility="visible"
oWrapper.style.display="block"
oEl.replaceNode(oWrapper)
oWrapper.appendChild(oEl)
oEl=oWrapper}else{oEl.style.zoom=1
if(/\s$/.test(oEl.innerHTML)){oParent.insertBefore(document.createTextNode(" "),oEl.nextSibling)
bSkip=true}}}
if(getTag(oEl)=="IMG"&&!/\.(jpe?g|png|bmp)$/i.test(oEl.src)){if(oEl.readyState=="complete"){oEl=wrapImage(oEl)}else{oEl.emuReadyState="loading"
oEl.attachEvent("onload",wrapImageHandler)}}
if(oEl.style.visibility!="hidden"&&oEl.currentStyle.visibility!="hidden")oEl.style.visibility="visible"
if(bSkip)oEl=oEl.nextSibling
oEl=oEl.nextSibling}else if(oEl.nodeType==3){var oNew=document.createElement("span")
oParent.insertBefore(oNew,oEl)
oNew.appendChild(oEl)
oNew.innerHTML=oNew.innerHTML.replace(/(\S+)/g,"<span style=\"z-index:-1;visibility:visible;zoom:1\">$1</span>")
oEl=oNew.nextSibling
oNew.removeNode(false)}else{oEl=oEl.nextSibling}}
oParent.style.visibility="hidden"
;/*@else @*/
oEl.setAttribute("forcetransparent","1")
;/*@end @*/}
function wrapImageHandler(event){event.srcElement.detachEvent("onload",wrapImageHandler)
wrapImage(event.srcElement)
event.srcElement.emuReadyState="complete"}
function wrapImage(oEl){var oNew=createCustom("imagenodewrapper"),oParent=oEl.parentNode,iOrigWidth=oEl.clientWidth,iOrigHeight=oEl.clientHeight
if(iOrigWidth==0||iOrigHeight==0){var oCur=oEl,aNone=[]
while(oCur&&oCur.nodeType==1){if(oCur.currentStyle.display=="none"){oCur.runtimeStyle.display="block"
aNone.push(oCur)}
oCur=oCur.parentNode}
iOrigWidth=oEl.offsetWidth
iOrigHeight=oEl.offsetHeight
for(var iNone=aNone.length;iNone--;)aNone[iNone].runtimeStyle.removeAttribute("display")}
oParent.replaceChild(oNew,oEl)
oNew.appendChild(oEl)
oNew.style.cssText=oEl.style.cssText
if(/auto|%/.test(oNew.currentStyle.width))oNew.style.width=iOrigWidth+"px"
if(/auto|%/.test(oNew.currentStyle.height))oNew.style.height=iOrigHeight+"px"
Classes.set(oNew,Classes.get(oEl))
Classes.set(oEl,"")
var aAttr=oEl.attributes
for(var iA=aAttr.length;iA--;){var sAttr=aAttr[iA].name
if(sAttr.indexOf("trans")==0){oNew.setAttribute(sAttr,oEl.getAttribute(sAttr))
oEl.removeAttribute(sAttr)}}
oEl.style.cssText="top:0;left:0;position:relative;z-index:-1"
oEl.style.width=oNew.currentStyle.width
oEl.style.height=oNew.currentStyle.height
return oNew}
function findAttribute(sAttr,oFinder,sDefault,bForceInherit){var sRes
sDefault=sDefault||"@NONE"
while(oFinder&&oFinder.nodeType==1&&(oFinder.nodeName!="config"||bForceInherit)&&!sRes){sRes=oFinder.getAttribute(sAttr)
oFinder=oFinder.parentNode}
if(sRes=="@INHERIT")return findAttribute(sAttr,oFinder,sDefault,true)
return sRes||sDefault}
function getNavNode(oResNode,sNavDir,bNoSkip,bSilent,bNoPageSkip){var oNode=oResNode,oNavNode
if(oNode){var sLock=oNode.getAttribute("lock")
if(!sLock){sLock=findAttribute("lock",oNode,"false")}
switch(sNavDir){case"next":if(!oNode.hasChildNodes()||sLock=="true"){oNavNode=oNode.nextSibling
while(!oNavNode){oNode=oNode.parentNode
if(oNode.nodeName=="structure")break
oNavNode=oNode.nextSibling}}else{oNavNode=oNode.firstChild}
break
case"previous":oNavNode=oNode.previousSibling
if(oNavNode&&sLock!="true"){while(oNavNode.hasChildNodes()){oNavNode=oNavNode.lastChild}}else if(oNode.parentNode&&oNode.parentNode.nodeName!="structure"){oNavNode=oNode.parentNode}
break
case"up":if(oNode.parentNode.nodeName!="structure"){oNavNode=oNode.parentNode}
break
case"nextchap":oNavNode=oNode
do{oNavNode=getNavNode(oNavNode,"next",true,true)}while(oNavNode&&oNavNode.nodeName=="page")
break
case"prevchap":oNavNode=oNode
do{oNavNode=getNavNode(oNavNode,"previous",true,true)}while(oNavNode&&oNavNode.nodeName=="page")
if(oNode&&oNavNode&&oNode.parentNode&&oNavNode==oNode.parentNode){do{oNavNode=getNavNode(oNavNode,"previous",true,true)}while(oNavNode&&oNavNode.nodeName=="page")}
break
default:oNavNode=oResNode
break}
if(!oNavNode){return false}
var sSkip=bNoSkip?"false":oNavNode.getAttribute("skip"),sLock=oNavNode.getAttribute("lock")
if(oNavNode.nodeName!="page"){if(!sSkip||sSkip=="@DEFAULT"){sSkip=findAttribute("skip",oNavNode,"false")
oNavNode.setAttribute("skip",sSkip)}}
if(!sLock){sLock=findAttribute("lock",oNavNode,"false")}
if(bNoPageSkip==true&&oNavNode.nodeName=="page"&&(sSkip=="true"||sSkip=="hide")){sSkip="false"}
if(sSkip=="true"||sSkip=="hide"||sLock=="true"){var oNewNav=getNavNode(oNavNode,(sNavDir&&sNavDir!="custom"?sNavDir:"next"),null,bSilent,bNoPageSkip)
if(oNewNav){return oNewNav}else{return false}}else{return oNavNode}}
return false}
function initPage(oResNode,bPartial,fActivityDone){var oAncestor=oResNode.parentNode.parentNode,iScormNdxCnt=0
setLockState(0,true)
setResVisited(oResNode)
var aFB=getByTagAll(oTopLevelLayer,"feedback"),oLockTags={hotpop:"hotspots",mplayer:"mplayer",objtrans:"objtrans"},iLock=0
for(var i=0;i<aFB.length;i++){var oFB=aFB[i]
if((oFB.getAttribute("locknext")=="true"||oFB.getAttribute("locknext")=="all")&&!bPartial){iLock++}
if(oAncestor.getAttribute("game")=="true"&&oAncestor.getAttribute("forceOneTry")=="true"){oFB.setAttribute("attempts","1")}
oFB.setAttribute("scormSubNdx",iScormNdxCnt++)
oFB.onactivityinit=activityInit
oFB.onactivitycheck=activityCheck
oFB.onactivitydone=fActivityDone?fActivityDone:activityDone}
var oSVs=getByTagAll(oTopLevelLayer,"survey")
for(var iS=0;iS<oSVs.length;iS++){var oSV=oSVs[iS],bItems=oSV.getAttribute("storeitems")=="true",bTotal=oSV.getAttribute("storetotal")=="true",bAvg=oSV.getAttribute("storeavg")=="true"
if(bItems){var oItems=getByTagAll(oSV,"svitem")
for(var iI=0;iI<oItems.length;iI++){oItems[iI].setAttribute("scormSubNdx",iScormNdxCnt++)}}
if(bTotal)oSV.setAttribute("scormSubNdx_total",iScormNdxCnt++)
if(bAvg)oSV.setAttribute("scormSubNdx_avg",iScormNdxCnt++)
oSV.onactivityinit=activityInit
oSV.onactivitycheck=activityCheck
oSV.onactivitydone=activityDone}
var oRs=getByTagAll(oTopLevelLayer,"rating")
for(var iR=0;iR<oRs.length;iR++){var oR=oRs[iR]
if(oR.getAttribute("autoscorm")!="false"){oR.setAttribute("scormSubNdx",iScormNdxCnt++)
oR.onactivityinit=activityInit
oR.onactivitycheck=activityCheck
oR.onactivitydone=activityDone}}
for(var sTag in oLockTags){var aLE=getByTagAll(oTopLevelLayer,sTag)
for(var iL=0;iL<aLE.length;iL++){var oLE=aLE[iL]
if(oLE.getAttribute("locknext")=="true"&&!oLE.getAttribute("complete")){while(getTag(oLE)!=oLockTags[sTag]&&getTag(oLE)!="body")oLE=oLE.parentNode
if(getTag(oLE)==oLockTags[sTag]){oLE.oncomplete=activityCompleted
iLock++}}}}
if(iLock>0&&oResNode.getAttribute("unlocked")!="true"){setLockState(iLock)}else{setResDone(oResNode)}
document.body.attachEvent("onnewcontent",function(event){var oElem=event.element,aFB=getByTagAll(oElem,"feedback")
for(var iFB=aFB.length;iFB--;){var oFB=aFB[iFB]
oFB.onactivityinit=activityInit
oFB.onactivitycheck=activityCheck
oFB.onactivitydone=fActivityDone?fActivityDone:activityDone}})
if(oAncestor.nodeName=="test"){var sFeedbackMode=oAncestor.getAttribute("feedbackMode")||"test"
oResNode.setAttribute("testPage","true")
oResNode.setAttribute("persist","true")
oResNode.setAttribute("partial",oAncestor.getAttribute("partial")||"normal")
if(sFeedbackMode=="test"){var oCheck=getByTagAll(oTopLevelLayer,"fbvalidate")
for(var i=0;i<oCheck.length;i++){oCheck[i].style.visibility="hidden"}}
if(oAncestor.getAttribute("reviewMode")=="true"){var aFB=getByTagAll(oTopLevelLayer,"feedback")
for(var i=0;i<aFB.length;i++){aFB[i].review=true}}else{oResource.setAttribute("total",oResource.getAttribute("finalTotal")||0)
oResource.setAttribute("correct",oResource.getAttribute("finalCorrect")||0)
oResource.removeAttribute("accumulatedScore")}}
if(!bPartial){if(oAncestor.nodeName=="test"||oResNode.nodeName=="section"){var oGlobal
if(oAncestor.getAttribute("globalTime")||oResNode.parentNode.getAttribute("globalTime")||oResNode.getAttribute("globalTime")){if(oAncestor.getAttribute("globalTime")&&oAncestor.getAttribute("globalTimeStarted")!="true"){oGlobal=oAncestor}else if(oResNode.getAttribute("globalTime")&&oResNode.getAttribute("globalTimeStarted")!="true"){oGlobal=oResNode}else if(oResNode.parentNode.getAttribute("globalTime")&&oResNode.parentNode.getAttribute("globalTimeStarted")!="true"){oGlobal=oResNode.parentNode}
if(oGlobal){initGlobalTimer(oGlobal)}}else if(oAncestor.nodeName=="test"&&oAncestor.getAttribute("game")!="true"&&oAncestor.getAttribute("reviewMode")!="true"){window.lTimer=initLocalTimer(oResNode,oAncestor)
if(window.lTimer)
document.body.attachEvent("onfinishpage",finishLocalTimer)}}
document.body.attachEvent("oninitpage",Hover.initpage)
document.body.attachEvent("onfinishpage",Hover.finishpage)
var aBox=getByTagAll(oTopLevelLayer,"box")
for(var iBox=aBox.length;iBox--;){var oBox=aBox[iBox]
if(oBox.getAttribute("overrule")||oBox.getAttribute("vhref"))opacityOverlay(aBox[iBox])}
var aLinks=getByTagAll(oTopLevelLayer,"vlink")
for(var iL=0;iL<aLinks.length;iL++){var oL=aLinks[iL]
if(!oL.getAttribute("overrule"))oL.setAttribute("overrule","over")
opacityOverlay(oL)}
if(oResNode.nodeName=="test"){var oStage=getByTagOne(oTopLevelLayer,"gamestage")
if(oStage){var oGame=oStage.parentNode,bScreenReader=bStatic&&!bSnapshot
oResNode.setAttribute("game","true")
oResNode.setAttribute("feedbackMode","question")
oResNode.setAttribute("skip","false")
oResNode.setAttribute("forceOneTry",(oGame.getAttribute("forceOneTry")=="true"||oGame.getAttribute("continueMode")=="auto check")?"true":"false")
oResNode.removeAttribute("reviewMode")
var oSects=oResNode.selectNodes(".//section")
for(var iS=0;iS<oSects.length;iS++){var oSc=oSects[iS],oQs=oSc.selectNodes(".//page")
oSc.setAttribute("skip","hide")
for(var iQ=0;iQ<oQs.length;iQ++){oQs[iQ].setAttribute("skip",bScreenReader?"false":"hide")}}
if(bScreenReader){oResNode.setAttribute("skip","hide")
var oNextRes=getNavNode(oResNode,"next")
oDocContent.innerHTML=""
if(oNextRes){setTimeout(function(){document.body.fireEvent("onfinishpage");loadResource(oNextRes);},10)
return}}else{initGameStyle()}}}
oStatus.loading=false
document.body.fireEvent("oninitpage")
if(bSnapshot){var aImg=getByTagAll(oTopLevelLayer,"img")
for(var iI=aImg.length;iI--;)if(aImg[iI].attachEvent)aImg[iI].attachEvent("onerror",imgError)
var aMarquee=getByTagAll(oTopLevelLayer,"marquee")
for(var iM=aMarquee.length;iM--;)try{aMarquee[iM].contentEditable=true;}catch(e){}
setTimeout(waitForPage,10)}else{if(sProjectId=="Res"){State.store(oConfig,oResNode.getAttribute("id"))}}}}
function waitForPage(){if(!arguments.callee.timeLimit)arguments.callee.timeLimit=new Date().getTime()+15000
if(new Date().getTime()<arguments.callee.timeLimit)try{var aEls=oTopLevelLayer.all,i=arguments.callee.firstIdx||0
for(;i<aEls.length;i++){var oEl=aEls[i]
if(getTag(oEl)=="SCRIPT")continue
if(getTag(oEl)=="IMG"&&oEl.readyState=="uninitialized"&&oEl.hasErrored)continue
if(getTag(oEl)=="EMBED"&&oEl.getAttribute("comptype")=="flash"&&oEl.PercentLoaded()<100)throw"not loaded"
if(oEl.readyState&&oEl.readyState!="complete")throw"not ready"
if(oEl.emuReadyState&&oEl.emuReadyState!="complete")throw"not ready"
try{if(oEl.getRendered)throw"has method"}catch(e){if(!oEl.getRendered())throw"not rendered"}}}catch(e){arguments.callee.firstIdx=i
return setTimeout(waitForPage,50)}
delete arguments.callee.firstIdx
delete arguments.callee.timeLimit
mediaMonitor()
switchEmbeds(oTopLevelLayer,"off")
var oEvent=document.createEventObject()
document.fireEvent("ondataavailable",oEvent)}
function imgError(event){event.srcElement.hasErrored=true}
function initGameStyle(){var oG=getByTagOne(oTopLevelLayer,"gamestage").parentNode,sG=oG.getAttribute("moduleName")
if(sG){oGameStyle.disabled=false
oGameStyle.href=cacheURL(GENERIC_REDIRECTOR+"?cache=1&t=res&readonly=1&project="+encodeURIComponent(sProjectId)+"&resource="+encodeURIComponent("../../Themes/"+sG+".css"))}}
function initGlobalTimer(oGlobal,iRate,bUnCache){var targetNode,oGlobalTimer=getChildByTagOne(oTopLevelLayer,"navtimer")
if(oGlobalTimer&&oGlobalTimer.globalTimer){oGlobalTimer.persisted=true
oGlobalTimer.invalidateTimer()}else{oGlobalTimer=createNavTimer(oTopLevelLayer)}
if(oGlobalTimer){if(oGlobal.nodeName=="test"){targetNode=oGlobal.nextSibling}else{targetNode=oGlobal==oGlobal.parentNode.lastChild?oGlobal.parentNode.nextSibling:getNavNode(oGlobal.lastChild,"next")}
oGlobal.setAttribute("globalTimeStarted","true")
oGlobalTimer.setAttribute("period",oGlobal.getAttribute("globalTime"))
oGlobalTimer.setAttribute("ontimer","custom")
if(targetNode){oGlobalTimer.setAttribute("id",targetNode.getAttribute("id"))}
oGlobalTimer.globalTimer=true
oTopLevelLayer.appendChild(oGlobalTimer)
if(oGlobalTimer.persisted){oGlobalTimer.style.visibility="visible"
oGlobalTimer.initTimer()}}
if(!oGlobalTimer){oGlobalTimer=getChildByTagOne(oTopLevelLayer,"navtimer")}
if(oGlobalTimer){var oTimers=getByTagAll(oTopLevelLayer,"navtimer")
if(bUnCache){window.cachedTimer=null}
window.gTimer={}
window.gTimer.timer=oGlobalTimer
window.gTimer.rate=iRate
for(var iT=0;iT<oTimers.length;iT++){var oTimer=oTimers[iT]
if(oTimer.uniqueID!=window.cachedTimer&&(oTimer.getAttribute("ontimer")=="auto"||!oTimer.getAttribute("ontimer"))&&!oTimer.globalTimer){(function(){if(oTimer.emuReadyState!="complete"||oGlobalTimer.emuReadyState!="complete")
return setTimeout(arguments.callee,30)
oTimer.style.visibility="visible"
oTimer.initTimer(oGlobalTimer,iRate)})()
window.cachedTimer=oTimer.uniqueID
break}}}
return oGlobalTimer?{main:oGlobalTimer,visual:oTimers}:false}
function initLocalTimer(oResNode,oAncestor){var sPeriod=oResNode.getAttribute("limitPeriod"),bNew=false
if(sPeriod){var oTimer=getByTagOne(oContentArea,"navtimer")
if(!oTimer||oTimer.globalTimer){oTimer=createNavTimer()
bNew=true}
if(oTimer){oTimer.setAttribute("ontimer",oAncestor.getAttribute("feedbackMode")=="question"?"check":"check and next")
if(!oTimer.getAttribute("period")||oTimer.getAttribute("period")=="auto"){oTimer.setAttribute("autoPeriod","true")
oTimer.setAttribute("period",sPeriod)}
if(bNew){oDocContent.appendChild(oTimer)}else{try{oTimer.initTimer()}catch(e){}}}
return{main:oTimer}}
return false}
function finishLocalTimer(event){var oTimer=window.lTimer&&window.lTimer.main
if(oTimer){if(oTimer.startTime&&oTimer.calcPeriod){var iPeriod=oTimer.calcPeriod-(new Date().getTime()-oTimer.startTime)
if(iPeriod<2000){oTimer.startTime=new Date().getTime()-oTimer.calcPeriod-500
oTimer.updateTimer()}else{if(oResource.getAttribute("limitPeriodOrig")===null)
oResource.setAttribute("limitPeriodOrig",oResource.getAttribute("limitPeriod"))
oResource.setAttribute("limitPeriod",iPeriod)}}
if(oTimer.getAttribute("autoPeriod")=="true"){oTimer.setAttribute("period","auto")
oTimer.invalidateTimer()}}
window.lTimer=null}
function createNavTimer(){var oNT=createCustom("navtimer")
oNT.setAttribute("visible","false")
oNT.setAttribute("showmsg","false")
oNT.setAttribute("mode","horizontal")
oNT.setAttribute("ontimer","auto")
oNT.setAttribute("period","auto")
oNT.innerText="\xA0"
return oNT}
function evaluatePage(oCurResource,oNewResource,bForceFB,bExit){var oTestNode=oCurResource
while(oTestNode&&oTestNode.nodeName!="test"){oTestNode=oTestNode.parentNode}
if(oTestNode&&oTestNode.nodeName=="test"&&oTestNode.getAttribute("reviewMode")!="true"){if(oCurResource.nodeName=="page"&&oCurResource.getAttribute("timerDone")!="true"){var aFB=getByTagAll(oTopLevelLayer,"feedback")
var	bShowFB=bForceFB&&oTestNode.getAttribute("feedbackMode")=="question"
for(var i=0;i<aFB.length;i++){var oFB=aFB[i]
if(oFB.getAttribute("done")!="1"){oFB.silent=!bShowFB
oFB.checkAnswer(bForceFB)}}
calcResScore(oCurResource)}
var oLastPage=oTestNode.lastChild&&oTestNode.lastChild.lastChild
if((bExit&&oLastPage&&!getNavNode(oLastPage,"next")&&!oTestNode.selectSingleNode(".//page[not(@visited) and (not(@skip) or @skip=\"false\")]"))||(oNewResource&&oNewResource!=oTestNode&&oNewResource.parentNode!=oTestNode&&oNewResource.parentNode.parentNode!=oTestNode)){evaluateTest(oTestNode)}}
if(oNewResource){loadResource(oNewResource)}}
function calcResScore(oCurResource){var iTotal=oCurResource.getAttribute("total")||0,iCorrect=oCurResource.getAttribute("correct")||0,iMaxScore=oCurResource.getAttribute("maxScore")||0,iScore=0
if(iTotal!=0){var iAccuScore=(oResource.getAttribute("accumulatedScore")||0)*1
if(iAccuScore){if(iCorrect==iTotal)iAccuScore=iMaxScore
iScore=getPrecision(iAccuScore)}else{iScore=calcScoreByRules(oCurResource,iCorrect,iTotal,iMaxScore)}}
oCurResource.setAttribute("score",iScore)}
function calcScoreByRules(oCurResource,iCorrect,iTotal,iMaxScore){var sPartial=oCurResource.getAttribute("partial"),iFactor=iCorrect/iTotal,iScore=0
if(sPartial!="normal"&&iFactor>0&&iFactor<1){iFactor=sPartial=="full"?1:0}
iScore=getPrecision(iFactor*iMaxScore)
return iScore}
function evaluateTest(oTestNode,bForceUsed){if(oTestNode&&oTestNode.getAttribute("reviewMode")!="true"){var aSects=oTestNode.childNodes,oResults={totalscore:0,totalquestions:0,correct:0,incorrect:0,partial:0,hints:0,sections:[]},bVSectsion=oTestNode.getAttribute("vsections")=="true",iMaxAttempts=(oTestNode.getAttribute("attempts")||0)*1,iCurAttempts=(oTestNode.getAttribute("curAttempts")||0)*1,bGameTest=oTestNode.getAttribute("game")=="true"
oTestNode.setAttribute("curAttempts",iCurAttempts+1)
for(var iSect=0;iSect<aSects.length;iSect++){var oSect=aSects[iSect],oSectResults={totalquestions:0,totalscore:0,correct:0,incorrect:0,partial:0,hints:0,title:oSect.getAttribute(bVSectsion?"vtitle":"title")},aQuest=bVSectsion?oTestNode.selectNodes(".//page[@vsectindex=\""+iSect+"\"]"):oSect.childNodes
oResults.sections[iSect]=oSectResults
for(var iQuest=0;iQuest<aQuest.length;iQuest++){var oQuest=aQuest[iQuest],iScore=+oQuest.getAttribute("score")||0,iMaxScore,bCount=!bForceUsed||(oQuest.getAttribute("score")!==null)
setResDone(oQuest,true)
if(iMaxScore==0)continue
if(bGameTest&&oQuest.getAttribute("score")===null&&(!bStatic||bSnapshot)){continue}
iMaxScore=+oQuest.getAttribute("maxScore"),oSectResults.totalscore+=iScore
if(bCount)oSectResults.totalquestions++
if(iMaxScore&&getFixed(iScore)==getFixed(iMaxScore)){oSectResults.correct++}else if(!iScore){oSectResults.incorrect++}else{oSectResults.partial++}
if(oQuest.getAttribute("hints")){oSectResults.hints++}}
setResDone(oSect,true)
if(oSectResults.totalquestions&&oSectResults.correct==oSectResults.totalquestions&&!bForceUsed&&!oSectResults.hints){oSectResults.totalscore=+oSect.getAttribute("weight")}
oSect.setAttribute("totalscore",oSectResults.totalscore)
oSect.setAttribute("totalquestions",oSectResults.totalquestions)
oSect.setAttribute("correct",oSectResults.correct)
oSect.setAttribute("incorrect",oSectResults.incorrect)
oSect.setAttribute("part",oSectResults.partial)
oResults.totalscore+=oSectResults.totalscore
oResults.totalquestions+=oSectResults.totalquestions
oResults.correct+=oSectResults.correct
oResults.incorrect+=oSectResults.incorrect
oResults.partial+=oSectResults.partial
oResults.hints+=oSectResults.hints}
if(oResults.totalquestions&&oResults.correct==oResults.totalquestions&&!oResults.hints){oResults.totalscore=100}
oTestNode.setAttribute("totalscore",getFixed(oResults.totalscore))
oTestNode.setAttribute("totalquestions",oResults.totalquestions)
oTestNode.setAttribute("correct",oResults.correct)
oTestNode.setAttribute("incorrect",oResults.incorrect)
oTestNode.setAttribute("part",oResults.partial)
var sStatus="failed"
if(getFixed(oResults.totalscore)>=(oTestNode.getAttribute("passScore")*1||0))sStatus="passed"
oTestNode.setAttribute("status",sStatus)
LMSInterface.RegisterObjective(oTestNode)
if(window.gTimer){var oTimer=window.gTimer.timer
oTimer.invalidateTimer()
window.gTimer=null}
if(oTestNode.getAttribute("allowReview")=="false"){oTestNode.setAttribute("lock","true")}else{oTestNode.setAttribute("reviewMode","true")}}}
function finishPage(){var bRes=document.body.fireEvent("onfinishpage")!==false
if(bRes){persistPage()}
return bRes}
function persistPage(){var sPersist=findAttribute("persist",oResource,"true")
if(sPersist=="true"&&(getByTagOne(oDocContent,"feedback")||getByTagOne(oDocContent,"hotspots"))){var sId=oResource.getAttribute("id")
document.body.fireEvent("onbeforepersist")
oPersist[sId]={content:oDocContent.innerHTML,globalStyle:oDocContent.globalStyle}}else{oResource.removeAttribute("unlocked")}}
function activityInit(event){if(event.activity&&"correctPattern"in event){var oSrc=event.srcElement,iSubNdx=event.subindex!==null&&event.subindex!=undefined?event.subindex:oSrc.getAttribute("scormSubNdx"),iTotalEl=getByTagAll(oTopLevelLayer,"feedback").length
if(LMSInterface.PreRegisterInteraction)LMSInterface.PreRegisterInteraction(oResource,iSubNdx,iTotalEl,event.activity,event.correctPattern,event.unscored,event.start,event.desc)}}
function activityCheck(event){var oSrc=event.srcElement,sLock=oSrc.getAttribute("locknext"),iSubNdx=event.subindex!==null&&event.subindex!=undefined?event.subindex:oSrc.getAttribute("scormSubNdx")
if(oStatus.locked&&(sLock=="true"||sLock=="all")){if(sLock=="true"||event.correct==event.total||event.lastAttempt){setLockState(oStatus.locked-1)}}
if("userPattern"in event){if(LMSInterface.InteractionAnswered)LMSInterface.InteractionAnswered(oResource,iSubNdx,event.userPattern,event.checkTime,event.total,event.correct)}}
function activityDone(event){var oSrc=event.srcElement,iSubNdx=event.subindex!==null&&event.subindex!=undefined?event.subindex:oSrc.getAttribute("scormSubNdx"),iQIP=getByTagAll(oTopLevelLayer,"feedback").length
if(!oSrc.activityDone&&!event.unscored){if(oResource.getAttribute("testPage")=="true"){var iTotal=oResource.getAttribute("total")||0,iCorrect=oResource.getAttribute("correct")||0,iFinalTotal=oResource.getAttribute("finalTotal")||0,iFinalCorrect=oResource.getAttribute("finalCorrect")||0,iActivityTotal=event.total,iActivityCorrect=event.correct,iMaxScore=oResource.getAttribute("maxScore")||0
if(event.isDone&&!oSrc.silent){oResource.setAttribute("finalTotal",iFinalTotal*1+iActivityTotal)
oResource.setAttribute("finalCorrect",iFinalCorrect*1+iActivityCorrect)}
oResource.setAttribute("total",iTotal*1+iActivityTotal)
oResource.setAttribute("correct",iCorrect*1+iActivityCorrect)
if(iQIP>1){var iMaxPerQ=iMaxScore/iQIP,sAccuScore=oResource.getAttribute("accumulatedScore"),iAccuScore=sAccuScore?sAccuScore*1:0,iScore=calcScoreByRules(oResource,iActivityCorrect,iActivityTotal,iMaxPerQ)
oResource.setAttribute("accumulatedScore",iScore*1+iAccuScore)}}
oSrc.activityDone=true}
if(LMSInterface.RegisterInteraction)LMSInterface.RegisterInteraction(oResource,iSubNdx,iQIP,event.total,event.correct,event.activity,event.userPattern,event.unscored)}
function activityCompleted(event){if(oStatus.locked&&event.targetElement.getAttribute("locknext")=="true"){setLockState(oStatus.locked-1)}}
function setLockState(iLock,bInit){oStatus.locked=iLock
document.body.fireEvent("onlockchange")
if(iLock==0&&!bInit){oResource.setAttribute("unlocked","true")
setResDone(oResource)}}
function setResDone(oResNode,bForce,bSilent){var iChild=oResNode.childNodes.length,iDone=iChild&&oResNode.selectNodes('*[@done="true"] | *[@skip!="false" and not(.//*[not(@skip) or @skip="false"])]').length
if(bForce)oResNode.setAttribute("visited","true")
if((oResNode.getAttribute("visited")=="true"||findAttribute("skip",oResNode,"false")!="false")&&iChild==iDone){oResNode.setAttribute("done","true")
document.body.fireEvent("onstatuschange",{resource:oResNode})
if(oResNode.nodeName!="structure"){setResDone(oResNode.parentNode,false,true)}
if(!bSilent)document.body.fireEvent("onvarchange")}}
function setResVisited(oResNode){do{oResNode.setAttribute("visited","true")
oResNode=oResNode.parentNode}while(oResNode.nodeName!="structure"&&findAttribute("skip",oResNode,"false")!="false")}
function opacityOverlay(oElem,bChild){if(oElem.getAttribute("forcetransparent"))return
if(!bSnapshot&&oElem.currentStyle.position!="static"){;/*@if(@_jscript)
var sTag=getTag(oElem).toLowerCase()
if(oElem.canHaveHTML&&oElem.contentEditable!=="true"&&sTag!="table"&&sTag!="transalpha"&&sTag!="transalphainner"&&!getChildByTagOne(oElem,"transalpha")){var oTransObj=createCustom("transalpha")
oTransObj.appendChild(createCustom("transalphainner"))
oElem.appendChild(oTransObj)}
;/*@else @*/
oElem.setAttribute("forceopaque","1")
;/*@end @*/}
if(!bChild){var aChild=getByTagAll(oElem,"*")
for(var iChild=aChild.length;iChild--;){var oChild=aChild[iChild]
if(oChild.currentStyle.position!="static")opacityOverlay(oChild,true)}}}
function setDisabled(oEl,bDisabled){oEl.disabled=bDisabled
document.body.fireEvent("ondisabledchange",{element:oEl})}
function setOpacity(oEl,iOpacity){if("opacity"in oEl.style)
oEl.style.opacity=isNaN(iOpacity)?"":iOpacity
else if("filter"in oEl.style)
oEl.style.filter=isNaN(iOpacity)?"":"alpha(opacity="+(iOpacity*100)+")"}
var oScope={timerProxy:function(oOrigin,sTimerProc,oArg,iDelay){var oTimer=setTimeout(getTimerProc(oOrigin,sTimerProc,oArg),iDelay)
oOrigin=null
oArg=null
return oTimer}}
function getTimerProc(oOrigin,sTimerProc,oArg){return function(){try{if(bSnapshot){try{if(!oOrigin.getRendered)return}catch(e){if(oOrigin.getRendered())return}}
if(oOrigin&&oOrigin.ownerDocument.documentElement.contains(oOrigin)){oOrigin[sTimerProc](oArg)}}catch(oError){}
oArg=null}}
function callOnInit(fFunc,oThis,oArgs){document.body.attachEvent("oninitpage",function(){fFunc.apply(oThis,oArgs)})}
var Navigation={_goTo:function(sOperation,sId,bNoHistory){if(bSnapshot)return
if(oStatus.loading)return callOnInit(arguments.callee,this,arguments)
if(sOperation!="next"||!oStatus.locked){var oNewResource
if(sId){oNewResource=oConfig.selectSingleNode(".//*[@id=\""+sId+"\"]")}else{oNewResource=getNavNode(oResource,sOperation)}
if(oNewResource){var sSkip=oNewResource.getAttribute("skip")
if(sSkip!==null&&sSkip!="false"){oNewResource=getNavNode(oNewResource,sOperation)}
if(finishPage()!==false){if(!bNoHistory)History.collect()
if(oResource.getAttribute("testPage")=="true"||oResource.nodeName=="test"||oResource.nodeName=="section"){evaluatePage(oResource,oNewResource)}else{loadResource(oNewResource)}}}}},next:function(){this._goTo("next");},previous:function(){this._goTo("previous");},up:function(){this._goTo("up");},custom:function(sId,bNoHistory){this._goTo("custom",sId,bNoHistory);}}
var History={_data:{back:[],forward:[]},collect:function(){this._data.back.push(oResource.getAttribute("id"))
this._data.forward.length=0},go:function(sDir){var sOpp=sDir=="back"?"forward":"back"
var sId=this._data[sDir].pop()
this._data[sOpp].push(oResource.getAttribute("id"))
Navigation.custom(sId,true)},has:function(sDir){return this._data[sDir].length>0},save:function(){return this._data.back.join(":")+"|"+this._data.forward.join(":")},load:function(sData){var aData=sData.split("|")
if(aData.length==2){if(aData[0])this._data.back=aData[0].split(":")
if(aData[1])this._data.forward=aData[1].split(":")}}}
var Compress={flag:"\uF849",attributes:["title","description","menu","theme","master","persist","pageNum","chapPageTotal","chapPageNum"],replaces:[[/<config>[\s\S]*?<\/config>/g,""],["<page","\uF850"],["<chapter","\uF851"],["<test","\uF852"],["<section","\uF853"],["</chapter>","\uF854"],["</test>","\uF855"],["</section>","\uF856"],["page/","\uF860"],["chapter/","\uF861"],["test/","\uF862"],["section/","\uF863"],[" pageNum=","\uF864"],[" visited=\"true\"","\uF865"],["true","\uF866"],["false","\uF867"],[".html","\uF868"],[" id=\"","\uF869"]],compress:function(sStr){var aStr=Compress.getStr(),aRX=Compress.getRX(),sStr
for(var i=0;i<aRX.length;i++){sStr=sStr.replace(aRX[i][0],aStr[i][1])}
return Compress.flag+sStr},restore:function(sStr,oOrig){if(sStr.charAt(0)!=Compress.flag)return sStr
var sStr=sStr.substr(1),aStr=Compress.getStr(),aRX=Compress.getRX(),oXML=getXMLDocument(),oConfig=oOrig&&oOrig.selectSingleNode("/project/config")
for(var i=0;i<aStr.length;i++){if(aStr[i][1])sStr=sStr.replace(aRX[i][1],aStr[i][0])}
if(oXML.loadXML(sStr)&&oXML.firstChild){if(oConfig)oXML.firstChild.insertBefore(oConfig.cloneNode(true),oXML.firstChild.firstChild)
if(oOrig){var aNodes=oXML.selectNodes("//*[@id]"),oHash={}
for(var i=0;i<aNodes.length;i++){var oNode=aNodes[i]
oHash[oNode.getAttribute("id")]=oNode}
var aNodes=oOrig.selectNodes("//*[@id]")
for(var i=0;i<aNodes.length;i++){var oNode=aNodes[i],oResNode=oHash[oNode.getAttribute("id")]
if(oResNode)Compress.copyAttributes(oNode,oResNode)}
Compress.copyAttributes(oOrig.firstChild,oXML.firstChild)}
return oXML.xml}
return null},getStr:function(){if(Compress.replaceStr)return Compress.replaceStr
return Compress.replaceStr=[[new RegExp(' ('+Compress.attributes.join('|')+')="[^"]*"',"g"),""]].concat(Compress.replaces)},getRX:function(){if(Compress.replaceRX)return Compress.replaceRX
var aStr=Compress.getStr(),aRX=[]
for(var i=0;i<aStr.length;i++){var xA=aStr[i][0],xB=aStr[i][1]
if(typeof xA=="string")xA=Compress.createRX(xA)
if(typeof xB=="string")xB=Compress.createRX(xB)
aRX.push([xA,xB])}
Compress.replaceRX=aRX
return aRX},createRX:function(sMatch){return new RegExp(sMatch.toRX(),"g")},copyAttributes:function(oFrom,oTo){var aAttr=Compress.attributes
for(var i=0;i<aAttr.length;i++){var sName=aAttr[i],sVal=oFrom.getAttribute(sName)
if(sVal!==null&&!oTo.getAttribute(sName)){oTo.setAttribute(sName,sVal)}}}}
var State={generate:function(oXML){oXML.firstChild.setAttribute("history",History.save())
var sXML=oXML.xml
oXML.firstChild.removeAttribute("history")
if(this.rawXML!=sXML){this.rawXML=sXML
this.data=Compress.compress(sXML)}
return this.data},store:function(oXML,sLocation,bForce){if(LMSInterface.RegisterBookmark&&oConfig.firstChild.getAttribute("restoreLMS")=="true"){if(!sLocation){var oRestore=LMSInterface.GetBookmark()
if(oRestore)sLocation=oRestore.location}
LMSInterface.RegisterBookmark(this.generate(oXML),sLocation)}
if(oConfig.firstChild.getAttribute("restoreUserdata")=="true"||bForce){var oStorage,oRoot
try{oStorage=top.oRestore.oStorage||oLoader;}
catch(e){oStorage=oLoader;}
oStorage.XMLDocument.loadXML("<ROOTSTUB/>")
oRoot=oStorage.XMLDocument.firstChild
if(sLocation)oRoot.setAttribute("location",sLocation)
oRoot.appendChild(oStorage.XMLDocument.createCDATASection(this.generate(oXML)))
try{oStorage.save("ProjectData")}catch(oErr){}}},restore:function(oXML,oDefaults,bForce){var oProject=oConfig.firstChild,oRestore,bSuccess
if(oProject.getAttribute("restoreLMS")=="true"&&LMSInterface.GetBookmark){oRestore=LMSInterface.GetBookmark()}
if(!oRestore&&(oProject.getAttribute("restoreUserdata")=="true"||bForce)){var oStorage,oRoot
try{oStorage=top.oRestore.oStorage;}catch(e){}
if(!oStorage)oStorage=oLoader
oStorage.load("ProjectData")
oRoot=oStorage.XMLDocument.firstChild
oRestore={}
if(oRoot.firstChild){oRestore.xml=oRoot.firstChild.nodeValue}else{oRestore.xml=oRoot.getAttribute("project")}
oRestore.location=oRoot.getAttribute("location")}
if(oRestore&&oRestore.xml){oRestore.xml=Compress.restore(oRestore.xml,oConfig)
if(oRestore.xml){var sOrigXML=oXML.xml,sCurId=oProject.getAttribute("publicId"),sCurTag=oProject.getAttribute("publishTag")
bSuccess=oXML.loadXML(oRestore.xml)
if(bSuccess){if(oXML.firstChild.getAttribute("publicId")!=sCurId||oXML.firstChild.getAttribute("publishTag")!=sCurTag){bSuccess=false}else{if(oRestore.location&&oDefaults&&!oDefaults.resource)oDefaults.resource=oRestore.location
if(oDefaults)oDefaults.history=oXML.firstChild.getAttribute("history")
oXML.firstChild.removeAttribute("history")}}
if(!bSuccess)oXML.loadXML(sOrigXML)
sOrigXML=null}}
return bSuccess}}
var Classes={set:function(oEl,sClass){var sCurClass=oEl.className,sTagClass=/\btag--\S+\b/.test(sCurClass)&&RegExp.lastMatch,sNewClass=sTagClass?sClass?sTagClass+" "+sClass:sTagClass:sClass
oEl.className=sNewClass
return sClass},get:function(oEl){return oEl.className.replace(/tag--\S+\s+|\s*tag--\S+/,"")},has:function(oEl,sClass){if(oEl&&sClass)return new RegExp("\\b"+sClass.toRX()+"\\b").test(oEl.className)},add:function(oEl,sClass,bHover){if(oEl&&sClass){var sCurrent=Classes.get(oEl),sBase=oEl.getAttribute("baseClass"),sSuper=oEl.getAttribute("superClass")
if(sBase===null){oEl.setAttribute("baseClass",sCurrent)
sBase=sCurrent}
if(bHover){Classes.promote(sClass,sBase)
Classes.promote(sClass,sSuper,true)}else{Classes.promote(sClass,sCurrent)
oEl.setAttribute("superClass",(sSuper||"")+" "+sClass)}
return Classes.set(oEl,sCurrent+" "+sClass)}},remove:function(oEl,sClass){if(oEl&&sClass){var rxClass=new RegExp("(?:\\b|\\s+)"+sClass.toRX()+"(?:\\b|\\s+)")
var sSuper=oEl.getAttribute("superClass")
if(sSuper){sSuper=sSuper.replace(rxClass,"")
if(sSuper)oEl.setAttribute("superClass",sSuper)
else oEl.removeAttribute("superClass")}
return Classes.set(oEl,Classes.get(oEl).replace(rxClass,""))}},promote:function(sClass,sOther,bRev){var aOther=sOther&&sOther.match(/\S+/g),oOther,aClass,oClass
if(aOther&&aOther.length){sClass=sClass.toLowerCase()
aClass=sClass.match(/\S+/g)
oClass={}
for(var i=0;i<aClass.length;i++)oClass[aClass[i].toLowerCase()]=true
aClass=null
oOther={}
for(var i=0;i<aOther.length;i++)oOther[aOther[i].toLowerCase()]=true
aOther=null
var aRules=oCustomRules.styleSheet.rules,rxClass=/\.(\S+)$/,iIndex,oMatch,bApply=false
for(var i=bRev?0:aRules.length-1;bRev?i<aRules.length:i>=0;bRev?i++:i--){var vCur=aRules[i],sSelector=vCur.selectorText
if(rxClass.test(sSelector)){var sName=RegExp.$1.toLowerCase()
if(oClass[sName]){oMatch=vCur
iIndex=i
break}else if(oOther[sName]){bApply=true}}}
if(bApply&&oMatch){var sSelector=oMatch.selectorText,sCSS=oMatch.style.cssText
oCustomRules.styleSheet.removeRule(iIndex)
oCustomRules.styleSheet.addRule(sSelector,sCSS,bRev?0:oCustomRules.styleSheet.rules.length)}}}}
var Hover={downEls:[],apply:function(oEl,bClear,bDown){var sType=bDown?"down":"over"
if(sType=="over"&&bMobile)return
while(oEl&&oEl!=document.body&&oEl.nodeType==1){var sClass=oEl.getAttribute(sType+"rule")
if(sClass!==null&&oEl.disabled!==true){if(bClear&&oEl.getAttribute(sType+"ruleactive")){Classes.remove(oEl,sClass)
oEl.removeAttribute(sType+"ruleactive")}else if(!bClear&&!oEl.getAttribute(sType+"ruleactive")){Classes.add(oEl,sClass,!bDown)
oEl.setAttribute(sType+"ruleactive","1")
if(bDown)Hover.downEls.push(oEl)}}
oEl=oEl.parentNode}},clear:function(oEl){Hover.apply(oEl,true)},applyDown:function(oEl){Hover.apply(oEl,false,true)},clearDown:function(){var aDown=Hover.downEls
Hover.downEls=[]
for(var iD=aDown.length;iD--;){Hover.apply(aDown[iD],true,true)}},mouseover:function(event){Hover.apply(event.srcElement)},mouseout:function(event){Hover.clear(event.srcElement)},mousedown:function(event){Hover.isDown=true
Hover.applyDown(event.srcElement)},mouseup:function(event){Hover.isDown=false
Hover.clearDown()},initpage:function(){if(Hover.mouseX!==undefined&&Hover.mouseY!==undefined){var oOver=document.elementFromPoint(Hover.mouseX,Hover.mouseY)
if(oOver){Hover.apply(oOver)
if(Hover.isDown)Hover.applyDown(oOver)}}
document.attachEvent("onmouseover",Hover.mouseover)
document.attachEvent("onmouseout",Hover.mouseout)
document.attachEvent("onmousedown",Hover.mousedown)
document.attachEvent("onmouseup",Hover.mouseup)},finishpage:function(){document.detachEvent("onmouseover",Hover.mouseover)
document.detachEvent("onmouseout",Hover.mouseout)
document.detachEvent("onmousedown",Hover.mousedown)
document.detachEvent("onmouseup",Hover.mouseup)
if(Hover.mouseX!==undefined&&Hover.mouseY!==undefined){var oOver=document.elementFromPoint(Hover.mouseX,Hover.mouseY)
if(oOver)Hover.clear(oOver)
if(Hover.isDown)Hover.clearDown()}},track:function(event){Hover.mouseX=event.clientX
Hover.mouseY=event.clientY}}
document.attachEvent("onmousemove",Hover.track)
document.attachEvent("onclick",flashMouse)
document.attachEvent("onmouseup",flashMouse)
function flashMouse(event){try{var oSrc=event.srcElement,FM=flashMouse
if(oSrc==FM.current){FM.fired[event.type]=true}else if(oSrc.tagName=="EMBED"&&(oSrc.getAttribute("comptype")=="flash"||oSrc.GetVariable("$version"))){FM.current=oSrc
FM.fired={}
FM.fired[event.type]=true
clearTimeout(FM.timer)
FM.timer=setTimeout(function(){if(FM.fired.click^FM.fired.mouseup){try{FM.current.fireEvent(FM.fired.click?"onmouseup":"onclick");}catch(e){}}
FM.current=null
FM.fired=null},150)}}catch(oErr){}}
if(document.documentMode){if(window.MutationObserver){new MutationObserver(function(aRec,oObserver){oObserver.disconnect()
stripHashFromTitle()
oObserver.observe(document.getElementsByTagName("title")[0],{characterData:true,childList:true,subtree:true})}).observe(document.getElementsByTagName("title")[0],{characterData:true,childList:true,subtree:true})}else{document.attachEvent("onpropertychange",function(event){if(event.propertyName=="title"){document.detachEvent("onpropertychange",arguments.callee)
stripHashFromTitle()
document.attachEvent("onpropertychange",arguments.callee)}})}
function stripHashFromTitle(){if(location.hash&&document.title.indexOf(location.hash)>-1)
document.title=document.title.replace(location.hash,"")}}
document.attachEvent("onkeydown",function(event){if(event.keyCode=="9"&&!event.altKey){if(!("fresh"in navigKeyReset)){document.body.attachEvent("onnavigate",navigKeyReset)
navigKeyReset()}
if(navigKeyReset.fresh){navigKeyReset.fresh=false
var aEls=getByTagAll(oTopLevelLayer,"*")
oBoundsHash={},aSorted=[],iBase=1000,iThresh=3
for(var iE=0;iE<aEls.length;iE++){var oEl=aEls[iE],sTag=getTag(oEl).toUpperCase(),bKeyNav=window.KeyNav&&KeyNav[sTag]||oEl.getAttribute("vhref")||sTag=="A"||sTag=="INPUT"||sTag=="TEXTAREA",oBounds
if(bKeyNav&&(!oEl.attributes.tabIndex||!oEl.attributes.tabIndex.specified||oEl.tabIndex==0||oEl.tabIndex>=iBase)){oBoundsHash[oEl.uniqueID]=oBounds=navigGetBounds(oEl)
for(var iS=aSorted.length;iS--;){var oSib=aSorted[iS],oSibBounds=oBoundsHash[oSib.uniqueID]
if(!oBounds)break
if(oSibBounds&&(oSibBounds.top<oBounds.top-iThresh||oSibBounds.top<oBounds.top+iThresh&&oSibBounds.left<=oBounds.left))break}
aSorted.splice(iS+1,0,oEl)}
if(oEl.hideFocus)oEl.hideFocus=false}
for(var iS=aSorted.length;iS--;){aSorted[iS].tabIndex=iS+iBase}
document.body.setAttribute("keynavactive","1")}}
var oSrc=event.srcElement,sSrcTag=getTag(oSrc)
if(event.keyCode==13&&sSrcTag!="A"){navigKeyClick(oSrc)}
if(sSrcTag!="INPUT"&&sSrcTag!="TEXTAREA"&&(event.keyCode>=48&&event.keyCode<=57||event.keyCode>=65&&event.keyCode<=90||event.keyCode>=33&&event.keyCode<=40)){var oMap={33:"PgUp",34:"PgDn",35:"End",36:"Home",37:"\u2190",38:"\u2191",39:"\u2192",40:"\u2193"},iCode=event.keyCode,aCombo=[],oElem
if(event.ctrlKey)aCombo.push("Ctrl")
if(event.altKey)aCombo.push("Alt")
if(event.shiftKey)aCombo.push("Shift")
aCombo.push(oMap[iCode]||String.fromCharCode(iCode))
oElem=oKeyCombos[aCombo.join("+")]
if(oElem&&oTopLevelLayer.contains(oElem)&&getCurrentDisplay(oElem)!="none"&&!oElem.disabled){navigKeyClick(oElem)
try{event.keyCode=0;}catch(e){}
event.cancelBubble=true
return event.returnValue=false}}})
function navigKeyClick(oElem){try{oElem.fireEvent("onmousedown");oElem.fireEvent("onclick");oElem.fireEvent("onmouseup");}catch(e){}}
function navigKeyReset(){navigKeyReset.fresh=true
document.body.removeAttribute("keynavactive")}
function navigGetBounds(oEl){var oCur=oEl,iX=0,iY=0
if(!oEl.offsetWidth&&!oEl.offsetHeight)return
while(oCur&&oCur!=oTopLevelLayer){iX+=oCur.offsetLeft+oCur.clientLeft
iY+=oCur.offsetTop+oCur.clientTop
oCur=oCur.parentNode}
return{left:iX,top:iY,right:iX+oEl.offsetWidth,bottom:iY+oEl.offsetHeight}}
function scanKeyCombos(){var aEls=oTopLevelLayer.querySelectorAll("*[keycombo]")
oKeyCombos={}
for(var iE=aEls.length;iE--;){var oEl=aEls[iE],sKeyCombo=oEl.getAttribute("keycombo")
if(sKeyCombo){var oCur=oKeyCombos[sKeyCombo]
if(oCur&&oCur!=oEl)
alert("以下键盘快捷键被分配到多个元素： "+sKeyCombo)
oKeyCombos[sKeyCombo]=oEl}}}
function initOffsetFixer(){if(Object.defineProperty&&Object.getOwnPropertyDescriptor){var oOuter=document.body.appendChild(document.createElement("div")),oInner=oOuter.appendChild(document.createElement("div")),bPatchBorder=false,bPatchZoom=false
oOuter.style.cssText="position:absolute;border-left:17px solid black;overflow:hidden;"
oInner.style.cssText="position:absolute;left:0px;top:10px;"
if(oInner.offsetLeft>0&&oInner.offsetLeft==oOuter.clientLeft)bPatchBorder=true
oOuter.style.zoom="2"
if(window.oCourse&&oInner.offsetTop==oInner.style.pixelTop*oOuter.style.zoom)bPatchZoom=true
document.body.removeChild(oOuter)
if(bPatchBorder||bPatchZoom){var pOffsetLeft=Object.getOwnPropertyDescriptor(Element.prototype,"offsetLeft"),fOffsetLeft=pOffsetLeft&&pOffsetLeft.get,pOffsetTop=Object.getOwnPropertyDescriptor(Element.prototype,"offsetTop"),fOffsetTop=pOffsetTop&&pOffsetTop.get
if(fOffsetLeft&&fOffsetTop){Object.defineProperty(Element.prototype,"offsetLeft",{get:function(){if(bPatchZoom)oCourse.runtimeStyle.zoom="1"
var iVal=fOffsetLeft.call(this)-(bPatchBorder&&this.offsetParent?this.offsetParent.clientLeft:0)
if(bPatchZoom)oCourse.runtimeStyle.removeAttribute("zoom")
return iVal}})
Object.defineProperty(Element.prototype,"offsetTop",{get:function(){if(bPatchZoom)oCourse.runtimeStyle.zoom="1"
var iVal=fOffsetTop.call(this)-(bPatchBorder&&this.offsetParent?this.offsetParent.clientTop:0)
if(bPatchZoom)oCourse.runtimeStyle.removeAttribute("zoom")
return iVal}})}}}}
function fixBGPositionBug(oContainer){if(arguments.callee.needsFix===undefined){var oTest=document.createElement("div")
oTest.style.backgroundPosition="left top"
arguments.callee.needsFix=oTest.style.cssText==""}
if(arguments.callee.needsFix){var aEls=oContainer.getElementsByTagName("*")
for(var iE=aEls.length;iE--;){var oEl=aEls[iE]
if(oEl.style.backgroundPosition&&/^(?:left|0\S+)\s+(?:top|0\S+)$/.test(oEl.style.backgroundPosition))
oEl.setAttribute("bgposfix","1")}}}
function printWin(){print()}
var Lightbox={animationDuration:600,open:function(xContent,oStyle,oArgs){var oTargetWin=window,iOverlayOpacity=0.4
try{if(parent&&(parent.CacheManager||parent.Lightbox)&&parent.document&&parent.document.body)
oTargetWin=parent}catch(e){}
Lightbox.close()
var oTargetDoc=oTargetWin.document,bURLContent=typeof xContent=="string",oContainer=oTargetDoc.createElement("div"),oOverlay=oTargetDoc.createElement("div"),oMarginer=oTargetDoc.createElement("div"),oSizer=oTargetDoc.createElement("div"),oOffsetter=oTargetDoc.createElement("div"),oBox=oTargetDoc.createElement("div"),oClipper=oTargetDoc.createElement("div"),oFrame=oTargetDoc.createElement(bURLContent?"iframe":"div"),oCloseOuter=oTargetDoc.createElement("div"),oCloseInner=oTargetDoc.createElement("div"),oCloseImg=oTargetDoc.createElement("img"),sOutlineWidth=oStyle.outlineWidth||"4px"
oContainer.style.position="fixed"
oContainer.style.left="0px"
oContainer.style.top="0px"
oContainer.style.width="100%"
oContainer.style.height="100%"
oContainer.style.cursor="default"
oOverlay.style.position="absolute"
oOverlay.style.left="0px"
oOverlay.style.top="0px"
oOverlay.style.width="100%"
oOverlay.style.height="100%"
oOverlay.style.backgroundColor="black"
oMarginer.style.position="absolute"
oMarginer.style.left="5%"
oMarginer.style.top="5%"
oMarginer.style.right="5%"
oMarginer.style.bottom="5%"
oSizer.style.position="absolute"
oSizer.style.left="50%"
oSizer.style.top="50%"
oSizer.style.maxWidth="100%"
oSizer.style.maxHeight="100%"
oSizer.style.width=oStyle.width
oSizer.style.height=oStyle.height
oOffsetter.style.position="absolute"
oBox.style.position="absolute"
oBox.style.left="-"+sOutlineWidth
oBox.style.top="-"+sOutlineWidth
oBox.style.right="-"+sOutlineWidth
oBox.style.bottom="-"+sOutlineWidth
oBox.style.borderStyle="solid"
oBox.style.borderWidth=sOutlineWidth
oBox.style.borderColor=oStyle.outlineColor||"black"
oBox.style.backgroundColor="white"
oClipper.style.position="absolute"
oClipper.style.left="0px"
oClipper.style.top="0px"
oClipper.style.width="100%"
oClipper.style.height="100%"
oClipper.style.overflow="hidden"
if(bURLContent){oFrame.frameBorder="0"
oFrame.scrolling=oStyle.scroll&&!bMobile?"auto":"no"}
oFrame.style.position="absolute"
oFrame.style.left="0px"
oFrame.style.top="0px"
oFrame.style.width="100%"
oFrame.style.height="100%"
oCloseOuter.style.position="absolute"
oCloseOuter.style.right="-"+sOutlineWidth
oCloseOuter.style.top="-"+sOutlineWidth
oCloseInner.style.position="relative"
oCloseInner.style.left=oStyle.closeLeft||"0px"
oCloseInner.style.top=oStyle.closeTop||"0px"
oCloseImg.style.visibility="hidden"
oCloseImg.onload=function(){oCloseInner.style.marginRight=-oCloseImg.width/2+"px"
oCloseInner.style.marginTop=-oCloseImg.height/2+"px"
oCloseImg.style.visibility="visible"}
oCloseImg.style.cursor="pointer"
oCloseImg.src=oStyle.closeImage||"Runtime/Images/close.png"
oTargetDoc.body.appendChild(oContainer)
oContainer.appendChild(oOverlay)
oContainer.appendChild(oMarginer)
oMarginer.appendChild(oSizer)
oSizer.appendChild(oOffsetter)
oOffsetter.appendChild(oBox)
oBox.appendChild(oClipper)
if(!bURLContent)oClipper.appendChild(oFrame)
oBox.appendChild(oCloseOuter)
oCloseOuter.appendChild(oCloseInner)
oCloseInner.appendChild(oCloseImg)
opacityOverlay(oOverlay)
Lightbox.active={container:oContainer,win:oTargetWin,doc:oTargetDoc,sizer:oSizer,box:oBox,closeImg:oCloseImg}
Lightbox.closeCallback=oStyle.closeCallback
if(oStyle.labelPosition){var oLabel=oTargetDoc.createElement("div"),oLabelText=oTargetDoc.createElement("div")
oLabel.style.position="absolute"
oLabel.style.overflow="hidden"
oLabel.style.visibility="hidden"
oLabel.style.textAlign="center"
oLabel.setAttribute("mode",oStyle.labelPosition)
oLabelText.style.position="relative"
oLabelText.style.fontFamily=oStyle.labelFont
oLabelText.style.fontSize=oStyle.labelSize
oLabelText.style.color=oStyle.labelColor
oLabelText.style.MozUserSelect="none"
if(oStyle.labelPosition=="over"){var oLabelBG=oTargetDoc.createElement("div")
oLabel.style.bottom="0px"
oLabel.style.width="100%"
oLabel.style.maxHeight="70%"
oLabelText.style.padding="5px"
oLabelBG.style.position="absolute"
oLabelBG.style.left="0px"
oLabelBG.style.top="0px"
oLabelBG.style.width="100%"
oLabelBG.style.height="100%"
oLabelBG.style.backgroundColor=oStyle.labelBGColor
setOpacity(oLabelBG,0.5)
oLabel.appendChild(oLabelBG)
oLabel.appendChild(oLabelText)
oBox.appendChild(oLabel)}else{var oShape=oTargetDoc.createElement("span")
oLabel.style.top="50%"
oLabel.style.marginTop=sOutlineWidth
oLabel.style.borderTop="5px solid transparent"
oLabelText.style.display="inline-block"
oLabelText.style.padding="10px 15px"
oLabelText.style.marginTop="-3px"
oShape.style.position="absolute"
oShape.style.left="50%"
oShape.style.top="0px"
oShape.className="tag--flashape"
oShape.setAttribute("subType","rect")
oShape.setAttribute("cornerRadius","15")
oShape.setAttribute("shadowEnabled","true")
oShape.setAttribute("lineType","solid")
oShape.setAttribute("lineWidth","2")
oShape.setAttribute("lineColor","#ffffff")
oShape.setAttribute("fillType","solid")
oShape.setAttribute("fillColor",oStyle.labelBGColor)
oLabel.appendChild(oShape)
oLabel.appendChild(oLabelText)
oSizer.appendChild(oLabel)
EmuBehavior.scanForBehaviors(oShape,true)
oTargetWin.attachEvent("onresize",Lightbox.adjustOuterLabel);(function(){if(oShape.emuReadyState=="complete")
Lightbox.adjustOuterLabel()
else
setTimeout(arguments.callee,20)})()}
Lightbox.active.label=oLabel
Lightbox.active.labelText=oLabelText}
if(oArgs&&bURLContent){var sUID=new Date().getTime()
xContent+="?"+sUID
oFrame.id="lightbox_"+sUID
oFrame.popupArgs=oArgs
oFrame.lightboxClose=Lightbox.close}
if(oTargetDoc.documentElement)
Classes.add(oTargetDoc.documentElement,"disableScroll")
var iStart=new Date();(function(){var iT=(new Date()-iStart)/Lightbox.animationDuration
if(iT>=1){setOpacity(oOverlay,iOverlayOpacity)
setOpacity(oBox)
setOpacity(oCloseImg)
oOffsetter.style.left="-50%"
oOffsetter.style.top="-50%"
oOffsetter.style.width="100%"
oOffsetter.style.height="100%"
if(bURLContent){oClipper.appendChild(oFrame)
oFrame.src=xContent
Lightbox.active.frame=oFrame}
if(typeof xContent=="function")
xContent()}else{var iQ=iT>0.5?1:Math.sqrt(1-(iT/0.5-1)*(iT/0.5-1)),iZ=Math.pow(2,10*(iT-1)),iW=iQ*100,iH=iZ*100
setOpacity(oOverlay,iQ*iOverlayOpacity)
setOpacity(oBox,iQ)
setOpacity(oCloseImg,iZ)
oOffsetter.style.left=(-iW/2)+"%"
oOffsetter.style.top=(-iH/2)+"%"
oOffsetter.style.width=iW+"%"
oOffsetter.style.height=iH+"%"
Lightbox.timer=setTimeout(arguments.callee,30)}})()
oContainer.attachEvent("onmousedown",Lightbox.click)
return oFrame},click:function(event){var oSrc=event.srcElement
if(!Lightbox.active.box.contains(oSrc)&&(!Lightbox.active.labelText||!Lightbox.active.label.contains(oSrc)||oSrc==Lightbox.active.label)||oSrc==Lightbox.active.closeImg)
Lightbox.close()},close:function(){clearTimeout(Lightbox.timer)
clearTimeout(Lightbox.sizeTimer)
if(Lightbox.closeCallback)
Lightbox.closeCallback()
if(Lightbox.active){if(Lightbox.active.frame)
Lightbox.active.frame.src=""
if(Lightbox.active.container)
Lightbox.active.container.removeNode(true)
if(Lightbox.active.doc&&Lightbox.active.doc.documentElement)
Classes.remove(Lightbox.active.doc.documentElement,"disableScroll")
if(Lightbox.active.win)
Lightbox.active.win.detachEvent("onresize",Lightbox.adjustOuterLabel)
Lightbox.active=null}
Lightbox.closeCallback=null},setSize:function(iTargetW,iTargetH){var oSizer=Lightbox.active.sizer,iStartW=oSizer.offsetWidth,iStartH=oSizer.offsetHeight,iDiffW=iTargetW-iStartW,iDiffH=iTargetH-iStartH,iStart=new Date()-15
clearTimeout(Lightbox.sizeTimer);(function(){var iT=(new Date()-iStart)/Lightbox.animationDuration
if(iT>=1){oSizer.style.width=iTargetW+"px"
oSizer.style.height=iTargetH+"px"}else{var iV=1-Math.pow(2,-10*iT)
oSizer.style.width=Math.round(iStartW+iV*iDiffW)+"px"
oSizer.style.height=Math.round(iStartH+iV*iDiffH)+"px"
Lightbox.sizeTimer=setTimeout(arguments.callee,30)}})()},setLabel:function(sTxt){var oLabel=Lightbox.active.label,oLabelText=Lightbox.active.labelText
if(oLabel&&oLabelText){oLabel.style.visibility=sTxt?"":"hidden"
oLabelText.innerText=sTxt
if(oLabel.getAttribute("mode")=="outer")
Lightbox.adjustOuterLabel()}},adjustOuterLabel:function(){var oContainer=Lightbox.active.container,oLabel=Lightbox.active.label,oLabelText=Lightbox.active.labelText
if(oContainer&&oLabel&&oLabelText){var iW=oContainer.offsetWidth,oCanvas=oLabel.getElementsByTagName("canvas")[0],oEmbed=oLabel.getElementsByTagName("embed")[0]
oLabel.style.width=iW+"px"
oLabel.style.left=-iW/2+"px"
oLabel.parentNode.parentNode.style.marginBottom=(oLabel.clientHeight+5)+"px"
if(oCanvas){oCanvas.style.marginLeft=-oLabelText.clientWidth/2+"px"
oCanvas.style.width=oLabelText.clientWidth+"px"
oCanvas.style.height=oLabel.clientHeight+"px"
oCanvas.parentElement.setCanvasSize(oLabelText.clientWidth,oLabel.clientHeight)}
if(oEmbed){oEmbed.style.marginLeft=-oLabelText.clientWidth/2+"px"
oEmbed.style.width=oLabelText.clientWidth+"px"
oEmbed.style.height=oLabel.clientHeight+"px"}}}}
function createNarrationHolder(){var oNarrateHolder=document.getElementById("narrateHolder")
if(!oNarrateHolder){oNarrateHolder=document.createElement("div")
oNarrateHolder.id="narrateHolder"
document.body.appendChild(oNarrateHolder)
oNarrateHolder.innerHTML=new Array(bMobile?2:3).join('<span class="tag--mplayer audio" forceStart="true" style="position:absolute"><span class="tag--mpstage"></span></span>')
EmuBehavior.scanForBehaviors(oNarrateHolder)
if(bMobile&&(!/iP(hone|[oa]d)/.test(navigator.userAgent)||/OS (\d+)/.test(navigator.userAgent)&&+RegExp.$1>4)){document.attachEvent("onmousedown",function(event){var oPlayer=oNarrateHolder.childNodes[0]
if(oPlayer&&!oPlayer.getAttribute("source")){oPlayer.setAttribute("source","data:audio/wav;base64,")
oPlayer.reset()
oPlayer.play()}
document.detachEvent("onmousedown",arguments.callee)})}}
return oNarrateHolder}
