/*@cc_on @*/
var oDE=document.documentElement,oBD=document.body,oCourse=document.getElementById("oCourse"),iChromeHeight=0,iChromeWidth=0,winTooSmall,winTooLarge,iIEDocMode=document.documentMode,bBrowserIE8=iIEDocMode&&!("performance"in window),bBrowserIE9=iIEDocMode&&!bBrowserIE8&&!("spellcheck"in oBD),bCanZoom=oBD.style.zoom!==undefined,bAccessTop=false,bHTA=false
function scaleInit(bRescale){try{if(top.document){bAccessTop=true
bHTA=/[\/\\]run\.hta$/i.test(top.location.pathname)||oPopupArgs&&oPopupArgs.win.bHTA}}catch(e){}
var oPrj=oConfig.firstChild
if(bAccessTop&&!bSnapshot&&oRequest.mode!="embed")top.document.title=oPrj.getAttribute("title")
var winType=oPrj.getAttribute("windowType")
sizeMatch=!oPopupArgs&&(bHTA&&winType!="max"||winType==null||winType=="new"||winType.indexOf("resize")>-1)||oPopupArgs&&!bHTA
if(oRequest.forcezoom){oPrj.setAttribute("windowTooLarge",+oRequest.forcezoom?"zoom":"center")
oPrj.setAttribute("windowTooSmall",+oRequest.forcezoom?"zoom":"scroll")}
winTooSmall=oPrj.getAttribute("windowTooSmall")||"zoom"
winTooLarge=oPrj.getAttribute("windowTooLarge")||"zoom"
if(!bMobile&&sizeMatch&&bAccessTop&&(top==window||(top.document.body.clientWidth==oDE.offsetWidth&&top.document.body.clientHeight==oDE.offsetHeight)||bHTA)){ensureWinSize(defWidth,defHeight)
var iWidthDif=(screen.availWidth-iChromeWidth)/defWidth,iHeightDif=(screen.availHeight-iChromeHeight)/defHeight
if(iWidthDif<1||iHeightDif<1){var iFinWidth,iFinHeight
if(winTooSmall=="zoom"&&bCanZoom){var iScale=iWidthDif>iHeightDif?iHeightDif:iWidthDif
iFinWidth=Math.round(defWidth*iScale)+iChromeWidth
iFinHeight=Math.round(defHeight*iScale)+iChromeHeight}else{var iAddedWidth=0
if(iWidthDif<1^iHeightDif<1){var oDiv=oBD.appendChild(document.createElement("div"))
oDiv.style.cssText="margin-top:-10000px;width:100px;height:100px;overflow-y:scroll"
iAddedWidth=oDiv.offsetWidth-oDiv.clientWidth
oBD.removeChild(oDiv)}
iFinWidth=Math.min(screen.availWidth,defWidth+iChromeWidth+iAddedWidth)
iFinHeight=Math.min(screen.availHeight,defHeight+iChromeHeight+iAddedWidth)}
top.moveTo(Math.round((screen.availWidth-iFinWidth)/2),Math.round((screen.availHeight-iFinHeight)/2))
top.resizeTo(iFinWidth,iFinHeight)}}
if(bRescale===true)return
scaleContent()
window.attachEvent("onresize",scaleContent)
if(oPrj.getAttribute("allowSelection")!="true"){if("WebkitUserSelect"in oBD.style){oBD.style.WebkitUserSelect="none"}else if("onselectstart"in document){document.attachEvent("onselectstart",disableSelect)}else if("MozUserSelect"in oBD.style){oBD.style.MozUserSelect="none"
document.addEventListener("mouseover",mozDisableSelect,true)}
document.attachEvent("ondragstart",disableSelect)}
if(oPrj.getAttribute("allowContextMenu")!="true"){document.attachEvent("oncontextmenu",disableSelect)}
var bgColor=oPrj.getAttribute("windowBackColor")||"#808080",bgColor2=oPrj.getAttribute("windowBackColor2")||bgColor
oDE.style.backgroundColor=oBD.style.backgroundColor=bgColor2
try{oDE.style.background=oCourse.parentNode.style.background="-webkit-gradient(linear, left top, left bottom, color-stop(0%,"+bgColor+"), color-stop(100%,"+bgColor2+"))"
oDE.style.background=oCourse.parentNode.style.background="-webkit-linear-gradient(top, "+bgColor+" 0%,"+bgColor2+" 100%)"
oDE.style.background=oCourse.parentNode.style.background="linear-gradient(to bottom, "+bgColor+" 0%,"+bgColor2+" 100%)"}catch(e){}
if(iIEDocMode&&iIEDocMode<=9){document.getElementById("oCourseBG").style.filter="progid:DXImageTransform.Microsoft.gradient(startColorstr='"+bgColor+"',endColorstr='"+bgColor2+"')"}
if(winType=="max")oBD.style.borderWidth=0
document.getElementById("oInitLoader").style.visibility="hidden"
oCourse.className=""
try{window.frameElement&&frameElement.oncourseload&&frameElement.oncourseload();}catch(e){}}
var CacheManager={isCapable:function(oPrj){if(bHTA)return false
this.cacheAhead=+(oPrj.getAttribute("cacheAhead")||3)
if(this.cacheAhead==0)return false
this.schemeNode=oPrj.selectSingleNode("config/cacheScheme")
if(!this.schemeNode||!this.schemeNode.getAttribute("data"))return false
this.capable=true
return true},init:function(aIdsToCache,fCallback){if(this.capable||this.isCapable()){this.scheme=this.parse(this.schemeNode.getAttribute("data"))
this.dump=oBD.appendChild(document.createElement("div"))
this.dump.id="oDump"
this.initMode=true
this._initCallback=fCallback
if(!this.scheme.global)this.scheme.global=[]
this.scheme.global.push("../../Runtime/Images/trans.gif")
this.cache("global",true)
for(var iReq=0;iReq<aIdsToCache.length;iReq++)this.cache(aIdsToCache[iReq])}
delete this.init},parse:function(sData){var aData=sData.split("|!|"),oFileId={},oData={}
for(var iDoc=0,iLen=aData.length;iDoc<iLen;iDoc++){var aFiles,aMerge=null
if(aData[iDoc]){aFiles=aData[iDoc].split("|")
var sId=aFiles.splice(0,1)[0]
if(oData[sId])aMerge=oData[sId]
oData[sId]=aFiles
for(var iFile=0,iFileLen=aFiles.length;iFile<iFileLen;iFile++){var sFile=aFiles[iFile]
if(sFile.indexOf(":")>-1){var aIdFile=sFile.split(":")
oFileId[aIdFile[0]]=aIdFile[1]
aFiles[iFile]=aIdFile[1]}else{aFiles[iFile]=oFileId[sFile]}}
if(aMerge)oData[sId]=oData[sId].concat(aMerge)}}
return oData},cache:function(sId,bSkipId){if(sId in this.scheme){if(!bSkipId){this._pending++
httpLoader("GET","Projects/Res/"+sId,this.itemComplete)}
var aFiles=this.scheme[sId]
if(aFiles){for(var iFile=aFiles.length;iFile--;){var sFile=("Projects/Res/"+aFiles[iFile]).replace("Projects/Res/../../","")
if(!this._done[sFile]){this._pending++
if(/\.(?:gif|png|jpe?g)$/i.test(sFile)){var oPreload=this.dump.appendChild(document.createElement("img"))
oPreload.onload=oPreload.onerror=this.itemComplete
oPreload.src=sFile}else httpLoader("GET",sFile,this.itemComplete,"",false,!/\.(?:js|xml)$/i.test(sFile))
this._done[sFile]=true}}}
delete this.scheme[sId]
return true}
return false},itemComplete:function(){var iPend=CacheManager._pending--
if(CacheManager.initMode){if(!loadStatus.cache||loadStatus.cache<iPend)loadStatus(null,iPend)
else loadStatus()}
if(CacheManager._pending==0){if(CacheManager.initMode){loadStatus(100)
setTimeout(scaleInit,150)
delete CacheManager.initMode}
if(CacheManager._initCallback){var fCall=CacheManager._initCallback
delete CacheManager._initCallback
fCall()}}},disable:function(){loadStatus(100)
setTimeout(scaleInit,150)},_done:{},_pending:0}
loadStatus(1)
function loadStatus(iForce,iTotal){try{var iCur=loadStatus.current;}catch(e){return;}
if(!document.getElementById("oCachePct"))return clearTimeout(loadStatus.updater)
if(iForce)iCur=iForce
if(iTotal){loadStatus.cache=iTotal
loadStatus.cacheStart=iCur
clearTimeout(loadStatus.updater)}
if(!loadStatus.cache){if(iCur<40){iCur=Math.min(iCur+Math.floor(Math.random()*3+1),40)
loadStatus.updater=setTimeout(loadStatus,Math.max(250,900*iCur/40),null,null)}}else{var iDiff=100-loadStatus.cacheStart
iCur=loadStatus.cacheStart+Math.round(iDiff-(CacheManager._pending/loadStatus.cache)*iDiff)}
if(iCur==100)clearTimeout(loadStatus.updater)
document.getElementById("oCachePct").innerText=iCur
document.getElementById("oCacheFill").style.width=iCur+"%"
loadStatus.current=iCur}
function isTextInput(oSrc){while(oSrc&&oSrc.nodeType!=1)oSrc=oSrc.parentNode
return getTag(oSrc)=="INPUT"||getTag(oSrc)=="TEXTAREA"||oSrc.contentEditable=="true"}
function disableSelect(event){event.returnValue=isTextInput(event.srcElement)}
function mozDisableSelect(event){oBD.style.MozUserSelect=isTextInput(event.target)?"":"none"}
function ensureWinSize(iWidth,iHeight){try{if(top.outerWidth){top.resizeBy(iWidth-oDE.offsetWidth,iHeight-oDE.offsetHeight)
top.moveTo((screen.availWidth-top.outerWidth)/2,(screen.availHeight-top.outerHeight)/2)}else{var iRWidth=Math.min(iWidth+6,screen.availWidth),iRHeight=Math.min(iHeight+30,screen.availHeight)
if(top.screenLeft+iRWidth>screen.availWidth||top.screenTop+iRHeight>screen.availHeight)top.moveTo(screen.availWidth-iRWidth,screen.availHeight-iRHeight)
top.resizeTo(iRWidth,iRHeight)
iChromeWidth=iRWidth-oBD.offsetWidth
iChromeHeight=iRHeight-oBD.offsetHeight
top.moveTo(Math.round((screen.availWidth-iWidth-iChromeWidth)/2),Math.round((screen.availHeight-iHeight-iChromeHeight)/2))
top.resizeTo(iWidth+iChromeWidth,iHeight+iChromeHeight)}}catch(e){}}
function scaleContent(tooSmall,tooLarge){if(bMobile)return
if(oDE.sourceIndex){var iST=oDE.scrollTop,iSL=oDE.scrollLeft
oCourse.className="hideContent"}
var fHeight=oDE.clientHeight,fWidth=oDE.clientWidth,iZoom=Math.min(fWidth/defWidth,fHeight/defHeight),iFinalZoom
window.detachEvent("onresize",scaleContent)
tooSmall=(typeof tooSmall=="string"&&tooSmall)||winTooSmall
tooLarge=(typeof tooLarge=="string"&&tooLarge)||winTooLarge
if((iZoom<1&&tooSmall=="zoom")||(iZoom>1&&tooLarge=="zoom")){iFinalZoom=iZoom}else{iFinalZoom=1
if(iZoom<1){if(!oDE.style.overflow)
oDE.style.overflow="auto"}}
if((iZoom>1||iFinalZoom!=1)&&oDE.style.overflow){oDE.style.removeAttribute("overflow")}
try{window.globalZoom=iFinalZoom
oCourse.style[oCourse.style._zoom!==undefined?"_zoom":"zoom"]=iFinalZoom}catch(oErr){}
oCourse.style.width=defWidth*(bBrowserIE8||bBrowserIE9&&iIEDocMode==8?1:iFinalZoom)+"px"
oCourse.style.height=defHeight*(iIEDocMode<=9?1:iFinalZoom)+"px"
window.attachEvent("onresize",scaleContent)
if(oDE.sourceIndex){oCourse.className=""
oDE.scrollTop=iST
oDE.scrollLeft=iSL}
var oTD=oCourse.parentNode
oTD.style.paddingTop=oTD.style.paddingLeft=0
if(iFinalZoom==1){if(oTD.offsetHeight>defHeight)oTD.style.paddingTop=(oTD.offsetHeight+defHeight)%2+"px"
if(oTD.offsetWidth>defWidth)oTD.style.paddingLeft=(oTD.offsetWidth+defWidth)%2+"px"}}
