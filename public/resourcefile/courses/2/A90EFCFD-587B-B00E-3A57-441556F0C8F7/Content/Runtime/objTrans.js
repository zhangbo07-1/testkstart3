/*@cc_on @*/
function Transition(oElem,oHost){try{this.elem=oElem
this.host=oHost
if(oElem.activetrans)oElem.activetrans.stop()}
catch(oErr){}}
Transition.activeHash={}
Transition.stopAll=function(){for(var sT in Transition.activeHash){try{var oT=Transition.activeHash[sT]
if(oT){oT.stop()}}catch(oErr){}}}
Transition.prototype.init=function(){var sElemType=this.elem.getAttribute("transtype"),sElemDuration=this.elem.getAttribute("transduration"),oRef=sElemType&&sElemType!="inherit"&&sElemType!="继承"||!this.host?this.elem:this.host
this.type=oRef.getAttribute("transtype")
this.prop=(oRef.getAttribute("transprop")||"").hash()
oRef=sElemDuration&&sElemDuration!="inherit"&&sElemDuration!="继承"||!this.host?this.elem:this.host
this.duration=Transition.getDuration(oRef.getAttribute("transduration")||"1500")
this.mode=this.elem.currentStyle.visibility=="visible"?"hidden":"visible"
this.container=oContentArea
Transition.activeHash[this.elem.uniqueID]=this}
Transition.prototype.play=function(){try{var oChild,aFound=[],oChildren=getByTagAll(this.elem,"*")
try{for(var iC=0;iC<oChildren.length;iC++){oChild=oChildren[iC]
if(oChild.currentStyle.filter.indexOf("opacity=0")!=-1){oChild.restoreDisplay=oChild.currentStyle.display
oChild.style.display="none"
aFound[aFound.length]=oChild}}}
catch(oErr){}
this.tpElems=aFound
this.conf=Transition.getConfig(this.type)||Transition.config.Show
var skipWrap=false,sTag=getTag(this.elem).toLowerCase()
if(sTag=="mplayer"||getByTagOne(this.elem,"mplayer")){if(this.conf==Transition.config.Fly){this.type="Fade"
this.prop={}
this.conf=Transition.getConfig(this.type)}
delete Transition.config["Fly"]
delete Transition.config["Fly Fade"]
skipWrap=true}
;/*@if(!@_jscript)@*/
var aCheck=["object","embed","mplayer","flash","youtube"]
for(var i=aCheck.length;i--;)if(sTag==aCheck[i]||getByTagOne(this.elem,aCheck[i])){this.conf=Transition.config.Show
break}
;/*@end @*/
if(this.elem.scopeName=="gmv"&&(getTag(this.elem)!="rect"||!getByTagOne(this.elem,"imagedata"))){skipWrap=true}
this.func=this[this.conf.func]?this.conf.func:"show"
this.elem.activetrans=this
if(this.func!="show"&&!skipWrap){this.wrapElem()}
this[this.func].play(this)}
catch(oErr){this.stop(this)}}
Transition.prototype.stop=function(){if(this.elem.activetrans){this.elem.style.removeAttribute("filter")
if(this[this.func].stop)this[this.func].stop(this)
this.unwrapElem()
this.elem.style.visibility=this.mode
this.elem.activetrans=null
if(this.onFinish)this.onFinish()
Transition.activeHash[this.elem.uniqueID]=null
try{for(var iF=0;iF<this.tpElems.length;iF++){var oChild=this.tpElems[iF]
if(oChild.restoreDisplay){oChild.style.display=oChild.restoreDisplay
oChild.removeAttribute("restoreDisplay")}}}
catch(oErr){}}}
Transition.prototype.wrapElem=function(){if(getTag(this.elem)!="transwrapper"){var oEl=this.elem,oNew=createCustom("transwrapper"),oParent=oEl.parentNode,oTemp=oEl.document.createElement("div"),iW=oEl.offsetWidth,iH=oEl.offsetHeight,bHasLayout=oEl.currentStyle.hasLayout,sPosition=oEl.currentStyle.position,sTop=oEl.currentStyle.top,sLeft=oEl.currentStyle.left,sBottom=oEl.currentStyle.bottom,sRight=oEl.currentStyle.right,sElDisplay=oEl.currentStyle.display,sNewDisplay=sElDisplay
oNew.setAttribute("origStyle",oEl.style.cssText)
if(oEl.currentStyle.padding!="0px"||oEl.currentStyle.borderWidth!="0px"&&oEl.currentStyle.borderStyle!="none"){oEl.style.borderWidth="0px"
oEl.style.padding="0px"
iW=oEl.offsetWidth
iH=oEl.offsetHeight
oEl.style.cssText=oNew.getAttribute("origStyle")}
if(sElDisplay!="inline"){oEl.style.width=iW+"px"
oEl.style.height=iH+"px"}else{oEl.style.display=sNewDisplay=bHasLayout?"block":"inline-block"}
oNew.style.position=sPosition
oNew.style.top=sTop
oNew.style.left=sLeft
oNew.style.right=sRight
if(sBottom&&sBottom!="auto")oNew.style.bottom=sBottom
oNew.style.display=sNewDisplay=="list-item"?"block":sNewDisplay
Transition.copyCurrentStyles(oNew,oEl,["visibility","zIndex","verticalAlign"])
if(getTag(oEl)=="LI"&&(getTag(oParent)=="UL"||getTag(oParent)=="OL")){Transition.copyCurrentStyles(oEl,oEl,["listStyleType","listStyleImage","listStylePosition"])
var sDir=oEl.currentStyle.direction=="ltr"?"Left":"Right"
oNew.style["margin"+sDir]="-16px"
oNew.style["padding"+sDir]="16px"
;/*@if(@_jscript)
oEl.style.width=(iW-16)+"px"
;/*@end @*/}
if(getTag(oEl)=="UL"){oNew.style.marginTop=oEl.currentStyle.marginTop
oNew.style.marginBottom=oEl.currentStyle.marginBottom
oEl.style.marginTop=0
oEl.style.marginBottom=0}
if(sPosition=="absolute"){oEl.style.position="relative"}
oEl.style.top="0"
oEl.style.left="0"
oEl.style.bottom="auto"
oEl.style.right="auto"
oEl.style.verticalAlign="top"
oEl.style.visibility="inherit"
oParent.appendChild(oNew)
oNew.appendChild(oTemp)
oEl.swapNode(oNew)
oEl.swapNode(oTemp)
oTemp.removeNode()
if(sElDisplay!="inline"){oNew.style.width=oNew.offsetWidth+"px"
oNew.style.height=oNew.offsetHeight+"px"}
oNew.activetrans=oEl.activetrans
this.elem=oNew
;/*@if(!@_jscript)@*/
if(getTag(oEl)=="IMG"&&!oEl.complete&&oEl.style.height==""&&oEl.style.width==""){oEl.addEventListener("load",function(e){oEl.style.width=""
oEl.style.height=""
oEl.style.width=oEl.width+"px"
oEl.style.height=oEl.height+"px"
oNew.style.width=oEl.width+"px"
oNew.style.height=oEl.height+"px"
oEl.removeEventListener("load",arguments.callee,false)},false)}
;/*@end @*/}}
Transition.prototype.unwrapElem=function(){if(getTag(this.elem)=="transwrapper"){var oWrapper=this.elem,oElem=oWrapper.firstChild,oParent=oWrapper.parentNode
if(oElem&&oWrapper.getAttribute("origStyle")!==null){oElem.style.cssText=oWrapper.getAttribute("origStyle")
oWrapper.removeAttribute("origStyle")}
if(oParent){var oTemp=oElem.document.createElement("div")
oParent.insertBefore(oTemp,oWrapper)
oTemp.swapNode(oElem)
oWrapper.removeNode(true)}
if(oElem){this.elem=oElem}}}
Transition.getDuration=function(sDur){if(sDur){var iDur=+sDur.replace(/[^0-9\.\-]/g,""),sUnit=sDur.replace(/\-?\d*\.?/g,"")
if(!isNaN(iDur)){if(sUnit=="sec")iDur*=1000
if(sUnit=="min")iDur*=60000
return iDur}}
return null}
Transition.getConfig=function(sType){var pTransObj=Transition.config[sType]
while(pTransObj&&pTransObj.alias)pTransObj=Transition.config[pTransObj.alias]
return pTransObj}
Transition.prototype.show={play:function(oTrans){oTrans.timer=setTimeout(function(){oTrans.stop();},oTrans.prop.delay||10)},stop:function(oTrans){clearTimeout(oTrans.timer)}}
Transition.prototype.random={play:function(oTrans){var aTypes=[],sType,oConf,sProp,oProp
for(sType in Transition.config){oConf=Transition.getConfig(sType)
if(oConf.func=="random"||oConf.func=="show")continue
aTypes[aTypes.length]=sType}
sType=aTypes[Math.floor(Math.random()*aTypes.length)]
oConf=Transition.getConfig(sType)
oTrans.randType=oTrans.type
try{if(typeof(element)!="undefined"&&element.document.getElementById("_randView_")){element.document.getElementById("_randView_").innerText=sType}}catch(oErr){}
oTrans.type=sType
oTrans.conf=oConf
for(sProp in oConf.prop){oProp=oConf.prop[sProp]
if(oProp.type=="select"){oTrans.prop[sProp]=oProp.opt[Math.floor(Math.random()*oProp.opt.length)]
if(oProp.map){oTrans.prop[sProp]=oProp.map[oTrans.prop[sProp]]}}else if(oProp.type=="number"&&oProp.def){oTrans.prop[sProp]=oProp.def
if(oProp.factor){oTrans.prop[sProp]=oTrans.prop[sProp]*oProp.factor}}}
oTrans[oTrans.randFunc=oConf.func].play(oTrans)},stop:function(oTrans){oTrans.type=oTrans.randType
oTrans[oTrans.randFunc].stop(oTrans)}}
Transition.prototype.fade={play:function(oTrans){var self=this
if(oTrans.mode=="visible"){oTrans.elem.style.visibility="visible"
oTrans.base=0
oTrans.target=1
setOpacity(oTrans.elem,0)}else{oTrans.base=1
oTrans.target=0}
if(CSSTransition.isSupported()){CSSTransition.init(oTrans,"opacity")
setOpacity(oTrans.elem,oTrans.target)}else{oTrans.start=new Date()
oTrans.step=(oTrans.target-oTrans.base)/oTrans.duration
oTrans.delay=30
oTrans.interval=setInterval(function(){self.step(oTrans)},oTrans.delay)}},stop:function(oTrans){oTrans.elem.style.visibility=oTrans.mode
setOpacity(oTrans.elem)
if(oTrans.interval){clearTimeout(oTrans.interval)
oTrans.interval=null}},step:function(oTrans){var iTime=new Date()-oTrans.start
if(iTime>=oTrans.duration){oTrans.stop()}else{setOpacity(oTrans.elem,oTrans.base+iTime*oTrans.step)}}}
Transition.prototype.fly={play:function(oTrans){for(var sProp in oTrans.conf.prop){var oProp=oTrans.conf.prop[sProp]
while(oTrans.prop[sProp]=="random")oTrans.prop[sProp]=oProp.opt[Math.floor(Math.random()*oProp.opt.length)]}
var oActivePos={left:"left",right:"left",top:"top",bottom:"top"},sDirection=oTrans.mode=="hidden"&&oTrans.prop.outDirection||oTrans.prop.inDirection||"left",getOffsetFunc=sDirection=="left"||sDirection=="right"?getOffsetLeft:getOffsetTop,offsetDim=sDirection=="left"||sDirection=="right"?"offsetWidth":"offsetHeight",sType=oTrans.mode=="visible"&&(oTrans.prop.inType||"ease soft in")||oTrans.prop.outType||"linear",self=this
oTrans.activePos=oActivePos[sDirection]
oTrans.originalPos=getOffsetFunc(oTrans.elem,oTrans.container)
if(oTrans.container.id!="oPreview"){var oTargetParent=oTrans.elem.parentElement,oPlaceHolder=createCustom("transplaceholder"),sStyle=oTrans.elem.style.cssText
while(oTargetParent.parentElement!=oTrans.container){oTargetParent=oTargetParent.parentElement}
Transition.copyCurrentStyles(oTrans.elem,oTrans.elem,["fontFamily","fontSize","color","fontWeight","fontStyle","textDecoration","fontVariant","textTransform","textAlign","letterSpacing","lineHeight","textIndent","wordWrap","wordBreak","textOverflow","whiteSpace"])
oPlaceHolder.setAttribute("origStyle",sStyle)
oPlaceHolder.style.cssText=(getTag(oTrans.elem)=="transwrapper"?oTrans.elem.firstChild.style.cssText:"")+sStyle
Classes.set(oPlaceHolder,Classes.get(oTrans.elem))
oPlaceHolder.innerText="\uFEFF"
oPlaceHolder.style.visibility="hidden"
oPlaceHolder.style.width=oTrans.elem.offsetWidth+"px"
oPlaceHolder.style.height=oTrans.elem.offsetHeight+"px"
oPlaceHolder.style.borderWidth="0px"
oPlaceHolder.style.padding="0px"
oPlaceHolder.activetrans=oTrans.elem.activetrans
if(getTag(oTrans.elem)=="LI"||getTag(oTrans.elem)=="transwrapper"&&getTag(oTrans.elem.firstChild)=="LI"){var oUL=oTargetParent.appendChild(document.createElement("ul"))
oUL.style.padding="0"
oUL.style.margin="0"
oUL.appendChild(oPlaceHolder)
oPlaceHolder.listWrapper=oUL}else{oTargetParent.appendChild(oPlaceHolder)}
oTrans.elem.swapNode(oPlaceHolder)
oTrans.elem.style.position="absolute"
oTrans.elem.style.display="block"
oTrans.elem.style.margin=0
oTrans.elem.style.zIndex=1000
oTrans.elem.style.right="auto"
oTrans.elem.style.bottom="auto"
oTrans.elem.style.left=getOffsetLeft(oPlaceHolder,oTargetParent)+"px"
oTrans.elem.style.top=getOffsetTop(oPlaceHolder,oTargetParent)+"px"
oTrans.elem.placeholder=oPlaceHolder}
if(oTrans.mode=="visible"){oTrans.target=getOffsetFunc(oTrans.elem,oTrans.container)
if(sDirection=="left"||sDirection=="top"){oTrans.elem.style[sDirection]=-oTrans.elem[offsetDim]+"px"}else{oTrans.elem.style[oTrans.activePos]=oTrans.container[offsetDim]+"px"}
oTrans.elem.style.visibility="visible"}else{if(sDirection=="left"||sDirection=="top"){oTrans.target=-oTrans.elem[offsetDim]}else{oTrans.target=oTrans.container[offsetDim]}}
oTrans.bAlpha=oTrans.type=="Fly Fade"
if(CSSTransition.isSupported()&&this.getCSSFunc(sType)!==false){if(oTrans.bAlpha)oTrans.elem.style.opacity=oTrans.mode=="visible"?"0":"1"
CSSTransition.init(oTrans,oTrans.activePos+(oTrans.bAlpha?" opacity":""),this.getCSSFunc(sType))
oTrans.elem.style[oTrans.activePos]=oTrans.target+"px"
if(oTrans.bAlpha)oTrans.elem.style.opacity=oTrans.mode=="visible"?"1":"0"}else{oTrans.start=getOffsetFunc(oTrans.elem,oTrans.container)
oTrans.dist=oTrans.target-oTrans.start
oTrans.startTime=new Date()
oTrans.duration=oTrans.duration
oTrans.delay=30
oTrans.lastPos=0
oTrans.flyFunc=this.getFunc(sType)
oTrans.interval=setInterval(function(){self.step(oTrans)},oTrans.delay)}},stop:function(oTrans){if(oTrans.interval){clearInterval(oTrans.interval)
oTrans.interval=null}
oTrans.elem.style[oTrans.activePos]=oTrans.originalPos+"px"
var oPlaceHolder=oTrans.elem.placeholder
if(oPlaceHolder){try{if(oPlaceHolder.getAttribute("origStyle")!==null){oTrans.elem.style.cssText=oPlaceHolder.getAttribute("origStyle")
oPlaceHolder.removeAttribute("origStyle")}
oPlaceHolder.replaceNode(oTrans.elem)
if(oPlaceHolder.listWrapper)oPlaceHolder.listWrapper.removeNode(true)
oPlaceHolder.listWrapper=null
oTrans.elem.placeholder=null}catch(oErr){}}},step:function(oTrans){var iCurTime=new Date()-oTrans.startTime
if(iCurTime>=oTrans.duration){oTrans.stop()}else{var iPos=oTrans.flyFunc(iCurTime,0,1,oTrans.duration)
oTrans.elem.style[oTrans.activePos]=(oTrans.start+iPos*oTrans.dist)+"px"
oTrans.lastPos=iPos
if(oTrans.bAlpha){if(oTrans.mode=="hidden")iPos=1-iPos
iPos=Math.min(Math.max(iPos,0),1)
setOpacity(oTrans.elem,iPos)}}},getFunc:function(sType){switch(sType){case"ease soft in":return function(t,b,c,d){return c*Math.sqrt(1-(t=t/d-1)*t)+b;}
case"ease soft out":return function(t,b,c,d){return-c*(Math.sqrt(1-(t/=d)*t)-1)+b;}
case"ease hard in":return function(t,b,c,d){return(t==d)?b+c:c*(-Math.pow(2,-10*t/d)+1)+b;}
case"ease hard out":return function(t,b,c,d){return(t==0)?b:c*Math.pow(2,10*(t/d-1))+b;}
case"ease back in":return function(t,b,c,d){return c*((t=t/d-1)*t*(2.70158*t+1.70158)+1)+b;}
case"ease back out":return function(t,b,c,d){return c*(t/=d)*t*(2.70158*t-1.70158)+b;}
case"elastic in":return function(t,b,c,d){if(t==0)return b;if((t/=d)==1)return b+c
var p=650,s=p/4,a=c
return a*Math.pow(2,-10*t)*Math.sin((t*d-s)*(2*Math.PI)/p)+c+b}
case"elastic out":return function(t,b,c,d){if(t==0)return b;if((t/=d)==1)return b+c
var p=650,s=p/4,a=c
return-(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b}
case"bounce in":return function(t,b,c,d){if((t/=d)<(1/2.75))return c*(7.5625*t*t)+b
else if(t<(2/2.75))return c*(7.5625*(t-=(1.5/2.75))*t+.75)+b
else if(t<(2.5/2.75))return c*(7.5625*(t-=(2.25/2.75))*t+.9375)+b
else return c*(7.5625*(t-=(2.625/2.75))*t+.984375)+b}
case"bounce out":return function(t,b,c,d){return c-this.fly.getFunc("bounce in")(d-t,0,c,d)+b}
default:return function(t,b,c,d){return b+c*t/d;}}},getCSSFunc:function(sType){switch(sType){case"ease soft in":return"cubic-bezier(0.215, 0.61, 0.355, 1)"
case"ease soft out":return"cubic-bezier(0.6, 0.04, 0.98, 0.335)"
case"ease hard in":return"cubic-bezier(0.19, 1, 0.22, 1)"
case"ease hard out":return"cubic-bezier(0.95, 0.05, 0.795, 0.035)"
case"ease back in":return"cubic-bezier(0.175, 0.885, 0.32, 1.275)"
case"ease back out":return"cubic-bezier(0.6, -0.28, 0.735, 0.045)"
case"linear":return"linear"}
return false}}
Transition.prototype.blink={play:function(oTrans){var self=this
if(!oTrans.prop.count)oTrans.prop.count=4
oTrans.delay=oTrans.duration/(oTrans.prop.count*2)
oTrans.count=0
oTrans.time=oTrans.duration+oTrans.delay
oTrans.start=new Date().getTime()
if(oTrans.prop.rate=="decelerate"){oTrans.delay/=50}
oTrans.interval=setTimeout(function(){self.step(oTrans)},oTrans.delay)},stop:function(oTrans){oTrans.elem.style.visibility=oTrans.mode
if(oTrans.interval){clearTimeout(oTrans.interval)
oTrans.interval=null}},step:function(oTrans){var self=this
if(new Date().getTime()-oTrans.start>=oTrans.time&&oTrans.count%2){oTrans.stop()}else{oTrans.count++
if(oTrans.elem.style.visibility=="visible"){oTrans.elem.style.visibility="hidden"}else{if(oTrans.prop.rate=="accelerate"){oTrans.delay*=0.85}else if(oTrans.prop.rate=="decelerate"){oTrans.delay*=1.15}
oTrans.elem.style.visibility="visible"}
oTrans.interval=setTimeout(function(){self.step(oTrans)},oTrans.delay)}}}
Transition.revealElementDef=function(oElem,oDefElem,sDisplay,sVisible,fCallback){var bStatic=oElem.getAttribute("transactive")!="true"&&(!oDefElem||oDefElem.getAttribute("transactive")!="true")||window.bStatic
sVisible=sVisible||"visible"
if(bStatic){oElem.style.visibility=sVisible
if(sDisplay)oElem.style.display=sDisplay
;/*@if(@_jscript)
oElem.runtimeStyle.width=oElem.offsetWidth+"px"
oElem.runtimeStyle.removeAttribute("width")
;/*@end @*/
if(fCallback)fCallback()
return}else{oElem.style.visibility=sVisible=="visible"?"hidden":"visible"
if(sVisible=="visible"&&sDisplay)oElem.style.display=sDisplay
var oTrans=new Transition(oElem,oDefElem)
oTrans.onFinish=function(){if(sVisible=="hidden"&&sDisplay)oElem.style.display=sDisplay
if(fCallback)fCallback.apply(oTrans,arguments)}
oTrans.init()
oTrans.play()}}
Transition.copyCurrentStyles=function(oTo,oFrom,aStyles){for(var iS=aStyles.length;iS--;){var sProp=aStyles[iS],sVal=oFrom.currentStyle[sProp]
if(sProp=="lineHeight"&&sVal.slice(-2)=="px"&&sVal!=oFrom.style[sProp]&&oTo==oFrom){for(var iC=oFrom.childNodes.length;iC--;){var oCh=oFrom.childNodes[iC]
if(oCh.nodeType==1)
Transition.copyCurrentStyles(oCh,oCh,["lineHeight"])}}
oTo.style[sProp]=sVal}}
var CSSTransition={getProp:function(){var aProps=["WebkitTransition","transition","MozTransition","msTransition","OTransition"]
for(var iP=0;iP<aProps.length;iP++)
if(aProps[iP]in document.documentElement.style)
return aProps[iP]
return false},isSupported:function(){return this.getProp()!==false},init:function(oTrans,sProp,sTimingFunc){var sCSSTrans=this.getProp()
oTrans.elem.offsetWidth
oTrans.elem.style[sCSSTrans+"Property"]=sProp
oTrans.elem.style[sCSSTrans+"Duration"]=oTrans.duration+"ms"
oTrans.elem.style[sCSSTrans+"TimingFunction"]=sTimingFunc||"linear"
oTrans.elem.addEventListener({"transition":"transitionend","WebkitTransition":"webkitTransitionEnd","MozTransition":"transitionend","OTransition":"oTransitionEnd","msTransition":"MSTransitionEnd"}[sCSSTrans],function(event){oTrans.elem.removeEventListener(event.type,arguments.callee,false)
oTrans.elem.style[sCSSTrans+"Property"]=""
oTrans.elem.style[sCSSTrans+"Duration"]=""
oTrans.elem.style[sCSSTrans+"TimingFunction"]=""
oTrans.stop(oTrans)},false)}}
Transition.config={Show:{func:"show"},Fly:{func:"fly",prop:{inDirection:{name:"方向",type:"select",opt:["left","right","top","bottom","random"],optText:["左","右","上对齐","下对齐","随机"]},inType:{name:"排版",type:"select",opt:["ease soft in","ease soft out","ease hard in","ease hard out","ease back in","ease back out","elastic in","elastic out","bounce in","bounce out","linear","random"],optText:["快速软停止","慢速软停止","快速硬停止","慢速硬停止","刹车","撞击","快速刹车","快速撞击","进入跳跃j","跳跃进入","线形","随机"]},outDirection:{name:"外部方向",type:"select",opt:["left","right","top","bottom","random"],optText:["左","右","上对齐","下对齐","随机"]},outType:{name:"外部类型",type:"select",opt:["ease soft in","ease soft out","ease hard in","ease hard out","ease back in","ease back out","elastic in","elastic out","bounce in","bounce out","linear","random"],optText:["快速软停止","慢速软停止","快速硬停止","慢速硬停止","刹车","撞击","快速刹车","快速撞击","进入跳跃j","跳跃进入","线形","随机"]}}},"Fly Fade":{alias:"Fly"},Blink:{func:"blink",prop:{count:{name:"计数",type:"number",step:1,low:2,high:10,def:4},rate:{name:"评级",type:"select",opt:["normal","accelerate","decelerate"],optText:["一般","加速","减速"]}}},Fade:{func:"fade"},Random:{func:"random"}}
String.prototype.hash=function(){var hasher={}
if(this!=""){var tmpArr=this.split(",")
for(var iT=0;iT<tmpArr.length;iT++){var itemArr=tmpArr[iT].split("=")
hasher[itemArr[0]]=itemArr[1]}}
return hasher}
function getOffsetTop(oEl,oFinal){for(var iY=0;oEl!=oFinal&&oEl!=null;oEl=oEl.offsetParent){iY+=oEl.offsetTop+oEl.clientTop}
return iY}
function getOffsetLeft(oEl,oFinal){for(var iX=0;oEl!=oFinal&&oEl!=null;oEl=oEl.offsetParent){iX+=oEl.offsetLeft+oEl.clientLeft}
return iX}
