function ShapeMain(){with(ShapeMain){with(this){this.Stage=undefined
this.timeline=undefined
this.shp=undefined
this.shpProps=undefined
this.setProps=function(args,update){var argsProxy=new ArgsProxy(args)
if(argsProxy.getValue("subType")!==undefined){shp=new(getShapeClass(argsProxy.getValue("subType")))()
shp.Stage=Stage
shp.timeline=timeline
shpProps=shp.getProps()
shp.width=Stage.width
shp.height=Stage.height}
var oldShadowOffX=shp.shadowEnabled?shp.shadowOffsetX:0
var oldShadowOffY=shp.shadowEnabled?shp.shadowOffsetY:0
shpProps.apply(argsProxy,shp)
if(shp.width>0&&shp.height>0){timeline.clear()
shp.draw()}}
this.updateProp=function(args){setProps(args,true)}
this.resize=function(stgWidth,stgHeight){var shpWidth=shp.width
var shpHeight=shp.height
var doDraw=false
if(Math.round(shpWidth)!=stgWidth){shp.width=stgWidth
if(stgWidth>0)doDraw=true}
if(Math.round(shpHeight)!=stgHeight){shp.height=stgHeight
if(shpHeight>0)doDraw=true}
if(doDraw){timeline.clear()
shp.draw()}}
this.getShapeClass=function(name){switch(name){case"oval":return Oval
case"polygon":return Polygon
case"star":return Star
case"calloutOval":return CalloutOval
case"calloutRect":return CalloutRect
case"arrow":return Arrow
case"line":return Arrow
case"trapezoid":return Trapezoid
case"parallelogram":return Parallelogram
case"cross":return Cross
case"diamond":return Diamond
default:return Rect}};(function(context){this.Stage=new StageType(context)
this.timeline=new MovieClip(context)}).apply(this,arguments)}}}
function BaseShape(){with(BaseShape){with(this){this._timeline=undefined
this.lineEnabled=false
this._lineWidth=1
this.lineStroke="solid"
this.fillEnabled=false
this.gradientEnabled=false
this.gradientType="linear"
this.currentX=0
this.currentY=0
this.pList=undefined
this._aspectRatio=0
this.Stage=undefined
Object.defineProperty(this,"timeline",{set:function(val){_timeline=val},get:function(){return _timeline}})
this.width=0
this.height=0
Object.defineProperty(this,"lineType",{set:function(val){lineEnabled=val!="none"
if(lineEnabled){lineStroke=val}else lineStroke="solid"},get:function(){return lineEnabled?lineStroke:"none"}})
Object.defineProperty(this,"lineWidth",{set:function(val){_lineWidth=val},get:function(){return lineEnabled?_lineWidth:0}})
this.lineColor=0x000000
this.dashWidth=4
this.dashSpacing=2
Object.defineProperty(this,"fillType",{set:function(val){fillEnabled=val!="none"
gradientEnabled=val!="solid"
if(gradientEnabled){gradientType=val}},get:function(){return fillEnabled?gradientEnabled?gradientType:"solid":"none"}})
this.fillColor=0xFFFFFF
this.fillOpacity=100
this.gradientColor=0x000000
this.gradientOpacity=100
this.gradientAngle=0
this.rotation=0
this.rotationScaling="normal"
Object.defineProperty(this,"aspectRatio",{set:function(val){_aspectRatio=val},get:function(){return rotationScaling=="normal"?_aspectRatio:0}})
this.shadowEnabled=false
this.shadowColor=0x000000
this.shadowOpacity=30
this.shadowOffsetX=3
this.shadowOffsetY=3
Object.defineProperty(this,"offsetX",{get:function(){return pList.offX}})
Object.defineProperty(this,"offsetY",{get:function(){return pList.offY}})
this.draw=function(){pList=new PointList()
populatePoints()
pList.transform(rotation,aspectRatio)
pList.normalize(Math.max(Math.round(width-Math.abs(shadowEnabled?shadowOffsetX:0)),0),Math.max(Math.round(height-Math.abs(shadowEnabled?shadowOffsetY:0)),0),lineWidth)
if(shadowEnabled){if(shadowOffsetX>0)timeline.offsetX+=shadowOffsetX
if(shadowOffsetY>0)timeline.offsetY+=shadowOffsetY
var lineEnabledOrig=lineEnabled
var fillEnabledOrig=fillEnabled
var fillTypeOrig=fillType
var fillColorOrig=fillColor
var fillOpacityOrig=fillOpacity
lineEnabled=false
fillEnabled=true
fillType="solid"
fillColor=shadowColor
fillOpacity=shadowOpacity
drawPoints(pList.offX,pList.offY)
lineEnabled=lineEnabledOrig
fillEnabled=fillEnabledOrig
fillType=fillTypeOrig
fillColor=fillColorOrig
fillOpacity=fillOpacityOrig
timeline.offsetX-=shadowOffsetX
timeline.offsetY-=shadowOffsetY}
drawPoints(pList.offX,pList.offY)
if(shadowEnabled){if(shadowOffsetX<0)timeline.offsetX+=shadowOffsetX
if(shadowOffsetY<0)timeline.offsetY+=shadowOffsetY}}
this.drawPoints=function(offX,offY){if(lineStroke=="dashed"){if(fillEnabled){timeline.lineStyle(lineWidth,lineColor,100)
lineStroke="solid"
timeline.beginPath()
beginFill(offX,offY)
enumPoints(this,drawPoint)
timeline.endFill()
lineStroke="dashed"}
if(lineEnabled){timeline.beginPath()
enumPoints(this,drawPoint)
lineStyle()
timeline.stroke()}}else if(fillEnabled||lineEnabled){timeline.beginPath()
lineStyle()
beginFill(offX,offY)
enumPoints(this,drawPoint)
if(fillEnabled)timeline.endFill()
if(lineEnabled)timeline.stroke()}}
this.getProps=function(){return new PropList(new Prop("lineType",Prop.STRING),new Prop("lineWidth",Prop.NUMBER,{min:1}),new Prop("lineColor",Prop.COLOR),new Prop("dashWidth",Prop.NUMBER,{min:1,max:25}),new Prop("dashSpacing",Prop.NUMBER,{min:1,max:25}),new Prop("fillType",Prop.STRING),new Prop("fillColor",Prop.COLOR),new Prop("fillOpacity",Prop.NUMBER,{min:0,max:100}),new Prop("gradientColor",Prop.COLOR),new Prop("gradientOpacity",Prop.NUMBER,{min:0,max:100}),new Prop("gradientAngle",Prop.NUMBER,{min:0,max:360}),new Prop("rotation",Prop.NUMBER,{min:0,max:360}),new Prop("rotationScaling",Prop.STRING),new Prop("aspectRatio",Prop.NUMBER,{min:0}),new Prop("shadowEnabled",Prop.BOOL),new Prop("shadowColor",Prop.COLOR),new Prop("shadowOpacity",Prop.NUMBER,{min:0,max:100}),new Prop("shadowOffsetX",Prop.NUMBER),new Prop("shadowOffsetY",Prop.NUMBER))}
this.getNewBounds=function(newRotation){var prevRotation=rotation%180
if(prevRotation>90)prevRotation=180-prevRotation
prevRotation*=Point.DEG_TO_RAD
var shadowOffX=Math.abs(shadowEnabled?shadowOffsetX:0)
var shadowOffY=Math.abs(shadowEnabled?shadowOffsetY:0)
pList=new PointList()
populatePoints()
pList.transform(rotation,aspectRatio)
pList.normalize(width-_lineWidth-shadowOffX,height-_lineWidth-shadowOffY,0)
pList.transform((newRotation-rotation)%360+360,0)
var minMax=pList.getMinMax()
var min=minMax.min
var max=minMax.max
return new Point(max.x-min.x+_lineWidth+shadowOffX,max.y-min.y+_lineWidth+shadowOffY)}
this.populatePoints=function(){}
this.drawPoint=function(){}
this.lineStyle=function(transparent){timeline.lineStyle(lineWidth,lineColor,!transparent?100:0)}
this.beginFill=function(offX,offY){if(fillEnabled){if(gradientEnabled){var shadowOffX=Math.abs(shadowEnabled?shadowOffsetX:0)
var shadowOffY=Math.abs(shadowEnabled?shadowOffsetY:0)
var matrix={matrixType:"box",x:(offX?offX:0),y:(offY?offY:0),w:width-shadowOffX,h:height-shadowOffY,r:gradientAngle*(Math.PI/180)}
timeline.beginGradientFill(gradientType,[fillColor,gradientColor],[fillOpacity,gradientOpacity],[0,0xFF],matrix)}else{timeline.beginFill(fillColor,fillOpacity)}}}
this.moveTo=function(x,y){timeline.moveTo(x,y)
currentX=x
currentY=y}
this.lineTo=function(x,y,gapStart,gapEnd){switch(lineStroke){case"dashed":var dx=x-currentX
var dy=y-currentY
var length=Math.sqrt(dx*dx+dy*dy)
var segments=Math.ceil(length/(dashWidth+lineWidth)/(dashSpacing+1))*(dashSpacing+1)+(gapStart&&gapEnd?2:gapStart||gapEnd?0:1)
var n=gapStart?dashSpacing:1
for(var i=0;i<segments;i+=dashSpacing+1-n){var p0=getPointOnLine(currentX,currentY,x,y,i/segments)
var p1=getPointOnLine(currentX,currentY,x,y,(i+=n)/segments)
lineStyle(!gapStart)
timeline.lineTo(p0.x,p0.y)
lineStyle(gapStart)
timeline.lineTo(p1.x,p1.y)}
lineStyle(!gapStart)
timeline.lineTo(x,y)
lineStyle()
break
default:timeline.lineTo(x,y)}
currentX=x
currentY=y}
this.curveTo=function(cx,cy,x,y,gapStart,gapEnd){switch(lineStroke){case"dashed":var dx=cx-currentX
var dy=cy-currentY
var length=Math.sqrt(dx*dx+dy*dy)
dx=x-cx
dy=y-cy
length+=Math.sqrt(dx*dx+dy*dy)
var segments=Math.ceil(length/(dashWidth+lineWidth)/(dashSpacing+1))*(dashSpacing+1)+(gapStart&&gapEnd?2:gapStart||gapEnd?0:1)
var n=gapStart?0:1
for(var i=0;i<=segments;i++){var p0=getPointOnCurve(currentX,currentY,cx,cy,x,y,i/segments)
lineStyle(i%(dashSpacing+1)!=n)
timeline.lineTo(p0.x,p0.y)}
lineStyle()
break
default:timeline.curveTo(cx,cy,x,y)}
currentX=x
currentY=y}
this.curveThrough=function(cx,cy,x,y,gapEnd,gapStart){curveTo(cx*2-(currentX+x)/2,cy*2-(currentY+y)/2,x,y,gapEnd,gapStart)}
this.realLineTo=function(x,y){timeline.lineTo(x,y)
currentX=x
currentY=y}
this.realCurveTo=function(cx,cy,x,y){timeline.curveTo(cx,cy,x,y)
currentX=x
currentY=y}
this.realCurveThrough=function(cx,cy,x,y){timeline.curveTo(cx*2-(currentX+x)/2,cy*2-(currentY+y)/2,x,y)
currentX=x
currentY=y}
this.getPointOnLine=function(sx,sy,ex,ey,t){t=Math.max(Math.min(t,1),0)
return new Point(sx+(ex-sx)*t,sy+(ey-sy)*t)}
this.getPointOnCurve=function(sx,sy,cx,cy,ex,ey,t){t=Math.max(Math.min(t,1),0)
var c=1-t
return new Point(c*c*sx+2*t*c*cx+t*t*ex,c*c*sy+2*t*c*cy+t*t*ey)}
this.addPoint=function(p,data){if(data!=null)p.data=data
pList.addPoint(p)}
this.getPoint=function(i){return pList.getPoint(i)}
this.enumPoints=function(target,func){var lastPoint
var curPoint
var nextPoint
var n=0
for(var i=0;i<=pList.length+1;i++){lastPoint=curPoint
curPoint=nextPoint
nextPoint=getPoint(i)
while(nextPoint.data.skip&&i<pList.length)nextPoint=getPoint(++i)
if(curPoint){func.call(target,lastPoint,curPoint,nextPoint,n++)}}}}}}
function BasePolygon(){with(BaseShape){with(BasePolygon){with(this){var Super=extendClass(BaseShape,this,arguments)
this.cornerRadius=0
this.getProps=function(){return Super.getProps().extend(new Prop("cornerRadius",Prop.NUMBER,{min:0}))}
this.drawPoint=function(lastPoint,curPoint,nextPoint,i){if(cornerRadius>0&&curPoint.data.sharp!=true){var cPNext=getCornerPoint(curPoint,nextPoint,cornerRadius)
if(lastPoint){var cPPrev=getCornerPoint(curPoint,lastPoint,cornerRadius)
lineTo(cPPrev.x,cPPrev.y,true)
curveTo(curPoint.x,curPoint.y,cPNext.x,cPNext.y,true)}else{moveTo(cPNext.x,cPNext.y)}}else{if(lastPoint){lineTo(curPoint.x,curPoint.y)}else{moveTo(curPoint.x,curPoint.y)}}}
this.getCornerPoint=function(corner,other,radius){var dx=other.x-corner.x
var dy=other.y-corner.y
var len=Math.sqrt(dx*dx+dy*dy)
var ratio
if(2*radius>len)ratio=0.5
else ratio=radius/len
var x=corner.x+dx*ratio
var y=corner.y+dy*ratio
return new Point(x,y)}}}}}
function Oval(){with(BaseShape){with(Oval){with(this){var Super=extendClass(BaseShape,this,arguments)
this.jagFlip=undefined
this.jagEnabled=false
this.jagSpan=15
this.jagSize=15
this.getProps=function(){return Super.getProps().extend(new Prop("jagEnabled",Prop.BOOL),new Prop("jagSpan",Prop.NUMBER,{min:10}),new Prop("jagSize",Prop.NUMBER,{min:5,max:50}))}
this.populatePoints=function(){var angDash
var angInc
if(jagEnabled){angInc=Math.PI/Math.ceil(Math.PI/Math.asin(Math.min(jagSpan*8/(height+width),1)))
angDash=angInc/2}else if(lineStroke=="dashed"){angDash=Math.asin((dashWidth+lineWidth)/(height+width)*2)*2
angInc=angDash*(dashSpacing+1)
angInc=Math.PI/Math.ceil(Math.PI*2/angInc)
angDash=angInc/(dashSpacing+1)}else{var span=Math.PI/SEGMENTS
angDash=span
angInc=span*2}
var count=Math.PI/angInc*2
addPoint(getPointOnOval(0))
for(var cur=0,i=0;i<count;i++){var ang=i%2==0?angDash:angInc-angDash
addPoint(getPointOnOval(cur+=ang),{dash:i%2==0})
if(i<count-1){addPoint(getPointOnOval(cur+=ang))}}}
this.draw=function(){if(jagEnabled)seedJag()
Super.draw()}
this.drawPoints=function(){Super.drawPoints()}
this.drawPoint=function(lastPoint,curPoint,nextPoint,i){if(i==0){moveTo(curPoint.x,curPoint.y)}else if(i%2==1||jagEnabled){if(curPoint.data.mark==true){drawMark(lastPoint,curPoint,nextPoint,i)}else if(jagEnabled){lineTo(curPoint.x,curPoint.y,false,true)}else{if(lastPoint.data.special==true){curveThrough(curPoint.x,curPoint.y,nextPoint.x,nextPoint.y,true,false)}else{if(lineStroke=="dashed")lineStyle(curPoint.data.dash!=true)
realCurveThrough(curPoint.x,curPoint.y,nextPoint.x,nextPoint.y)}}}}
this.drawMark=function(lastPoint,curPoint,nextPoint,i){}
this.getPointOnOval=function(angle){var amp=jagEnabled?getJag():1
return new Point(Math.cos(angle)*amp,Math.sin(angle)*amp)}
this.seedJag=function(){PRNG.seed()
jagFlip=false}
this.getJag=function(){var n=PRNG.generate()
var r=jagSize/100
jagFlip=!jagFlip
return 1-n*r-(jagFlip?r:0)}}}}}
Oval.SEGMENTS=24
function CalloutOval(){with(BaseShape){with(Oval){with(CalloutOval){with(this){var Super=extendClass(Oval,this,arguments)
this.pMark=null
this.tailType=0
this.tailSize=1
this.orientation=1
Object.defineProperty(this,"tail",{set:function(val){val--
tailType=TYPE_ARRAY[val]
tailSize=SIZE_ARRAY[val]}})
this.getProps=function(){return Super.getProps().remove("jagEnabled","jagSpan","jagSize").extend(new Prop("orientation",Prop.NUMBER,{min:0,max:3}),new Prop("tail",Prop.NUMBER,{min:1,max:12}))}
this.populatePoints=function(){var angDash
var angInc
if(lineStroke=="dashed"){angDash=Math.asin((dashWidth+lineWidth)/(height+width)*2)*2
angInc=angDash*(dashSpacing+1)
angInc=Math.PI/Math.ceil(Math.PI*2/angInc)
angDash=angInc/(dashSpacing+1)}else{var span=Math.PI/SEGMENTS
angDash=span
angInc=span*2}
var angMark=45*Point.DEG_TO_RAD
var angMarkStart=angMark-15*Point.DEG_TO_RAD
var angMarkEnd=angMark+15*Point.DEG_TO_RAD
var marked=false
var count=Math.PI/angInc*2
addPoint(getPointOnOval(0))
for(var cur=0,i=0;i<count;i++){var ang=i%2==0?angDash:angInc-angDash
var last=cur
if(cur<angMarkEnd&&cur+ang*2>angMarkStart){if(!marked){switch(tailSize){case 1:pMark=new Point(1.2,1.2);break
case 2:pMark=new Point(0.5,1.5);break
default:pMark=new Point(1,1)}
pMark.data={mark:true}
if(tailType==3||tailType==4){pMark.data.orig=getPointOnOval(angMark)
addPoint(pMark.data.orig,{skip:true})}
addPoint(pMark)
marked=true}
cur+=ang*2
continue}
if(marked){if(tailType==0&&aspectRatio!=0){last-=22.5*Point.DEG_TO_RAD-Math.atan(aspectRatio>1?1/aspectRatio:aspectRatio)/2}
addPoint(getPointOnOval(last),{special:true})
marked=false}
cur+=ang*2
addPoint(getPointOnOval((last+cur)/2),{dash:i%2==0})
if(i<count-1){addPoint(getPointOnOval(cur))}}
if(orientation>0){pList.flip(orientation&1,orientation&2)}}
this.drawMark=function(lastPoint,curPoint,nextPoint,i){var dashLast=lineStroke=="dashed"&&getPoint(i-2).data.dash==true
var dashNext=lineStroke=="dashed"&&getPoint(i+2).data.dash==true
switch(tailType){case 0:lineTo(curPoint.x,curPoint.y,dashLast)
lineTo(nextPoint.x,nextPoint.y,false,dashNext)
break
case 1:var midPointX=(lastPoint.x*2+nextPoint.x*2+curPoint.x)/5
var midPointY=(lastPoint.y*2+nextPoint.y*2+curPoint.y)/5
curveTo(midPointX,midPointY,curPoint.x,curPoint.y,dashLast)
curveTo(midPointX,midPointY,nextPoint.x,nextPoint.y,false,dashNext)
break
case 2:var midPointX=(lastPoint.x*2+nextPoint.x*2+curPoint.x)/5
var midPointY=(lastPoint.y*2+nextPoint.y*2+curPoint.y)/5
var halfPointX=(lastPoint.x+nextPoint.x+curPoint.x*2)/4
var halfPointY=(lastPoint.y+nextPoint.y+curPoint.y*2)/4
curveTo(midPointX,midPointY,halfPointX,halfPointY,dashLast)
lineTo(curPoint.x,curPoint.y)
curveTo(midPointX,midPointY,nextPoint.x,nextPoint.y,false,dashNext)
break
case 3:case 4:var origPoint=curPoint.data.orig
curveThrough(origPoint.x,origPoint.y,nextPoint.x,nextPoint.y,dashLast,dashNext)
break}}
this.drawPoints=function(){if(tailType==3||tailType==4){var markPoint=pMark.clone()
var origPoint=pMark.data.orig.clone()
markPoint.rotate(-rotation)
origPoint.rotate(-rotation)
var s=Math.floor(Math.min(Math.abs(markPoint.baseX)-Math.abs(origPoint.baseX),Math.abs(markPoint.baseY)-Math.abs(origPoint.baseY))*2)
createOval(0,markPoint,origPoint,tailType==3?s*.11:s*.32)
createOval(1,markPoint,origPoint,tailType==3?s*.19:s*.45,.85)
createOval(2,markPoint,origPoint,tailType==3?s*.34:s*.64,.61)}
Super.drawPoints.apply(this,arguments)}
this.createOval=function(num,markPoint,origPoint,size,off){if(off==undefined)off=1
var mc=timeline
var oval=new Oval()
var p=new Point(origPoint.baseX+(markPoint.baseX-origPoint.baseX)*off+(orientation&1?1:-1)*(size/2+2),origPoint.baseY+(markPoint.baseY-origPoint.baseY)*off+(orientation&2?1:-1)*size/2,rotation,pMark.offX,pMark.offY)
var offsetX=Math.floor(p.x-size/2)
var offsetY=Math.floor(p.y-size/2)
timeline.offsetX+=offsetX
timeline.offsetY+=offsetY
oval.timeline=timeline
getProps().transfer(this,oval)
oval.shadowEnabled=false
oval.rotation=0
oval.width=size
oval.height=size
oval.aspectRatio=aspectRatio
oval.draw()
timeline.offsetX-=offsetX
timeline.offsetY-=offsetY}}}}}}
CalloutOval.SIZE_ARRAY=[0,1,2,0,1,0,1,2,0,1,0,1]
CalloutOval.TYPE_ARRAY=[0,0,0,1,1,2,2,2,3,3,4,4]
function Rect(){with(BaseShape){with(BasePolygon){with(Rect){with(this){var Super=extendClass(BasePolygon,this,arguments)
this.populatePoints=function(){addPoint(new Point(-1,-1))
addPoint(new Point(1,-1))
addPoint(new Point(1,1))
addPoint(new Point(-1,1))}}}}}}
function CalloutRect(){with(BaseShape){with(BasePolygon){with(Rect){with(CalloutRect){with(this){var Super=extendClass(Rect,this,arguments)
this.tailType=0
this.orientation=1
Object.defineProperty(this,"tail",{set:function(val){tailType=val-1}})
this.getProps=function(){return Super.getProps().extend(new Prop("orientation",Prop.NUMBER,{min:0,max:3}),new Prop("tail",Prop.NUMBER,{min:1,max:4}))}
this.populatePoints=function(){var factor=aspectRatio!=0?Math.min(1/aspectRatio,2):1
var off=1-factor
var data={sharp:true}
addPoint(new Point(-1,-1))
addPoint(new Point(1,-1))
addPoint(new Point(1,1))
if(tailType==0){addPoint(new Point(off+0.8*factor,1),data)
addPoint(new Point(off+1*factor,1.5),data)
addPoint(new Point(off+0.5*factor,1),data)}else if(tailType==1){addPoint(new Point(off+1.2*factor,1.5),data)
addPoint(new Point(off+0.5*factor,1),data)}else if(tailType==2){addPoint(new Point(off+0.6*factor,1),data)
addPoint(new Point(off+1*factor,2),data)
addPoint(new Point(off,1),data)}else if(tailType==3){addPoint(new Point(off+0.8*factor,1),data)
addPoint(new Point(off+1.2*factor,2),data)
addPoint(new Point(off+0.3*factor,1),data)}
addPoint(new Point(-1,1))
if(orientation>0){pList.flip(orientation&1,orientation&2)}}}}}}}}
function Polygon(){with(BaseShape){with(BasePolygon){with(Polygon){with(this){var Super=extendClass(BasePolygon,this,arguments)
this.points=5
this.getProps=function(){return Super.getProps().extend(new Prop("points",Prop.NUMBER,{min:3}))}
this.populatePoints=function(){var ang=Math.PI*2/points
var off=-Math.PI/2
for(var i=0;i<points;i++){var x=Math.cos(off)
var y=Math.sin(off)
addPoint(new Point(x,y))
off+=ang}}}}}}}
function Arrow(){with(BaseShape){with(BasePolygon){with(Arrow){with(this){var Super=extendClass(BasePolygon,this,arguments)
this.arrows=1
this.headSize=40
this.headFactor=0
this.barSize=0
this.getProps=function(){return Super.getProps().extend(new Prop("arrows",Prop.NUMBER,{min:0,max:3}),new Prop("headSize",Prop.NUMBER,{min:0,max:100}),new Prop("headFactor",Prop.NUMBER,{min:-100,max:100}),new Prop("barSize",Prop.NUMBER,{min:0,max:100}))}
this.populatePoints=function(){var factor=aspectRatio!=0?1/aspectRatio:1
var barLen=arrows==3?1:2
var headW=Math.min(headSize/50*factor,barLen)
var barH=barSize/100
var headX=headFactor>0?(1-barH)*headW*headFactor/100:(barLen-headW)*headFactor/100
if(!(arrows&2)){addPoint(new Point(-1,-barH))}
if(arrows&1){addPoint(new Point(1-headW+headX,-barH))
addPoint(new Point(1-headW,-1))
addPoint(new Point(1,0))
addPoint(new Point(1-headW,1))
addPoint(new Point(1-headW+headX,barH))}else{addPoint(new Point(1,-barH))
addPoint(new Point(1,barH))}
if(arrows&2){addPoint(new Point(-1+headW-headX,barH))
addPoint(new Point(-1+headW,1))
addPoint(new Point(-1,0))
addPoint(new Point(-1+headW,-1))
addPoint(new Point(-1+headW-headX,-barH))}else{addPoint(new Point(-1,barH))}}}}}}}
function Cross(){with(BaseShape){with(BasePolygon){with(Cross){with(this){var Super=extendClass(BasePolygon,this,arguments)
this.barSize=30
this.getProps=function(){return Super.getProps().extend(new Prop("barSize",Prop.NUMBER,{min:0,max:100}))}
this.populatePoints=function(){addPoint(new Point(-barSize,-100))
addPoint(new Point(barSize,-100))
addPoint(new Point(barSize,-barSize))
addPoint(new Point(100,-barSize))
addPoint(new Point(100,barSize))
addPoint(new Point(barSize,barSize))
addPoint(new Point(barSize,100))
addPoint(new Point(-barSize,100))
addPoint(new Point(-barSize,barSize))
addPoint(new Point(-100,barSize))
addPoint(new Point(-100,-barSize))
addPoint(new Point(-barSize,-barSize))}}}}}}
function Diamond(){with(BaseShape){with(BasePolygon){with(Diamond){with(this){var Super=extendClass(BasePolygon,this,arguments)
this.populatePoints=function(){addPoint(new Point(0,-1))
addPoint(new Point(1,0))
addPoint(new Point(0,1))
addPoint(new Point(-1,0))}}}}}}
function Parallelogram(){with(BaseShape){with(BasePolygon){with(Parallelogram){with(this){var Super=extendClass(BasePolygon,this,arguments)
this.baseOff=25
this.getProps=function(){return Super.getProps().extend(new Prop("baseOff",Prop.NUMBER,{min:-100,max:100}))}
this.populatePoints=function(){addPoint(new Point(Math.abs(baseOff)-50,-50))
addPoint(new Point(50,-50))
addPoint(new Point(50-Math.abs(baseOff),50))
addPoint(new Point(-50,50))
if(baseOff<0){pList.flip(true,false)}}}}}}}
function Star(){with(BaseShape){with(BasePolygon){with(Star){with(this){var Super=extendClass(BasePolygon,this,arguments)
this.points=5
this.innerRadius=40
this.getProps=function(){return Super.getProps().extend(new Prop("points",Prop.NUMBER,{min:2}),new Prop("innerRadius",Prop.NUMBER,{min:0,max:100}))}
this.populatePoints=function(){var inRad=innerRadius/100
var ang=Math.PI/points
var off=-Math.PI/2
for(var i=0;i<points*2;i++){var x=Math.cos(off)*(i%2?inRad:1)
var y=Math.sin(off)*(i%2?inRad:1)
addPoint(new Point(x,y))
off+=ang}}}}}}}
function Trapezoid(){with(BaseShape){with(BasePolygon){with(Trapezoid){with(this){var Super=extendClass(BasePolygon,this,arguments)
this.baseWidth=50
this.getProps=function(){return Super.getProps().extend(new Prop("baseWidth",Prop.NUMBER,{min:0,max:100}))}
this.populatePoints=function(){addPoint(new Point(-baseWidth,-100))
addPoint(new Point(baseWidth,-100))
addPoint(new Point(100,100))
addPoint(new Point(-100,100))}}}}}}
function PointList(){with(PointList){with(this){this.rawMin=undefined
this.rawMax=undefined
this.points=undefined
Object.defineProperty(this,"length",{set:function(val){points.length=val},get:function(){return points.length}})
this.offX=0
this.offY=0
this.addPoint=function(p){points.push(p)
if(rawMin==undefined)rawMin=new Point(p.baseX,p.baseY)
else{if(p.baseX<rawMin.x)rawMin.baseX=p.baseX
if(p.baseY<rawMin.y)rawMin.baseY=p.baseY}
if(rawMax==undefined)rawMax=new Point(p.baseX,p.baseY)
else{if(p.baseX>rawMax.x)rawMax.baseX=p.baseX
if(p.baseY>rawMax.y)rawMax.baseY=p.baseY}}
this.getPoint=function(i){return points[i%length]}
this.transform=function(rotation,aspectRatio){if(aspectRatio>0){var shapeRatio=(rawMax.x-rawMin.x)/(rawMax.y-rawMin.y)
if(shapeRatio>0){aspectRatio/=shapeRatio}}
for(var i=0;i<length;i++){var p=getPoint(i)
if(aspectRatio>0){if(aspectRatio>1){p.baseY/=aspectRatio}else{p.baseX*=aspectRatio}}
if(rotation>0){p.rotate(rotation)}}}
this.normalize=function(width,height,lW){var minMax=getMinMax()
var min=minMax.min
var max=minMax.max
var diffX=max.x-min.x
var diffY=max.y-min.y
var w=diffX!=0?(width-Math.floor(lW))/diffX:lW
var h=diffY!=0?(height-Math.floor(lW))/diffY:lW
offX=-(max.x+min.x)*w/2
offY=-(max.y+min.y)*h/2
var cX=width/2+offX
var cY=height/2+offY
for(var i=0;i<length;i++){var p=getPoint(i)
p.baseX*=w
p.baseY*=h
p.offX=cX
p.offY=cY}}
this.getMinMax=function(){var p0=getPoint(0)
var min=new Point(p0.x,p0.y)
var max=new Point(p0.x,p0.y)
for(var i=1;i<length;i++){var p=getPoint(i)
if(p.x>max.x)max.baseX=p.x
else if(p.x<min.x)min.baseX=p.x
if(p.y>max.y)max.baseY=p.y
else if(p.y<min.y)min.baseY=p.y}
return{min:min,max:max}}
this.flip=function(horizontal,vertical){for(var i=0;i<length;i++){var p=getPoint(i)
if(horizontal)p.baseX*=-1
if(vertical)p.baseY*=-1}};(function(){points=[]}).apply(this,arguments)}}}
function Point(){with(Point){with(this){this.baseX=0
this.baseY=0
this.offX=0
this.offY=0
Object.defineProperty(this,"x",{get:function(){return baseX+offX}})
Object.defineProperty(this,"y",{get:function(){return baseY+offY}})
this.data={}
this.rotate=function(angle){if(angle!=0){angle=angle*DEG_TO_RAD+Math.atan2(baseY,baseX)
var len=Math.sqrt(baseX*baseX+baseY*baseY)
baseX=Math.cos(angle)*len
baseY=Math.sin(angle)*len}}
this.clone=function(){return new Point(baseX,baseY,undefined,offX,offY)}
this.toString=function(){return"["+baseX+"x"+baseY+"]"};(function(baseX,baseY,angle,offX,offY){if(baseX!==undefined)this.baseX=baseX
if(baseY!==undefined)this.baseY=baseY
if(angle!==undefined)rotate(angle)
if(offX!==undefined)this.offX=offX
if(offY!==undefined)this.offY=offY}).apply(this,arguments)}}}
Point.DEG_TO_RAD=Math.PI/180
function PropList(){with(Array){with(PropList){with(this){this.extend=function(){for(var i=0;i<arguments.length;i++){this.push(arguments[i])}
return this}
this.remove=function(){var hash={}
for(var i=0;i<arguments.length;i++){hash[arguments[i]]=true}
for(var i=this.length;i--;){if(hash[this[i].name])this.splice(i,1)}
return this}
this.apply=function(from,to){for(var i=0;i<this.length;i++){var prop=this[i]
var val=from.getValue(prop.name)
if(val!==undefined){to[prop.name]=prop.fromString(val)}}}
this.transfer=function(from,to){for(var i=0;i<this.length;i++){var prop=this[i]
to[prop.name]=from[prop.name]}}
extend.apply(this,arguments)}}}}
PropList.prototype=new Array()
function Prop(){with(Prop){with(this){this.name=undefined
this.type=undefined
this.options=undefined
this.fromString=function(val){switch(type){case NUMBER:var num=Number(val)
if(options&&options.min!==undefined)num=Math.max(num,options.min)
if(options&&options.max!==undefined)num=Math.min(num,options.max)
return num
case BOOL:return val=="true"||val=="yes"
case COLOR:return Number("0x"+val.substr(1))}
return val};(function(name,type,options){this.name=name
this.type=type
this.options=options}).apply(this,arguments)}}}
Prop.STRING=1
Prop.NUMBER=2
Prop.BOOL=3
Prop.COLOR=4
var PRNG={MAX_INT:4294967295,seed:function(){PRNG.x=123456789
PRNG.y=362436069
PRNG.z=521288629
PRNG.w=88675123},generate:function(){with(PRNG){var t=x^(x<<11)
x=y;y=z;z=w;w=(w^(w>>19))^(t^(t>>8))
return w/MAX_INT}}}
function StageType(){with(StageType){with(this){this.context=undefined
this.canvas=undefined
Object.defineProperty(this,"width",{get:function(){return canvas?canvas.width:0}})
Object.defineProperty(this,"height",{get:function(){return canvas?canvas.height:0}});(function(context){this.context=context
this.canvas=context.canvas}).apply(this,arguments)}}}
function MovieClip(){with(MovieClip){with(this){this.context=undefined
this.lineTransparent=undefined
this.offsetX=0
this.offsetY=0
this.lineStyle=function(thickness,rgb,alpha){context.lineJoin="round"
context.lineCap="round"
context.lineWidth=thickness
context.strokeStyle=getCSSColor(rgb,alpha)
lineTransparent=alpha==0}
this.beginFill=function(rgb,alpha){context.fillStyle=getCSSColor(rgb,alpha)}
this.beginGradientFill=function(fillType,colors,alphas,ratios,matrix){if(!matrix||matrix.matrixType!="box")throw new Error("Not implemented")
var gradient
var halfW=matrix.w/2
var halfH=matrix.h/2
var centerX=matrix.x+halfW
var centerY=matrix.y+halfH
if(fillType=="linear"){var angle=Math.atan2(Math.sin(matrix.r+Math.PI/2)*halfH,Math.cos(matrix.r+Math.PI/2)*halfW)-Math.PI/2
var radius=Math.sqrt(Math.pow(Math.cos(angle)*halfW,2)+Math.pow(Math.sin(angle)*halfH,2))
gradient=context.createLinearGradient(centerX-Math.cos(angle)*radius,centerY-Math.sin(angle)*radius,centerX+Math.cos(angle)*radius,centerY+Math.sin(angle)*radius)}
if(fillType=="radial")
gradient=context.createRadialGradient(centerX,centerY,0,centerX,centerY,Math.min(halfW,halfH))
if(!gradient)throw new Error("Not implemented")
for(var i=0;i<colors.length;i++)
gradient.addColorStop(ratios[i]/255,getCSSColor(colors[i],alphas[i]))
context.fillStyle="black"
context.fillStyle=gradient}
this.endFill=function(){context.fill()}
this.moveTo=function(x,y){context.moveTo(x+offsetX,y+offsetY)}
this.lineTo=function(x,y){if(lineTransparent)
context.moveTo(x+offsetX,y+offsetY)
else
context.lineTo(x+offsetX,y+offsetY)}
this.curveTo=function(controlX,controlY,anchorX,anchorY){if(lineTransparent)
context.moveTo(anchorX+offsetX,anchorY+offsetY)
else
context.quadraticCurveTo(controlX+offsetX,controlY+offsetY,anchorX+offsetX,anchorY+offsetY)}
this.clear=function(){context.beginPath()
context.clearRect(0,0,context.canvas.width,context.canvas.height)}
this.beginPath=function(){context.beginPath()}
this.closePath=function(){context.closePath()}
this.stroke=function(){context.stroke()}
function getCSSColor(rgb,alpha){return"rgba("+(rgb>>16&0xFF)+","+(rgb>>8&0xFF)+","+(rgb&0xFF)+","+(alpha/100)+")"}
(function(context){this.context=context}).apply(this,arguments)}}}
function ArgsProxy(){with(ArgsProxy){with(this){this.args=undefined
this.getValue=function(prop){var val=args.getAttribute?args.getAttribute(prop):args[prop]
return val===null?undefined:val};(function(args){this.args=args}).apply(this,arguments)}}}
function extendClass(superClass,targetObject,constructorArgs){var Super={}
superClass.apply(targetObject,constructorArgs)
for(var prop in targetObject)
if(targetObject.hasOwnProperty(prop)&&typeof targetObject[prop]=="function")
Super[prop]=getDelegate(targetObject,targetObject[prop])
return Super
function getDelegate(oObj,fFunc){return function(){return fFunc.apply(oObj,arguments)}}}