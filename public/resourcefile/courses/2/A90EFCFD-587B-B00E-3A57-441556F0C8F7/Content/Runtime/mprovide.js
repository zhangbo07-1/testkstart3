var Providers,bIsIE=!!document.documentMode,bIsFF=navigator.userAgent.indexOf("Gecko/")>-1
Providers={init:function(){var oYT=this.YouTube,oYTarg=oYT.engines.Frame.installed()?oYT.engines.Frame:oYT.engines.Flash
for(var sProp in oYTarg)oYT[sProp]=oYTarg[sProp]},set:function(providerName,oEl){this.current=this[providerName]
oEl.innerHTML=this.current.create()
this.current.engine=oEl.firstChild
return this.current},get:function(){return this.current},detect:function(fileName,oEl){if(!fileName)return
var sExt=fileName.replace(/\?.*/,"").replace(/.+\.([^\.]+)$/,"$1").toLowerCase(),provider="WMP"
if(window.bMobile){if(!Providers.audio&&this.SWF.installed())provider="SWF"
else provider="HTML5"}else{switch(sExt){case"mp4":case"flv":case"m4v":case"m4a":case"f4v":case"mov":case"3gp":case"3g2":if(this.SWF.installed())provider="SWF"
else provider="HTML5"
break
case"mp3":if(this.SWF.installed()){provider="SWF"
break}
default:if(this.WMP.installed())provider="WMP"
else provider="HTML5"}}
if(oEl)provider=this.set(provider,oEl)
return provider},WMP:{name:"WMP",installed:function(){if(bIsIE){try{new ActiveXObject("WMPlayer.OCX");return true;}catch(e){return false;}}else{return!!navigator.mimeTypes["application/x-ms-wmp"]}},create:function(){if(bIsIE){return'<object tabindex="-1" classid="clsid:6BF52A52-394A-11D3-B153-00C04F79FAA6"><param name="stretchToFit" value="true"><param name="uiMode" value="none"><param name="windowlessVideo" value="'+(Providers.windowed?"false":"true")+'"></object>'}else{return'<object tabindex="-1" data="" type="application/x-ms-wmp" style="width:100%;height:100%"><param name="uiMode" value="none" /><param name="stretchToFit" value="true" /><param name="windowlessVideo" value="'+(Providers.windowed||this.alwaysWindowed()?"false":"true")+'" /></object>'}},play:function(){this.engine.controls.play();this.playPressed=true;},pause:function(){this.engine.controls.pause();},stop:function(){this.engine.controls.stop();},close:function(){this.playPressed=false
this._ignorePlayState=null
this._loaded=false
this._lastBuff=null
this.listenPlayState(null)
this.stop()
this.setFile("")},isInteractive:function(){return typeof(this.engine.playState)=="number"},isLoaded:function(){if(this.getFile()&&this._loaded&&this.engine.openState==13){if(!this.getAutoStart()&&this.getTime()<=0)this.setTime(0)
return true}
return false},_stateMap:{1:"stop",2:"pause",3:"play"},getState:function(){return((this.getAutoStart()||this._loaded)&&this._stateMap[this.engine.playState])||"unknown";},getTime:function(){return this.engine.controls.currentPosition;},setTime:function(time){this.engine.controls.currentPosition=time;},getBufferedTime:function(){if(this.getState()!="unknown"){var iBuff=Math.max(-1,this.getDuration()*this.engine.network.downloadProgress/100)
if(iBuff!=0)this._lastBuff=iBuff}
return this._lastBuff||-1},isSeekable:function(){return this.getDuration()>-1;},getFile:function(){return this.engine.URL;},setFile:function(fileName){this._loaded=true
if(location.protocol=="file:")fileName=decodeURIComponent(fileName)
this.engine.URL=fileName
if(fileName){this._loaded=false
this.removeListener("OpenStateChange")
var oProv=this
this.addListener("OpenStateChange",function(){if(!this||!this.error)return
if(this.openState==13){var fHnd=arguments.callee
if(oProv.getTime()>0){if(!oProv.playPressed&&!oProv.getAutoStart()){oProv.pause()
oProv.setTime(0)}
oProv.setVolume(iVol)
oProv._loaded=true
clearInterval(fHnd._loadWaiter)
fHnd._loadWaiter=null
this.style.visibility=""
if(this.playStateHandler)this.playStateHandler("",oProv.getState())}else if(!arguments.callee._loadWaiter){var oEng=this
fHnd._loadWaiter=setInterval(function(){fHnd.call(oEng);},1)}
oProv.removeListener("OpenStateChange")}})
var iVol=this.getVolume()
this.setVolume(0)
this._ignorePlayState=true
this.engine.style.visibility="hidden"
this.engine.controls.play()}},getVolume:function(){return this.engine.settings.volume;},setVolume:function(pct){this.engine.settings.volume=pct;},getWidth:function(){return this.engine.currentMedia.imageSourceWidth;},getHeight:function(){return this.engine.currentMedia.imageSourceHeight;},getDuration:function(){return this.getFile()&&this.engine.currentMedia.duration?this.engine.currentMedia.duration:-1;},getAutoStart:function(){return this.engine.settings.autoStart;},setAutoStart:function(autoStart){this.engine.settings.autoStart=autoStart;},getContextMenu:function(){return this.engine.enableContextMenu;},setContextMenu:function(ctxMenu){this.engine.enableContextMenu=ctxMenu;},alwaysWindowed:function(){return!bIsIE;},needsVideoDelay:function(){return!bIsIE;},addListener:function(name,handler){if(!("handle"+name in this.engine)){if(bIsIE){var id=this.engine.uniqueID,oScr=oDoc.createElement("script")
oScr.id="listener"+id
oScr.defer=true
oScr.htmlFor=id
oScr.event=name
oScr.text="try { "+id+"['handle"+name+"'](); } catch (e) {}"
element.appendChild(oScr)}else{var oPrv=this,oEng=this.engine,sEvent="OnDS"+name+"Evt",fPrevHandler=window[sEvent]
window[sEvent]=function(){if(oEng["handle"+name])oEng["handle"+name]()
if(fPrevHandler)fPrevHandler()}}}
this.engine["handle"+name]=handler},removeListener:function(name){if(this.engine["handle"+name]){this.engine["handle"+name]=null
if(bIsIE){var id="listener"+this.engine.uniqueID
if(window[id])window[id].outerHTML=""
try{delete this.engine["handle"+name];}catch(e){}}}},listenPlayState:function(handler){if(!("playStateHandler"in this.engine)){var oPrv=this
this.addListener("PlayStateChange",function(){if(this.playStateHandler&&!oPrv._ignorePlayState)this.playStateHandler("",oPrv.getState())
else oPrv._ignorePlayState=null})}
this.engine.playStateHandler=handler}},HTML5:{name:"HTML5",installed:function(){return true},create:function(){var sTag=Providers.audio?"audio":"video"
return'<'+sTag+' preload="auto"></'+sTag+'>'},play:function(){this._played=true
this.engine.play()
this._manualStall()},pause:function(){this._played=false;this.engine.pause();},stop:function(){this._played=false;this.engine.pause();},close:function(){this.stop()
this.setFile("")},isInteractive:function(){return true;},isLoaded:function(){if(this.getFile()&&(!this.engine.error||!this.engine.error.code)&&(window.bMobile||this.engine.readyState>=1))return true
return false},_state:"unknown",getState:function(){return this._state;},_fireState:function(state){this._state=state
if(this._handler)this._handler("",state)},getTime:function(){return this.engine.currentTime;},setTime:function(time){this.engine.currentTime=time;},getBufferedTime:function(){var iMax=0,oBf=this.engine.buffered
for(var i=0;i<oBf.length;i++)if(oBf.end(i)>iMax)iMax=oBf.end(i)
return iMax},isSeekable:function(){return this.engine.seekable&&this.engine.seekable.length>1;},getFile:function(){return this._fileName;},setFile:function(fileName){this._fileName=fileName
this._state="unknown"
this._played=fileName&&this._autoplay
this.engine.src=fileName
this._manualStall()},getVolume:function(){return Math.round(this.engine.volume*100);},setVolume:function(pct){this.engine.volume=pct/100},getWidth:function(){return this.engine.videoWidth;},getHeight:function(){return this.engine.videoHeight;},getDuration:function(){return this.getFile()&&this.engine.duration>0&&this.engine.duration<Infinity?this.engine.duration:-1;},getAutoStart:function(){return this._autoplay;},setAutoStart:function(autoStart){this.engine.autoplay=this._autoplay=autoStart;},getContextMenu:function(){return this._ctxOn;},setContextMenu:function(ctxMenu){var oPrv=this
if(!"_ctxOn"in this)this.engine.attachEvent("oncontextmenu",function(e){return oPrv._handleContext(e);})
this._ctxOn=ctxMenu},_handleContext:function(e){e.cancelBubble=true
e.returnValue=this._ctxOn
return this._ctxOn},_manualStall:function(){if(bMobile){clearTimeout(this._staller)
if(this._played&&this._state=="unknown"&&!this._ignoreSuspend){oPrv=this
this._staller=setTimeout(function(){if(oPrv._state=="unknown")oPrv._fireState("suspend");},3500)}}},listenPlayState:function(handler){if(this._handler)return this._handler=handler
var oPrv=this
this._handler=handler
this.engine.attachEvent("onplay",function(){oPrv._fireState("play");oPrv._ignoreSuspend=true;})
this.engine.attachEvent("onpause",function(){oPrv._fireState("pause");})
this.engine.attachEvent("onended",function(){oPrv._fireState("stop");})
this.engine.attachEvent("onerror",function(){oPrv._fireState("stop");})
this.engine.attachEvent("onstalled",function(){if(oPrv._played&&!oPrv._ignoreSuspend)oPrv._fireState("suspend");})
this.engine.attachEvent("onsuspend",function(){if(oPrv._played&&!oPrv._ignoreSuspend)oPrv._fireState("suspend");})
this.engine.attachEvent("onloadeddata",function(){if(oPrv._state=="suspend")oPrv._fireState(oPrv._played?"play":"init");})}},SWF:{name:"SWF",installed:function(){if(bIsIE){try{new ActiveXObject("ShockwaveFlash.ShockwaveFlash");return true;}catch(e){return false;}}else{return!!navigator.plugins["Shockwave Flash"]}},create:function(){var sId="SWFMPlayer"+new Date().getTime()+Math.round(Math.random()*Math.pow(10,10)),sAPI="Runtime/mplayer.swf",sAttr=bIsIE?'classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"':'data="'+sAPI+'"'
return'<object tabindex="-1" '+sAttr+' type="application/x-shockwave-flash" comptype="flash" name="'+sId+'" id="'+sId+'"><param name="movie" value="'+sAPI+'"><param name="quality" value="best"><param name="allowFullScreen" value="true"><param name="allowScriptAccess" value="always"><param name="wmode" value="'+(Providers.transparent?"transparent":(Providers.windowed&&!bIsFF?"window":"opaque"))+'"></object>'},play:function(){this._cmd("play")
this._fireState("play")},pause:function(){this._cmd("pause")
this._fireState("pause")},stop:function(bIncoming){if(!bIncoming)this._cmd("stop")
this._fireState("stop")
this._state="init"},close:function(){this.inited=false
this.isSeeking=false
this.pause()
this.listenPlayState(null)
this.setFile("")
this._state="init"},isInteractive:function(){try{return this.engine.GetVariable("duration")!=undefined;}catch(e){return false;}},isLoaded:function(){var sFile=this.getFile(),bMP3=/\.mp3$/i.test(sFile)
if(bMP3)try{return this.getFile()&&this.engine.GetVariable("progress")>0;}catch(e){return false;}
else try{return this.getFile()&&this.getWidth()>0;}catch(e){return false;}},_state:"init",getState:function(){return this._state;},_fireState:function(state){var oldState=this._state
this._state=state
if(this.engine.playStateHandler)this.engine.playStateHandler(oldState,state)},getTime:function(){return+this.engine.GetVariable("time");},setTime:function(time){this.isSeeking=true
this._cmd("seek","time",time)},getBufferedTime:function(){return Math.max(-1,this.getDuration()*this.engine.GetVariable("progress"));},isSeekable:function(){return this.getDuration()>-1;},getFile:function(){return this._fileName||"";},setFile:function(fileName){if(fileName&&fileName.indexOf(":/")==-1&&fileName.charAt(0)!="/")fileName=location.href.replace(/[^\/\\]*$/,fileName)
this._cmd("updateProp","url",fileName)
this._fileName=fileName},getVolume:function(){return this._volume||75;},setVolume:function(pct){this._cmd("updateProp","volume",pct)
this._volume=pct},getWidth:function(){return+this.engine.GetVariable("flvWidth");},getHeight:function(){return+this.engine.GetVariable("flvHeight");},getDuration:function(){return+this.engine.GetVariable("duration")||-1;},getAutoStart:function(){return this._autoStart||false;},setAutoStart:function(autoStart){this._cmd("updateProp","autoPlay",autoStart)
this._autoStart=autoStart
if(this._autoStart)this._fireState("play")},getContextMenu:function(){return this.engine.Menu;},setContextMenu:function(ctxMenu){this.engine.Menu=ctxMenu;},listenPlayState:function(handler){this.engine.playStateHandler=handler},init:function(){var provider=this,oXML=getXMLDocument()
this.engine.processCommand=function(sXML){oXML.loadXML(sXML)
if(oXML.firstChild){var sCommand=oXML.firstChild.getAttribute("name").replace(/^[a-z]/,function(match){return match.toUpperCase();})
if(provider["_fire"+sCommand])provider["_fire"+sCommand](oXML)}}
var sId=this.engine.id
if(bIsIE){var oScr=oDoc.createElement("script")
oScr.defer=true
oScr.htmlFor=sId
oScr.event="FSCommand(sCommand,sArg)"
oScr.text="try { "+sId+".processCommand(sCommand,sArg); } catch (e) {}"
element.appendChild(oScr)}else{window[sId+"_DoFSCommand"]=function(sCommand,sArg){try{provider.engine.processCommand(sCommand,sArg);}catch(e){}}}
this.inited=true},_cmd:function(command,arg,argValue){var sArg=""
if(arg){if(argValue==null)argValue=""
sArg='<'+arg+'>'+argValue+'</'+arg+'>'}
this.engine.SetVariable("gm.In.call",'<command name="'+command+'">'+sArg+'</command>')},_firePause:function(){this.stop(true)},_fireSeek:function(oXML){this.isSeeking=false
this._fireState("play")}},YouTube:{name:"YouTube",play:function(){this.engine.playVideo();},pause:function(){this.engine.pauseVideo();},stop:function(){this.engine.seekTo(0,true);this.engine.stopVideo();},isLoaded:function(){if(this.getFile()&&this.engine.getPlayerState()>-1){if(this._autoStart)this.play()
return true}
return false},getState:function(){return this.engine.getPlayerState()==1?"play":this.engine.getPlayerState()==0?"stop":"pause";},getTime:function(){return Math.max(+this.engine.getCurrentTime(),0);},setTime:function(time){this.engine.seekTo(time,true);},getBufferedTime:function(){return+this.engine.getVideoLoadedFraction()<=0?-1:this.getDuration()*this.engine.getVideoLoadedFraction();},isSeekable:function(){return true;},getFile:function(){return this._fileName;},setFile:function(fileName){this._fileName=fileName;if(fileName)this.engine.cueVideoByUrl("http://www.youtube.com/v/"+fileName);},getVolume:function(){return this.engine.getVolume();},setVolume:function(pct){this.engine.setVolume(pct);},getWidth:function(){return 320;},getHeight:function(){return 240;},getDuration:function(){return Math.max(0,+this.engine.getDuration())||-1;},getAutoStart:function(){return this._autoStart;},setAutoStart:function(autoStart){this._autoStart=autoStart;},getContextMenu:function(){return this.engine.Menu;},setContextMenu:function(ctxMenu){this.engine.Menu=ctxMenu;},engines:{Flash:{installed:function(){return this.SWF.installed()},create:function(){if(!window.onYouTubePlayerReady)window.onYouTubePlayerReady=function(playerId){document.getElementById(playerId).playerReady=true;}
var sId="YTMPlayer"+new Date().getTime()+Math.round(Math.random()*Math.pow(10,10)),sAPI="https://www.youtube.com/apiplayer?version=3&enablejsapi=1&playerapiid="+sId,sAttr=bIsIE?'classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"':'data="'+sAPI+'"'
return'<object tabindex="-1" type="application/x-shockwave-flash" '+sAttr+' comptype="flash" id="'+sId+'"><param name="movie" value="'+sAPI+'"><param name="quality" value="best"><param name="allowFullScreen" value="true"><param name="allowScriptAccess" value="always"><param name="wmode" value="'+(Providers.windowed&&!bIsFF?"window":"opaque")+'"></object>'},close:function(){this.listenPlayState(null)
this.stop()
this.setFile("")},isInteractive:function(){return!!this.engine.playerReady;},listenPlayState:function(handler){if(handler){var oPrv=this
window["ytHandler"+this.engine.id]=function(){handler("",oPrv.getState())}
this.engine.addEventListener("onStateChange","ytHandler"+this.engine.id)}else if(this.engine&&this.engine.id){delete window["ytHandler"+this.engine.id]}}},Frame:{installed:function(){return window.postMessage&&(!document.documentMode||location.protocol!="file:")},create:function(){var sId="YTFMPlayer"+new Date().getTime()+Math.round(Math.random()*Math.pow(10,10))
return'<div id="'+sId+'"></div>'},close:function(){this.engine.stopVideo()
this.engine.clearVideo()
this.engine.destroy()},isInteractive:function(){if(this.engine.playerReady){return true}else{if(window.hasYouTubeAPI){if(!this.engine.__queued){var oPrv=this
this.engine=new YT.Player(this.engine.id,{playerVars:{controls:0,showinfo:0,modestbranding:1,rel:0,iv_load_policy:3,wmode:Providers.windowed&&!bIsFF?"window":"opaque"},events:{onReady:function(){oPrv.engine.playerReady=true;},onStateChange:function(){if(oPrv._stateChange)oPrv._stateChange();},}})
this.engine.__queued=true}}else{if(!window.onYouTubeIframeAPIReady){window.onYouTubeIframeAPIReady=function(){window.hasYouTubeAPI=true;}
var oScr=document.createElement('script')
oScr.src="https://www.youtube.com/iframe_api"
oScr.defer=true
document.body.appendChild(oScr)}}
return false}},listenPlayState:function(handler){if(handler){var oPrv=this
this._stateChange=function(){handler("",oPrv.getState())}}else{this._stateChange=null}}}}}}
Providers.init();