function getImplementations(){var bIsTouch="ontouchstart"in window
if(!window.addEventListener)
return
var oImplement={DOM:{Events:{attachEvent:_(["Window","HTMLDocument","HTMLElement","SVGElement"],function(sEvent,fHandler){this.addEventListener(sEvent.slice(2),fHandler,false)}),detachEvent:_(["Window","HTMLDocument","HTMLElement","SVGElement"],function(sEvent,fHandler){this.removeEventListener(sEvent.slice(2),fHandler,false)}),fireEvent:_(["Window","HTMLDocument","HTMLElement","SVGElement"],function(sEvent,oEvent){if(!oEvent){var oDoc=this instanceof Document?this:this.ownerDocument
oEvent=oDoc.createEvent("HTMLEvents")}
oEvent.initEvent(sEvent.slice(2),true,true)
return this.dispatchEvent(oEvent)}),fromElement:_(["MouseEvent"],{get:function(){return this.relatedTarget}}),offsetX:_(["MouseEvent"],{get:function(){return this.layerX;}}),offsetY:_(["MouseEvent"],{get:function(){return this.layerY;}}),returnValue:_(["Event"],{set:function(bVal){if(!bVal)this.preventDefault()},get:function(){return this.defaultPrevented?false:undefined}}),srcElement:_(["Event"],{get:function(){return this.target;}}),toElement:_(["MouseEvent"],{get:function(){return this.relatedTarget}})},Window:{ActiveXObject:_(["Window"],function(sID){if(sID.indexOf("XMLDOM")>-1||sID.indexOf("DOMDocument")>-1){return createXMLDocument()}else if(sID.indexOf("XMLHTTP")>-1){return new XMLHttpRequest()}
throw new Error("Unknown object ID in call to ActiveXObject")}),createEventObject:_(["Window"],function(){return this.document.createEventObject()})},Document:{createElement:_(["HTMLDocument"],function(sTag){if(sTag&&sTag.toLowerCase()=="xml"){return createXMLDocument(true)}else{return this.__createElement(sTag)}},ImpSpec.ATTRIB_OVERWRITE),createEventObject:_(["HTMLDocument"],function(){return this.createEvent("HTMLEvents")}),elementFromPoint:_(["HTMLDocument"],function(iX,iY){return this.body._opaqueAtPoint(iX,iY)},ImpSpec.ATTRIB_OVERWRITE)},Node:{contains:_(["HTMLElement","SVGElement"],function(oEl){while(oEl){if(oEl.nodeType!=1)return false
if(oEl==this)return true
oEl=oEl.parentNode}
return false}),disabled:_(["HTMLElement","SVGElement"],{get:function(){return this.hasAttribute("disabled");},set:function(bDisabled){if(bDisabled){this.setAttribute("disabled",1)
return true}else{this.removeAttribute("disabled")
return false}}}),document:_(["HTMLElement","SVGElement"],{get:function(){return this.ownerDocument;}}),innerText:_(["HTMLElement","SVGElement"],{get:function(){return this.textContent;},set:function(sText){return this.textContent=sText;}}),outerHTML:_(["HTMLElement","SVGElement"],{get:function(){var oEl=this.ownerDocument.createElement("div")
oEl.appendChild(this.cloneNode(true))
return oEl.innerHTML},set:function(sHTML){var oTemp=this.ownerDocument.createElement("div"),oParent=this.parentNode,aChild
oTemp.innerHTML=sHTML
aChild=oTemp.childNodes
for(var iCh=0;iCh<aChild.length;iCh++){oParent.insertBefore(aChild[iCh],this)}
oParent.removeChild(this)
return sHTML}}),parentElement:_(["HTMLElement","SVGElement"],{get:function(){return this.parentNode&&this.parentNode.nodeType==1?this.parentNode:null;}}),releaseCapture:_(["HTMLElement","SVGElement"],function(){MouseCapture.release()}),removeNode:_(["Node"],function(bRemoveChildren){var oParent=this.parentNode
if(!oParent)return this
if(!bRemoveChildren){var aChild=this.childNodes
for(var iCh=0;iCh<aChild.length;iCh++){oParent.insertBefore(aChild[iCh],this)}}
return oParent.removeChild(this)}),replaceNode:_(["Node"],function(oNewNode){return this.parentNode.replaceChild(oNewNode,this)}),setCapture:_(["HTMLElement","SVGElement"],function(){MouseCapture.set(this)}),swapNode:_(["HTMLElement","SVGElement"],function(oOther){var oLoc=this.nextSibling,oParent=this.parentNode
oOther.parentNode.insertBefore(this,oOther)
oParent.insertBefore(oOther,oLoc)
return this}),uniqueID:_(["HTMLDocument","HTMLElement","SVGElement"],{get:function(){return this.__UID?this.__UID:this.__UID="emu__id"+(arguments.callee.counter=(arguments.callee.counter||0)+1);}}),_elementFromPoint:_(["HTMLElement","SVGElement"],function(iX,iY,bOpaque){var sZoom=this.style._zoom||this.style.zoom
if(iX==undefined||iY==undefined)return null
if(sZoom&&!isNaN(sZoom)){iX/=sZoom
iY/=sZoom}
if(this.currentStyle.display=="none")return null
if(this.childNodes.length>0){var sOverflowX=this.currentStyle.overflowX,sOverflowY=this.currentStyle.overflowY,aChild=[]
if(sOverflowX!="visible"||sOverflowY!="visible"){var oPos=this._getAtomicPosition()
if(sOverflowX!="visible"&&iX>=oPos.x+this.offsetWidth)return null
if(sOverflowY!="visible"&&iY>=oPos.y+this.offsetHeight)return null}
for(var i=this.childNodes.length;i--;){var oEl=this.childNodes[i]
if(oEl.nodeType==1){var bStatic=oEl.currentStyle.position=="static",sZ=oEl.currentStyle.getPropertyValue("z-index")
aChild.push({el:oEl,srcIdx:i,zIdx:sZ=="auto"||bStatic?0:+sZ})}}
aChild.sort(zIndexSorter)
for(var i=0;i<aChild.length;i++){var oEl=aChild[i].el,oRes=oEl._elementFromPoint(iX,iY,bOpaque)
if(oRes)return oRes}}
if(!this.getAttribute("forcetransparent")&&getComputedStyle(this).visibility=="visible"){var oPos=this._getAtomicPosition()
if(iX>=oPos.x&&iY>=oPos.y&&iX<oPos.x+this.offsetWidth&&iY<oPos.y+this.offsetHeight){if(bOpaque&&this._isOpaque&&!this._isOpaque(true)){var iBorderLeft=this.currentStyle.borderLeftStyle!="none"?parseInt(this.currentStyle.borderLeftWidth):0,iBorderTop=this.currentStyle.borderTopStyle!="none"?parseInt(this.currentStyle.borderTopWidth):0
if(iX>=oPos.x+iBorderLeft&&iY>=oPos.y+iBorderTop&&iX<oPos.x+iBorderLeft+this.clientWidth&&iY<oPos.y+iBorderTop+this.clientHeight){return null}}
return this}}
return null}),_getAtomicPosition:_(["HTMLElement","SVGElement"],function(){var oBounds=this.getBoundingClientRect(),iTotalZoom=1
for(var oEl=this;oEl;oEl=oEl.offsetParent){iTotalZoom*=+(oEl.style._zoom||oEl.style.zoom)||1}
return{x:oBounds.left/iTotalZoom,y:oBounds.top/iTotalZoom}}),_isTransparent:_(["HTMLElement","SVGElement"],function(bFullOpaque){return this.getAttribute("forcetransparent")||this.tagName=="BODY"}),_isOpaque:_(["HTMLElement","SVGElement"],function(bFullOpaque){return!this._isTransparent()&&(this.getAttribute("forceopaque")||this.tagName=="IMG"||this.tagName=="CANVAS"||this.tagName=="EMBED"||this.tagName=="OBJECT"||this.tagName=="INPUT"||this.tagName=="TEXTAREA"||!bFullOpaque&&(this.currentStyle.position=="static"&&this.tagName!="TD"||this.currentStyle.backgroundColor!="transparent"&&this.currentStyle.backgroundColor!="rgba(0, 0, 0, 0)"||(this.currentStyle.backgroundImage&&!/^(?:none$|url\([\'\"]?(?:javascript:|.*#compnone))/.test(this.currentStyle.backgroundImage))))}),_opaqueAtPoint:_(["HTMLElement","SVGElement"],function(iX,iY){if(this._isOpaque&&!this._isOpaque(false,iX,iY)){var oOpaqueRes=this.ownerDocument.body._elementFromPoint(iX,iY,true)
if(oOpaqueRes&&oOpaqueRes!=this.ownerDocument.body)return oOpaqueRes
if(this._isTransparent())return this.ownerDocument.body._elementFromPoint(iX,iY,false)}
return this})}},CSS:{Rules:{rules:_(["CSSStyleSheet"],{get:function(){return this.cssRules;}}),addRule:_(["CSSStyleSheet"],function(sSelector,sStyle,iIndex){if(iIndex===undefined||iIndex<0)iIndex=this.cssRules.length
this.insertRule(sSelector+" { "+sStyle+" }",iIndex)
return-1}),removeRule:_(["CSSStyleSheet"],function(iIndex){this.deleteRule(iIndex||0)})},currentStyle:_(["HTMLElement","SVGElement"],{get:function(){return getComputedStyle(this,null);}}),removeAttribute:_(["CSSStyleDeclaration"],function(sName){this[sName]=""
this.removeProperty(sName)
return true}),styleSheet:_(["HTMLLinkElement","HTMLStyleElement"],{get:function(){try{if(this.sheet.rules)
return this.sheet
else
throw new Error("not ready")}catch(e){return null}}}),_zoom:_(["CSSStyleDeclaration"],{set:function(sZoom){var sTransformProp=this._transformProp
if(sTransformProp){if(sZoom!=1){if(!this[sTransformProp+"Origin"])this[sTransformProp+"Origin"]="left top"
this[sTransformProp]="scale("+sZoom+")"}else if(this[sTransformProp]){this[sTransformProp]=""}}
return sZoom},get:function(){var sTransformProp=this._transformProp
if(sTransformProp){var sTransform=this[sTransformProp]
if(sTransform&&/scale\(([^)]+)\)/.test(sTransform))return RegExp.$1
return""}
return undefined}}),_transformProp:_(["CSSStyleDeclaration"],{get:function(){var aPrefixes=["transform","MozTransform","WebkitTransform","msTransform","OTransform"]
for(var iP=0;iP<aPrefixes.length;iP++){if(this[aPrefixes[iP]]!==undefined)return aPrefixes[iP]}}})},XML:{Document:{cloneNode:_(["XMLDocument"],function(bDeep){var oNode=this.__cloneNode(bDeep)
if(!oNode){oNode=createXMLDocument()
oNode.appendChild(this.documentElement.cloneNode(true))}
if(this.__props){if(!oNode.__props)oNode.__props={}
for(var sKey in this.__props){oNode.__props[sKey]=this.__props[sKey]}}
return oNode},ImpSpec.ATTRIB_OVERWRITE),createNode:_(["XMLDocument"],function(iType,sName,sURI){switch(iType){case this.ELEMENT_NODE:return sURI?this.createElementNS(sURI,sName):this.createElement(sName)
case this.ATTRIBUTE_NODE:return sURI?this.createAttributeNS(sURI,sName):this.createAttribute(sName)
case this.TEXT_NODE:return this.createTextNode("")
case this.CDATA_SECTION_NODE:return this.createCDATASection("")
case this.ENTITY_REFERENCE_NODE:return this.createEntityReference(sName)
case this.PROCESSING_INSTRUCTION_NODE:return this.createProcessingInstruction(sName,"")
case this.COMMENT_NODE:return this.createComment("")
case this.DOCUMENT_FRAGMENT_NODE:return this.createDocumentFragment()}}),createNSResolver:_(["XMLDocument"],function(oNode){var oNSResolver=this.__createNSResolver&&this.__createNSResolver(oNode),sNS=this.getProperty("SelectionNamespaces")
if(sNS){var rxNS=/xmlns:([^=]+)=([\"\'])(.*?)\2/g,aMatch,oNS={}
while(aMatch=rxNS.exec(sNS)){var sPrefix=aMatch[1],sURI=aMatch[3]
if(sPrefix&&sURI){oNS[sPrefix]=sURI}}
return function(sPrefix){return oNS[sPrefix]||(oNSResolver?oNSResolver.lookupNamespaceURI(sPrefix):null)}}
if(oNSResolver)
return oNSResolver
return function(){return null;}},ImpSpec.ATTRIB_OVERWRITE),getAttribute:_(["XMLDocument"],function(attrName){return this[attrName]}),getProperty:_(["XMLDocument"],function(sName,sVal){return this.__props?this.__props[sName]:undefined}),load:_(["XMLDocument"],function(sURL){this.loadXML("")
var oXML=this,oXH=new XMLHttpRequest()
oXH.open("GET",sURL,true)
oXH.onreadystatechange=function(){if(oXH.readyState==4){var oResXML=oXH.responseXML
if(oResXML){for(var iNode=0;iNode<oResXML.childNodes.length;iNode++)oXML.appendChild(oXML.importNode(oResXML.childNodes[iNode],true))}
oXML.hasLoaded()}}
oXH.send(null)},ImpSpec.ATTRIB_OVERWRITE),loadXML:_(["XMLDocument"],function(sXML){try{while(this.hasChildNodes())this.removeChild(this.lastChild)
if(sXML){var oXMLParser=new DOMParser().parseFromString(sXML,"text/xml")
if(oXMLParser.documentElement&&oXMLParser.documentElement.tagName=="parsererror")throw new Error("Parse error")
for(var iNode=0;iNode<oXMLParser.childNodes.length;iNode++)this.appendChild(this.importNode(oXMLParser.childNodes[iNode],true))}
return true}catch(oErr){return false}}),selectNodes:_(["XMLDocument"],function(sXPath,xNode){if(!xNode)xNode=this
var oNSResolver=this.createNSResolver(this.documentElement),aItems=this.evaluate(sXPath,xNode,oNSResolver,XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,null),aResult=[]
for(var i=0;i<aItems.snapshotLength;i++)aResult[i]=aItems.snapshotItem(i)
return aResult}),selectSingleNode:_(["XMLDocument"],function(sXPath,xNode){if(!xNode)xNode=this
var xItems=this.selectNodes(sXPath,xNode)
if(xItems.length>0)return xItems[0]
return null}),setAttribute:_(["XMLDocument"],function(attrName,attrValue){return this[attrName]=attrValue}),setProperty:_(["XMLDocument"],function(sName,sVal){if(!this.__props)this.__props={}
this.__props[sName]=sVal}),readyState:_(["XMLDocument"],{get:function(){return this.___readyState;},set:function(sState){return this.___readyState=sState;}}),responseXML:_(["XMLHttpRequest"],{get:function(){return this.__responseXML.get.call(this)||createXMLDocument()}},ImpSpec.ATTRIB_OVERWRITE)},Node:{selectNodes:_(["Element"],function(sXPath){return this.ownerDocument.selectNodes(sXPath,this)}),selectSingleNode:_(["Element"],function(sXPath){return this.ownerDocument.selectSingleNode(sXPath,this)}),text:_(["Node"],{get:function(){return this.textContent;},set:function(sVal){return this.textContent=sVal;}}),xml:_(["Node"],{get:function(){return new XMLSerializer().serializeToString(this);}})}}}
var EventUtils={rerouteEvent:function(){var oSrcEvent=arguments[1],oTarget=Array.prototype.shift.call(arguments),oEvent=EventUtils.duplicateEvent.apply(this,arguments)
oEvent.rerouted=true
if(!oTarget.dispatchEvent(oEvent)){oSrcEvent.preventDefault()}else if(oEvent.type==(bIsTouch?"click":"mousedown")&&(oTarget.tagName=="INPUT"||oTarget.tagName=="TEXTAREA")){try{oTarget.focus();}catch(e){}
oSrcEvent.preventDefault()}},duplicateEvent:function(oE,sType,bBubbles,oRelated,iX,iY){var oEvent=document.createEvent("MouseEvents")
oEvent.initMouseEvent(sType!==undefined?sType:oE.type,bBubbles!==undefined?bBubbles:oE.bubbles,oE.cancelable,oE.view,oE.detail,oE.screenX,oE.screenY,iX!==undefined?iX:oE.clientX,iY!==undefined?iY:oE.clientY,oE.ctrlKey,oE.altKey,oE.shiftKey,oE.metaKey,oE.button,oRelated!==undefined?oRelated:oE.relatedTarget)
oEvent.fromTouch=oE.fromTouch
return oEvent}}
var MouseThrough={events:["mousedown","mouseup","click","dblclick"],lastEvent:{},init:function(){var aEvents=MouseThrough.events,fHandler=MouseThrough.handler,oSS=document.createElement("style")
oSS.type="text/css"
document.getElementsByTagName("head")[0].appendChild(oSS)
MouseThrough.cursorRule=oSS.sheet.cssRules[oSS.sheet.insertRule("* {}",0)]
document.addEventListener("mousemove",MouseThrough.move,true)
document.addEventListener("mouseover",MouseThrough.over,true)
document.addEventListener("mouseout",MouseThrough.out,true)
for(var i=0;i<aEvents.length;i++){document.addEventListener(aEvents[i],fHandler,true)}},move:function(oE){var oCurTarget=MouseThrough.targetEl,oTarget
if(oE.rerouted)return
if(oE.target._opaqueAtPoint&&document.body){oTarget=oE.target._opaqueAtPoint(oE.clientX,oE.clientY)
if(oTarget&&oTarget.contains(oE.target))oTarget=null}
if(oCurTarget!=oTarget&&oCurTarget!=oE.target){EventUtils.rerouteEvent(oCurTarget||oE.target,oE,"mouseout",true,oTarget||oE.target)}
if(oCurTarget!=oTarget)
MouseThrough.cursorRule.style.removeProperty("cursor")
if(oTarget){EventUtils.rerouteEvent(oTarget,oE)
oE.stopPropagation()}
if(oCurTarget!=oTarget&&oCurTarget!=oE.target){EventUtils.rerouteEvent(oTarget||oE.target,oE,"mouseover",true,oCurTarget||oE.target)}
if(oTarget&&oE.target.currentStyle.cursor!=oTarget.currentStyle.cursor)
MouseThrough.cursorRule.style.setProperty("cursor",oTarget.currentStyle.cursor,"important")
MouseThrough.targetEl=oTarget},over:function(oE){if(oE.rerouted)return
if(MouseThrough.targetEl){oE.stopPropagation()
return}
if(oE.target._opaqueAtPoint&&document.body){var oTarget=oE.target._opaqueAtPoint(oE.clientX,oE.clientY)
if(oTarget&&!oTarget.contains(oE.target)){oE.stopPropagation()
MouseThrough.targetEl=oE.relatedTarget}}},out:function(oE){if(oE.rerouted)return
if(MouseThrough.targetEl){oE.stopPropagation()
return}
var bHasRelated=false
try{bHasRelated=oE.relatedTarget&&oE.relatedTarget._opaqueAtPoint}catch(oErr){bHasRelated=false;}
if(bHasRelated&&document.body){var oTarget=oE.relatedTarget._opaqueAtPoint(oE.clientX,oE.clientY)
if(oTarget&&!oTarget.contains(oE.relatedTarget)){oE.stopPropagation()
MouseThrough.targetEl=oE.target}}},handler:function(oE){MouseThrough.lastEvent[oE.type]=oE.target
if(oE.rerouted)return
if(oE.target._opaqueAtPoint&&document.body){var oTarget=oE.target._opaqueAtPoint(oE.clientX,oE.clientY)
if(oTarget&&!oTarget.contains(oE.target)){if(oE.type!="click"||MouseThrough.lastEvent["mousedown"]==oTarget&&MouseThrough.lastEvent["mouseup"]==oTarget)
EventUtils.rerouteEvent(oTarget,oE)
oE.stopPropagation()}}}}
var MouseCapture={element:null,events:["mousedown","mouseup","mousemove","click","dblclick","mouseover","mouseout"],listener:function(oE){if(!window._mouseCaptureElement)return
oE.stopPropagation()
window.removeEventListener(oE.type,MouseCapture.listener,true)
EventUtils.rerouteEvent(window._mouseCaptureElement,oE,oE.type,false)
window.addEventListener(oE.type,MouseCapture.listener,true)},set:function(oEl){MouseCapture.release()
window._mouseCaptureElement=oEl
for(var iE=0;iE<MouseCapture.events.length;iE++){window.addEventListener(MouseCapture.events[iE],MouseCapture.listener,true)}},release:function(){for(var iE=0;iE<MouseCapture.events.length;iE++){window.removeEventListener(MouseCapture.events[iE],MouseCapture.listener,true)}
window._mouseCaptureElement=null},DisabledSink:{init:function(){for(var iE=0;iE<MouseCapture.events.length;iE++){document.addEventListener(MouseCapture.events[iE],MouseCapture.DisabledSink.listener,true)}},listener:function(oE){var oRetarget
if(window._mouseCaptureElement)return
for(var oCur=oE.target;oCur&&oCur.nodeType==1;oCur=oCur.parentNode)
if(oCur.hasAttribute("disabled"))oRetarget=oCur.parentNode
if(oRetarget&&oRetarget.nodeType==1){oE.stopPropagation()
var oDupeE=EventUtils.duplicateEvent(oE)
oDupeE.rerouted=true
oRetarget.dispatchEvent(oDupeE)}}}}
var TouchEvents={events:{"touchstart":"mousedown","touchmove":"mousemove","touchend":"mouseup","touchcancel":"mouseup"},init:function(){var oBlockHash={}
for(var sEvent in TouchEvents.events){var sTargetEvent=TouchEvents.events[sEvent]
document.body.addEventListener(sEvent,TouchEvents.handler,false)
if(!oBlockHash[sTargetEvent]){oBlockHash[sTargetEvent]=true
window.addEventListener(sTargetEvent,TouchEvents.block,true)}
if(!document.documentMode)
window.addEventListener("click",TouchEvents.click,true)}},handler:function(oE){if(!oE.changedTouches||!oE.changedTouches[0])return
var iX=document.documentMode?oE.changedTouches[0].clientX:oE.changedTouches[0].pageX-window.pageXOffset,iY=document.documentMode?oE.changedTouches[0].clientY:oE.changedTouches[0].pageY-window.pageYOffset,oEvent=EventUtils.duplicateEvent(oE,TouchEvents.events[oE.type],undefined,undefined,iX,iY)
TouchEvents.touchTarget=oE.target
TouchEvents.touchX=iX
TouchEvents.touchY=iY
oEvent.fromTouch=true
if(!oE.target.dispatchEvent(oEvent)){oE.preventDefault()}},block:function(oE){if(!oE.fromTouch)
oE.stopPropagation()},click:function(oSrcEvent){if(!oSrcEvent.touchFixed&&!oSrcEvent.rerouted&&TouchEvents.touchTarget){var oEmuEvent=EventUtils.duplicateEvent(oSrcEvent,undefined,undefined,undefined,TouchEvents.touchX,TouchEvents.touchY)
oEmuEvent.touchFixed=true
oSrcEvent.stopPropagation()
if(!TouchEvents.touchTarget.dispatchEvent(oEmuEvent)){oSrcEvent.preventDefault()}
TouchEvents.touchTarget=null}}}
function zIndexSorter(oA,oB){var iZA=oA.zIdx,iZB=oB.zIdx,iSrcA=oA.srcIdx,iSrcB=oB.srcIdx
if(iZB>iZA)return 1
if(iZA>iZB)return-1
if(iSrcB>iSrcA)return 1
if(iSrcA>iSrcB)return-1
return 0}
function createXMLDocument(bHTML){var oXML=document.implementation.createDocument("","",null)
if(!("readyState"in oXML)){if(bHTML){oXML.readyState="loading"
oXML.hasLoaded=function(){oXML.readyState="interactive"
if(typeof oXML.onreadystatechange=="function")oXML.onreadystatechange()
oXML.readyState="complete"
if(typeof oXML.onreadystatechange=="function")oXML.onreadystatechange()}}else{oXML.readyState=1
oXML.hasLoaded=function(){oXML.readyState=3
if(typeof oXML.onreadystatechange=="function")oXML.onreadystatechange()
oXML.readyState=4
if(typeof oXML.onreadystatechange=="function")oXML.onreadystatechange()}}}
return oXML}
if(typeof XMLDocument!="undefined"&&typeof XPathResult=="undefined"&&window.ActiveXObject===undefined)
document.write('<script type="text/javascript" charset="utf-8" src="Runtime/xpath.js"></script>')
if(bIsTouch)
addEventListener("load",TouchEvents.init)
if(typeof MSEventObj=="undefined"||!document.fireEvent){MouseThrough.init()}else if(document.documentMode<10){oImplement=null}
MouseCapture.DisabledSink.init()
if("currentStyle"in HTMLElement.prototype&&!("currentStyle"in SVGElement.prototype)&&Object.defineProperty){Object.defineProperty(SVGElement.prototype,"currentStyle",{get:function(){return getComputedStyle(this,null);}})}
setTimeout(function(){if(arguments.length>0){function patchMethod(sMethod){var fNative=window[sMethod]
window[sMethod]=function(xFunc,iDelay){if(typeof xFunc=="function"){return fNative.call(window,function(){xFunc();},iDelay)}else{return fNative.apply(window,arguments)}}}
patchMethod("setInterval")
patchMethod("setTimeout")}})
return oImplement}