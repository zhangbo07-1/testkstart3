/*@cc_on @*/
(function(){var getTag,getByTagAll,getByTagOne,getChildByTagAll,getChildByTagOne
var Reader={threshold:3,plainTags:{"BR":1,"IMG":1,"UL":1,"OL":1,"LI":1,"INPUT":1,"TEXTAREA":1,"TABLE":1,"THEAD":1,"TBODY":1,"TFOOT":1,"TR":1,"TH":1,"TD":1,"CAPTION":1,"SUB":1,"SUP":1},skipTags:{"TRANSALPHA":1,"OBJTRANS":1,"DRAGDROP":1,"MATCHING":1,"SORT":1,"MPLAYER":1,"NAVPROGRESSDONE":1,"NAVTIMER":1,"NAVMARKDONE":1,"NAVMARKSTARTED":1,"NAVMARKCURRENT":1,"NARRATEMUTE":1,"CCBUTTON":1},mediaTags:{"EMBED":1,"OBJECT":1},listTags:{"NAVMENU":1,"NAVMENULEVEL":1},ratingTags:{"RATINGON":1,"RATINGOFF":1,"RATINGEDIT":1},superAtomicTags:{"FEEDBACK":1},visibleTags:{"CCBOX":1},navlinkTags:{"NAVBUTTON":1,"NAVMENU":1,"NAVTREEMENU":1},listTypeUnordered:{"none":1,"disc":1,"circle":1,"square":1},prepare:function(){document.write('\
			<style>\
			#oReaderInitial {\
				position:absolute;\
				left:0px;\
				top:-20000em;\
			}\
			#oReaderContainer {\
				position:absolute;\
				left:0px;\
				top:0px;\
				right:0px;\
				bottom:0px;\
				background-color:white;\
				overflow:auto;\
				padding:10px;\
			}\
			.readerActive [aria-hidden=true] {\
				margin-top:-20000em !important;\
			}\
			</style>\
			<div id="oReaderInitial" tabIndex="1">\
				<a id="oReaderRedirect" href="#s" target="_self"></a>\
				<a id="oReaderTarget" name="s"></a>\
				<a id="oReaderActivate" href="#" target="_self" tabIndex="1">Screen reader version</a>\
			</div>\
		')
var oInitial=document.getElementById("oReaderInitial"),oRedirect=document.getElementById("oReaderRedirect"),oActivate=document.getElementById("oReaderActivate"),oParent=oInitial.parentNode,aSib=oParent.children
for(var iS=aSib.length;iS--;)
if(aSib[iS]!=oInitial)
aSib[iS].setAttribute("aria-hidden","true")
oRedirect.click()
try{oInitial.focus();}catch(oErr){}
oInitial.removeChild(oRedirect)
oActivate.onclick=function(){var oDoc=oParent.ownerDocument
oDoc.documentElement.className+=" readerActive"
oActivate.onclick=Reader.viewportOpen
if(!window.bReaderPopup){Reader.viewportOpen()}else{Reader.container=oParent.insertBefore(oDoc.createElement("DIV"),oInitial.nextSibling)
Reader.container.id="oReaderContainer"}
(function(){var oCourseWindow=window.oLaunchFrame?oLaunchFrame.contentWindow||oLaunchFrame:window
if(oCourseWindow)
oCourseWindow.bStatic=true
if(oCourseWindow&&oCourseWindow.document.body&&oCourseWindow.document.body.emuReadyState=="complete"&&oCourseWindow.oConfig)
Reader.enable(oCourseWindow)
else
setTimeout(arguments.callee,50)})()
return false}
if(window.bReaderPopup)
oActivate.onclick()},viewportOpen:function(){if(!Reader.viewport||Reader.viewport.closed){var oViewportWin=open("about:blank","_blank","location=no,menubar=no,status=no,toolbar=no,resizable=yes,scrollbars=yes"),oViewportDoc=oViewportWin.document
oViewportDoc.open()
oViewportDoc.write('<!doctype html><head><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta http-equiv="content-type" content="text/html; charset=utf-8"></head><body></body></html>')
oViewportDoc.close()
oViewportWin.onunload=Reader.pageReset
Reader.container=oViewportDoc.body.appendChild(oViewportDoc.createElement("DIV"))
Reader.viewport=oViewportWin
if(Reader.course){Reader.pageReset()
Reader.pageUpdate()}}else{Reader.forceRead()}
return false},viewportClose:function(){try{Reader.viewport.close();}catch(e){}},enable:function(oCourseWindow){getTag=oCourseWindow.getTag
getByTagAll=oCourseWindow.getByTagAll
getByTagOne=oCourseWindow.getByTagOne
getChildByTagAll=oCourseWindow.getChildByTagAll
getChildByTagOne=oCourseWindow.getChildByTagOne
Reader.pageReset()
Reader.course=oCourseWindow
Reader.topLevelLayer=Reader.course.oTopLevelLayer
oCourseWindow.switchEmbeds(Reader.course.document.body,"off",true)
oCourseWindow.document.body.fireEvent("onfinishpage")
oCourseWindow.document.body.attachEvent("onnavigate",Reader.pageInit)
oCourseWindow.document.body.attachEvent("onreadrequest",Reader.readRequest)
oCourseWindow.document.body.attachEvent("ondisabledchange",Reader.disabledChange)
if(Reader.viewport&&!Reader.viewport.closed&&Reader.viewport.document){Reader.viewport.document.title=oCourseWindow.document.title
oCourseWindow.attachEvent("onunload",Reader.viewportClose)}
oCourseWindow.loadResource(oCourseWindow.oResource)
oCourseWindow.document.attachEvent("onbeforeactivate",function(event){if(oCourseWindow.oTopLevelLayer.contains(event.srcElement))
return event.returnValue=false})},pageInit:function(){var oTopLevelLayer=Reader.topLevelLayer
if((!Reader.viewport||Reader.viewport.closed)&&!window.bReaderPopup)
return
Reader.container.disabled=true
Reader.pageReset()
for(var sOTag in Reader.mediaTags)
Reader.stripMedia(oTopLevelLayer,sOTag)
Reader.stripFrames(oTopLevelLayer)
var iTimeLimit=new Date().getTime()+5000,iLastIndex=0;(function(){if(new Date().getTime()<iTimeLimit)try{var aOEls=getByTagAll(oTopLevelLayer,"*")
for(var iOE=iLastIndex;iOE<aOEls.length;iOE++){var oOEl=aOEls[iOE]
if(getTag(oOEl)=="SCRIPT"||getTag(oOEl)=="IMG")continue
if(oOEl.readyState&&oOEl.readyState!="complete")throw"not ready"
try{if(oOEl.getRendered)throw"has method"}catch(e){if(!oOEl.getRendered())throw"not rendered"}}}catch(e){iLastIndex=iOE
return setTimeout(arguments.callee,50)}
for(var sOTag in Reader.mediaTags)
Reader.stripFrames(oTopLevelLayer,true)
Reader.pageUpdate()})()},pageReset:function(){Reader.detachAllEvents()
Reader.originalHash={}
Reader.normalizedHash={}
Reader.boundsHash={}
Reader.lastClick=null
Reader.Show.reset()
clearTimeout(Reader.refocusTimer)},pageUpdate:function(){Reader.container.disabled=false
Reader.container.innerHTML=""
Reader.unlockPlayers(Reader.course.oDocContent)
var sTitleVar=Reader.process(Reader.topLevelLayer,Reader.container)
Reader.createHeading(sTitleVar)
Reader.attachEventOnce(Reader.course.document.body,"onnewcontent",Reader.newContent)
Reader.forceRead()},newContent:function(event){var oONew=event.element,oONewParent=oONew.parentNode,oNNew=null,oNNewContainer=Reader.container,oNNewParent=null,aNAll=getByTagAll(Reader.container,"*")
Reader.lastClick=null
Reader.Show.reset()
for(var iNE=aNAll.length;iNE--;){var oNEl=aNAll[iNE],oOEl=oNEl&&Reader.originalHash[oNEl.uniqueID]
if(oOEl&&!Reader.topLevelLayer.contains(oOEl))
Reader.destroyElement(oNEl,oOEl)}
while(oONewParent&&oONewParent.nodeType==1&&oONewParent!=Reader.topLevelLayer&&oNNewContainer==Reader.container){if(Reader.isAtomic(oONewParent))
oNNewContainer=Reader.normalizedHash[oONewParent.uniqueID]||Reader.container
oONewParent=oONewParent.parentNode}
if(oONew.currentStyle.position!="absolute")
oNNewParent=Reader.normalizedHash[oONewParent.uniqueID]
Reader.process(oONew,oNNewContainer,oNNewParent)
oNNew=Reader.normalizedHash[oONew.uniqueID]
if(oNNew)
Reader.forceRead(oNNew)},readRequest:function(event){var oOEl=event.element,oNEl=Reader.normalizedHash[oOEl.uniqueID]
if(oNEl)
Reader.forceRead(oNEl)},process:function(oOContainer,oNContainer,oNContainerParent){var aWorkQueue=[{oel:oOContainer,ntop:oNContainer,nparent:oNContainerParent}],oDoc=oNContainer.ownerDocument,oOriginalHash=Reader.originalHash,oNormalizedHash=Reader.normalizedHash,oBoundsHash=Reader.boundsHash,sTitleVar="project title"
for(var iQ=0;iQ<aWorkQueue.length;iQ++){var oJob=aWorkQueue[iQ],oOEl=oJob.oel,oNTop=oJob.ntop,oNParent=oJob.nparent||oNTop
if(oOEl.nodeType==3){var sText=oOEl.nodeValue.replace(/\uFEFF/g,"")
oNParent.appendChild(oDoc.createTextNode(sText))}else{var sOTag=getTag(oOEl).toUpperCase()
if(Reader.skipTags[sOTag])continue
if(sOTag=="IMG"&&!oOEl.alt&&!oOEl.title)continue
if(sOTag=="VARIABLE"){var sVarType=oOEl.getAttribute("vardesc")
if(sVarType=="project title"||sVarType=="location title"){sTitleVar=sVarType=="location title"?sVarType:sTitleVar
continue}}
Reader.attachEventOnce(oOEl,"onpropertychange",Reader.propChange)
if(oOEl.currentStyle.display!="none"){var aOCh=oOEl.childNodes,bBlock=oOEl.currentStyle.position=="absolute"||oOEl.currentStyle.display=="block"&&oOEl.currentStyle.width=="auto"||oOEl.currentStyle.position=="relative"&&parseInt(oOEl.currentStyle.width,10)>2||oOEl.currentStyle.width=="100%",bAtomic=Reader.isAtomic(oOEl),bSuperAtomic=Reader.superAtomicTags[sOTag],bNavlink=Reader.navlinkTags[sOTag]&&oOEl.getAttribute("readerNavlink")!="false"||oOEl.getAttribute("readerNavlink")=="true",oOClickable=Reader.findClickable(oOEl),sCheckId=Reader.getCheckId(oOEl),bCheckPrimary=sCheckId==oOEl.uniqueID,bListItem,bRatingItem=Reader.ratingTags[sOTag],bSkipChildren=false,sNTag="SPAN",oNEl
if(bSuperAtomic)
for(var oNTemp=oNParent;oNTemp!=oNContainer;oNTemp=oNTemp.parentNode)
if(oNTemp.getAttribute("atomic")=="1"){oNParent=oNTemp.parentNode
break}
for(var oNTemp=oNParent;oOClickable&&oNTemp!=oNContainer;oNTemp=oNTemp.parentNode)
if(oNTemp.getAttribute("clickable")=="1")
oOClickable=false
if(bBlock&&getTag(oNParent)=="LI")bBlock=false
bListItem=getTag(oNParent)=="UL"
if(Reader.plainTags[sOTag])sNTag=sOTag
else if(Reader.listTags[sOTag])sNTag="UL"
else if(sCheckId)sNTag=bCheckPrimary?"DIV":"LABEL"
else if(bRatingItem)sNTag="BUTTON"
else if(oOClickable)sNTag="A"
else if(bListItem)sNTag="LI"
else if(bBlock)sNTag="P"
if(sOTag=="UL"&&oOEl.currentStyle.listStyleImage=="none"&&!Reader.listTypeUnordered[oOEl.currentStyle.listStyleType])
sNTag="OL"
oNEl=oNormalizedHash[oOEl.uniqueID]
if(oNEl)Reader.destroyElement(oNEl)
oNEl=oDoc.createElement(sNTag)
if(bCheckPrimary){var oNCheck=oDoc.createElement("INPUT"),oNLabel=oDoc.createElement("LABEL")
oNCheck.type="checkbox"
oNCheck.id="chk"+sCheckId
oNLabel.htmlFor="chk"+sCheckId
oNEl.appendChild(oNCheck)
oNEl.appendChild(oNLabel)
oNEl.checkChild=oNCheck
if(oOEl.getAttribute("checked"))
oNCheck.checked=true
oOriginalHash[oNCheck.uniqueID]=oOEl
oOriginalHash[oNLabel.uniqueID]=oOEl}else if(sCheckId){oNEl.htmlFor="chk"+sCheckId}
if(sOTag=="BLANKITEM"){var oOBlank=getChildByTagOne(oOEl,"blankselect")||getChildByTagOne(oOEl,"input")
if(getTag(oOBlank).toUpperCase()=="INPUT"){var oNInput=oDoc.createElement("INPUT")
oNInput.type="text"
oNInput.title=oOEl.title||"blank"
oNInput.disabled=oOBlank.readOnly
oNInput.value=oOBlank.value
Reader.attachEventOnce(oNInput,"onchange",Reader.blankChange)
oNEl.appendChild(oNInput)
oNEl.inputChild=oNInput}else{var oOOpts=getByTagOne(oOEl,"BLANKOPTIONS"),aOOpts=getByTagAll(oOOpts,"BLANKOPTION"),sGroupId="radio"+oOEl.uniqueID,bODisabled=!oOBlank.onclick,sOSelected=oOBlank.innerHTML
oOOpts.style.visibility="visible"
for(var iO=0;iO<aOOpts.length;iO++){var oOOpt=aOOpts[iO],oNRadio=oDoc.createElement("INPUT"),oNLabel=oDoc.createElement("LABEL"),sRadioId=sGroupId+"_"+iO
oNRadio.type="radio"
oNRadio.name=sGroupId
oNRadio.id=sRadioId
oNRadio.value=iO
oNRadio.disabled=bODisabled
Reader.attachEventOnce(oNRadio,"onclick",Reader.blankChange)
oNLabel.htmlFor=sRadioId
oNEl.appendChild(oNRadio)
oNEl.appendChild(oNLabel)
oNEl["inputChild"+iO]=oNRadio
oNRadio.checked=oOOpt.innerHTML==sOSelected
for(var iC=0;iC<oOOpt.childNodes.length;iC++)
Reader.process(oOOpt.childNodes[iC],oNLabel)}}
Reader.attachEventOnce(oOBlank,"onpropertychange",Reader.propChange)
bSkipChildren=true}
if(sOTag=="RICHTEXT"){var oNTextArea=oDoc.createElement("TEXTAREA")
oNTextArea.cols="40"
oNTextArea.rows="10"
oNTextArea.value=oOEl.text||""
Reader.attachEventOnce(oNTextArea,"onchange",Reader.richtextChange)
oNEl.appendChild(oNTextArea)
oNEl.textAreaChild=oNTextArea
Reader.attachEventOnce(oOEl,"oninit",Reader.richtextInit)
bSkipChildren=true}
if(sOTag=="INPUT"||sOTag=="TEXTAREA"){if(sOTag=="INPUT"){oNEl.type=oOEl.type}else if(sOTag=="TEXTAREA"){oNEl.cols="40"
oNEl.rows="10"}
oNEl.value=oOEl.value
if(oOEl.readOnly)
oNEl.readOnly=true
if(oOEl.checked)
oNEl.checked=true
Reader.attachEventOnce(oNEl,"onchange",Reader.inputChange)}
if(bRatingItem){var iIdx=oOEl.getAttribute("idx")
oNEl.innerText=iIdx}
if(sOTag=="UENGINE"){oNEl.innerHTML=oOEl.innerHTML
var oORadio=getByTagOne(oOEl,"input"),oNRadio=getByTagOne(oNEl,"input")
if(oORadio&&oNRadio){oOriginalHash[oNRadio.uniqueID]=oORadio
oNormalizedHash[oORadio.uniqueID]=oNRadio
Reader.attachEventOnce(oORadio,"onpropertychange",Reader.propChange)
oNRadio.setAttribute("clickable","1")
Reader.attachEventOnce(oNRadio,"onclick",Reader.proxyClick)}
bSkipChildren=true}
if(sOTag=="TEXTART"){oNEl.innerText=oOEl.getAttribute("taString")
bSkipChildren=true}
if(oOEl.getAttribute("frameHTML"))
oNEl.innerHTML=oOEl.getAttribute("frameHTML")
oNEl.style.visibility=Reader.getVisibility(oOEl)
if(oOEl.isDisabled||oOEl.style.cursor=="default")
oNEl.disabled=true
if(oOEl.title)
oNEl.title=oOEl.title
if(sOTag=="IMG"&&oOEl.alt)
oNEl.alt=oOEl.alt
if(sOTag=="IMG"&&oOEl.src)
oNEl.src=oOEl.src
if((sOTag=="TD"||sOTag=="TH")&&oOEl.scope)
oNEl.scope=oOEl.scope
oOriginalHash[oNEl.uniqueID]=oOEl
oNormalizedHash[oOEl.uniqueID]=oNEl
if(oOClickable){if(getTag(oNEl)=="IMG")
oNEl=oDoc.createElement("A").appendChild(oNEl).parentNode
if(getTag(oOClickable)=="A"){oNEl.href=oOClickable.href
oNEl.target=oOClickable.target||"_blank"}else if(oOClickable.getAttribute("vhref")){oNEl.href=oOClickable.getAttribute("vhref")
oNEl.target=oOClickable.getAttribute("vtarget")||"_blank"}else{oNEl.setAttribute("proxyLink","1")
if(getTag(oNEl)=="A")
oNEl.href="#"
Reader.attachEventOnce(oNEl,"onclick",Reader.proxyClick)}
oNEl.setAttribute("clickable","1")}
if(getTag(oNEl)=="A"&&oNEl.style.visibility=="hidden"){oNEl.setAttribute("origHref",oNEl.href)
oNEl.href=""}
if(!bSkipChildren)for(var iC=0;iC<aOCh.length;iC++){var oOChild=aOCh[iC],oCJob={oel:oOChild,ntop:oNTop}
if(bAtomic)
oCJob.ntop=oNEl
if(oOChild.nodeType!=1||oOChild.currentStyle.position!="absolute"){oCJob.nparent=bCheckPrimary?oNLabel:oNEl}
aWorkQueue.push(oCJob)}
if(oOClickable&&bBlock)
oNEl=oDoc.createElement(bListItem?"LI":"P").appendChild(oNEl).parentNode
if(bNavlink)
oNEl.setAttribute("navlink","1")
if(bAtomic)
oNEl.setAttribute("atomic","1")
if(bSuperAtomic)
oNEl.setAttribute("superAtomic","1")
Reader.insertByPosition(oBoundsHash,oOEl,oNEl,oNParent)}}}
var aNLinks=getByTagAll(oNContainer,"A")
for(var iNL=aNLinks.length;iNL--;){var oNLink=aNLinks[iNL]
if(oNLink.childNodes.length==0){var sTitle=oNLink.title||!oNLink.getAttribute("proxyLink")&&oNLink.href
if(sTitle)
oNLink.appendChild(oDoc.createTextNode(sTitle))
else
Reader.destroyElement(oNLink)}else if(oNLink.childNodes.length==1&&oNLink.firstChild.tagName=="P"){oNLink.firstChild.removeNode()}}
return sTitleVar},forceRead:function(oTargetEl){var sHash="s"+new Date().getTime()
if(oTargetEl)
oTargetEl.id=sHash
Reader.container.ownerDocument.parentWindow.location.hash=sHash
Reader.delayedRefocus(Reader.lastClick)},propChange:function(event){var oOEl=event.srcElement,oNEl=Reader.normalizedHash[oOEl.uniqueID],sProp=event.propertyName,sOTag=getTag(oOEl).toUpperCase()
if(oNEl){if(sProp=="disabled")
oNEl.disabled=oOEl.isDisabled
if(sProp=="style.cursor")
oNEl.disabled=oOEl.isDisabled||oOEl.style.cursor=="default"
if(sProp=="style.visibility")
Reader.updateVisibility(oOEl,oNEl,Reader.getVisibility(oOEl))
if(sOTag=="INPUT"&&sProp=="checked")
setTimeout(function(){oNEl.checked=oOEl.checked;},0)
if(sOTag=="MCANSWER"&&sProp=="checked"&&oNEl.checkChild)
setTimeout(function(){oNEl.checkChild.checked=!!oOEl.getAttribute("checked");},0)
if(sOTag=="VARIABLE"&&sProp=="innerText")
oNEl.innerText=oOEl.innerText}
if(sOTag=="INPUT"){var oOItem=oOEl.parentNode,oNItem=Reader.normalizedHash[oOItem.uniqueID],oNInput=oNItem&&oNItem.inputChild
if(oNInput){if(sProp=="value")
oNInput.value=oOEl.value
if(sProp=="readOnly")
oNInput.disabled=oOEl.readOnly}}
if(sOTag=="BLANKSELECT"){var oOItem=oOEl.parentNode,oNItem=Reader.normalizedHash[oOItem.uniqueID]
if(oNItem){if(sProp=="innerHTML"){var aOOpts=getByTagAll(oOItem,"BLANKOPTION"),sOSelected=oOEl.innerHTML
for(var iOO=aOOpts.length;iOO--;)
if(aOOpts[iOO].innerHTML==sOSelected){oNItem["inputChild"+iOO].checked=true
break}}
if(sProp=="onclick"){var aNOpts=getByTagAll(oNItem,"INPUT"),bDisabled=!oOEl.onclick
for(var iNO=aNOpts.length;iNO--;)
aNOpts[iNO].disabled=bDisabled}}}
if(sProp=="style.display"){var bShow=Reader.getDisplay(oOEl)!="none"
if(bShow&&!oNEl)
Reader.Show.showEl(oOEl)
if(!bShow)
Reader.Show.hideEl(oOEl)}},disabledChange:function(event){var oOEl=event.element,oNEl=oOEl&&Reader.normalizedHash[oOEl.uniqueID]
if(oNEl)
oNEl.disabled=oOEl.isDisabled},proxyClick:function(event){var oNEl=event.srcElement,oOEl
while(oNEl&&oNEl!=Reader.container&&oNEl.getAttribute("clickable")!="1")
oNEl=oNEl.parentNode
oOEl=oNEl&&Reader.originalHash[oNEl.uniqueID]
if(oOEl){if(oNEl.checkChild)
oNEl=oNEl.checkChild
Reader.lastClick=oNEl
Reader.Show.reset()
Reader.delayedRefocus(oNEl)
oOEl.fireEvent("onmousedown")
oOEl.fireEvent("onmouseup")
oOEl.fireEvent("onclick")
if(getTag(oOEl)=="INPUT"&&oOEl.type=="radio")
oOEl.checked=true}
return event.returnValue=false},insertByPosition:function(oBoundsHash,oOEl,oNEl,oNParent){var aNSib=oNParent.childNodes,oNPos=null,oBounds=null,bNElNav=oNEl.getAttribute("navlink")=="1"
if(oOEl.currentStyle.position=="absolute")
oBoundsHash[oNEl.uniqueID]=oBounds=Reader.getBounds(oOEl)
for(var iS=aNSib.length;iS--;){var oNSib=aNSib[iS],oNSibBounds=oBoundsHash[oNSib.uniqueID],bNSibNav=oNSib.nodeType==1&&oNSib.getAttribute("navlink")=="1"
if(oNParent!=Reader.container||!bNElNav||bNSibNav){if(!oNSibBounds)break
if(oBounds&&(oNSibBounds.top<oBounds.top-Reader.threshold||oNSibBounds.top<oBounds.top+Reader.threshold&&oNSibBounds.left<=oBounds.left||oNParent==Reader.container&&bNSibNav&&!bNElNav))break}
oNPos=oNSib}
oNParent.insertBefore(oNEl,oNPos)},getBounds:function(oOEl){var oOCur=oOEl,iX=0,iY=0
while(oOCur&&oOCur!=Reader.topLevelLayer){iX+=oOCur.offsetLeft+oOCur.clientLeft
iY+=oOCur.offsetTop+oOCur.clientTop
oOCur=oOCur.parentNode}
return{left:iX,top:iY,right:iX+oOEl.offsetWidth,bottom:iY+oOEl.offsetHeight}},getVisibility:function(oOEl){while(oOEl&&oOEl.parentNode&&oOEl.currentStyle.visibility=="inherit"&&!Reader.visibleTags[getTag(oOEl).toUpperCase()])
oOEl=oOEl.parentNode
return oOEl&&oOEl.nodeType==1&&!Reader.visibleTags[getTag(oOEl).toUpperCase()]?oOEl.currentStyle.visibility:"visible"},getDisplay:function(oOEl){for(var oParent=oOEl;oParent&&oParent.nodeType==1;oParent=oParent.parentNode)
if(oParent.currentStyle.display=="none")
return"none"
return oOEl.currentStyle.display},updateVisibility:function(oOEl,oNEl,sVisible){var aOCh=oOEl.childNodes
try{var oNActive=oNEl.ownerDocument.activeElement;}catch(e){}
if(oNEl.contains(oNActive))Reader.container.ownerDocument.body.focus()
oNEl.style.visibility=sVisible
if(getTag(oNEl)=="A"){if(sVisible=="hidden"){if(oNEl.getAttribute("origHref")===null){oNEl.setAttribute("origHref",oNEl.href)
oNEl.href=""}}else{if(oNEl.getAttribute("origHref")!==null){oNEl.href=oNEl.getAttribute("origHref")
oNEl.removeAttribute("origHref")}}}
for(var iOC=aOCh.length;iOC--;){var oOCh=aOCh[iOC]
if(oOCh.nodeType==1&&oOCh.currentStyle.visibility=="inherit"){var oNCh=Reader.normalizedHash[oOCh.uniqueID]
if(oNCh)Reader.updateVisibility(oOCh,oNCh,sVisible)}}},isAtomic:function(oOEl){var oAtomicEls=Reader.course.AtomicEls
if(oAtomicEls&&oAtomicEls[getTag(oOEl).toUpperCase()])
return true},findClickable:function(oOEl){var oKeyNav=Reader.course.KeyNav
if(oKeyNav)do{var sOTag=getTag(oOEl).toUpperCase()
if(sOTag=="HOTWORDPOP"||sOTag=="HINTPOP"||sOTag=="LOGINFORM"||sOTag=="BLANKOPTION")
return false
if(oKeyNav[sOTag]||oOEl.getAttribute("vhref")||sOTag=="A")
return oOEl}while((oOEl=oOEl.parentNode)&&oOEl.nodeType==1)
return false},getCheckId:function(oOEl){for(;oOEl&&oOEl.nodeType==1;oOEl=oOEl.parentNode)
if(getTag(oOEl).toUpperCase()=="MCANSWER")
return oOEl.uniqueID
return false},blankChange:function(event){var oNInput=event.srcElement,oOEl=Reader.originalHash[oNInput.parentNode.uniqueID]
if(oNInput.type=="text"){var oOBlank=getByTagAll(oOEl,"INPUT")[0]
if(oOBlank){oOBlank.value=oNInput.value
oOBlank.fireEvent("onchange")}}else{var oBlank=getByTagOne(oOEl,"BLANKSELECT"),aOOpts=getByTagAll(oOEl,"BLANKOPTION")
for(var iO=0;iO<aOOpts.length;iO++){var oOOpt=aOOpts[iO]
if(iO==oNInput.value){oOOpt.setAttribute("selected","true")
if(oBlank)
oBlank.innerHTML=oOOpt.innerHTML}else{oOOpt.removeAttribute("selected")}}}},richtextInit:function(event){var oORichText=event.srcElement,oNEl=Reader.normalizedHash[oORichText.uniqueID],oNTextArea=oNEl&&oNEl.textAreaChild
if(oNTextArea)
oNTextArea.value=oORichText.text},richtextChange:function(event){var oNTextArea=event.srcElement,oORichText=Reader.originalHash[oNTextArea.parentNode.uniqueID]
if(oORichText)
oORichText.content=oNTextArea.value.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/\"/g,"&quot;").replace(/\'/g,"&#39;").replace(/\r?\n/g,"<br>")},inputChange:function(event){var oNEl=event.srcElement,oOEl=Reader.originalHash[oNEl.uniqueID]
if(oOEl){oOEl.value=oNEl.value
oOEl.fireEvent("onchange")}},createHeading:function(sTitleVar){var oContainer=Reader.container,oDoc=oContainer.ownerDocument,oLink=oDoc.createElement("A"),oCh=oContainer.firstChild,oH=oDoc.createElement("H1")
if(oCh&&oCh.getAttribute("navlink")=="1"){oLink.href="#"
oLink.setAttribute("proxyLink","1")
oLink.innerText="Skip Navigation"
oLink.onclick=function(){Reader.forceRead(oH)
return false}
oContainer.insertBefore(oLink,oCh)}
while(oCh&&oCh.getAttribute("navlink")=="1")
oCh=oCh.nextSibling
oH.innerText=Reader.course.VarInterface?Reader.course.VarInterface.Get(sTitleVar):Reader.course.document.title
oContainer.insertBefore(oH,oCh)},destroyElement:function(oNEl,oOEl){var oNParent=oNEl.parentNode
try{var oNActive=oNEl.ownerDocument.activeElement;}catch(e){}
if(oNEl.contains(oNActive))Reader.container.ownerDocument.body.focus()
if(!oOEl)
oOEl=Reader.originalHash[oNEl.uniqueID]
delete Reader.originalHash[oNEl.uniqueID]
if(oOEl)
delete Reader.normalizedHash[oOEl.uniqueID]
if(oNParent){oNParent.removeChild(oNEl)
if(oNParent&&oNParent.nodeType==1&&oNParent!=Reader.container&&oNParent.childNodes.length==0)
Reader.destroyElement(oNParent)}
if(Reader.Show.container==oNEl)
Reader.Show.reset()},stripMedia:function(oOTop,sOTag){var aOEls=getByTagAll(oOTop,sOTag)
for(var iOE=aOEls.length;iOE--;){var oOEl=aOEls[iOE],oOParent=oOEl.parentNode
oOParent.removeChild(oOEl)}},stripFrames:function(oOTop,bFinal){var aOEls=getByTagAll(oOTop,"IFRAME")
for(var iOE=aOEls.length;iOE--;){var oOEl=aOEls[iOE],oOParent=oOEl.parentNode,sOParentTag=getTag(oOParent).toUpperCase(),sWidth=oOEl.offsetWidth,sHeight=oOEl.offsetHeight,oOPlaceholder=oOEl.ownerDocument.createElement("DIV")
if(!bFinal&&sOParentTag=="FBLIKE")
continue
oOPlaceholder.style.display=oOEl.currentStyle.display
oOPlaceholder.style.visibility=oOEl.currentStyle.visibility
oOPlaceholder.style.position=oOEl.currentStyle.position
oOPlaceholder.style.left=oOEl.currentStyle.left
oOPlaceholder.style.top=oOEl.currentStyle.top
oOPlaceholder.style.right=oOEl.currentStyle.right
oOPlaceholder.style.bottom=oOEl.currentStyle.bottom
oOEl.removeAttribute("style")
oOEl.removeAttribute("className")
oOEl.style.width=sWidth
oOEl.style.height=sHeight
oOPlaceholder.setAttribute("frameHTML",oOEl.outerHTML)
oOParent.insertBefore(oOPlaceholder,oOEl)
oOParent.removeChild(oOEl)}},unlockPlayers:function(oOContainer){var aOMP=getByTagAll(oOContainer,"mplayer")
for(var iOMP=0;iOMP<aOMP.length;iOMP++){var oOMP=aOMP[iOMP]
try{oOMP.oncomplete({targetElement:oOMP});}catch(e){}}},delayedRefocus:function(oNEl){clearTimeout(Reader.refocusTimer)
Reader.refocusTimer=setTimeout(function(){try{Reader.container.ownerDocument.body.focus();}catch(e){}
try{if(oNEl&&Reader.container.contains(oNEl))
oNEl.focus()}catch(e){}},10)},attachEventOnce:function(oEl,sEvent,fHandler){oEl.detachEvent(sEvent,fHandler)
oEl.attachEvent(sEvent,fHandler)
Reader.eventHandlers.push({element:oEl,event:sEvent,handler:fHandler})},detachAllEvents:function(){if(Reader.eventHandlers){for(var iE=Reader.eventHandlers.length;iE--;)try{var oE=Reader.eventHandlers[iE]
oE.element.detachEvent(oE.event,oE.handler)}catch(e){}}
Reader.eventHandlers=[]},Show:{timer:null,container:null,showEl:function(oOEl){var oNClick=Reader.lastClick,oNContainer=Reader.Show.container,bHasAtomic=false
if(!oNContainer){oNContainer=Reader.container.ownerDocument.createElement("DIV")
if(oNClick)
oNClick.parentNode.insertBefore(oNContainer,oNClick.nextSibling)
else
Reader.container.appendChild(oNContainer)
Reader.Show.container=oNContainer}
Reader.process(oOEl,oNContainer)
if(oNContainer.getAttribute("superAtomic")!="1"){var bHasAtomic=false,aNCh=oNContainer.childNodes
for(var iNC=aNCh.length;iNC--&&!bHasAtomic;)
if(aNCh[iNC].getAttribute("superAtomic")=="1")
bHasAtomic=true
if(bHasAtomic){var oNPos=oNClick
while(oNPos&&oNPos!=Reader.container&&oNPos.getAttribute("atomic")!="1")
oNPos=oNPos.parentNode
if(oNPos&&oNPos.getAttribute("atomic")=="1")
oNPos.parentNode.insertBefore(oNContainer,oNPos.nextSibling)
oNContainer.setAttribute("superAtomic","1")}}
if(!Reader.Show.timer)
Reader.Show.timer=setTimeout(Reader.Show.read)},hideEl:function(oOEl){var aOCh=oOEl.childNodes,oNEl=Reader.normalizedHash[oOEl.uniqueID]
for(var iOC=aOCh.length;iOC--;)
if(aOCh[iOC].nodeType==1)
Reader.Show.hideEl(aOCh[iOC])
if(oNEl)
Reader.destroyElement(oNEl,oOEl)},reset:function(){clearTimeout(Reader.Show.timer)
Reader.Show.timer=null
Reader.Show.container=null},read:function(){Reader.forceRead(Reader.Show.container)
Reader.Show.reset()}}}
if(document.documentMode<=10&&(!window.oRequest||oRequest.mode!="snapshot"))
Reader.prepare()})();