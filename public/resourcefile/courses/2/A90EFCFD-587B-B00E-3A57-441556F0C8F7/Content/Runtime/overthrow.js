/*! Overthrow v.0.1.0. An overflow:auto polyfill for responsive design. (c) 2012: Scott Jehl, Filament Group, Inc. http://filamentgroup.github.com/Overthrow/license.txt */
(function(w,undefined){var doc=w.document,docElem=doc.documentElement,canBeFilledWithPoly="ontouchmove"in doc,overflowProbablyAlreadyWorks="WebkitOverflowScrolling"in docElem.style||(!canBeFilledWithPoly&&w.screen.width>1200)||(function(){var ua=w.navigator.userAgent,webkit=ua.match(/AppleWebKit\/([0-9]+)/),wkversion=webkit&&webkit[1],wkLte534=webkit&&wkversion>=534
return(ua.match(/Android ([0-9]+)/)&&RegExp.$1>=3&&wkLte534||ua.match(/ Version\/([0-9]+)/)&&RegExp.$1>=0&&w.blackberry&&wkLte534||ua.indexOf(/PlayBook/)>-1&&RegExp.$1>=0&&wkLte534||ua.match(/Fennec\/([0-9]+)/)&&RegExp.$1>=4||ua.match(/wOSBrowser\/([0-9]+)/)&&RegExp.$1>=233&&wkLte534||ua.match(/NokiaBrowser\/([0-9\.]+)/)&&parseFloat(RegExp.$1)===7.3&&webkit&&wkversion>=533)})(),defaultEasing=function(t,b,c,d){return c*((t=t/d-1)*t*t+1)+b},enabled=false,timeKeeper,toss=function(elem,options){var i=0,sLeft=elem.scrollLeft,sTop=elem.scrollTop,o={top:"+0",left:"+0",duration:100,easing:w.overthrow.easing},endLeft,endTop
if(options){for(var j in o){if(options[j]!==undefined){o[j]=options[j]}}}
if(typeof o.left==="string"){o.left=parseFloat(o.left)
endLeft=o.left+sLeft}
else{endLeft=o.left
o.left=o.left-sLeft}
if(typeof o.top==="string"){o.top=parseFloat(o.top)
endTop=o.top+sTop}
else{endTop=o.top
o.top=o.top-sTop}
timeKeeper=setInterval(function(){if(i++<o.duration){elem.scrollLeft=o.easing(i,sLeft,o.left,o.duration)
elem.scrollTop=o.easing(i,sTop,o.top,o.duration)}
else{if(endLeft!==elem.scrollLeft){elem.scrollLeft=endLeft}
if(endTop!==elem.scrollTop){elem.scrollTop=endTop}
intercept()}},1)
return{top:endTop,left:endLeft,duration:o.duration,easing:o.easing}},isScrollValue=function(val){return val=="auto"||val=="scroll"},closest=function(target,ascend){if(ascend){target=target.parentNode}
while(target&&target.nodeType==1){var computedStyle=getComputedStyle(target)
if(computedStyle){var overflowX=computedStyle.getPropertyValue("overflow-x"),overflowY=computedStyle.getPropertyValue("overflow-y")
if(isScrollValue(overflowX)||isScrollValue(overflowY)){return target}}
target=target.parentNode}},intercept=function(){clearInterval(timeKeeper)},enable=function(){if(enabled){return}
enabled=true
if(overflowProbablyAlreadyWorks||!canBeFilledWithPoly){return}
var elem,lastTops=[],lastLefts=[],lastDown,lastRight,resetVertTracking=function(){lastTops=[]
lastDown=null},resetHorTracking=function(){lastLefts=[]
lastRight=null},finishScroll=function(){var top=(lastTops[0]-lastTops[lastTops.length-1])*8,left=(lastLefts[0]-lastLefts[lastLefts.length-1])*8,duration=Math.max(Math.abs(left),Math.abs(top))/8
top=(top>0?"+":"")+top
left=(left>0?"+":"")+left
if(!isNaN(duration)&&duration>0&&(Math.abs(left)>80||Math.abs(top)>80)){toss(elem,{left:left,top:top,duration:duration})}},inputs,setPointers=function(el,val){inputs=el.querySelectorAll("textarea, input")
for(var i=0,il=inputs.length;i<il;i++){inputs[i].style.pointerEvents=val}},changeScrollTarget=function(startEvent,ascend){if(doc.createEvent){var newTarget=(!ascend||ascend===undefined)&&elem.parentNode||elem.touchchild||elem,tEnd
if(newTarget&&newTarget!==elem){tEnd=doc.createEvent("HTMLEvents")
tEnd.initEvent("touchend",true,true)
elem.dispatchEvent(tEnd)
newTarget.touchchild=elem
elem=newTarget
newTarget.dispatchEvent(startEvent)
return true}}},start=function(e){intercept()
resetVertTracking()
resetHorTracking()
elem=closest(e.target)
if(!elem||elem===docElem||e.touches.length>1){return}
setPointers(elem,"none")
var touchStartE=e,computedStyle=getComputedStyle(elem),overflowX=computedStyle.getPropertyValue("overflow-x"),overflowY=computedStyle.getPropertyValue("overflow-y"),scrollT=elem.scrollTop,scrollL=elem.scrollLeft,height=elem.offsetHeight,width=elem.offsetWidth,startY=e.touches[0].pageY,startX=e.touches[0].pageX,scrollHeight=elem.scrollHeight,scrollWidth=elem.scrollWidth,move=function(e){var ty=scrollT+(isScrollValue(overflowY)?startY-e.touches[0].pageY:0),tx=scrollL+(isScrollValue(overflowX)?startX-e.touches[0].pageX:0),down=ty>=(lastTops.length?lastTops[0]:0),right=tx>=(lastLefts.length?lastLefts[0]:0)
if((Math.max(ty,elem.scrollTop)>0&&Math.min(ty,elem.scrollTop)<scrollHeight-height)||(Math.max(tx,elem.scrollLeft)>0&&Math.min(tx,elem.scrollLeft)<scrollWidth-width)){e.preventDefault()}
else if(changeScrollTarget(touchStartE)){return}
if(lastDown&&down!==lastDown){resetVertTracking()}
if(lastRight&&right!==lastRight){resetHorTracking()}
lastDown=down
lastRight=right
elem.scrollTop=ty
elem.scrollLeft=tx
lastTops.unshift(ty)
lastLefts.unshift(tx)
if(lastTops.length>3){lastTops.pop()}
if(lastLefts.length>3){lastLefts.pop()}},end=function(e){var el=elem
finishScroll()
setPointers(el,"auto")
setTimeout(function(){setPointers(el,"none")},450)
elem.removeEventListener("touchmove",move,false)
elem.removeEventListener("touchend",end,false)}
elem.addEventListener("touchmove",move,false)
elem.addEventListener("touchend",end,false)}
doc.addEventListener("touchstart",start,false)}
w.overthrow={set:enable,easing:defaultEasing,toss:toss,intercept:intercept,closest:closest,support:overflowProbablyAlreadyWorks?"native":canBeFilledWithPoly&&"polyfilled"||"none"}
enable()})(this);