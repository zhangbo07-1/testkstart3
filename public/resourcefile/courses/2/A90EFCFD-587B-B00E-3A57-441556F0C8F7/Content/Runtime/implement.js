
function ImpSpec(aProtos,xImp,iAttribs){this.protos=aProtos
this.imp=xImp
this.attribs=iAttribs}
ImpSpec.ATTRIB_OVERWRITE=1
ImpSpec.prototype.bind=function(sName){var aProtos=this.protos,xImp=this.imp,bOverwrite=(this.attribs&ImpSpec.ATTRIB_OVERWRITE)>0
for(var iP=0;iP<aProtos.length;iP++){var sProto=aProtos[iP],oProto=null
if(sProto in window)oProto=window[sProto].prototype
else if(sProto=="Window")oProto=window.constructor!=Object?window.constructor.prototype:window
if(oProto){var bExists=Object.prototype.hasOwnProperty.call(oProto,sName)
if(!bExists&&oProto instanceof Element)
bExists=Object.prototype.hasOwnProperty.call(Element.prototype,sName)
if(!bExists||bOverwrite){if(typeof xImp=="function"){if(sName in oProto&&!("__"+sName in oProto))oProto["__"+sName]=oProto[sName]
oProto[sName]=xImp}else{if(sName in oProto&&!("__"+sName in oProto)){var oOrigImp
if(oProto.__lookupGetter__){var fOrigGet=oProto.__lookupGetter__(sName),fOrigSet=oProto.__lookupSetter__(sName)
if(fOrigGet||fOrigSet)
oOrigImp={get:fOrigGet,set:fOrigSet}}else if(Object.getOwnPropertyDescriptor){oOrigImp=Object.getOwnPropertyDescriptor(oProto,sName)}
if(oOrigImp)oProto["__"+sName]=oOrigImp}
if(Object.defineProperty){Object.defineProperty(oProto,sName,xImp)}else if(oProto.__defineGetter__){if(xImp.get)oProto.__defineGetter__(sName,xImp.get)
if(xImp.set)oProto.__defineSetter__(sName,xImp.set)}}}}}}
ImpSpec.propertyDelegate=function(oImp,sType){if(oImp[sType])return function(){return oImp[sType].apply(this,arguments)}}
function _(aProtos,xImp,iAttribs){return new ImpSpec(aProtos,xImp,iAttribs)}
if(typeof getImplementations=="function")(function(oObj){for(var sName in oObj)if(oObj.hasOwnProperty(sName)){var oSpec=oObj[sName]
if(typeof oSpec=="object"){if(oSpec instanceof ImpSpec){oSpec.bind(sName)}else{arguments.callee(oSpec)}}}})(getImplementations());