<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        // $this->assertTrue(true);
        $this->visit('/')
            ->see('登录');
    }


    public function testLogin()
    {
        $this->visit('/')
            ->see('登录')
            ->type('bruce@lureroad.com','email')
            ->type('admin','password')
            ->press('登录')
            ->seePageIs('/');
    }
    /*
    public function testAddUser()
    {
        $this->visit('/')
            ->see('登录')
            ->type('bruce@lureroad.com','email')
            ->type('admin','password')
            ->press('登录')
            ->seePageIs('/')
            ->visit('/users')
            ->see('新建用户')
            ->visit('/users/create')
            ->seePageIs('/users/create')
            ->type('phpunit2','name')
            ->type('test2@phpunit.com','email')
            ->type('123456','password')
            ->type('123456','password_confirmation')
            ->type('phpunit','realname')
            ->press('确定')
            ->visit('/users')
            ->see('phpunit2')
            ->visit('/logout')
            ->visit('/')
            ->see('登录')
            ->type('test2@phpunit.com','email')
            ->type('123456','password')
            ->press('登录')
            ->seePageIs('/');
    }

    public function testOriganization()
    {
        $this->visit('/')
            ->see('登录')
            ->type('bruce@lureroad.com','email')
            ->type('admin','password')
            ->press('登录')
            ->seePageIs('/')
            ->visit('/organization')
            ->see('组织结构管理')
            ->visit('/organization/1/create')
            ->see('组织结构名')
            ->type('phpunittest','name')
            ->type('PT','tag')
            ->press('确定')
            ->see('操作成功')
            ;
    }
    */
    public function testAddCourse()
    {
        $this->visit('/')
            ->see('登录')
            ->type('bruce@lureroad.com','email')
            ->type('admin','password')
            ->press('登录')
            ->visit('/courses/create')
            ->type('PT_001','course_id')
            ->type('ptcourse0001','title')
            ->type('1','catalog_id')
            ->press('确定')
            ->see('课件上传');
    }
}
