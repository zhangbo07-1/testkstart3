<?php

use Illuminate\Database\Seeder;
use App\Models\Attribute;
class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Attribute::create([
            'first_title' => '属性1',
            'second_title' => '属性2',
            'third_title' => '属性3',
            'fourth_title' => '属性4',
            'fifth_title' => '属性5',
        ]);
    }
}
