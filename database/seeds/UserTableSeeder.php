<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\UserManage;
use App\Models\Profile;
use App\Models\LoginLog;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $admin = User::create( [
            'name'      => 'admin',
            'email'     => 'admin@knowsurface.com',
            'password'  => bcrypt( 'admin' ),
            'role_id'   => 4,
            'status_id' => 2,
            'department_id' => 1,
        ] );

        Profile::create( [
            'user_id' => intval( $admin->id ),
        ] );
        UserManage::create([
            'model_id' => intval( $admin->id ),
            'manage_id' => 1,
        ]);
    }
}
