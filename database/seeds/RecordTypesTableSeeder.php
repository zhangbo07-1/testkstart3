<?php

use Illuminate\Database\Seeder;
use App\Models\RecordType;
class RecordTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        RecordType::create([
            'title' => 'record.type1',
        ]);

        RecordType::create([
            'title' => 'record.type2',
        ]);
        RecordType::create([
            'title' => 'record.type3',
        ]);



    }
}
