<?php

use Illuminate\Database\Seeder;
use App\Models\UserStatus;
class UserStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserStatus::create([
            'tag' => 'PV',
            'title' => '未激活',
        ]);

        UserStatus::create([
            'tag' => 'A',
            'title' => '活动',
        ]);

        UserStatus::create([
            'tag' => 'PA',
            'title' => '待审核',
        ]);

        UserStatus::create([
            'tag' => 'C',
            'title' => '关闭',
        ]);
    }
}
