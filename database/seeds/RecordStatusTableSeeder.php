<?php

use Illuminate\Database\Seeder;
use App\Models\RecordStatus;
class RecordStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RecordStatus::create([
            'title' => 'not_start',
        ]);

        RecordStatus::create([
            'title' => 'under_way',
        ]);

        RecordStatus::create([
            'title' => 'done',
        ]);

        RecordStatus::create([
            'title' =>'all_done',
        ]);
        RecordStatus::create([
            'title' => 'overdue',
        ]);

    }
}
