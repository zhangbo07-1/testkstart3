<?php

use Illuminate\Database\Seeder;
use App\Models\Catalog;

class CatalogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Catalog::create([
            'name' => '根目录'
        ]);
    }
}
