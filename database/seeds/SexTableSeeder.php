<?php

use Illuminate\Database\Seeder;
use App\Models\Sex;
class SexTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sex::create([
            'tag' => 'F',
            'title' => 'table.woman',
        ]);

        Sex::create([
            'tag' => 'M',
            'title' => 'table.man',
        ]);
    }
}
