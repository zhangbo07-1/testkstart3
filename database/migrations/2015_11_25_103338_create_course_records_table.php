<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('process')->default(1);
            $table->integer('overdue')->default(0);
            $table->integer('study_times')->default(0);
            $table->double('study_span',15,1)->unsigned()->default(0);
            $table->timestamp('started_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->integer('max_score')->default(0);
            $table->integer('exam_max_score')->default(0);
            $table->integer('star')->default(0);
            $table->timestamps();
        });
        Schema::table('course_records', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_records', function(Blueprint $table) {
            $table->dropForeign('course_records_user_id_foreign');
            $table->dropForeign('course_records_course_id_foreign');
        });
        Schema::drop('course_records');
    }
}
