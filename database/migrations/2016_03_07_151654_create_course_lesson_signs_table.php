<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseLessonSignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_lesson_signs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classRecord_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('lesson_id')->unsigned();
            $table->integer('process')->default(1);
            $table->timestamps();
        });
        Schema::table('course_lesson_signs', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('classRecord_id')->references('id')->on('course_class_records')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('lesson_id')->references('id')->on('course_lessons')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_lesson_signs');
    }
}
