<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemSelectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_selects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('item_id')->unsigned();
            $table->string('percent')->default(0);
            $table->timestamps();
        });
        Schema::table('item_selects', function(Blueprint $table) {
            $table->foreign('item_id')->references('id')->on('items')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_selects', function(Blueprint $table) {
            $table->dropForeign('item_selects_item_id_foreign');

        });
        Schema::drop('item_selects');
    }
}
