<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('send_limits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->string('attribute1_list')->nullable();
            $table->string('attribute2_list')->nullable();
            $table->string('attribute3_list')->nullable();
            $table->string('attribute4_list')->nullable();
            $table->string('attribute5_list')->nullable();


            $table->timestamps();
        });
        Schema::table('send_limits', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('send_limits', function(Blueprint $table) {
            $table->dropForeign('send_limits_course_id_foreign');
        });
        Schema::drop('send_limits');
    }
}
