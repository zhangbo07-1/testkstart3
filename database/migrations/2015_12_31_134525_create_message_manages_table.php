<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageManagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_manages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id')->unsigned();
            $table->integer('manage_id')->unsigned();
            $table->timestamps();
        });
         Schema::table('message_manages', function(Blueprint $table) {
            $table->foreign('model_id')->references('id')->on('messages')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('manage_id')->references('id')->on('organizations')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message_manages', function(Blueprint $table) {
            $table->dropForeign('message_manages_model_id_foreign');
            $table->dropForeign('message_manages_manage_id_foreign');
        });
        Schema::drop('message_manages');
    }
}
