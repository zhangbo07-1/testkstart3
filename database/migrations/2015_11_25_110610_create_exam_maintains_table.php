<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamMaintainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_maintains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id')->unsigned();
            $table->integer('maintain_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('exam_maintains', function(Blueprint $table) {
            $table->foreign('model_id')->references('id')->on('exams')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('maintain_id')->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_maintains', function(Blueprint $table) {
            $table->dropForeign('exam_maintains_model_id_foreign');
            $table->dropForeign('exam_maintains_maintain_id_foreign');
        });
        Schema::drop('exam_maintains');
    }
}
