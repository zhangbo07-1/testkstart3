<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('class_id')->unsigned();
            $table->timestamp('start_at')->default(0);
            $table->timestamp('end_at')->default(0);
            $table->text('address')->nullable();
            $table->timestamps();
        });
        Schema::table('course_lessons', function(Blueprint $table) {
            $table->foreign('class_id')->references('id')->on('course_classes')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_lessons', function(Blueprint $table) {
            $table->dropForeign('course_lessons_class_id_foreign');

        });
        Schema::drop('course_lessons');
    }
}
