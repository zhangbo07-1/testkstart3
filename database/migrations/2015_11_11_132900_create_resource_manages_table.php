<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceManagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_manages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id')->unsigned();
            $table->integer('manage_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('resource_manages', function(Blueprint $table) {
            $table->foreign('model_id')->references('id')->on('resources')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('manage_id')->references('id')->on('organizations')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resource_manages', function(Blueprint $table) {
            $table->dropForeign('resource_manages_model_id_foreign');
            $table->dropForeign('resource_manages_manage_id_foreign');
        });
        Schema::drop('resource_manages');
    }
}
