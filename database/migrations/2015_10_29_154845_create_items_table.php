<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            /*
             *type = 1 选择
             *type = 2 评分
             */
            $table->integer('type');
            $table->integer('survey_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('items', function(Blueprint $table) {
            $table->foreign('survey_id')->references('id')->on('surveys')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function(Blueprint $table) {
            $table->dropForeign('items_survey_id_foreign');
        });
        Schema::drop('items');
    }
}
