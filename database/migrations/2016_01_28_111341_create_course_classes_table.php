<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->integer('late')->default(1);
            $table->string('title');
            $table->string('teacher')->nullable();
            $table->text('ps')->nullable();
            $table->timestamps();
        });
        Schema::table('course_classes', function(Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_classes', function(Blueprint $table) {
            $table->dropForeign('course_classes_course_id_foreign');

        });
        Schema::drop('course_classes');
    }
}
