<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('suspend_data')->nullable();
            $table->text('location')->nullable();
            $table->text('mode')->nullable();
            $table->text('completion_status')->nullable();
            $table->timestamps();
        });
        Schema::table('study_logs', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('course_id')->references('id')->on('courses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('study_logs', function(Blueprint $table) {
            $table->dropForeign('study_logs_user_id_foreign');
            $table->dropForeign('study_logs_course_id_foreign');
        });
        Schema::drop('study_logs');
    }
}
