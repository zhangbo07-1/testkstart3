<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hightitle');
            $table->string('lowtitle');
            $table->integer('item_id')->unsigned();
            $table->integer('avg')->default(0);
            $table->integer('numbers')->default(0);
            $table->timestamps();
        });
        Schema::table('item_grades', function(Blueprint $table) {
            $table->foreign('item_id')->references('id')->on('items')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_grades', function(Blueprint $table) {
            $table->dropForeign('item_grades_item_id_foreign');
        });
        Schema::drop('item_grades');
    }
}
