<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionSelectResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_select_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('select_id')->unsigned();
            $table->integer('result_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('question_select_results', function(Blueprint $table) {
            $table->foreign('select_id')->references('id')->on('question_selects')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('result_id')->references('id')->on('question_results')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_select_results', function(Blueprint $table) {
            $table->dropForeign('question_select_results_select_id_foreign');
            $table->dropForeign('question_select_results_result_id_foreign');

        });
        Schema::drop('question_select_results');
    }
}
