<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('realname')->nullable();
            //$table->integer('position_id')->nullable();
            //$table->integer('rank_id')->nullable();
            $table->integer('attribute1_id')->nullable();
            $table->integer('attribute2_id')->nullable();
            $table->integer('attribute3_id')->nullable();
            $table->integer('attribute4_id')->nullable();
            $table->integer('attribute5_id')->nullable();
            $table->integer('sex_id')->nullable();
            $table->string('company')->nullable();
            $table->string('remark')->nullable();
            $table->string('tel')->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
        });
        Schema::table('profiles', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function(Blueprint $table) {
            $table->dropForeign('profiles_user_id_foreign');
        });
        Schema::drop('profiles');
    }
}
