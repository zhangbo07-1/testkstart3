<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->integer('user_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->string('model_type');
            $table->timestamps();
        });
        Schema::table('records', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('records', function(Blueprint $table) {
            $table->dropForeign('records_user_id_foreign');
        });
        Schema::drop('records');
    }
}
