<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionSelectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_selects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('if_true')->default(0);;
            $table->integer('question_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('question_selects', function(Blueprint $table) {
            $table->foreign('question_id')->references('id')->on('questions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_selects', function(Blueprint $table) {
            $table->dropForeign('question_selects_question_id_foreign');
        });
        Schema::drop('question_selects');
    }
}
