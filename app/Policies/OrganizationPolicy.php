<?php

namespace App\Policies;
use App\Models\User;
use App\Models\Organization;
use App\Repositories\UserRepository;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrganizationPolicy extends BasePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        Organization $organization,
        UserRepository $user_gestion
    )
    {
        $this->model = $organization;
        $this->user_gestion = $user_gestion;
    }

    public function delete(User $user,$data)
    {
        return $this->modify($user,$data);
    }

    public function modify(User $user,$data)
    {
        foreach($user->manages as $manage)
        {
            $user_id[] = $manage->manage_id;
        }
        $data_id[] = $data->id;
        if(($user->role_id > 2) && ($this->user_gestion->ownerOrNot($user_id,$data_id)))
        {
            return true;
        }
        else
            return false;
    }
}
