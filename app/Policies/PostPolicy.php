<?php
namespace App\Policies;
use App\Models\User;
use App\Models\Post;
use App\Repositories\UserRepository;
use App\Repositories\PostRepository;
use Auth;
class PostPolicy extends BasePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        Post $post,
        UserRepository $user_gestion,
        PostRepository $post_gestion

    )
    {
        $this->model = $post;
        $this->user_gestion = $user_gestion;
        $this->post_gestion = $post_gestion;
    }

     public function modify(User $user, $data)
    {
        if($user->id === $data->user_id)
        {
            return true;
        }
        foreach($data->user->manages as $manage)
        {
            $data_id[] = $manage->manage_id;
        }
        foreach($user->manages as $manage)
        {
            $user_id[] = $manage->manage_id;
        }
        if(($user->role_id > 2)  && ($this->post_gestion->modify($data->id)) && ($this->user_gestion->ownerOrNot($user_id,$data_id)))
        {
            return true;
        }
        return false;
    }

    public function delete(User $user, $data)
    {
        return $this->modify($user,$data);
    }


}

