<?php

function get_info()
{
    $info =DB::table('system_info')->first();
        return "Phone:{$info->phone} | {$info->email}";
}

function get_register()
{
    $info =DB::table('system_info')->first();
    return $info->register;;
}

function set_language()
{
    App::setLocale('cn');
}
function get_logo_url()
{
    $info =DB::table('system_info')->first();
    if($info->logo)
        return "http://".env('OSS_BUCKET').".".env('OSS_END_POINT')."/".env('OSS_PATH');
    else
        return '';
}
function get_url()
{
    return "http://".env('OSS_BUCKET').".".env('OSS_END_POINT')."/".env('OSS_PATH');
}
function errors_for($attribute,$errors)
{
    if($attribute == 'success')
    {
        return $errors->first($attribute, '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>:message</div>');
    }
    elseif($attribute == 'fail')
    {
         return $errors->first($attribute, '<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>:message</div>');
    }
    else
        return $errors->first($attribute, '<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>:message</div>');
    //   return $errors->first($attribute, '<div class="dm3-alert dm3-alert-error">:message</div>');
}

function errors_for_tag($attribute,$errors)
{
    return $errors->first($attribute, 'has-error has-feedback');
}
function getEndDate($beginDays,$days)
{
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $beginDays)->addDay($days);

}

