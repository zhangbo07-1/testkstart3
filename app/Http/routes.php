<?php

/*
   |--------------------------------------------------------------------------
   | Application Routes
   |--------------------------------------------------------------------------
   |
   | Here is where you can register all of the routes for an application.
   | It's a breeze. Simply tell Laravel the URIs it should respond to
   | and give it the controller to call when that URI is requested.
   |
 */



Route::get('weixin','WeixinController@check');
Route::get('weixin/qiye','WeixinController@checkQiye');
Route::get('weixin/qiye/login','WeixinController@loginQiye');
Route::get('weixin/login','WeixinController@login');
Route::get('weixin/course','WeixinController@course');
Route::get('weixin/bind','WeixinController@bind');
Route::get('weixin/course/start','WeixinController@courseStart');
Route::post('weixin/bind/save','WeixinController@update');
// 认证路由...
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', ['as'=>'login','uses'=>'Auth\AuthController@postLogin']);
Route::get('logout', ['as'=>'logout','uses'=>'Auth\AuthController@getLogout']);
// 注册路由...
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', ['as'=>'register','uses'=>'Auth\AuthController@postRegister']);

Route::get('register/resend', [
    'as'   => 'getResend',
        'uses' => 'Auth\AuthController@getResend'

]);

Route::get('register/verify/{confirmationCode}', [
    'as'   => 'confirmation_path',
        'uses' => 'Auth\AuthController@confirm'

]);

Route::get('manuals/registration', function () {
    return view('manual.registration');

});
Route::group(['middleware' => 'lang'], function () {
Route::get('password-reset', 'Auth\AuthController@passwordReset');
Route::post('password-reset', 'Auth\AuthController@resetPassword');
    Route::group(['middleware' => 'auth'], function () {

        Route::get('/', 'MenuController@index');
        Route::get('manuals/manager', function () {
            return view('manual.manager.manager');
        });
        Route::get('manuals/user/pc', function () {
            return view('manual.user.pc');

        });

        Route::get('manuals/user/wechat', function () {
            return view('manual.user.wechat');

        });
        Route::get('catalogs','CatalogsController@index');
        Route::post('catalogs/edit','CatalogsController@edit');
        Route::get('catalogs/select','CatalogsController@catalogSelect');
         Route::get('catalogs/{id}/create','CatalogsController@create');
        Route::post('catalogs/{id}/create','CatalogsController@saveCreate');
        Route::get('catalogs/{id}/edit','CatalogsController@rename');
         Route::post('catalogs/{id}/edit','CatalogsController@saveRename');
        Route::post('organization/check','OrganizationController@checked');
        Route::get('organization','OrganizationController@index');
        Route::get('organization/{id}/create','OrganizationController@create');
        Route::post('organization/{id}/create','OrganizationController@saveCreate');
        Route::get('organization/{id}/edit','OrganizationController@edit');
         Route::post('organization/{id}/edit','OrganizationController@saveEdit');
        Route::post('organization/show','OrganizationController@show');
        Route::get('organization/example','OrganizationController@exampleExcel');
        Route::get('organization/manage/select','OrganizationController@manageSelect');
        Route::get('organization/department/select','OrganizationController@departmentSelect');
        Route::get('organization/import','ImportLogsController@organizationImportView');
        Route::get('organization/excel','OrganizationController@excel');
        Route::post('organization/import','ImportLogsController@organizationImport');
        Route::get('users/import','ImportLogsController@userImportView');
        Route::post('users/import','ImportLogsController@userImport');
        Route::get('users/department','UsersController@searchByDepartment');
        Route::get('users/excel','UsersController@usersExcel');
        Route::get('users/example','UsersController@exampleExcel');
        Route::post('users/{id}/changePassword', 'UsersController@changePassword');
        //Route::get('users','UsersController@searchByDepartment');
        Route::resource('users', 'UsersController');
        Route::get('import-log/{id}/download','ImportLogsController@downLoad');
        Route::resource('import-log','ImportLogsController');

        Route::get('profile','UsersController@profile');
        Route::get('posts-manage','PostsController@manage');
        Route::resource('posts', 'PostsController');
        Route::resource('comments', 'CommentsController');
        Route::resource('resources-manage', 'ResourcesController');
        Route::get('resources', 'ResourcesController@lists');
        Route::get('resources/{id}/download', 'ResourcesController@downLoad');
        Route::any('/api/imageUp', 'UmeditorController@fileUpdateAPI');
        Route::resource('news-manage', 'MessagesController');
        Route::get('news/lists','MessagesController@lists');
        Route::get('news/{id}','MessagesController@show');
        Route::resource('email-manage','EmailTempletsController');
        Route::get('log-manage','LogsController@login');
        Route::get('log-manage/login','LogsController@login');
        Route::get('log-manage/email','LogsController@email');
        Route::get('system-manage', 'SystemInfoController@logo');
        Route::get('system-manage/logo', 'SystemInfoController@logo');
        Route::post('system-manage/logo/upload', 'SystemInfoController@saveLogo');
        Route::get('system-manage/info','SystemInfoController@info');
        Route::post('system-manage/info','SystemInfoController@saveInfo');
        Route::get('system-manage/censor','SystemInfoController@censor');
        Route::post('system-manage/censor','SystemInfoController@saveCensor');
        Route::get('system-manage/email','SystemInfoController@email');
        Route::post('system-manage/email','SystemInfoController@saveEmail');
        Route::get('survey/lists','SurveysController@lists');
        Route::get('survey/{id}/answer', 'SurveysController@answer');
        Route::post('survey/{id}/score', 'SurveyResultsController@score');
        Route::get('survey/{id}/result', 'SurveyResultsController@result');
         Route::get('survey/{id}/excel', 'SurveyResultsController@excel');
        Route::get('survey/{id}/push', 'SurveysController@push');
        Route::resource('survey.items', 'ItemsController');
        Route::resource('survey', 'SurveysController');
        Route::post('evaluation/{id}/distribute','EvaluationsController@distribute');
        Route::post('evaluation/deallocate','EvaluationsController@deallocate');
         Route::post('course-survey/{id}/distribute','CourseSurveysController@distribute');
        Route::post('course-survey/deallocate','CourseSurveysController@deallocate');
        Route::resource('evaluation.items', 'ItemsController');
        Route::resource('evaluation', 'EvaluationsController');
        Route::resource('course-survey', 'CourseSurveysController');
        Route::resource('course-survey.items', 'ItemsController');
        Route::get('api/evaluation/distribute','EvaluationsController@getDistributeList');
         Route::get('api/course-survey/distribute','CourseSurveysController@getDistributeList');
        Route::resource('question-bank', 'QuestionBanksController');
        Route::resource('question-bank.questions', 'QuestionsController');
        Route::post('question-bank/{bank_id}/questions/import','QuestionsController@import');
        Route::get('question-bank/{bank_id}/excel','QuestionsController@excel');
        Route::get('questions/example','QuestionsController@exampleExcel');
        Route::resource('exams','ExamsController');

        Route::get('courses/send-limit','AttributesController@sendLimit');
        Route::resource('courses','CoursesController');
        Route::get('courses/{course_id}/delete','CoursesController@delete');

        Route::get('course/{course_id}/exam/{exam_id}/QR-code','ExamsController@courseExamQR');
        Route::get('course/{course_id}/course-survey/{survey_id}/QR-code','SurveyResultsController@courseSurveyQR');

        Route::get('course/{course_id}/evaluation/{survey_id}/QR-code','SurveyResultsController@evaluationQR');

        Route::get('course/{course_id}/survey/{survey_id}/result','SurveyResultsController@courseSurveyResult');
        Route::get('course/{course_id}/evaluation/{survey_id}/result','SurveyResultsController@courseSurveyResult');

        Route::resource('courses.distribute','CourseRecordsController');
        Route::get('courses/{course_id}/class/{id}/distribute','CourseClassesController@distribute');
        Route::post('classes/{id}/distribute','CourseClassesController@addClassRecords');
         Route::delete('classes/{id}/distribute','CourseClassesController@deleteClassRecords');
        Route::delete('courses/{id}/distribute','CourseRecordsController@destroy');
        Route::post('courses/{id}/redistribute','CourseRecordsController@redistribute');
        Route::post('classes/{id}/redistribute','CourseClassesController@classRedistribute');
        Route::get('courses/{course_id}/teachers','CoursesController@assignTeacher');
        Route::resource('courses/{course_id}/upload','CourseWaresController');
        Route::get('courses/{course_id}/exam','CoursesController@assignExam');

        Route::get('courses/{course_id}/evaluation','CoursesController@assignEvaluation');
        Route::get('courses/{course_id}/survey','CoursesController@assignSurvey');
        Route::get('courses/{course_id}/launch','CourseWaresController@launch');
        Route::get('courses/{course_id}/preview','CourseWaresController@preview');
        Route::get('courses/{course_id}/push','CoursesController@push');
        Route::post('courses/{course_id}/pushed','CoursesController@pushed');

        Route::post('courses/{course_id}/launch', 'CourseRecordsController@saveRecordMessage');
  Route::get('courses/{course_id}/resources','ResourcesController@courseResourceManage');

   Route::get('courses/{course_id}/report', 'CourseRecordsController@report');
   Route::get('courses/{course_id}/report/excel', 'CourseRecordsController@reportExcel');

    Route::get('courses/{course_id}/exam/{exam_id}/result', 'ExamsController@report');
        Route::get('courses/{course_id}/add_resources','CoursesController@assignResource');
        Route::get('courses/{course_id}/resources/create','ResourcesController@courseResourceCreate');
       Route::resource('courses.class','CourseClassesController');
       Route::get('courses/{course_id}/class/{class_id}/lesson/{lesson_id}/forsign','CourseClassesController@forSign');

       Route::get('courses/{course_id}/class/{class_id}/lesson/{lesson_id}/sign','CourseClassesController@sign');
       
       Route::get('courses/{course_id}/class/{class_id}/forenroll','CourseClassesController@forEnroll');

       Route::get('courses/{course_id}/class/{class_id}/enroll','CourseClassesController@enroll');
         
      
 	Route::get('course-ware/{id}/download','CourseWaresController@download');
        Route::get('api/courses/{id}/assigned-teacher','CourseMaintainsController@getAsssignedTeacherList');
        Route::get('api/courses/{id}/unassigned-teacher','CourseMaintainsController@getUnAsssignedTeacherList');
        Route::get('api/courses/{id}/distribute','CourseRecordsController@getDistributeUserList');

        Route::get('api/courses/{id}/undistribute','CourseRecordsController@getUnDistributeUserList');
        Route::get('api/classes/{id}/undistribute','CourseClassesController@getClassUnDistributeUserList');
         Route::get('api/classes/{id}/distribute','CourseClassesController@getClassDistributeUserList');

         Route::get('api/classes/{id}/record','CourseClassesController@getClassRecords');

         Route::post('api/course/{course_id}/classes/{id}/beOver','CourseClassesController@beOver');
         Route::post('api/course/{course_id}/classes/{id}/beNotOver','CourseClassesController@beNotOver');
         
         
        Route::get('api/courses/{id}/overdue','CourseRecordsController@getOverdueUserList');
        Route::delete('courses-maintain','CourseMaintainsController@destroy');
        Route::post('courses-maintain','CourseMaintainsController@store');
        Route::resource('exams.combinate', 'ExamCombinationsController');

        Route::get('exams/{exam_id}/delete','ExamsController@delete');
        Route::get('exams/{exam_id}/teachers', 'ExamsController@assignTeacher');
        Route::get('exams/{exam_id}/distribute','ExamsController@distribute');
        Route::get('exams/{exam_id}/mark', 'ExamResultsController@mark');
        Route::get('exams/{exam_id}/preview','ExamsController@preview');
        Route::get('exams/{exam_id}/result/{id}/mark','ExamResultsController@markQuestion');
        Route::post('exams/{exam_id}/questionResult/{id}/marking','ExamResultsController@scoreQuestion');
        Route::get('api/exams/assign-course','ExamDistributionsController@getAssignList');
        Route::post('exams/{id}/assign-course','ExamDistributionsController@assignCourse');
        Route::post('exams/deallocate-course','ExamDistributionsController@deallocateCourse');
        Route::resource('exams.distribute','ExamDistributionsController');
        Route::delete('exams/{id}/distribute','ExamDistributionsController@destroy');
        Route::get('api/exams/{id}/distribute','ExamDistributionsController@getDistributeUserList');
        Route::get('api/exams/{id}/undistribute','ExamDistributionsController@getUnDistributeUserList');


        Route::get('exam/{exam_id}/QR-code','ExamsController@examQR');

        Route::get('course/{course_id}/exam/{exam_id}/QR-code','ExamsController@courseExamQR');
        Route::get('rank/lists','CourseRecordsController@rank');
        Route::get('lessons', 'CourseRecordsController@lessons');
        Route::get('lessons-done', 'CourseRecordsController@lessonsDone');
        Route::get('lessons-overdue', 'CourseRecordsController@lessonsOverdue');
        Route::get('lessons/{course_id}/posts','PostsController@coursePost');
         Route::get('lessons/{course_id}/posts/create','PostsController@courseCreate');
         Route::get('lessons/{course_id}/posts/{id}','PostsController@courseShow');

        Route::get('lessons/{course_id}/posts/{id}/edit','PostsController@courseEdit');
      Route::get('lessons/{course_id}/resources','ResourcesController@courseResource');
        Route::get('lessons/{course_id}/log', 'CourseRecordsController@getLog');
        Route::get('lessons/{course_id}/exam/{exam_id}/answer', 'ExamsController@courseAnswer');
        Route::get('lessons/{exam_id}/log/{result_id}/detailed', 'ExamResultsController@getDetailed');

        Route::get('exam-lessons/{exam_id}/answer', 'ExamsController@examAnswer');
        Route::get('lessons/exam/{distribution_id}/answer', 'ExamsController@answer');
        Route::get('exam-lessons/{exam_id}/log', 'ExamDistributionsController@getLog');
         Route::get('exam-lessons/{exam_id}/info', 'ExamsController@examAnswer');
         Route::get('exam-lessons/{exam_id}/return', 'ExamDistributionsController@returnURL');
        Route::get('exam-lessons/{exam_id}/log/{result_id}/detailed', 'ExamResultsController@getDetailed');
        Route::get('exam/{exam_id}/answer/{questionResult_id}', 'ExamResultsController@getAnswer');
         Route::get('exam/{exam_id}/answer/{questionResult_id}/Forward', 'ExamResultsController@getAnswerForward');
          Route::get('exam/{exam_id}/answer/{questionResult_id}/Next', 'ExamResultsController@getAnswerNext');

        Route::post('exams/{distribution_id}/score', 'ExamResultsController@score');
        Route::get('lessons/{course_id}/info', 'CoursesController@info');
         Route::get('lessons/{course_id}/return', 'CourseRecordsController@returnURL');
        Route::any('lessons/{id}/updatestar', 'CourseRecordsController@updateStar'); //need self confirm

        Route::get('lessons/{course_id}/evaluation/{evaluation_id}/answer', 'EvaluationsController@answer');
        Route::post('lessons/{course_id}/evaluation/{evaluation_id}/score', 'EvaluationResultsController@CourseScore');

        Route::post('lessons/{course_id}/evaluation/{evaluation_id}/score', 'SurveyResultsController@CourseScore');
        Route::get('lessons/{course_id}/course-survey/{survey_id}/answer', 'EvaluationsController@answer');
        Route::post('lessons/{course_id}/course-survey/{survey_id}/score', 'EvaluationResultsController@CourseScore');

        Route::post('lessons/{course_id}/course-survey/{survey_id}/score', 'SurveyResultsController@CourseScore');


        Route::get('api/exams/{id}/assigned-teacher','ExamMaintainsController@getAsssignedTeacherList');
        Route::get('api/exams/{id}/unassigned-teacher','ExamMaintainsController@getUnAsssignedTeacherList');

        Route::delete('exams-maintain','ExamMaintainsController@destroy');
        Route::post('exams-maintain','ExamMaintainsController@store');


        Route::get('reports/', 'ReportsController@getCourse');
        Route::get('reports/user', 'ReportsController@getUser');
        Route::get('reports/user/excel', 'ReportsController@userExcel');
        Route::get('reports/course', 'ReportsController@getCourse');
        Route::get('reports/course/excel', 'ReportsController@courseExcel');
        Route::get('reports/exam','ReportsController@getExam');
         Route::get('reports/exam/excel', 'ReportsController@examExcel');
        Route::get('reports/evaluation', 'ReportsController@getEvaluation');
        Route::get('reports/evaluation/{id}/result', 'ReportsController@getItem');
        Route::get('reports/evaluation/{id}/result/excel', 'ReportsController@itemExcel');
        Route::get('reports/question', 'ReportsController@getQuestion');
        Route::get('reports/question/excel', 'ReportsController@questionExcel');

        Route::get('attribute','AttributesController@firstAttribute');

        Route::get('attribute/first-attribute','AttributesController@firstAttribute');
        Route::delete('attribute/delete-first-attribute','AttributesController@firstAttributeDelete');
        Route::post('attribute/add-first-attribute','AttributesController@firstAttributeAdd');
        Route::post('attribute/update-first-attribute','AttributesController@firstAttributeUpdate');
        Route::post('attribute/rename-first-attribute','AttributesController@firstAttributeRename');
        Route::post('attribute/first-attribute/import','AttributesController@firstAttributeImport');

         Route::get('attribute/second-attribute','AttributesController@secondAttribute');
        Route::delete('attribute/delete-second-attribute','AttributesController@secondAttributeDelete');
        Route::post('attribute/add-second-attribute','AttributesController@secondAttributeAdd');
        Route::post('attribute/update-second-attribute','AttributesController@secondAttributeUpdate');
        Route::post('attribute/rename-second-attribute','AttributesController@secondAttributeRename');
        Route::post('attribute/second-attribute/import','AttributesController@secondAttributeImport');

         Route::get('attribute/third-attribute','AttributesController@thirdAttribute');
        Route::delete('attribute/delete-third-attribute','AttributesController@thirdAttributeDelete');
        Route::post('attribute/add-third-attribute','AttributesController@thirdAttributeAdd');
        Route::post('attribute/update-third-attribute','AttributesController@thirdAttributeUpdate');
        Route::post('attribute/rename-third-attribute','AttributesController@thirdAttributeRename');

        Route::post('attribute/third-attribute/import','AttributesController@thirdAttributeImport');

        Route::get('attribute/fourth-attribute','AttributesController@fourthAttribute');
        Route::delete('attribute/delete-fourth-attribute','AttributesController@fourthAttributeDelete');
        Route::post('attribute/add-fourth-attribute','AttributesController@fourthAttributeAdd');
         Route::post('attribute/update-fourth-attribute','AttributesController@fourthAttributeUpdate');
        Route::post('attribute/rename-fourth-attribute','AttributesController@fourthAttributeRename');
        Route::post('attribute/fourth-attribute/import','AttributesController@fourthAttributeImport');

        Route::get('attribute/fifth-attribute','AttributesController@fifthAttribute');
        Route::delete('attribute/delete-fifth-attribute','AttributesController@fifthAttributeDelete');
        Route::post('attribute/add-fifth-attribute','AttributesController@fifthAttributeAdd');
        Route::post('attribute/update-fifth-attribute','AttributesController@fifthAttributeUpdate');
        Route::post('attribute/rename-fifth-attribute','AttributesController@fifthAttributeRename');
        Route::post('attribute/fifth-attribute/import','AttributesController@fifthAttributeImport');

        Route::get('attribute/{name}/example','AttributesController@example');






    });
});
