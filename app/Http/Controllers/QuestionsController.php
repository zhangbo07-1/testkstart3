<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\BankRepository;
use App\Repositories\QuestionRepository;
use Gate,Auth,Redirect,Excel,Response;
class QuestionsController extends Controller
{
    /**
     * The PostRepository instance.
     *
     * @var App\Repositories\PostRepository
     */
    protected $bank_gestion;

    /**
     * The ItemRepository instance.
     *
     * @var App\Repositories\ItemRepository
     */
    protected $question_gestion;
    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\PostRepository $blog_gestion
     * @return void
     */
    public function __construct(
        BankRepository $bank_gestion,
        questionRepository $question_gestion
    )
    {
        $this->bank_gestion = $bank_gestion;
        $this->question_gestion = $question_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $bank = $this->bank_gestion->getById($id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        $questions = $this->question_gestion->index($id,10);
        return view('questions.index',compact('bank','questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $bank = $this->bank_gestion->getById($id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        return view('questions.create',compact('bank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $this->validate($request, [
            'question_id' => 'required|regex:/^[a-zA-Z\d\-\_]+$/|max:100|unique:questions,question_id',
        ]);
        $bank = $this->bank_gestion->getById($id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        $this->question_gestion->store($request->all(), $id);
        return redirect()->route('question-bank.questions.index', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($bank_id,$question_id)
    {
        $bank = $this->bank_gestion->getById($bank_id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        $question = $this->question_gestion->getById($question_id);
        return view('questions.edit',compact('question','bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$bank_id, $question_id)
    {
        $this->validate($request, [
            'question_id' => 'required|regex:/^[a-zA-Z\d\-\_]+$/|max:100|unique:questions,question_id,'.$question_id,
        ]);
        $bank = $this->bank_gestion->getById($bank_id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        $this->question_gestion->update($request->all(), $bank_id,$question_id);
        return redirect()->route('question-bank.questions.index', ['id' => $bank_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($bank_id,$question_id)
    {
        $bank = $this->bank_gestion->getById($bank_id);
        if (!Gate::denies('maintain',$bank))
        {
            $this->question_gestion->destroy($question_id);
        }
        return Redirect::back();
    }

    public function import($bank_id,Request $request)
    {

        $bank = $this->bank_gestion->getById($bank_id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        if($request->hasFile('file'))
        {
            $extension = $request->file('file')->getClientOriginalExtension();
            if($extension != 'xls'&&$extension != 'xlsx')
            {
                return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00006')));
            }

            $res =$this->question_gestion->import($bank_id,$request->file('file'));

             return redirect()->back()->withInput()->withErrors($res['message']);

        }
        else
        {
            return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00007')));
        }

    }

    public function excel($bank_id)
    {
        $bank = $this->bank_gestion->getById($bank_id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        $progs = $this->question_gestion->questionExcel($bank_id);
        Excel::create('Question Template', function($excel) use($progs)
                      {

                          $excel->sheet('Question Template', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                      })->export('xls');
    }


     public function exampleExcel()
    {
        $file = "Question Template.xls";
        $url = storage_path()."/app/example/".$file;
        return Response::download($url);
    }

}
