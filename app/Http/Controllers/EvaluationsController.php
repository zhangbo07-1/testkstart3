<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CourseRecord;
use App\Models\ExamRecord;
use App\Http\Requests;
use App\Models\Course;
use App\Http\Controllers\Controller;
use App\Repositories\SurveyRepository;
use Gate,Auth,Redirect;
class EvaluationsController extends Controller
{
     /**
     * The PostRepository instance.
     *
     * @var App\Repositories\PostRepository
     */
    protected $survey_gestion;
    protected $evaluationType = 2;

    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\PostRepository $blog_gestion
     * @return void
     */
    public function __construct(
        SurveyRepository $survey_gestion
    )
    {
        $this->survey_gestion = $survey_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('manage_evaluation',Auth::user()))
        {
            return Redirect::back();
        }
        $surveys = $this->survey_gestion->index(10,$this->evaluationType,$request->all());
        return view('evaluation.index',compact('surveys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manage_evaluation',Auth::user()))
        {
            return Redirect::back();
        }
        return view('evaluation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if (Gate::denies('manage_evaluation',Auth::user()))
        {
            return Redirect::back();
        }
        $this->validate($request, [
            'title' => 'required|max:255',
        ]);
        $res = $this->survey_gestion->store($request->all(),$this->evaluationType,Auth::user()->id);
        if(!$res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
        return redirect()->route('evaluation.edit', ['id' => $res['id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifyEvaluation',$survey))
        {
           return Redirect::back();
        }
        return view('evaluation.show',compact('survey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifyEvaluation',$survey))
        {
           return Redirect::back();
        }
        $manages = "";
        $manageIds = "";
        foreach($survey->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }
        return view('evaluation.edit',compact('survey','manages','manageIds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifyEvaluation',$survey))
        {
           return Redirect::back();
        }
       $this->validate($request, [
            'title' => 'required|max:255',
        ]);
        $res = $this->survey_gestion->update($request->all(),$id);
        if(!$res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (!Gate::denies('deleteEvaluation',$survey))
        {
           $this->survey_gestion->destroy($id);
        }
        return Redirect::back();
    }

    public function distribute(Request $request,$id)
    {
        $survey = $this->survey_gestion->getById($id);
        $res = $this->survey_gestion->distribute($id,$request->input('course_id'));
        return response()->json(['result'=>true,'record_title'=>$res->title,'message'=>trans('evaluation.assign_success')]);

    }
    public function deallocate(Request $request)
    {
        $res = $this->survey_gestion->deallocate($request->input('course_id'),$this->evaluationType);
        return response()->json(['result'=>true,'record_title'=>trans('table.none'),'message'=>trans('evaluation.deallocate_success')]);

    }

    public function getDistributeList(Request $request)
    {
        $evaluations = $this->survey_gestion->getDistributeList(5,$request->all(),$this->evaluationType);
        return response()->json($evaluations);

    }

    public function answer($course_id,$id)
    {

        $course = Course::where('course_id',$course_id)->first();
        $record = $this->survey_gestion->getRecordById($id);
        $survey = $record->survey;
        if (Gate::denies('apply',$survey))
        {
            return Redirect::back();
        }
        $res = $this->survey_gestion->answer($record->id,$course->id);
        if($res['status'])
            return view('evaluation.answer',compact('survey','course'));
        else
            return redirect()->back()->withErrors($res['message'])->withInput();
    }

}
