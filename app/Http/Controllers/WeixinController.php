<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\SystemInfoRepository;
use Auth,Redirect;
class WeixinController extends Controller
{
    protected $user_gestion;
    protected $info_gestion;


    public function __construct(
        UserRepository $user_gestion,
        SystemInfoRepository $info_gestion
    )
    {
        $this->user_gestion = $user_gestion;
        $this->info_gestion = $info_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $info = $this->info_gestion->info();
       
        $password = $request->input('password');
        $openid = $request->input('openid');
        /*
           if(User::where('openid',$openid)->first())
           {
           Flash::message('该微信已进行过绑定，请勿重复操作！！');
           return view('weixin.binder')->with('openid',$openid);
           
           }
         */
        if($info->user_email)
        {
             $email = $request->input('email');
             $user = $this->user_gestion->getByEmail($email);
        }
        else
        {
            $name = $request->input('name');
            $user = $this->user_gestion->getByName($name);
        }
        if(!$user)
        {
            $error = array('fail'=>trans('error.00060'));
            return view('weixin.binder')->with('openid',$openid)->with('info',$info)->withErrors($error);
        }

        if($info->user_email)
        {
            if(Auth::attempt(['email' => $email, 'password' => $password]))
            {
                $user->open_id = $openid;
                $user->save();
                $error = array('success'=>trans('error.10003'));
                return Redirect::to('/')->withErrors($error);;
            }
        }
        else
        {
            if(Auth::attempt(['name' => $name, 'password' => $password]))
            {
                $user->open_id = $openid;
                $user->save();
                $error = array('success'=>trans('error.10003'));
                return Redirect::to('/')->withErrors($error);;
            }
        }
        $error = array('fail'=>trans('error.00068'));
        return view('weixin.binder')->with('openid',$openid)->with('info',$info)->withErrors($error);;
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function checkQiye()
    {
        $corp_id = env('WEIXIN_CORP_ID');
        $url = urlencode("http://".$_SERVER['HTTP_HOST']."/weixin/qiye/login");
        $bind_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$corp_id."&redirect_uri=".$url."&response_type=code&scope=snsapi_base&state=123#wechat_redirect";
        return Redirect::to($bind_url);
    }



    public function loginQiye(Request $request)
    {
        $code = $request->input('code');
        $corp_id = env('WEIXIN_CORP_ID');
        $secret = env('WEIXIN_SECRET_ID','dd92671c88d37679e75fb486bff3b791');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=".$corp_id."&corpsecret=".$secret);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $output = curl_exec($ch);
        $output = json_decode($output,TRUE);

        if(isset($output["access_token"]))
        {
            $access = $output["access_token"];
            curl_close($ch);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=".$access."&code=".$code);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $output = curl_exec($ch);
            $output = json_decode($output,TRUE);
            if(isset($output['UserId']))
            {
                $user_name = $output['UserId'];
                $user = $this->user_gestion->getByName($user_name);
                if($user)
                {
                    if(Auth::loginUsingId($user->id))
                    {
                        return Redirect::to("/");
                    }
                }
            }
        }
        return Redirect::to("/");
    }




    public function login()
    {
        $app_id = env('WEIXIN_APP_ID','wx226bda725e21df23');
        $url = urlencode("http://".$_SERVER['HTTP_HOST']."/weixin/bind");
        $bind_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$app_id."&redirect_uri=".$url."&response_type=code&scope=snsapi_base&state=123#wechat_redirect";
        return Redirect::to($bind_url);


    }

    public function bind(Request $request)
    {
        $app_id = env('WEIXIN_APP_ID','wx226bda725e21df23');
        $secret = env('WEIXIN_SECRET_ID','dd92671c88d37679e75fb486bff3b791');
        $code = $request->input('code');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$app_id."&secret=".$secret."&code=".$code."&grant_type=authorization_code");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $output = curl_exec($ch);
        $output = json_decode($output,TRUE);

        $opid = $output["openid"];
        curl_close($ch);
        $user = $this->user_gestion->getByOpenID($opid);
        if($user)
        {
            if(Auth::loginUsingId($user->id))
            {
                return Redirect::to("/");
            }
        }
        $info = $this->info_gestion->info();
        return view('weixin.binder')->with('openid',$opid)->with('info',$info);
    }

    
    public function course()
    {
        $app_id = env('WEIXIN_APP_ID');
        $url = urlencode("http://".$_SERVER['HTTP_HOST']."/weixin/course/start");
        $bind_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$app_id."&redirect_uri=".$url."&response_type=code&scope=snsapi_base&state=123#wechat_redirect";
        return Redirect::to($bind_url);


    }



    public function courseStart(Request $request)
    {
        $app_id = env('WEIXIN_APP_ID','wx226bda725e21df23');
        $secret = env('WEIXIN_SECRET_ID','dd92671c88d37679e75fb486bff3b791');
        $code = $request->input('code');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$app_id."&secret=".$secret."&code=".$code."&grant_type=authorization_code");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $output = curl_exec($ch);
        $output = json_decode($output,TRUE);

        $opid = $output["openid"];
        curl_close($ch);
        $user = $this->user_gestion->getByOpenID($opid);
        if($user)
        {
            if(Auth::loginUsingId($user->id))
            {
                return Redirect::to("/lessons");
            }
        }
        $info = $this->info_gestion->info();
        return view('weixin.binder')->with('openid',$opid)->with('info',$info);

        
    }
}
