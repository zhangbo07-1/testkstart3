<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\EmailTempletRepository;
use Gate,Auth,Redirect;
class EmailTempletsController extends Controller
{
    /**
     * The EmailtempletRepository instance.
     *
     * @var App\Repositories\EmailtempletRepository
     */
    protected $emailtemplet_gestion;

    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\EmailtempletRepository $blog_gestion
     * @return void
     */
    public function __construct(
        EmailtempletRepository $emailtemplet_gestion
    )
    {
        $this->emailtemplet_gestion = $emailtemplet_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('manage_emailTemplet',Auth::user()))
        {
            return Redirect::back();
        }
        $type = $request->has('Etype') && $request->get('Etype') != null ? $request->get('Etype') : 1;
        $email = $this->emailtemplet_gestion->index($type);
        return view('email_templet.index',['email'=>$email,'type'=>$type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manage_emailTemplet',Auth::user()))
        {
            return Redirect::back();
        }
        $this->emailtemplet_gestion->store($request->all());
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
