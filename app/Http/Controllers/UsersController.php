<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\RoleRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\UserStatusRepository;
use App\Repositories\AttributeRepository;
use App\Repositories\SystemInfoRepository;
use Input,Redirect,Excel,Gate,Auth,Validator,Response;
class UsersController extends Controller
{

    /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user_gestion;
    /**
     * The RoleRepository instance.
     *

     * @var App\Repositories\RoleRepository
     */
    protected $role_gestion;

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\UserStatusRepository
     */
    protected $userStatus_gestion;
    protected $language_gestion;
    /**
     * The Organization instance.
     *
     * @var App\Repositories\OrganizationRepository
     */
    protected $organization_gestion;
    protected $attribute_gestion;

    protected $info_gestion;
    /**
     * Create a new UserController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion

     * @return void
     */
    public function __construct(
        UserRepository $user_gestion,
        RoleRepository $role_gestion,
        LanguageRepository $language_gestion,
        UserStatusRepository $userStatus_gestion,
        OrganizationRepository $organization_gestion,
        AttributeRepository $attribute_gestion,
        SystemInfoRepository $info_gestion
    )
    {
        $this->user_gestion = $user_gestion;
        $this->userStatus_gestion = $userStatus_gestion;
        $this->language_gestion = $language_gestion;
        $this->role_gestion = $role_gestion;
        $this->organization_gestion = $organization_gestion;
        $this->attribute_gestion = $attribute_gestion;
        $this->info_gestion = $info_gestion;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $res = $this->user_gestion->index(10,$inputs);
        $users = $res['user'];
        $status = 2;
        $userStatusSelects = $this->userStatus_gestion->getAllSelect();
         $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $userStatusSelects[0] = trans('table.both');
        $a = $inputs;
        $number = $res['number'];
         $manages = $this->organization_gestion->getManageName(isset($inputs['manage_id'])?$inputs['manage_id']:"");
        return view('users.index',compact('users','status','userStatusSelects','a','manages','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','number'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $sexSelects = $this->attribute_gestion->getAllSexSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('users.create', array_merge(
            compact('firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','attribute','sexSelects'),
            $this->role_gestion->getAllSelect(),
            $this->language_gestion->getAllSelect()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $info = $this->info_gestion->info();
        if($info->user_email)
        {
            $this->validate($request, [
                'name'         => 'required|regex:/^[a-zA-Z\d\-\_\.]+$/|between:1,20|unique:users,name',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|between:5,20|confirmed',
            ]);
        }
        else
        {
            $this->validate($request, [
                'name'         => 'required|regex:/^[a-zA-Z\d\-\_\.]+$/|between:1,20|unique:users,name',
                'password' => 'required|between:5,20|confirmed',
            ]);
        }

        $input = $request->only('password', 'password_confirmation');
        $v = Validator::make($input, [
            'password' => 'regex:/^(?![\d]+$)(?![a-zA-Z]+$)/',
        ]);
         if ( $v->fails() )
        {
            return redirect()->back()->withInput()->withErrors(['password'=>trans('error.00081')]);
        }
         $error = $this->user_gestion->checkApprove($request->input('approve'));
        if(!empty($error))
        {
            $error = trans('error.00055').implode(",",$error);
            return redirect()->back()->withErrors(['approve'=>$error])->withInput();
        }
        $res = $this->user_gestion->create($request->all());
        if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['error'])->withInput();
        }
        return redirect()->action('UsersController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($name)
    {
        
        $user = $this->user_gestion->getUserByName($name);
        if (Gate::denies('modify',$user))
        {
            return redirect()->back()->withErrors(['fail'=>trans('error.09998')]);
        }
        $manages = "";
        $manageIds = "";
        foreach($user->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }
        $userStatusSelects = $this->userStatus_gestion->getAllSelect();
        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $sexSelects = $this->attribute_gestion->getAllSexSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('users.edit',
                    array_merge(compact('user','manages','userStatusSelects','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','attribute','sexSelects','manageIds'),
                                $this->role_gestion->getAllSelect(),
                                $this->language_gestion->getAllSelect()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $info = $this->info_gestion->info();
        if($info->user_email)
        {
            $this->validate($request, [
                'name'         => 'required|regex:/^[a-zA-Z\d\-\_\.]+$/|between:1,20|unique:users,name,'.$id,
            'email' => 'required|email|max:255|unique:users,email,'.$id,
            ]);
        }
        else
        {
            $this->validate($request, [
                'name'         => 'required|regex:/^[a-zA-Z\d\-\_\.]+$/|between:1,20|unique:users,name,'.$id,
           
            ]);
        }
       
        $user = $this->user_gestion->getById($id);
        if (Gate::denies('modify',$user))
        {
            return Redirect::back();
        }

        $error = $this->user_gestion->checkApprove($request->input('approve'));
        if(!empty($error))
        {
            $error = trans('error.00055').implode(",",$error);
            return redirect()->back()->withErrors(['approve'=>$error])->withInput();
        }
        
        $res = $this->user_gestion->update($user->id,$request->all());
        if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['error'])->withInput();
        }
        if ( $user->name == \Request::user()->name )
        {
            return redirect('profile');
        }
        else
        {
            return redirect()->action('UsersController@index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($name)
    {

        $user = $this->user_gestion->getUserByName($name);
        if (!Gate::denies('delete',$user))
        {
            $this->user_gestion->closeUser($user->id);
        }
        return Redirect::back();
    }

   

    public function searchByDepartment()
    {
        $department_id = Input::has('id') && Input::get('id') !== '#' ? Input::get('id') : 0;
        if($department_id == 0)
        {
            $res = $this->user_gestion->index(10);
            $users = $res['user'];
        }
        else
        {
            $users = $this->user_gestion->searchByDepatment(10,$department_id);
        }
        return response()->json($users);

    }

    public function usersExcel()
    {

    }

    public function exampleExcel()
    {
        /*
        $progs = $this->user_gestion->exampleExcel();
        Excel::create('User Info Template', function($excel) use($progs)
                      {

                          $excel->sheet('User Info Template', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                      })->export('xls');
        */

         $file = "User Info Template.xls";
        $url = storage_path()."/app/example/".$file;
        return Response::download($url);
    }



    public function profile()
    {
        $user = Auth::user();
        if (Gate::denies('modify',$user))
        {
            return Redirect::back();
        }
        
        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $sexSelects = $this->attribute_gestion->getAllSexSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('users.show',
                    array_merge(compact('user','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','attribute','sexSelects'),
                                $this->role_gestion->getAllSelect(),
                                $this->language_gestion->getAllSelect()));
    }




    public function changePassword( Request $request, $id )
    {
        $input = $request->only('new_password', 'new_password_confirmation');
        $v = Validator::make($input, [
            'new_password' => 'required|between:5,20|confirmed',
        ]);
        
        if ( $v->fails() )
        {
            return redirect()->back()->withErrors($v->errors());
        }
         $v = Validator::make($input, [
            'new_password' => 'regex:/^(?![\d]+$)(?![a-zA-Z]+$)/',
        ]);
         if ( $v->fails() )
        {
            return redirect()->back()->withErrors(['new_password'=>trans('error.00081')]);
        }
        $user = $this->user_gestion->getById($id);
        if (Gate::denies('modify',$user))
        {
            return Redirect::back();
        }

         $this->user_gestion->changePassword($user->id,$request->all());
       
        if ( $user->name == \Request::user()->name )
        {
            return redirect('profile');
        }
        else
        {
            return redirect()->action('UsersController@index');
        }
    }

}
