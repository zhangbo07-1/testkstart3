<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CatalogRepository;
use Input,Redirect;
class CatalogsController extends Controller
{

    /**
     * The CatalogRepository instance.
     *
     * @var App\Repositories\CatalogRepository
     */
    protected $catalog_gestion;

    /**
     * Create a new UserController instance.
     *
     * @param  App\Repositories\CatalogRepository $catalog_gestion

     * @return void
     */
    public function __construct(
        CatalogRepository $catalog_gestion)
    {
        $this->catalog_gestion = $catalog_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('catalog/index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('catalog/create',compact('id'));
    }




    public function saveCreate(Request $request,$id)
    {

        $name = $request->input('name');
        $res = $this->catalog_gestion->create($id,$name);
        return redirect()->back()->withErrors(array('success'=>trans('error.10000').','.trans('table.could_add_continue')));
    }


     public function rename($id)
    {

        $node = $this->catalog_gestion->getById($id);
        return view('catalog/edit',compact('node'));
    }


    public function saveRename(Request $request,$id)
    {
        $name = $request->input('name');
        $res = $this->catalog_gestion->rename($id,$name);
        return redirect()->back()->withErrors(array('success'=>trans('error.10000')));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        if(Input::has('operation'))
        {
            $operation = Input::get('operation');
            try {
                $rslt = null;
                switch($operation) {
                    case 'get_node':
                        $node = Input::has('id') && Input::get('id') !== '#' ? Input::get('id') : 0;
                        $catalog = Input::has('catalog') && Input::get('catlog') !== '' ? Input::get('catalog') : '/';
                         $NUM = Input::has('NUM') && Input::get('NUM') !== '' ? Input::get('NUM') : false;
                         $rslt = $this->catalog_gestion->lst($node,$catalog,$NUM);
                        break;


                    case 'delete_node':
                        $node = Input::has('id') && Input::get('id') !== '#' ? Input::get('id') : '/';
                        $rslt = $this->catalog_gestion->remove($node);
                        break;

                case 'search_node':
                    $keyword = Input::has('keyword') ? Input::get('keyword') : '/';
                    $rslt = $this->catalog_gestion->search($keyword);
                    break;
                    default:
                        break;
                }
                return response()->json($rslt);

            }
            catch (Exception $e) {
                header($_SERVER["SERVER_PROTOCOL"] . ' 500 Server Error');
                header('Status:  500 Server Error');
                // echo $e->getMessage();
            }

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function catalogSelect(Request $request)
    {
        $catalog_name = $request->has('catalog_name')?$request->input('catalog_name'):"";
        return view('catalog.catalogSelect',compact('catalog_name'));
    }
}
