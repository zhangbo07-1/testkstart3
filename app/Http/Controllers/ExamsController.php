<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CatalogRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\BankRepository;
use App\Repositories\ExamRepository;
use App\Repositories\CourseRepository;
use App\Repositories\AttributeRepository;
use App\Repositories\ReportRepository;
use App\Repositories\CourseClassRepository;
use Input,Gate,Auth,Validator,Redirect;
class ExamsController extends Controller
{

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseCatalogRepository
     */
    protected $catalog_gestion;

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseRepository
     */
    protected $course_gestion;
    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseRepository
     */
    protected $language_gestion;
    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\ExamRepository
     */
    protected $exam_gestion;
    protected $bank_gestion;
    protected $attribute_gestion;
    protected $report_gestion;
    protected $class_gestion;

    /**
     * Create a new CourseController instance.
     *
     * @param  App\Repositories\CourseLanguageRepository $courseLanguage_gestion
     * @param  App\Repositories\CourseCatalogRepository $courseCatalog_gestion
     * @param  App\Repositories\CourseRepository $course_gestion
     * @param  App\Repositories\ExamRepository $exam_gestion
     * @return CourseController
     */
    public function __construct(
        LanguageRepository $language_gestion,
        CourseRepository $course_gestion,
        BankRepository $bank_gestion,
        ExamRepository $exam_gestion,
        CatalogRepository $catalog_gestion,
        AttributeRepository $attribute_gestion,
        ReportRepository $report_gestion,
        CourseClassRepository $class_gestion )
    {
        $this->courseLanguage_gestion = $language_gestion;
        $this->exam_gestion = $exam_gestion;
        $this->catalog_gestion = $catalog_gestion;
        $this->bank_gestion = $bank_gestion;
        $this->class_gestion = $class_gestion;
        $this->course_gestion = $course_gestion;
        $this->attribute_gestion = $attribute_gestion;
        $this->report_gestion = $report_gestion;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('manage_exam',Auth::user()))
        {
            return Redirect::back();
        }
         $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $exams = $this->exam_gestion->index(10,$inputs);
        $a = $inputs;
        $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        return view('exams.index',compact('exams','a','catalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manages = "";
        $manageIds = "";
        foreach(Auth::user()->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }
        return view('exams.create',compact('manages','manageIds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manage_exam',Auth::user()))
        {
            return Redirect::back();
        }
        $this->validate($request, [
            'exam_id'          => 'required|regex:/^[a-zA-Z\d\-\_]+$/|max:100|unique:exams,exam_id',
            'title'            => 'required',
            'exam_time'        => 'required|integer|between:0,60',
            'limit_times'      => 'required|integer|between:0,1000',
            'pass_score'       => 'required|integer|between:0,1000',
            'emailed'          => 'integer|between:0,1',
            'if_link'          => 'integer|between:0,1',
            
        ]);
        if(!$request->has('catalog_id'))
        {
            return redirect()->back()->withErrors(array('catalog_id'=>trans('error.00017')))->withInput();
        }

         $res = $this->exam_gestion->store($request->all(),$request->user()->id);
         if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['error'])->withInput();
        }
         return redirect()->route('exams.edit', ['id' => $res['id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($exam_id)
    {
         $exam = $this->exam_gestion->getExamByExamId($exam_id);
         if (Gate::denies('maintain',$exam))
        {
            return Redirect::back();
        }
         $manages = "";
         $manageIds = "";
        foreach($exam->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }
        return view('exams.edit', compact('exam','manages','manageIds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $this->validate($request, [
            'exam_id'          => 'required|regex:/^[a-zA-Z\d\-\_]+$/|max:100|unique:exams,exam_id,'.$id,
            'title'            => 'required',
            'exam_time'        => 'required|integer|between:0,60',
            'limit_times'      => 'required|integer|between:0,1000',
            'pass_score'       => 'required|integer|between:0,1000',
            'emailed'          => 'integer|between:0,1',
            'if_link'          => 'integer|between:0,1',
            
        ]);
        if(!$request->has('catalog_id'))
        {
            return redirect()->back()->withErrors(array('catalog_id'=>trans('error.00017')))->withInput();
        }

        $exam = $this->exam_gestion->getById($id);
        if (Gate::denies('maintain',$exam))
        {
            return Redirect::back();
        }

         $res = $this->exam_gestion->update($request->all(),$id);
         if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['error'])->withInput();
        }
         return redirect()->route('exams.edit', ['id' => $res['id']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exam = $this->exam_gestion->getById($id);
         if (Gate::denies('delete',$exam))
        {
            return Redirect::back();
        }
         $this->exam_gestion->destroy($id);
         return redirect()->to('exams');
    }

    public function delete($exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
         if (Gate::denies('delete',$exam))
        {
            return Redirect::back();
        }
         $manages = "";
        foreach($exam->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
        }
        return view('exams.delete', compact('exam','manages'));
    }


    public function combinate( $exam_id )
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        if (Gate::denies('maintain',$exam))
        {
            return Redirect::back();
        }
        $banks = $this->bank_gestion->getAll();
        return view('exams.combinate',compact('exam','banks'));
    }

    public function assignTeacher($exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        if (Gate::denies('modify',$exam))
        {
            return Redirect::back();
        }

        return view('exams.teachers', compact('exam'));
    }

    public function distribute($exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        if (Gate::denies('modify',$exam))
        {
            return Redirect::back();
        }
        
        if(!$exam->if_link)
        {
            $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('exams.distribute',compact('exam','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','attribute'));
        }else
        {
            return Redirect::back();
        }
    }

    public function preview($exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        if (Gate::denies('maintain',$exam))
        {
            return Redirect::back();
        }
        $questions = $this->exam_gestion->preview($exam->id);
        
        return view('exams/preview',compact('questions','exam'));
    }

    

    public function courseAnswer($course_id,$exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $res = $this->exam_gestion->courseAnswer($course->id,$exam->id);
        if($res['status'])
        {
            return $res['message'];
        }
        $distribution = $res['distribution'];
        $if_not_marked = $res['if_not_marked'];
        $Qnum = $res['number'];
        return view('exams.info',compact('distribution','if_not_marked','Qnum'));
    }

    public function examAnswer($exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        $res = $this->exam_gestion->examAnswer($exam->id);
        if($res['status'])
        {
            return $res['message'];
        }
        $distribution = $res['distribution'];
        $if_not_marked = $res['if_not_marked'];
        $Qnum = $res['number'];
        return view('exams.info',compact('distribution','if_not_marked','Qnum'));
        
        
    }

    public function answer($distribute_id)
    {
         $res = $this->exam_gestion->answer($distribute_id);
         if($res['status'])
        {
             return redirect()->back()->withErrors($res['message'])->withInput();
        }
        $questions = $res['questions'];
        $list = $res['list'];
        $distribution = $res['distribution'];
        return view('exams/start',compact('distribution','questions','list'));
    }


    public function info($exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        return view('exams.info',compact('exam'));
    }


    public function examQR($exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        return view('exams.QR',compact('exam'));
    }

    public function courseExamQR($course_id,$exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        return view('exams.QR',compact('exam','course'));
    }




    public function report($course_id,$exam_id,Request $request)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        if(empty($inputs['course_id']) || empty($inputs['exam_id']))
        {
            $inputs['course_id'] = $course->id;
            $inputs['exam_id'] = $exam->id;
        }
        $res = $this->report_gestion->getExam(5,$inputs);
        $records = $res['records'];
        $examResults = $res['examResults'];
        $trueSelect = $res['trueSelect'];
        $selectResults = $res['selectResults'];
        $questionSelects = $res['questionSelects'];
        $a = $inputs;
        $classes =  $this->class_gestion->getAllSelect($course->id);
        return view('exams.report',compact('records','examResults','trueSelect','selectResults','questionSelects','a','classes','course'));
    }
}
