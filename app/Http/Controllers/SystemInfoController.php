<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SystemInfoRepository;
use Gate,Auth,Redirect;
class SystemInfoController extends Controller
{
    /**
     * The PostRepository instance.
     *
     * @var App\Repositories\PostRepository
     */
    protected $info_gestion;

    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\PostRepository $blog_gestion
     * @return void
     */
    public function __construct(
        SystemInfoRepository $info_gestion
    )
    {
        $this->info_gestion = $info_gestion;

    }
    public function logo()
    {
        if (Gate::denies('manage_info',Auth::user()))
        {
            return Redirect::back();
        }
        return view('system_info/logo');
    }
    public function saveLogo(Request $request)
    {
        if (Gate::denies('manage_info',Auth::user()))
        {
            return Redirect::back();
        }
        $res = $this->info_gestion->saveLogo($request->all());
        return redirect()->back()->withInput()->withErrors(array('image'=>$res));

    }
    public function info()
    {
        if (Gate::denies('manage_info',Auth::user()))
        {
            return Redirect::back();
        }
        $info = $this->info_gestion->info();
        return view('system_info/info')->withInfo($info);

    }

    public function saveInfo(Request $request)
    {
        $this->validate($request, [
            'company' => 'required|alpha|max:255',
            'phone' => 'required|alpha_dash|max:255',
            'email' => 'required|email|max:255',

        ]);
        $this->info_gestion->saveInfo($request->all());
        return redirect()->back()->withErrors(array('success'=>trans('error.10000')));

    }


    public function censor()
    {
        if (Gate::denies('manage_censor',Auth::user()))
        {
            return Redirect::back();
        }
        $info = $this->info_gestion->info();
        return view('system_info/censor')->withInfo($info);
    }

    public function saveCensor(Request $request)
    {
        $this->info_gestion->saveCensor($request->all());
        return redirect()->back()->withErrors(array('success'=>trans('error.10000')));

    }

    public function email()
    {
        if (Gate::denies('manage_suffix',Auth::user()))
        {
            return Redirect::back();
        }
         $info = $this->info_gestion->info();
        return view('system_info/email')->withInfo($info);
    }


    public function saveEmail(Request $request)
    {
        $res = $this->info_gestion->saveEmail($request->all());
        return redirect()->back()->withErrors($res['message']);

    }






}
