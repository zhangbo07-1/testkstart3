<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SurveyRepository;
use App\Repositories\MessageRepository;
use App\Repositories\CourseRecordRepository;
class MenuController extends Controller
{
    protected $survey_gestion;
    protected $message_gestion;
    protected $record_gestion;
    public function __construct(
        SurveyRepository $survey_gestion,
        CourseRecordRepository $record_gestion,
        MessageRepository $message_gestion
    )
    {
        $this->survey_gestion = $survey_gestion;
        $this->record_gestion = $record_gestion;
        $this->message_gestion = $message_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveys = $this->survey_gestion->lst(5,1);
        $ranks = $this->record_gestion->getRanks(5);
        $messages = $this->message_gestion->lst(5);
        return view('menu.index',compact('surveys','ranks','messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
