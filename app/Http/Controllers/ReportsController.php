<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\ReportRepository;
use App\Repositories\QuestionRepository;
use App\Repositories\CourseRecordRepository;
use App\Repositories\ExamRecordRepository;
use App\Repositories\SurveyRepository;
use App\Repositories\SurveyResultRepository;
use App\Repositories\ExamDistributionRepository;
use App\Repositories\UserStatusRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\CatalogRepository;
use App\Repositories\AttributeRepository;
use App\Repositories\RecordTypeRepository;
use Excel;
class ReportsController extends Controller
{

    protected $user_gestion;
    protected $report_gestion;

    protected $userStatus_gestion;

    protected $courseRecord_gestion;
    protected $examRecord_gestion;
    protected $examDistribution_gestion;
    protected $survey_gestion;
    protected $surveyResult_gestion;
    protected $question_gestion;
    protected $organization_gestion;
    protected $catalog_gestion;
    protected $attribute_gestion;
    protected $recordType_gestion;
    public function __construct(
        UserRepository $user_gestion,
        ReportRepository $report_gestion,
        UserStatusRepository $userStatus_gestion,
        CourseRecordRepository $courseRecord_gestion,
        ExamDistributionRepository $examDistribution_gestion,
        SurveyRepository $survey_gestion,
        SurveyResultRepository $surveyResult_gestion,
        QuestionRepository $question_gestion,
        RecordTypeRepository $recordType_gestion,
        ExamRecordRepository $examRecord_gestion,
        OrganizationRepository $organization_gestion,
        CatalogRepository $catalog_gestion,
        AttributeRepository $attribute_gestion
    )
    {
        $this->user_gestion = $user_gestion;
        $this->report_gestion = $report_gestion;
        $this->userStatus_gestion = $userStatus_gestion;
        $this->courseRecord_gestion = $courseRecord_gestion;
        $this->examRecord_gestion = $examRecord_gestion;
        $this->survey_gestion = $survey_gestion;
        $this->surveyResult_gestion = $surveyResult_gestion;
        $this->question_gestion = $question_gestion;
        $this->examDistribution_gestion = $examDistribution_gestion;
        $this->organization_gestion = $organization_gestion;
        $this->catalog_gestion = $catalog_gestion;
        $this->recordType_gestion = $recordType_gestion;
        $this->attribute_gestion = $attribute_gestion;
    }
    public function getUser(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $res =  $this->user_gestion->index(10,$inputs);
        $users = $res['user'];
        $status = 2;
        $userStatusSelects = $this->userStatus_gestion->getAllSelect();
        $userStatusSelects[0] = trans('table.both');
        $attribute = $this->attribute_gestion->getAttribute();
        $a = $inputs;
        $manages = $this->organization_gestion->getManageName(isset($inputs['manage_id'])?$inputs['manage_id']:"");

        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        return view('reports.user',compact('users','status','userStatusSelects','a','manages','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','attribute'));
    }

    public function getExam(Request $request)
    {
         $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
         $res = $this->report_gestion->getExam(5,$inputs);
        $records = $res['records'];
        $examResults = $res['examResults'];
        $trueSelect = $res['trueSelect'];
        $selectResults = $res['selectResults'];
        $questionSelects = $res['questionSelects'];



        $status = 2;
        $userStatusSelects = $this->userStatus_gestion->getAllSelect();
        $userStatusSelects[0] = trans('table.both');
         $a = $inputs;
       $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        $attribute = $this->attribute_gestion->getAttribute();
 $manages = $this->organization_gestion->getManageName(isset($inputs['manage_id'])?$inputs['manage_id']:"");

        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();


        return view('reports.exam',compact('records','examResults','trueSelect','selectResults','questionSelects','a','catalogs','manages','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','attribute','userStatusSelects','status'));
    }

    public function getCourse(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();

        $res = $this->report_gestion->getCourse(10,$inputs);
        $records = $res['records'];
        $examRecord = $res['examRecord'];
        $evaluationRecord = $res['evaluationRecord'];
        $evaResult = $res['evaResult'];

        $surveyRecord = $res['surveyRecord'];
        $surveyResult = $res['surveyResult'];
        $status = 2;
        $userStatusSelects = $this->userStatus_gestion->getAllSelect();
        $userStatusSelects[0] = trans('table.both');

         $a = $inputs;
       $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        $attribute = $this->attribute_gestion->getAttribute();
 $manages = $this->organization_gestion->getManageName(isset($inputs['manage_id'])?$inputs['manage_id']:"");

        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $types =  $this->recordType_gestion->getAllSelect();

        return view('reports.course',compact('records','examRecord','evaluationRecord','evaResult','surveyRecord','surveyResult','a','catalogs','manages','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','types','attribute','userStatusSelects','status'));

    }

    public function getEvaluation(Request $request)
    {

        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $evaluationRecords = $this->survey_gestion->getRecord(10,$inputs);
        $a = $inputs;
        return view('reports.evaluation',compact('evaluationRecords','a'));

    }

    public function getItem($id)
    {
        $record = $this->survey_gestion->getRecordById($id);
        $result = $this->surveyResult_gestion->result($record->id);
        $evaluation = $record->survey;
        return view('reports.item',compact('result','record'));
    }
    public function getQuestion(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $a = $inputs;
        $res = $this->question_gestion->getReport(10,$inputs);
        return view('reports.question',array_merge($res,compact('a')));
    }

    public function userExcel(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):array();
        $res = $this->user_gestion->index(99999,$inputs);
        $users = $res['user'];
        $progs = $this->user_gestion->excel($users);
        Excel::create('User Info Report', function($excel) use($progs)
                      {

                          $excel->sheet('User Info Report', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                                        })->export('xls');
    }

    public function examExcel(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):array();
        $progs = $this->report_gestion->examExcel($inputs);
         Excel::create('Exam Record Report', function($excel) use($progs)
                      {

                          $excel->sheet('Exam Record Report', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                                        })->export('xls');


    }


     public function courseExcel(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):array();

        $progs = $this->report_gestion->courseExcel($inputs);

        Excel::create('Training Record Report', function($excel) use($progs)
                      {

                          $excel->sheet('Training Record Report', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                                        })->export('xls');



    }

    public function questionExcel(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):array();
        $res = $this->question_gestion->getReport(99999,$inputs);
        $progs = $this->question_gestion->excel($res);
        Excel::create('Question Analysis Report', function($excel) use($progs)
                      {

                          $excel->sheet('Question Analysis Report', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                                        })->export('xls');
    }



    public function itemExcel($id ,Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):null;
        $record = $this->survey_gestion->getRecordById($id);
        $result = $this->surveyResult_gestion->result($record->id,$inputs);
        $progs = $this->report_gestion->itemExcel($result,$record);
        Excel::create('Evaluation Report', function($excel) use($progs)
                      {

                          $excel->sheet('Evaluation Report', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                                        })->export('xls');
    }





}
