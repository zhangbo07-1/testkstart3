<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\AttributeRepository;
use Gate,Auth,Response;
class AttributesController extends Controller
{
    protected $attribute_gestion;

    /**
     * Create a new UserController instance.
     *
     * @param  App\Repositories\CatalogRepository $catalog_gestion

     * @return void
     */
    public function __construct(
        AttributeRepository $attribute_gestion)
    {
        $this->attribute_gestion = $attribute_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function firstAttribute()
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('attribute.firstAttribute',compact('firstAttributeSelects','attribute'));
    }


    public function firstAttributeRename(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->firstAttributeRename($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }




    public function firstAttributeAdd(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $title = $request->input('add_attribute');
        $res = $this->attribute_gestion->firstAttributeAdd($title);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }


    public function firstAttributeUpdate(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->firstAttributeUpdate($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function firstAttributeDelete(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $id = $request->has('delete_attribute')?$request->input('delete_attribute'):0;
        $res = $this->attribute_gestion->firstAttributeDelete($id);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }


    public function firstAttributeImport(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }

        if($request->hasFile('file'))
        {
            $extension = $request->file('file')->getClientOriginalExtension();
            if($extension != 'xls'&&$extension != 'xlsx')
            {
                return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00006')));
            }

            $res = $this->attribute_gestion->firstAttributeImport($request->file('file'));
            return redirect()->back()->withInput()->withErrors($res['message']);

        }
        else
        {
            return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00007')));
        }

    }






    public function secondAttribute()
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('attribute.secondAttribute',compact('secondAttributeSelects','attribute'));
    }


    public function secondAttributeRename(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->secondAttributeRename($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }




    public function secondAttributeAdd(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $title = $request->input('add_attribute');
        $res = $this->attribute_gestion->secondAttributeAdd($title);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }


    public function secondAttributeUpdate(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->secondAttributeUpdate($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function secondAttributeDelete(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $id = $request->has('delete_attribute')?$request->input('delete_attribute'):0;
        $res = $this->attribute_gestion->secondAttributeDelete($id);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function secondAttributeImport(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }

        if($request->hasFile('file'))
        {
            $extension = $request->file('file')->getClientOriginalExtension();
            if($extension != 'xls'&&$extension != 'xlsx')
            {
                return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00006')));
            }

            $res = $this->attribute_gestion->secondAttributeImport($request->file('file'));
            return redirect()->back()->withInput()->withErrors($res['message']);

        }
        else
        {
            return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00007')));
        }

    }




    public function thirdAttribute()
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('attribute.thirdAttribute',compact('thirdAttributeSelects','attribute'));
    }


    public function thirdAttributeRename(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->thirdAttributeRename($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }




    public function thirdAttributeAdd(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $title = $request->input('add_attribute');
        $res = $this->attribute_gestion->thirdAttributeAdd($title);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }


    public function thirdAttributeUpdate(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->thirdAttributeUpdate($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function thirdAttributeDelete(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $id = $request->has('delete_attribute')?$request->input('delete_attribute'):0;
        $res = $this->attribute_gestion->thirdAttributeDelete($id);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function thirdAttributeImport(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }

        if($request->hasFile('file'))
        {
            $extension = $request->file('file')->getClientOriginalExtension();
            if($extension != 'xls'&&$extension != 'xlsx')
            {
                return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00006')));
            }

            $res = $this->attribute_gestion->thirdAttributeImport($request->file('file'));
            return redirect()->back()->withInput()->withErrors($res['message']);

        }
        else
        {
            return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00007')));
        }

    }






    public function fourthAttribute()
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('attribute.fourthAttribute',compact('fourthAttributeSelects','attribute'));
    }


    public function fourthAttributeRename(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->fourthAttributeRename($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }




    public function fourthAttributeAdd(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $title = $request->input('add_attribute');
        $res = $this->attribute_gestion->fourthAttributeAdd($title);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function fourthAttributeUpdate(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->fourthAttributeUpdate($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function fourthAttributeDelete(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $id = $request->has('delete_attribute')?$request->input('delete_attribute'):0;
        $res = $this->attribute_gestion->fourthAttributeDelete($id);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }


    public function fourthAttributeImport(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }

        if($request->hasFile('file'))
        {
            $extension = $request->file('file')->getClientOriginalExtension();
            if($extension != 'xls'&&$extension != 'xlsx')
            {
                return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00006')));
            }

            $res = $this->attribute_gestion->fourthAttributeImport($request->file('file'));
            return redirect()->back()->withInput()->withErrors($res['message']);

        }
        else
        {
            return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00007')));
        }

    }







    public function fifthAttribute()
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('attribute.fifthAttribute',compact('fifthAttributeSelects','attribute'));
    }


    public function fifthAttributeRename(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->fifthAttributeRename($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }




    public function fifthAttributeAdd(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $title = $request->input('add_attribute');
        $res = $this->attribute_gestion->fifthAttributeAdd($title);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function fifthAttributeUpdate(Request $request)
    {

        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $res = $this->attribute_gestion->fifthAttributeUpdate($request->all());
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function fifthAttributeDelete(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }
        $id = $request->has('delete_attribute')?$request->input('delete_attribute'):0;
        $res = $this->attribute_gestion->fifthAttributeDelete($id);
        return redirect()->back()->withErrors($res['message'])->withInput();
    }

    public function fifthAttributeImport(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('manage_attribute',$user))
        {
            return Redirect::back();
        }

        if($request->hasFile('file'))
        {
            $extension = $request->file('file')->getClientOriginalExtension();
            if($extension != 'xls'&&$extension != 'xlsx')
            {
                return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00006')));
            }

            $res = $this->attribute_gestion->fifthAttributeImport($request->file('file'));
            return redirect()->back()->withInput()->withErrors($res['message']);

        }
        else
        {
            return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00007')));
        }

    }




    public function example($name)
    {
        $file = "attribute example.xls";
        $url = storage_path()."/app/example/".$file;
        return Response::download($url,$name.$file);
    }

    public function sendLimit()
    {
        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('courses.sendLimit',compact('fifthAttributeSelects','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','attribute'));
    }

}
