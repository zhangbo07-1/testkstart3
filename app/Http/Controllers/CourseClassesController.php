<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CourseRecordRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CourseRepository;
use App\Repositories\CourseClassRepository;
use App\Repositories\AttributeRepository;
use Gate;
class CourseClassesController extends Controller
{
    protected $course_gestion;
    protected $record_gestion;
    protected $courseClass_gestion;
    protected $attribute_gestion;


     public function __construct(
         CourseRepository $course_gestion,
         CourseRecordRepository $record_gestion,
         CourseClassRepository $courseClass_gestion,
         AttributeRepository $attribute_gestion
    )
    {
        $this->course_gestion = $course_gestion;
         $this->record_gestion = $record_gestion;
        $this->courseClass_gestion = $courseClass_gestion;
        $this->attribute_gestion = $attribute_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course) || $course->type != 2)
        {
            return Redirect::back();
        }
        $classes = $this->courseClass_gestion->lst($course->id,10);
        return view('class.index', compact('course','classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course) || $course->type != 2)
        {
            return Redirect::back();
        }
        return view('class.create', compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($course_id,Request $request)
    {
         $this->validate($request, [
            'title'              => 'required|max:255',
        ]);
         $res = $this->courseClass_gestion->store($request->all(),$request->user()->id);
         return redirect()->to('courses/'.$course_id.'/class');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($course_id,$id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $class = $this->courseClass_gestion->getById($id);


        $evaluationRecord = $course->surveyRecords()->where('type',2)->first();
        $surveyRecord = $course->surveyRecords()->where('type',3)->first();

        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('class.show', compact('course','class','evaluationRecord','surveyRecord','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($course_id,$id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $class = $this->courseClass_gestion->getById($id);
        return view('class.edit', compact('course','class'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $course_id,$id)
    {
        $this->validate($request, [
            'title'              => 'required|max:255',
        ]);
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $res = $this->courseClass_gestion->update($request->all(),$id);
         if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['message'])->withInput();
        }
         return redirect()->to('courses/'.$course_id.'/class');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($course_id,$id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $this->courseClass_gestion->destroy($id);
        return redirect()->to('courses/'.$course_id.'/class');
    }

    public function distribute($course_id,$id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $class = $this->courseClass_gestion->getById($id);

        if (Gate::denies('modify',$course) || !$course->pushed)
        {
            return Redirect::back();
        }
        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('class.distribute', compact('course','class','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','attribute'));
    }

    public function getClassDistributeUserList(Request $request,$id)
    {
        $distributeUsers = $this->record_gestion->getClassDistributeUser(5,$request->all(),$id);

        return response()->json($distributeUsers);
    }

    public function getClassUnDistributeUserList(Request $request,$id)
    {
        $unDistributeUsers = $this->record_gestion->getClassUnDistributeUser(5,$request->all(),$id);
         return response()->json($unDistributeUsers);
    }

    public function getClassRecords(Request $request,$id)
    {
         $records = $this->record_gestion->getClassRecords(10,$request->all(),$id);
         return response()->json($records);

    }


    public function addClassRecords(Request $request,$id)
    {
        $class = $this->courseClass_gestion->getById($id);
        $course = $class->course;
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->addClassRecords($request->all(),$id);
        return response()->json($res);
    }

    public function deleteClassRecords(Request $request,$id)
    {
        $class = $this->courseClass_gestion->getById($id);
        $course = $class->course;
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->deleteClassRecords($request->all(),$id);
        return response()->json($res);
    }



    public function classRedistribute(Request $request,$id)
    {
        $class = $this->courseClass_gestion->getById($id);
        $course = $class->course;
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->classRedistribute($request->all(),$course->id,$id);
        return response()->json($res);
    }



    public function forSign($course_id,$class_id,$lesson_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $class = $this->courseClass_gestion->getById($class_id);
        $lesson = $this->courseClass_gestion->getLessonById($lesson_id);
        return view('class.sign', compact('course','class','lesson'));
    }

     public function forEnroll($course_id,$class_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $class = $this->courseClass_gestion->getById($class_id);
        return view('class.enroll', compact('course','class'));
    }


    public function sign($course_id,$class_id,$lesson_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $res = $this->record_gestion->classSign($course->id,$class_id,$lesson_id);
        return $res['message'];
    }


    public function enroll($course_id,$id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('apply',$course))
        {
            return trans('error.09998');
        }
        $res = $this->record_gestion->classEnroll($course->id,$id);
        return $res['message'];

    }

    public function beOver(Request $request,$course_id,$id)
    {
        $course = $this->course_gestion->getById($course_id);
        if (Gate::denies('maintain',$course))
        {
             return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->beOver($request->all(),$id);
        return response()->json($res);

    }

    public function beNotOver(Request $request,$course_id,$id)
    {
        $course = $this->course_gestion->getById($course_id);
        if (Gate::denies('maintain',$course))
        {
             return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->beNotOver($request->all(),$id);
        return response()->json($res);

    }



}
