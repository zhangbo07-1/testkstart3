<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\LanguageRepository;
use App\Repositories\CatalogRepository;
use App\Repositories\CourseRepository;
use App\Repositories\SurveyRepository;
use App\Repositories\CourseWareRepository;
use App\Repositories\CourseMaintainRepository;
use App\Repositories\CourseClassRepository;
use Input,Gate,Auth,Redirect;
class CoursesController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseLanguageRepository
     */
    protected $language_gestion;

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseCatalogRepository
     */
    protected $catalog_gestion;

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseCatalogRepository
     */
    protected $user_gestion;

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseRepository
     */
    protected $course_gestion;

    protected $survey_gestion;
    protected $ware_gestion;
    protected $class_gestion;





    public function __construct(
        LanguageRepository $language_gestion,
        CourseRepository $course_gestion,
        CatalogRepository $catalog_gestion,
        SurveyRepository $survey_gestion,
        CourseMaintainRepository $maintain_gestion,
        CourseWareRepository $ware_gestion,
        CourseClassRepository $class_gestion
    )
    {
        $this->language_gestion = $language_gestion;
        $this->catalog_gestion = $catalog_gestion;
        $this->course_gestion = $course_gestion;
        $this->survey_gestion = $survey_gestion;
        $this->maintain_gestion = $maintain_gestion;
        $this->ware_gestion = $ware_gestion;
        $this->class_gestion = $class_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('manage_course',Auth::user()))
        {
            return Redirect::back();
        }
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $courses = $this->course_gestion->index(10,$inputs);
        $a = $inputs;
        $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        return view('courses.index',compact('courses','a','catalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manages = "";
        $manageIds = "";
        foreach(Auth::user()->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }
        return view('courses.create', array_merge(compact('manages','manageIds'),
                                                   $this->language_gestion->getAllSelect()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manage_course',Auth::user()))
        {
            return Redirect::back();
        }
        $this->validate($request, [
            'course_id'         => 'required|regex:/^[a-zA-Z\d\-\_]+$/|max:100|unique:courses,course_id',
            'language_id' => 'required|integer',
            'title'              => 'required',
            'hours'             => 'integer|between:0,1000',
            'minutes'           => 'integer|between:0,60',
            'limitdays'         => 'integer|between:0,1000',
            'if_order'          => 'integer|between:0,1',
            'emailed'           => 'integer|between:0,1',
            'if_send_new_user'  => 'integer|between:0,1',
        ]);
        if(!$request->has('catalog_id'))
        {
            return redirect()->back()->withErrors(array('catalog_id'=>trans('error.00017')))->withInput();
        }

         $res = $this->course_gestion->store($request->all(),$request->user()->id);
         if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['error'])->withInput();
        }
         if($request->has('type') && $request->input('type') == 2)
         {
              return redirect()->to('courses/'.$res['id'].'/class');
         }
         return redirect()->to('courses/'.$res['id'].'/upload');//, ['id' => $res['id']]);
    }

    /**
     * Display the specified resource
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $manages = "";
        $manageIds = "";
        foreach($course->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }
        $limit = $course->sendLimit;
        if($limit->attribute1_list || $limit->attribute2_list
        || $limit->attribute3_lis || $limit->attribute4_list
        || $limit->attribute5_list)
        {
            $limit_status = trans('course.is_set');
        }
        else
        {
            $limit_status = trans('course.not_set');
        }

        return view('courses.edit', array_merge(compact('course','manages','manageIds','limit_status'),
                                                $this->language_gestion->getAllSelect()));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'course_id'         => 'required|regex:/^[a-zA-Z\d\-\_]+$/|max:100|unique:courses,course_id,'.$id,
            'language_id' => 'required|integer',
            'title'              => 'required',
            'hours'             => 'integer|between:0,1000',
            'minutes'           => 'integer|between:0,60',
            'limitdays'         => 'integer|between:0,1000',
            'if_order'          => 'integer|between:0,1',
            'emailed'           => 'integer|between:0,1',
            'if_send_new_user'  => 'integer|between:0,1',

        ]);
        if(!$request->has('catalog_id'))
        {
            return redirect()->back()->withErrors(array('catalog_id'=>trans('error.00017')))->withInput();
        }

        $course = $this->course_gestion->getById($id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }

         $res = $this->course_gestion->update($request->all(),$id);
         if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['message'])->withInput();
        }
         return redirect()->route('courses.edit', ['id' => $res['id']])->withErrors($res['message']);
    }



    public function push($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('modify',$course))
        {
            return Redirect::back();
        }
        $ware = $this->ware_gestion->getLastWare($course->id);
        return view('courses.push',compact('course','ware'));


    }

    public function pushed($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('modify',$course))
        {
            return Redirect::back();
        }
        $res = $this->ware_gestion->push($course->id);
        return redirect()->route('courses.edit', ['id' => $course_id])->withErrors($res['message']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('delete',$course))
        {
            return Redirect::back();
        }
        $this->course_gestion->destroy($course->id);
        return redirect('courses/');
    }

    public function delete($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('delete',$course))
        {
            return Redirect::back();
        }
        $manages = "";
        foreach($course->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
        }

        return view('courses.delete', compact('course','manages'));

    }

    public function assignTeacher($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        return view('courses.teachers', compact('course'));
    }


    public function assignEvaluation($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $evaluationRecord = $course->surveyRecords()->where('type',2)->first();
        return view('courses.evaluation', compact('course','evaluationRecord'));
    }
    public function assignSurvey($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $surveyRecord = $course->surveyRecords()->where('type',3)->first();
        return view('courses.survey', compact('course','surveyRecord'));
    }


    public function assignExam( $course_id )
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        return view('courses.exam',compact('course'));

    }

    public function assignResource($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);

        if (!Gate::denies('maintain',$course))
        {
            $this->course_gestion->assignResource($course_id);
        }

        return Redirect::back();

    }

    public function info($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $evaluationRecord = $course->surveyRecords()->where('type',2)->first();
        $surveyRecord = $course->surveyRecords()->where('type',3)->first();
        $classRecord = $this->class_gestion->getRecords($course->id);
        return view('courses.info',compact('course','evaluationRecord','surveyRecord','classRecord'));
    }








}
