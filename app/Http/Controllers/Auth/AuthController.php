<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Validator;
use App\Http\Controllers\Controller;
use App\Repositories\LogRepository;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Repositories\SystemInfoRepository;
use App\Repositories\UserRepository;
use Auth;
class AuthController extends Controller
{
    /*
       |--------------------------------------------------------------------------
       | Registration & Login Controller
       |--------------------------------------------------------------------------
       |
       | This controller handles the registration of new users, as well as the
       | authentication of existing users. By default, this controller uses
       | a simple trait to add these behaviors. Why don't you explore it?
       |
     */
    protected $redirectTo = '/';
    protected $loginPath  = '/';
    protected $log_gestion;
    protected $user_gestion;
    protected $info_gestion;
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /** 
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(
        UserRepository $user_gestion,
        LogRepository $log_gestion,
        SystemInfoRepository $info_gestion
    )
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        $this->user_gestion = $user_gestion;
        $this->log_gestion = $log_gestion;
        $this->info_gestion = $info_gestion;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|regex:/^[a-zA-Z\d\-\_\.]+$/|between:1,20',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|between:5,20',
        ]);
    }

    public function loginUsername()
    {
        $info = $this->info_gestion->info();
        return $info->user_email ? 'email':'name';
    }


    public function postLogin( Request $request )
    {
        
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        $info = $this->info_gestion->info();
        $input = $request->all();//only('email', 'password');
        if($info->user_email)
        {
            $validator = Validator::make($input, [
                'email' => 'required|exists:users,email'

            ]);
            if ( $validator->fails() ) {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }

            $validator = Validator::make($input, [
                'password' => 'required|passcheckwith:email'

            ]);
            if ( $validator->fails() ) {
                return redirect()->back()->withErrors($validator->errors())->withInput();

            }

        }
        else
        {
            $validator = Validator::make($input, [
                'name' => 'required|exists:users,name'

            ]);
            if ( $validator->fails() ) {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }


            $validator = Validator::make($input, [
                'password' => 'required|passcheckwith:name'

            ]);
            if ( $validator->fails() ) {
                return redirect()->back()->withErrors($validator->errors())->withInput();

            }
        }
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ( $throttles && $this->hasTooManyLoginAttempts($request) ) {
            return $this->sendLockoutResponse($request);

        }


        $key = $request->input($this->loginUsername());
        $res = $this->user_gestion->checkStatus($key);
        if($res['type'])
        {
            $request->session()->put('user_id', $res['user_id']);
            return view('auth.notice')->withErrors($res['message'])->withType($res['type']);
        }


        $credentials = $this->getCredentials($request);

        if ( Auth::attempt($credentials, $request->has('remember')) ) {
            $this->log_gestion->newLoginRecord(1,Auth::user()->id);
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ( $throttles )
        {
            $this->incrementLoginAttempts($request);
        }


        
    }

    public function getRegister()
    {
        $view = 'auth.'.env('REGISTER','').'register';
        return view($view);

    }

    public function getLogout()
    {
        $this->log_gestion->newLoginRecord(2,Auth::user()->id);
        Auth::logout();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');

    }

    public function passwordReset()
    {
        return view('auth/reset');

    }
    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
        ]);
        $res = $this->user_gestion->resetPassword($request->all());
        if(!$res['status'])
        {
            return redirect()->back()->withErrors(['email'=>$res['message']])->withInput();
        }

        return redirect()->back()->withErrors(['success'=>$res['message']])->withInput();


    }


    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|regex:/^[a-zA-Z\d\-\_\.]+$/|between:1,20|unique:users,name',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|between:5,20',
            'realname' => 'required',
        ]);
        $input = $request->only('password', 'password_confirmation');
        $v = Validator::make($input, [
            'password' => 'regex:/^(?![\d]+$)(?![a-zA-Z]+$)/',
        ]);
         if ( $v->fails() )
        {
            return redirect()->back()->withInput()->withErrors(['password'=>trans('error.00081')]);
        }
        //$validator = $this->validator($request->all());

        //        if ($validator->fails()) {
        //            $this->throwValidationException(
        //                $request, $validator
        //            );
        //        }

        /* if ( $validator->fails())
           {
           return redirect()->back()->withErrors($validator->errors())->withInput();
           }*/
        if(env('REGISTER','') == "start")
        {
            $this->validate($request, [
                'company' => 'required',
            ]);
        }
        if(User::where('name',$request->input('name'))->first())
        {
            return redirect()->back()->withErrors(array('name'=>trans('error.00048')))->withInput();
        }
        if(User::where('email',$request->input('email'))->first())
        {
            return redirect()->back()->withErrors(array('email'=>trans('error.00049')))->withInput();
        }
        $res = $this->create($request->all());

        return redirect()->route('login')->withErrors($res['message']);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create( array $data )
    {

        $res = $this->user_gestion->register($data);
        return $res;
    }


    public function getLogin()
    {
        if ( view()->exists('auth.authenticate') ) {
            return view('auth.authenticate');
        }
        $info = $this->info_gestion->info();
        $emailSuffixes = $this->info_gestion->getAllSuffixes();
        return view('auth.login',compact('emailSuffixes','info'));


    }






    public function confirm( $confirmation_code )
    {
        if ( !$confirmation_code ) {
            throw new InvalidConfirmationCodeException;
        }
        $res = $this->user_gestion->validate($confirmation_code);

        return redirect()->route('login')->withErrors($res['message']);

    }



    public function getResend( Request $request )
    {
        $user_id = $request->session()->get('user_id');
        $this->user_gestion->resend($user_id);
        return redirect()->route('login')->withErrors(array('success'=>trans('error.00063')));
    }



}
