<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CourseRepository;
use App\Repositories\SurveyResultRepository;
use App\Repositories\SurveyRepository;
use App\Repositories\CourseClassRepository;
use Gate,Auth,Redirect,Validator,Excel;
class SurveyResultsController extends Controller
{
    protected $result_gestion;
    protected $survey_gestion;
    protected $course_gestion;
    protected $class_gestion;
    public function __construct(
        CourseRepository $course_gestion,
        SurveyResultRepository $result_gestion,
        SurveyRepository $survey_gestion,
        CourseClassRepository $class_gestion
    )
    {
        $this->result_gestion = $result_gestion;
        $this->survey_gestion = $survey_gestion;
        $this->course_gestion = $course_gestion;
        $this->class_gestion = $class_gestion;

    }

    public function score(Request $request, $id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('apply',$survey))
        {
           return Redirect::back();
        }
        $record = $this->survey_gestion->getRecordBySurveyId($survey->id);
        $this->result_gestion->score($record->id,$request->all());
        return Redirect::to('/');


    }

    public function CourseScore(Request $request, $course_id,$id)
    {
        $survey = $this->survey_gestion->getById($id);
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $record = $this->survey_gestion->getRecordBySurveyId($survey->id,$course->id);
        $this->result_gestion->score($record->id,$request->all());
        return redirect('lessons/');

    }

    public function result($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        $record = $survey->records[0];
        $result = $this->result_gestion->result($record->id);
        return view('survey.result',compact('survey','result'));
    }


    public function courseSurveyResult($course_id,$id,Request $request)
    {

        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $a=$request->all();
        $record = $this->survey_gestion->getRecordById($id);
        $result = $this->result_gestion->result($record->id,$a);
        $evaluation = $record->survey;
        $classes =  $this->class_gestion->getAllSelect($course->id);
        return view('reports.item',compact('result','record','course','classes','a'));
    }


    public function excel($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        $record = $survey->records[0];
        $result = $this->result_gestion->result($record->id);

         $progs = $this->result_gestion->excel($result,$survey);
        Excel::create('Survey report', function($excel) use($progs)
                      {

                          $excel->sheet('Survey Report', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                                        })->export('xls');

    }



    public function courseSurveyQR($course_id,$survey_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $surveyRecord = $course->surveyRecords()->where('type',3)->first();

        return view('survey.QR',compact('surveyRecord','course'));
    }



    public function evaluationQR($course_id,$survey_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $evaluationRecord = $course->surveyRecords()->where('type',2)->first();

        return view('survey.QR',compact('evaluationRecord','course'));
    }



}
