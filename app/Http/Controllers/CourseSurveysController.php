<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SurveyRepository;
use Gate,Auth,Redirect;
class CourseSurveysController extends Controller
{
    protected $survey_gestion;
    protected $surveyType = 3;

    public function __construct(
        SurveyRepository $survey_gestion
    )
    {
        $this->survey_gestion = $survey_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('manage_course_survey',Auth::user()))
        {
            return Redirect::back();
        }
         $surveys = $this->survey_gestion->index(10,$this->surveyType,$request->all());
        return view('course_survey.index',compact('surveys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manage_course_survey',Auth::user()))
        {
            return Redirect::back();
        }
        return view('course_survey.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manage_course_survey',Auth::user()))
        {
            return Redirect::back();
        }
        $this->validate($request, [
            'title' => 'required|max:255',
        ]);
        $res = $this->survey_gestion->store($request->all(),$this->surveyType,Auth::user()->id);
        if(!$res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
        return redirect()->route('course-survey.edit', ['id' => $res['id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifyCourseSurvey',$survey))
        {
           return Redirect::back();
        }
        return view('course_survey.show',compact('survey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifyCourseSurvey',$survey))
        {
           return Redirect::back();
        }
        $manages = "";
        $manageIds = "";
        foreach($survey->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }
        return view('course_survey.edit',compact('survey','manages','manageIds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifyCourseSurvey',$survey))
        {
           return Redirect::back();
        }
       $this->validate($request, [
            'title' => 'required|max:255',
        ]);
        $res = $this->survey_gestion->update($request->all(),$id);
        if(!$res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (!Gate::denies('deleteCourseSurvey',$survey))
        {
           $this->survey_gestion->destroy($id);
        }
        return Redirect::back();
    }



    public function distribute(Request $request,$id)
    {
        $survey = $this->survey_gestion->getById($id);
        $res = $this->survey_gestion->distribute($id,$request->input('course_id'));
        return response()->json(['result'=>true,'record_title'=>$res->title,'message'=>trans('survey.assign_success')]);

    }
    public function deallocate(Request $request)
    {
        $res = $this->survey_gestion->deallocate($request->input('course_id'),$this->surveyType);
        return response()->json(['result'=>true,'record_title'=>trans('table.none'),'message'=>trans('survey.deallocate_success')]);

    }

    public function getDistributeList(Request $request)
    {
        $evaluations = $this->survey_gestion->getDistributeList(5,$request->all(),$this->surveyType);
        return response()->json($evaluations);

    }

}
