<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ExamMaintainRepository;
use App\Repositories\ExamRepository;
use Gate;
class ExamMaintainsController extends Controller
{
    protected $maintain_gestion;
    protected $exam_gestion;

    public function __construct(
        ExamMaintainRepository $maintain_gestion,
        ExamRepository $exam_gestion
    )
    {
        $this->maintain_gestion = $maintain_gestion;
        $this->exam_gestion = $exam_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('exam_id');
        $exam = $this->exam_gestion->getById($id);
        if (Gate::denies('modify',$exam))
        {
            return response()->json(['result'=>false]);
        }
         $res = $this->maintain_gestion->addTeachers($request->all(),$id);
         return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('exam_id');
        $exam = $this->exam_gestion->getById($id);
        if (Gate::denies('modify',$exam))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->maintain_gestion->deleteTeachers($request->all(),$id);
        return response()->json($res);
    }


    public function getAsssignedTeacherList(Request $request,$id)
    {
        $assignedTeachers = $this->maintain_gestion->getAsssignedTeacher(5,$request->all(),$id);

        return response()->json($assignedTeachers);
    }

    public function getUnAsssignedTeacherList(Request $request,$id)
    {
        $unAssignedTeachers = $this->maintain_gestion->getUnAsssignedTeacher(5,$request->all(),$id);
         return response()->json($unAssignedTeachers);
    }
}
