<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ResourceRepository;
use App\Repositories\CatalogRepository;
use App\Repositories\CourseRepository;
use Gate,Auth,Redirect,Response;
use Validator;
class ResourcesController extends Controller
{
    /**
     * The ResourceRepository instance.
     *
     * @var App\Repositories\ResourceRepository
     */
    protected $resource_gestion;
    protected $catalog_gestion;
    protected $course_gestion;
    /**
     * Create a new RsourceController instance.
     *
     * @param  App\Repositories\ResourceRepository $resource_gestion
     * @return void
     */
    public function __construct(
        ResourceRepository $resource_gestion,
        CourseRepository $course_gestion,
        CatalogRepository $catalog_gestion
    )
    {
        $this->resource_gestion = $resource_gestion;
        $this->course_gestion = $course_gestion;
        $this->catalog_gestion = $catalog_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('create_resource',Auth::user()))
        {
            return Redirect::to('/');
        }
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $resources = $this->resource_gestion->index(10,$inputs);
        $a = $inputs;
         $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
         return view('resources.index',compact('resources','a','catalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create_resource',Auth::user()))
        {
            return Redirect::to('/');
        }

        $manages = "";
        $manageIds = "";
        foreach(Auth::user()->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }

        return view('resources.create',compact('manages','manageIds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('create_resource',Auth::user()))
        {
            return Redirect::to('/');
        }
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);
        if(!$request->has('catalog_id'))
        {
            return redirect()->back()->withErrors(array('catalog_id'=>trans('error.00017')))->withInput();
        }
        $res = $this->resource_gestion->store($request->all(),$request->user()->id);
        if($res['status'])
        {
            $error = array('success'=>trans('error.10002').trans('resource.could_add_continue'));
             return redirect()->back()->withErrors($error);
        }
        else
            $error = $res['error'];
        return redirect()->back()->withErrors($error)->withInput();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resource = $this->resource_gestion->getById($id);
        $manages = "";
        $manageIds = "";
        foreach($resource->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }
        return view('resources.edit',compact('resource','manages','manageIds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'name' => 'required|max:255',
        ]);
         if(!$request->has('catalog_id'))
        {
            return redirect()->back()->withErrors(array('catalog_id'=>trans('error.00017')))->withInput();
        }
         $res = $this->resource_gestion->update($request->all(),$id);
         if(!$res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
         return redirect()->to("/resources-manage");
    }


    public function downLoad($id)
    {
        $resource = $this->resource_gestion->getById($id);
        if (Gate::denies('apply',$resource))
        {
            return redirect()->back();
        }
        if($resource->type == 1)
        {
            $file = $this->resource_gestion->downLoad($id);
            return Response::download($file,$resource->filename);
        }
        else
        {
            return redirect()->to($resource->storagename);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resource = $this->resource_gestion->getById($id);
        if (!Gate::denies('delete',$resource))
        {
            $this->resource_gestion->destroy($id);
        }
        return redirect()->back();
    }
    public function lists(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $resources = $this->resource_gestion->lst(6,$inputs);
        $a = $inputs;
        $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        return view('resources.lists',compact('resources','a','catalogs'));
    }


    public function courseResource($course_id ,Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $a = $inputs;
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $resources = $this->resource_gestion->lst(6,$inputs,$course->resource_id);
        $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        return view('resources.lists',compact('resources','a','catalogs','course'));

    }
    public function courseResourceManage($course_id ,Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $a = $inputs;
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $resources = $this->resource_gestion->index(6,$inputs,$course->resource_id);
        $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        return view('resources.course',compact('resources','a','catalogs','course'));

    }

    public function courseResourceCreate($course_id)
    {
        if (Gate::denies('create_resource',Auth::user()))
        {
            return Redirect::to('/');
        }
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $manages = "";
        $manageIds = "";
        foreach(Auth::user()->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
            $manageIds = $manage->department->id.",".$manageIds;
        }

        return view('resources.create',compact('manages','manageIds','course'));
    }





}
