<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CourseRepository;
use App\Repositories\CourseRecordRepository;
use App\Repositories\ExamRecordRepository;
use App\Repositories\SurveyResultRepository;
use App\Repositories\ExamDistributionRepository;
use App\Repositories\CatalogRepository;
use App\Repositories\RecordTypeRepository;
use App\Repositories\AttributeRepository;
use App\Repositories\CourseClassRepository;
use Input,Gate,Auth,Redirect,Excel;
class CourseRecordsController extends Controller
{

    protected $course_gestion;
    protected $record_gestion;
    protected $examRecord_gestion;
    protected $surveyResult_gestion;
    protected $examDistribution_gestion;
    protected $catalog_gestion;
    protected $recordType_gestion;
    protected $attribute_gestion;
    protected $class_gestion;


    public function __construct(
        CourseRepository $course_gestion,
        CourseRecordRepository $record_gestion,
        ExamRecordRepository $examRecord_gestion,
        SurveyResultRepository $surveyResult_gestion,
        RecordTypeRepository $recordType_gestion,
        ExamDistributionRepository $examDistribution_gestion,
        CatalogRepository $catalog_gestion,
        AttributeRepository $attribute_gestion,
        CourseClassRepository $class_gestion
    )
    {
        $this->record_gestion = $record_gestion;
        $this->examRecord_gestion = $examRecord_gestion;
        $this->course_gestion = $course_gestion;
        $this->surveyResult_gestion = $surveyResult_gestion;
        $this->recordType_gestion = $recordType_gestion;
        $this->examDistribution_gestion = $examDistribution_gestion;
        $this->catalog_gestion = $catalog_gestion;
        $this->attribute_gestion = $attribute_gestion;
        $this->class_gestion = $class_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $course_id )
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);

        if (Gate::denies('modify',$course) || !$course->pushed)
        {
            return Redirect::back();
        }
        $firstAttributeSelects = $this->attribute_gestion->getAllFirstAttributeSelect();
        $secondAttributeSelects = $this->attribute_gestion->getAllSecondAttributeSelect();
        $thirdAttributeSelects = $this->attribute_gestion->getAllThirdAttributeSelect();
        $fourthAttributeSelects = $this->attribute_gestion->getAllFourthAttributeSelect();
        $fifthAttributeSelects = $this->attribute_gestion->getAllFifthAttributeSelect();
        $attribute = $this->attribute_gestion->getAttribute();
        return view('courses.distribute', compact('course','firstAttributeSelects','secondAttributeSelects','thirdAttributeSelects','fourthAttributeSelects','fifthAttributeSelects','attribute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $id = $request->input('course_id');
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->addRecords($request->all(),$id);
        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

        $id = $request->input('course_id');
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->deleteRecords($request->all(),$id);
        return response()->json($res);

    }


    public function redistribute(Request $request,$id)
    {
        $id = $request->input('course_id');
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->redistribute($request->all(),$id);
        return response()->json($res);
    }




    public function getDistributeUserList(Request $request,$id)
    {
        $distributeUsers = $this->record_gestion->getDistributeUser(5,$request->all(),$id);

        return response()->json($distributeUsers);
    }

    public function getUnDistributeUserList(Request $request,$id)
    {
        $unDistributeUsers = $this->record_gestion->getUnDistributeUser(5,$request->all(),$id);
        return response()->json($unDistributeUsers);
    }

    public function getOverdueUserList(Request $request,$id)
    {
        $overdueUsers = $this->record_gestion->getOverdueUser(5,$request->all(),$id);
        return response()->json($overdueUsers);
    }

    public function saveRecordMessage(Request $request,$id)
    {
        $course = $this->course_gestion->getById($id);
        $this->record_gestion->saveRecordMessage($course->id,$request->all());
        return response()->json([ 'result' => true ]);
    }


    public function lessons(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $records = $this->record_gestion->lessons(20,$inputs);
        $types =  $this->recordType_gestion->getAllSelect();
        $a = $inputs;
        $evaluationRecords = array();
        foreach($records as $record)
        {
            error_log("record".$record->id);
            if($record->type == 1 || $record->type == 3)
            {
                $evaluationRecords[$record->id] = $record->model->course->surveyRecords()->where('type',2)->first();

            }
        }
        $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        return view('courses.lesson',compact('records','a','catalogs','types','evaluationRecords'));

    }


    public function lessonsDone(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $records = $this->record_gestion->lessonsDone(20,$inputs);
        $types =  $this->recordType_gestion->getAllSelect();
        $a = $inputs;
        $evaluationRecords = array();
        foreach($records as $record)
        {
            if($record->type == 1 || $record->type == 3)
            {
                $evaluationRecords[$record->id] = $record->model->course->surveyRecords()->where('type',2)->first();

            }
        }
        $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        return view('courses.lesson-done',compact('records','a','catalogs','types','evaluationRecords'));
    }

    public function lessonsOverdue(Request $request)
    {
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $records = $this->record_gestion->lessonsOverdue(20,$inputs);
        $types =  $this->recordType_gestion->getAllSelect();
        $a = $inputs;
        $evaluationRecords = array();
        foreach($records as $record)
        {
            if($record->type == 1 || $record->type == 3)
            {
                $evaluationRecords[$record->id] = $record->model->course->surveyRecords()->where('type',2)->first();
            }
        }
        $catalogs = $this->catalog_gestion->getCatalogName(isset($inputs['catalog_id'])?$inputs['catalog_id']:"");
        return view('courses.lesson-overdue',compact('records','a','types','catalogs','evaluationRecords'));
    }




    public function getLog( $course_id )
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $record = $this->record_gestion->getRecordByCourseId($course->id);
        if($record->course->examDistribution)
        {
            $examRecord = $this->examRecord_gestion->getRecordByDistributeId($record->course->examDistribution->id);
        }
        else
        {
            $examRecord = '';
        }
        $evaluationRecord = $record->course->surveyRecords()->where('type',2)->first();

        if($evaluationRecord)
        {
            $evaResults = $this->surveyResult_gestion->getResultsByRecordId($evaluationRecord->id);
        }
        else
        {
            $evaResults = '';
        }
        $surveyRecord = $record->course->surveyRecords()->where('type',3)->first();

        if($surveyRecord)
        {
            $surveyResults = $this->surveyResult_gestion->getResultsByRecordId($surveyRecord->id);
        }
        else
        {
            $surveyResults = '';
        }
        $classRecord = $this->class_gestion->getRecords($course->id);

        $classSign = $this->class_gestion->getClassSigns($course->id);
        return view('courses.log',compact('record','examRecord','evaResults','evaluationRecord','surveyResults','surveyRecord','classSign','classRecord'));

    }




    public function updateStar( Request $request, $id )
    {
        $res = $this->record_gestion->updateStar($id,$request->all());
        return response()->json($res);

    }

    public function returnURL($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $record = $this->record_gestion->getRecordByCourseId($course->id);
        if($record->process < 4)
            return redirect()->to('/lessons');
        elseif($record->process == 4)
            return redirect()->to('/lessons-done');
        else
            return redirect()->to('/lessons-overdue');
    }



    public function rank()
    {
        $ranks = $this->record_gestion->getRanks(10);
        return view('rank.lists',compact('ranks'));
    }


    public function report($course_id, Request $request)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $res = $this->record_gestion->report(10,$inputs,$course->id);
        $records = $res['records'];
        $examRecord = $res['examRecord'];
        $evaluationRecord = $res['evaluationRecord'];
        $evaResult = $res['evaResult'];

        $surveyRecord = $res['surveyRecord'];
        $surveyResult = $res['surveyResult'];
        $a = $inputs;
        $classes =  $this->class_gestion->getAllSelect($course->id);
        return view('courses.report',compact('records','examRecord','evaluationRecord','evaResult','surveyRecord','surveyResult','a','course','classes'));

    }





    public function reportExcel($course_id,Request $request)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $inputs = $request->has('select')?json_decode($request->input('select'),true):array();

        $progs = $this->record_gestion->reportExcel($inputs,$course->id);

        Excel::create('Training Record Report', function($excel) use($progs)
                      {

                          $excel->sheet('Training Record Report', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                      })->export('xls');


    }





}
