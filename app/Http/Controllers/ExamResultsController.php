<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ExamRepository;
use App\Repositories\ExamResultRepository;
use App\Repositories\QuestionRepository;
use Gate,Redirect;
class ExamResultsController extends Controller
{
    protected $exam_gestion;

    protected $result_gestion;
    protected $question_gestion;


    public function __construct(
        ExamRepository $exam_gestion,
        ExamResultRepository $result_gestion,
        questionRepository $question_gestion)
    {
        $this->exam_gestion = $exam_gestion;
        $this->result_gestion = $result_gestion;
        $this->question_gestion = $question_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function Score(Request $request,$id)
    {
        $this->result_gestion->score($id,$request->all());
        return redirect('lessons/');
    }
    public function mark(Request $request,$exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        if (Gate::denies('maintain',$exam))
        {
            return Redirect::back();
        }
        $inputs = $request->has('select')?json_decode($request->input('select'),true):$request->all();
        $a = $inputs;
        $results = $this->result_gestion->mark(10,$exam->id,$inputs);
        return view('exams/mark',compact('results','exam','a'));
    }
    public function markQuestion($exam_id,$id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        $result = $this->result_gestion->getQuestionResult($id);
        return view('exams/markQuestion',compact('result','exam'));
    }

    public function scoreQuestion(Request $request,$exam_id,$id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        $this->result_gestion->scoreQuestion($exam->id,$id,$request->all());
        return redirect()->to('exams/'.$exam->exam_id.'/mark');
    }



    public function getDetailed( $Did ,$Rid)
    {
        $result = $this->result_gestion->getById($Rid);
        $exam = $result->record->distribution->exam;

        return view('exams.detailed',compact('result','exam'));


    }

    public function getAnswer($Eid ,$result_id)
    {
        $result = $this->question_gestion->getResultById($result_id);
        $examResult = $this->result_gestion->getById($Eid);
        $selectResult = array();
        foreach($result->selectResults as $SResult)
        {
            $selectResult[] = $SResult->select_id;
        }
        if($examResult->record->distribution->exam->limit_times == 1)
            return view('exams.answer',compact('result','examResult','selectResult'));
        else
            return trans('error.00072');


    }

    public function getAnswerForward($Eid ,$result_id)
    {

        $examResult = $this->result_gestion->getById($Eid);
        if($examResult->record->distribution->exam->limit_times == 1)
        {
            $forward = 0;
            foreach($examResult->questionResult as $result)
            {
                if($result->id == $result_id)
                    break;
                else
                    $forward = $result->id;

            }
            if($forward)
                $result = $this->question_gestion->getResultById($forward);
            else
                $result = $this->question_gestion->getResultById($result_id);


            return view('exams.answer',compact('result','examResult'));
        }
        else
            return trans('error.00072');


    }
     public function getAnswerNext($Eid ,$result_id)
    {

        $examResult = $this->result_gestion->getById($Eid);
        if($examResult->record->distribution->exam->limit_times == 1)
        {
            $next = 0;
            $fix = 0;
            foreach($examResult->questionResult as $result)
            {
                if($fix == 1)
                {
                    $next = $result->id;
                    break;
                }
                if($result->id == $result_id)
                    $fix = 1;
            }
            if($next)
                $result = $this->question_gestion->getResultById($next);
            else
                $result = $this->question_gestion->getResultById($result_id);


            return view('exams.answer',compact('result','examResult'));
        }
        else
            return trans('error.00072');


    }
}
