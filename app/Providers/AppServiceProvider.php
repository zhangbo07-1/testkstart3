<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Hash;
use Auth;
use App\Models\User;
use App\Models\SystemInfo;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('passcheckwith', function ($attribute, $value, $parameters,$validator)
        {
            $getUserEmail = array_get($validator->getData(),$parameters[0],null);
            $info = SystemInfo::first();
            if($info->user_email)
                return Hash::check($value, User::where('email',$getUserEmail)->first()->password);
            else
                return Hash::check($value, User::where('name',$getUserEmail)->first()->password);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
