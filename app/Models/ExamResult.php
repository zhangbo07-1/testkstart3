<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamResult extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');

    }
    public function record()
    {
        return $this->belongsTo('App\Models\ExamRecord','record_id');

    }
    public function questionResult()
    {
        return $this->hasMany('App\Models\QuestionResult','result_id');

    }
}
