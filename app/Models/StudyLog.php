<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class StudyLog extends Model
{
    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }
}
