<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseRecord extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');

    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course','course_id');

    }
    public function status()
    {
         return $this->belongsTo('App\Models\RecordStatus','process');
    }

    public function record()
    {
        return $this->morphMany('App\Models\Record','model');
    }
}
