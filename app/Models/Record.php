<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    public function model()
    {

       return $this->morphTo();

    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function recordType()
    {
        return $this->belongsTo('App\Models\RecordType','type');
    }

}
