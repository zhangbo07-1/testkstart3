<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public function manages()
    {
        return $this->hasMany('App\Models\ResourceManage','model_id');
    }
    public function catalog()
    {
        return $this->belongsTo('App\Models\Catalog','catalog_id');
    }
/*
    public function resourceArea()
    {
        return $this->belongsTo('App\Models\ResourceArea','area_id');
    }
*/
}
