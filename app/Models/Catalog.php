<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    public function courses()
    {
        return $this->hasMany('App\Models\Course','catalog_id');
    }
    public function exams()
    {
        return $this->hasMany('App\Models\Exam','catalog_id');
    }
     public function resources()
    {
        return $this->hasMany('App\Models\Resource','catalog_id');
    }
}
