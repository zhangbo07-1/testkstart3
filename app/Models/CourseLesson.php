<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseLesson extends Model
{
    public function courseClass()
    {
        return $this->belongsTo('App\Models\CourseClass','class_id');
    }
}
