<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    public function profile()
    {
        return $this->hasMany('App\Models\Profile','position_id');
    }
}
