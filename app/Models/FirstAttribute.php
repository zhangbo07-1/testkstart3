<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirstAttribute extends Model
{
    public function profile()
    {
        return $this->hasMany('App\Models\Profile','attribute1_id');
    }
}
