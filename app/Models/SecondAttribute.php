<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecondAttribute extends Model
{
    public function profile()
    {
        return $this->hasMany('App\Models\Profile','attribute2_id');
    }
}
