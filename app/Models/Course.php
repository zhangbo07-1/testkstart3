<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function manages()
    {
        return $this->hasMany('App\Models\CourseManage','model_id');
    }
    public function catalog()
    {
        return $this->belongsTo('App\Models\Catalog','catalog_id');
    }

    public function sendLimit()
    {
        return $this->hasOne('App\Models\SendLimit');
    }
    public function surveyRecords()
    {
        return $this->hasMany('App\Models\SurveyRecord');
    }

    public function examDistribution()
    {
        return $this->morphOne('App\Models\ExamDistribution','model');

    }
    public function maintains()
    {
        return $this->hasMany('App\Models\CourseMaintain','model_id');
    }
    public function wares()
    {
        return $this->hasMany('App\Models\CourseWare','course_id');
    }
    public function record()
    {
        return $this->hasMany('App\Models\CourseRecord','course_id');

    }
    public function studyLogs()
    {
        return $this->hasMany('App\Models\StudyLog','course_id');

    }
    public function language()
    {
        return $this->belongsTo('App\Models\Language');
    }

    public function postArea()
    {
        return $this->belongsTo('App\Models\PostArea','area_id');
    }

    public function resourceArea()
    {
        return $this->belongsTo('App\Models\ResourceArea','resource_id');
    }
    public function classes()
    {
        return $this->hasMany('App\Models\CourseClass','course_id');

    }

}
