<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamMaintain extends Model
{
    public function exam()
    {
        return $this->belongsTo('App\Models\Exam','model_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\user','maintain_id');
    }
}
