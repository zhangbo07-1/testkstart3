<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseClassRecord extends Model
{
     public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function courseClass()
    {
        return $this->belongsTo('App\Models\CourseClass','class_id');

    }

    public function classSigns()
    {
        return $this->hasMany('App\Models\CourseLessonSign','classRecord_id');

    }

}
