<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginRecord extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');

    }

    public function message()
    {
        return $this->belongsTo('App\Models\LoginMessage','message_id');
    }
}
