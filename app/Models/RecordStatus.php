<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecordStatus extends Model
{
    protected $table = 'record_status';
}
