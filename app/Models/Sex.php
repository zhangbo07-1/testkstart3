<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sex extends Model
{
    protected $table = 'sex_types';
    public function profile()
    {
         return $this->hasMany('App\Models\Profile','sex_id');
    }
}
