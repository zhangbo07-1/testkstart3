<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionSelectResult extends Model
{
    public function select()
    {
        return $this->belongsTo('App\Models\QuestionSelect','select_id');
    }
    public function result()
    {
        return $this->belongsTo('App\Models\QuestionResult','result_id');
    }
}
