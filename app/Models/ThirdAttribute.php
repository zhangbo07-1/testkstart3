<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThirdAttribute extends Model
{
    public function profile()
    {
        return $this->hasMany('App\Models\Profile','attribute3_id');
    }
}
