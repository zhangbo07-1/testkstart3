<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
        
    }
    public function sex()
    {
        return $this->belongsTo('App\Models\Sex','sex_id');
    }
    public function attribute1()
    {
        return $this->belongsTo('App\Models\FirstAttribute','attribute1_id');
    }
    public function attribute2()
    {
        return $this->belongsTo('App\Models\SecondAttribute','attribute2_id');
    }
    public function attribute3()
    {
        return $this->belongsTo('App\Models\ThirdAttribute','attribute3_id');
    }
    public function attribute4()
    {
        return $this->belongsTo('App\Models\FourthAttribute','attribute4_id');
    }
    public function attribute5()
    {
        return $this->belongsTo('App\Models\FifthAttribute','attribute5_id');
    }
}
