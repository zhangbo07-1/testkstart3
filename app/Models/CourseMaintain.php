<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseMaintain extends Model
{
    public function course()
    {
        return $this->belongsTo('App\Models\Course','model_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\user','maintain_id');
    }
}
