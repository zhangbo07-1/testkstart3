<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseLessonSign extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function classRecord()
    {
        return $this->belongsTo('App\Models\CourseClassRecord','classRecord_id');

    }
}
