<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginMessage extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');

    }

    public function loginRecords()
    {
        return $this->hasMany('app\Models\LoginRecord','message_id');
    }
}
