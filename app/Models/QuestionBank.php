<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionBank extends Model
{
    public function questions()
    {
        return $this->hasMany('App\Models\Question','bank_id');

    }
    public function manages()
    {
        return $this->hasMany('App\Models\QuestionBankManage','model_id');
    }
}
