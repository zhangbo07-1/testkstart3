<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailSuffix extends Model
{
    public function systemInfo()
    {
        return $this->belongsTo('App\Models\SystemInfo','system_info_id');

    }
}
