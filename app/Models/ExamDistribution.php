<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamDistribution extends Model
{
    public function model()
    {

       return $this->morphTo();

    }

    public function exam()
    {
        return $this->belongsTo('App\Models\Exam','exam_id');
    }
    public function records()
    {
        return $this->hasMany('App\Models\ExamRecord','distribution_id');
    }

    public function record()
    {
        return $this->morphMany('App\Models\Record','model');
    }

}
