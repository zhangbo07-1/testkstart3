<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function manages()
    {
        return $this->hasMany('App\Models\MessageManage','model_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
