<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionSelect extends Model
{
    public function question()
    {
        return $this->belongsTo('App\Models\Question','question_id');

    }
    public function selectResults()
    {
        return $this->hasMany('App\Models\QuestionSelectResult','select_id');
    }

}
