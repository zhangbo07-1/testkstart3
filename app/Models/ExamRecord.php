<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamRecord extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function distribution()
    {
        return $this->belongsTo('App\Models\ExamDistribution','distribution_id');
    }

    public function results()
    {
        return $this->hasMany('App\Models\ExamResult','record_id');
    }
    public function status()
    {
         return $this->belongsTo('App\Models\RecordStatus','process');
    }
}
