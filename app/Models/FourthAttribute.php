<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FourthAttribute extends Model
{
    public function profile()
    {
        return $this->hasMany('App\Models\Profile','attribute4_id');
    }
}
