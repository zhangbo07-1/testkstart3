<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function bank()
    {
        return $this->belongsTo('App\Models\QuestionBank','bank_id');

    }
    public function selects()
    {
        return $this->hasMany('App\Models\QuestionSelect');

    }
    public function questionResults()
    {
        return $this->hasMany('App\Models\QuestionResult');

    }
}
