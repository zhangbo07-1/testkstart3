<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FifthAttribute extends Model
{
    public function profile()
    {
        return $this->hasMany('App\Models\Profile','attribute5_id');
    }
}
