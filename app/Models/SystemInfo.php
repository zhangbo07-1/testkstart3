<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemInfo extends Model
{
    protected $table = 'system_info';
    public function emailSuffixes()
    {
        return $this->hasMany('App\Models\EmailSuffix','system_info_id');

    }
    public function department()
    {
        return $this->belongsTo('App\Models\Organization','department_id');
    }
}
