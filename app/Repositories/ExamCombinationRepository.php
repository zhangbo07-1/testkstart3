<?php namespace App\Repositories;

use App\Models\ExamCombination;
use App\Models\QuestionBank;
use App\Repositories\BankRepository;
use App\Repositories\ExamRepository;

class ExamCombinationRepository extends BaseRepository
{

    /**
     * The Role instance.
     *
     * @var App\Models\ExamRecord
     */
    protected $bank_gestion;
    protected $exam_gestion;

    public function __construct(
        ExamCombination $combination,
        ExamRepository $exam_gestion,
        BankRepository $bank_gestion )
    {
        $this->model = $combination;
        $this->exam_gestion = $exam_gestion;
        $this->bank_gestion = $bank_gestion;
    }

    public function getUNSelect($id)
    {
        $choseIdArray = $this->model->where('exam_id',$id)->lists('bank_id')->all();
        $bankList = $this->bank_gestion->getList();
        $questionBanks = QuestionBank::whereIn('id',$bankList)->whereNotIn('id',$choseIdArray)->get();
        return $questionBanks;
    }

    public function saveCombination($inputs,$id)
    {
        $res = array('status'=>1,'message'=>['success'=>trans('error.10000')],'id'=>'');
        $banks = $this->bank_gestion->getAll();
        $exam = $this->exam_gestion->getById($id);
        foreach($exam->combinations as $combination)
        {
            $combination->delete();
        }
        $weight = 0;
        foreach($banks as $questionBank)
        {
            if(isset($inputs['bankSelect'.$questionBank->id]))
            {
            $Qnumber = $inputs['bankSelect'.$questionBank->id];
            error_log($Qnumber);
            if($Qnumber > 0)
            {
                if(!$combinate = $this->model->where('exam_id',$id)->where('bank_id',$questionBank->id)->first())
                {
                    $combinate = new $this->model;
                    $combinate->exam_id = $id;
                    $combinate->bank_id = $questionBank->id;
                    $combinate->weight = $inputs['bankWeight'.$questionBank->id];
                    $combinate->numbers = $Qnumber;
                }
                else
                {
                    $combinate = $this->model->where('exam_id',$id)->where('bank_id',$questionBank->id)->first();
                    $combinate->weight += $inputs['bankWeight'.$questionBank->id];
                    $combinate->numbers += $Qnumber;

                }
                if($combinate->numbers > count($questionBank->questions))
                {
                    $res['status'] = 1;
                    $res['message']=array('fail'=>$questionBank->title.trans('error.00026'));
                    return $res;
                }
                $weight += $combinate->weight;
                $combinate->save();

            }
            if($weight > 100)
                {
                    $res['status'] = 1;
                    $res['message']=array('fail'=>$questionBank->title.trans('error.00027'));
                    return $res;
                }
            }
        }

        if($weight < 100)
                {
                    $res['status'] = 1;
                    $res['message']=array('fail'=>trans('error.00028'));
                    return $res;
                }
        return $res;

    }

}
