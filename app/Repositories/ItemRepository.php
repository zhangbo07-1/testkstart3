<?php namespace App\Repositories;

use App\Models\Item;
use App\Models\ItemSelect;
use App\Models\ItemGrade;
class ItemRepository extends BaseRepository {

    /**
     * Create a new CommentRepository instance.
     *
     * @param  App\Models\EvaluationItem $item
     * @return void
     */
    public function __construct(Item $item)
    {
	$this->model = $item;
    }


    /**
     * Save a item.
     *
     * @param  array $inputs

     * @return void
     */
    public function saveItem($item, $inputs, $survey_id)
    {
        if(array_key_exists('Qtype', $inputs))
        {
 	    switch( $inputs['Qtype'])
            {
                case 'select':
                $item->type = 1;
                break;
                case 'grade':
                $item->type = 2;
                break;
            }
        }
        $item->title = $inputs['title'];
        $item->survey_id = $survey_id;
        $item->save();
        
        if($item->type == 1)
        {
            foreach($item->selects as $select)
            {
                $select->delete();
            }

            $selectNum = $inputs['selectNUM'];
            for($i = 1;$i<=$selectNum; $i++)
            {
                
                $select = new ItemSelect;
                $select->title = $inputs['select'.$i];
                $select->item_id = $item->id;
                $select->save();
            }
        }
        if($item->type==2)
        {
            if($grade = $item->grade)
            {
                $grade->delete();
            }
            $grade =  new ItemGrade;
            $grade->hightitle = $inputs['hightitle'];
            $grade->lowtitle = $inputs['lowtitle'];
            $grade->item_id = $item->id;
            $grade->save();
        }
    }

    /**
     * Store a question.
     *
     * @param  array $inputs

     * @return void
     */
    public function store($inputs, $survey_id)
    {
        $item = new $this->model;
        $item = $this->saveItem($item, $inputs, $survey_id);
    }


    /**
     * update a item.
     *
     * @param  array $inputs

     * @return void
     */
    public function update($inputs, $survey_id, $item_id)
    {
        $item = $this->getById($item_id);
        $item = $this->saveItem($item, $inputs, $survey_id);
    }

}
