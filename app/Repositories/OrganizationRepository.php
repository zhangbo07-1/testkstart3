<?php namespace App\Repositories;
use App\Models\User;
use App\Models\Organization;
use App\Models\SystemInfo;
use App\Repositories\ImportLogRepository;
use Excel,Auth;
class OrganizationRepository extends BaseRepository
{

    protected $log_gestion;

    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
        Organization $organization,
        ImportLogRepository $log_gestion
    )
    {
        $this->model = $organization;
        $this->log_gestion = $log_gestion;
    }
    public function FetchRepeatMemberInArray($array)
    {
        // 获取去掉重复数据的数组
        $unique_arr = array_unique ( $array );
        // 获取重复数据的数组
        $repeat_arr = array_diff_assoc ( $array, $unique_arr );
        return $repeat_arr;
    }

    
    public function getUserNUM($id,$status=false)
    {
        $node = Organization::find($id);
        if($status)
        {
            $num = User::where('department_id',$node->id)->where('status_id',2)->count();//count($node->user);
        }
        else
        {
             $num = User::where('department_id',$node->id)->count();//count($node->user);
        }
        $nodes = Organization::where('parent_id',$id)->get();
        foreach($nodes as $child)
        {
            $num += $this->getUserNUM($child->id,$status);
        }
        return $num;
    }
    /**
     * list a node.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function lst($id,$user_id, $manages,$NUM,$status=false)
    {
        $res = array();
        $list = array();
        $user = User::find($user_id);
        foreach($user->manages as $manage)
        {
            $manageId[] = $manage->manage_id;
            $list = array_merge($list,$this->getUnderArray($manage->manage_id));
            $list = array_merge($list,$this-> getUperArray($manage->manage_id));
        }
        $list = array_unique($list);
        
        if($manages =='/')
        {
            $manageList = $manageId;
        }
        else
        {
            $manageNames = explode(',',$manages);
            
            foreach($manageNames as $manageName)
            {
                $department = $this->model->where('id',$manageName)->first();
                if($department)
                {
                    $manageList[] = $department->id;
                }
            }
            $selectNodes = $this->getUperArray($manageList);
        }
        $nodes = Organization::where('parent_id',$id)->get();
        foreach($nodes as $node)
        {
            if(in_array($node->id,$list))
            {
                if(count(Organization::where('parent_id',$node->id)->get()))
                {
                    $children = true;
                }
                else
                {
                    $children = false;
                }
                if (in_array($node->id, $manageList))
                {
                    $checked = true;
                }
                else
                {
                    $checked = false;
                }
                $opened = false;
                if(isset($selectNodes))
                {
                    if(in_array($node->id, $selectNodes))
                    {
                        $opened = true;
                    }
                }
                if($NUM)
                {
                    $tag = '('.$node->tag.')';
                    $userNum = '('.$this->getUserNUM($node->id,$status).')';
                }
                else
                {
                    $tag = '';
                    $userNum = "";
                }
                $res[] = array('id'=>$node->id,'text' =>$tag.$node->name.$userNum, 'children' =>$children,'state'=>['opened'=>$opened,'selected'=>$checked]);
                
            }
        }
        
        return $res;
    }



    /**
     * ceate a new node.
     *
     * @param  int $id
     * @param  string $name
     * @return array
     */
    public function create($id, $name,$tag)
    {
        $node = new Organization;
        $node->parent_id = $id;
        $node->name = $name;
        $node->tag = $tag;
        $node->save();
        return array('id' => $node->id,'text' => $node->name);
    }

    /**
     * rename a node.
     *
     * @param  int $id
     * @param  string $name
     * @return array
     */
    public function rename($id, $name,$tag)
    {
        $node = Organization::find($id);
        $node->name = $name;
        $node->tag = $tag;
        $node->save();
        return array('id' => $node->id,'text' => $node->name);
    }

    /**
     * search a node.
     *
     * @param  int $id
     * @param  string $name
     * @return array
     */
    public function search($keyword)
    {
        
        $nodes = Organization::where('name','LIKE','%'.$keyword.'%')->lists('id')->all();
        $selectNodes = $this->getUperArray($nodes);
        foreach($selectNodes as $node)
        {
             $res[] = array('id'=>$node,'state'=>['opened'=>true]);
        }
        return $res;
    }

    /**
     * remove a node.
     *
     * @param  int $id
     * @return array
     */
    public function remove($id)
    {
        if($id == 1)
        {
            return array('status'=>'False','error'=>trans('error.00004'));
        }
        if($this->getUserNUM($id))
        {
            return array('status'=>'False','error'=>trans('error.00005'));
        }
        $info = SystemInfo::first();
        if($info->department_id == $id)
        {
            return array('status'=>'False','error'=>trans('error.00074'));
        }
        $node = Organization::find($id);
        $nodes = Organization::where('parent_id',$id)->get();
        foreach($nodes as $child)
        {
            $this->remove($child->id);
        }
        $node->delete();
        return array('status' => 'OK');
    }

    /**
     * import a arganization list.
     *
     * @param  xlsfile $file
     * @return array
     */
    public function import($file)
    {
        $result = array('type'=>0);
        $log = $this->log_gestion->createLog($file,2);
        //$this->log_gestion->addLog($log,trans('error.10002'));
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            if(!empty($prog[array_search('操作',$tag)]))
            {
                $ID[] = $prog[array_search('组织ID',$tag)];
                $parent[] = $prog[array_search('上级组织ID',$tag)];
                $data[] = array('ID'=>$prog[array_search('组织ID',$tag)],
                    'organization'=> $prog[array_search('组织名称',$tag)],
                                'parent'=> $prog[array_search('上级组织ID',$tag)],
                                'act'=>$prog[array_search('操作',$tag)],
                );
            }
           
        }
        if((empty($ID)) || (empty($parent)))
        {
            $result['type'] = 1;
            $error = trans('error.00058');
            $this->log_gestion->addLog($log,$error);
        }
       $error = $this->FetchRepeatMemberInArray($ID);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00013');
            $this->log_gestion->addLog($log,$error);
        }
        
        if($result['type'])
        {
            $result['error'] =  trans('error.00012');
            return $result;
        }
        $success = 0;
        foreach($data as $info)
        {
            switch($info['act'])
            {
            case 'A':
                if(!empty($info['parent']))
                {
                    $parent = $this->model->where('tag',$info['parent'])->first();
                    if($parent)
                    {
                        $node = $this->model->whereTag($info['ID'])->first();
                        if($node)
                        {
                            $this->rename($node->id,$info['organization'],$info['ID']);
                            $error = trans('error.00015').$info['organization'];
                            $this->log_gestion->addLog($log,$error);
                        }
                        else
                        {
                            $this->create($parent->id,$info['organization'],$info['ID']);
                            $error = trans('error.00015').$info['organization'];
                            $this->log_gestion->addLog($log,$error);
                        }
                        $success ++;
                    }
                    else
                    {
                         $error = $info['organization'].trans('error.00014');
                         $this->log_gestion->addLog($log,$error);
                    }
                }
                else
                {
                    $node = $this->model->whereTag($info['ID'])->first();
                    if($node)
                    {
                        $this->rename($node->id,$info['organization'],$info['ID']);
                        $error = trans('error.00015').$info['organization'];
                        $this->log_gestion->addLog($log,$error);
                        $success++;
                    }
                    else
                    {
                        $error = trans('error.00070').$info['organization'];
                         $this->log_gestion->addLog($log,$error);
                    }
                }
                break;
            case 'D':
                $node = $this->model->where('tag',$info['ID'])->first();
                if($node)
                {
                    $res = $this->remove($node->id);
                    if($res['status'] == 'OK')
                    {
                    $error = trans('error.00016').$info['organization'];
                    $this->log_gestion->addLog($log,$error);
                    $success++;
                    }
                    else
                    {
                        $error = $info['organization'].trans('error.00005');
                        $this->log_gestion->addLog($log,$error);
                    }
                }
                break;
                
            }
        }
        $error = $success.trans('user.department').trans('error.10000');
         $this->log_gestion->addLog($log,$error);
    }


    /*
       获取下属列表
     */
    public function getUnderList($id)
    {
        $node = Organization::find($id);
        $list = $node->id;
        $nodes = Organization::where('parent_id',$id)->get();
        foreach($nodes as $child)
        {
            $list = $list.",".$this->getUnderList($child->id);
        }
        return $list;
    }

    /*
       获取下属列表
     */
    public function getUnderArray($ids)
    {
        $list = "";
        if(is_array($ids))
        {
            foreach($ids as $id)
            {
                $list = $list.','.$this->getUnderList($id);
            }
        }
        else
        {
            $list = $this->getUnderList($ids);
        }
        $list = explode(',',$list);
        $list = array_unique($list);
        return $list;
    }
    /*
       获取上方元素
     */
    public function getUperList($id)
    {
        $node = Organization::find($id);
        $list = $id;
        if($node)
        {
            $list = $list.','.$this->getUperList($node->parent_id);
        }
        return $list;
    }

    /*
       获取上方列表
     */
    public function getUperArray($ids)
    {
        $list = "";
        if(is_array($ids))
        {
            foreach($ids as $id)
            {
                $list = $list.','.$this->getUperList($id);
            }
        }
        else
        {
            $list = $this->getUperList($ids);
        }
        $list = explode(',',$list);
        $list = array_unique($list);
        return array_reverse($list);
    }

    public function belongOrNot($Alist,$Blist)
    {
        foreach($Alist as $Aid)
        {
            foreach($Blist as $Bid)
            {
                if($this->ownerOrNot($Bid,$Aid))
                    return true;
            }
        }
        return false;
    }
    
    /*
       a是否包含b
     */
    public function ownerOrNot($Aid,$Bid)
    {
        if($Aid === $Bid)
            return true;
        else
        {
            $node = $this->getById($Bid);
            if($node)
                return $this->ownerOrNot($Aid,$node->parent_id);
            else
                return false;
        }
    }

    public function checkManage($list)
    {
        $ids = explode(',',$list);
        $ids = array_filter($ids);
        $baselist=array();
        foreach(Auth::user()->manages as $manage)
        {
            $baselist = array_merge($baselist,$this->getUnderArray($manage->manage_id));
        }
        $baselist = array_unique($baselist);
        $result = array_diff($ids,$baselist);
        if(empty($result))
            return true;
        else
            return false;

    }

    public function getManageName($list)
    {
        $ids = explode(',',$list);
        $departments = $this->model->whereIn('id',$ids)->lists('name')->all();
        $manages = implode(',',$departments);
        return $manages;

    }


    public function setManage($list)
    {
        $ids = explode(',',$list);
        foreach(Auth::user()->manages as $manage)
        {
            foreach($ids as $id)
            {
                if($this->ownerOrNot($manage->manage_id,$id))
                {
                    $manageId[] = $id;
                }
                elseif($this->ownerOrNot($id,$manage->manage_id))
                {
                    $manageId[] = $manage->manage_id;
                }
            }
        }
        $manageId = array_unique($manageId);
        return $manageId;
    }


    public function getAllSelect()
    {
        $list = array();
        foreach(Auth::user()->manages as $manage)
        {
            $list = array_merge($list,$this->getUnderArray($manage->manage_id));
        }
        $list = array_unique($list);
        $userDepartmentSelects =  $this->model->whereIn('id',$list)->lists('name','id');
	return compact('userDepartmentSelects');
    }

    public function exampleExcel()
    {
        $prog[] = array('组织ID','组织名称','上级组织ID','操作');
        $prog[] = array('','','','A 增加');
        $prog[] = array('','','','D 删除');
        return $prog;
    }


    public function excel()
    {
        $list = array();
        $user = Auth::user();
        foreach($user->manages as $manage)
        {
            $manageId[] = $manage->id;
            $list = array_merge($list,$this->getUnderArray($manage->manage_id));
            $list = array_merge($list,$this-> getUperArray($manage->manage_id));
        }
        $list = array_unique($list);
        $prog[] = array('组织ID','组织名称','上级组织ID','操作');
        foreach($list as $id)
        {
            if($id > 1)
            {
                $node = $this->getById($id);
                $Fnode = $this->getById($node->parent_id);
                $prog[] = array($node->tag,$node->name,$Fnode->tag,'A');
            }
            
        }
        return $prog;
    }


    
}
