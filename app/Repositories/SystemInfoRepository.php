<?php namespace App\Repositories;

use App\Models\SystemInfo;
use App\Models\EmailSuffix;
use Storage;
class SystemInfoRepository extends BaseRepository
{
    protected $destinationPath = '/img/';
    protected $PATH = "";
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
        SystemInfo $systemInfo
    )
    {
        $this->model = $systemInfo;
        $this->PATH = env('OSS_PATH','test');
    }

    public function saveLogo($inputs)
    {
        $res = trans('error.10000');
        if(!array_key_exists('image',$inputs))
        {
            $res = trans('error.00046');
            return $res;
        }
        $extension = $inputs['image']->getClientOriginalExtension();
        if($extension != 'png')
        {
            $res =  trans('error.00047');
            return $res;

        }
        $imageInfo = getimagesize($inputs['image']);
        if($imageInfo[0]!=300 || $imageInfo[1]!= 100)
        {
            $res = trans('error.00047');
            return $res;
        }
        $res =  Storage::put(
            $this->PATH.$this->destinationPath.'logo.png',
            file_get_contents($inputs['image'])
        );
        
        if($res)
        {
            $info = $this->model->first();
            $info->logo = 1;
            $info->save();
            $res = trans('error.10000');
            return $res;
        }
        $res = trans('error.09999');
        return $res;
    }

    public function info()
    {
        $info = $this->model->first();
        return $info;
    }
    public function saveInfo($inputs)
    {
        $info = $this->model->first();
        $info->company = $inputs['company'];
        $info->phone = $inputs['phone'];
        $info->email = $inputs['email'];
        $info->save();
        return $info;
    }

    public function saveCensor($inputs)
    {
        $info = $this->model->first();
        $info->censor = $inputs['censor'];
        $info->validate = $inputs['validate'];
        $info->register = $inputs['register'];
        $info->department_id = $inputs['department_id'];
        $info->save();
        return $info;
    }


    public function saveEmail($inputs)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
        $info = $this->model->first();
        $info->user_email = $inputs['user_email'];
        $info->save();
        foreach($info->emailSuffixes as $email)
        {
            $email->delete();
                    
        }
        $emailNum = $inputs['emailNUM'];
        for($i = 1;$i<=$emailNum; $i++)
        {

            $email = new EmailSuffix;

            $email->email = $inputs['email'.$i];
            if(strpos( $email->email,'@')!==false)
            {
                $res['message'] = array('fail'=>trans('error.00059'));
                return $res;
                                
            }


            $email->system_info_id = $info->id;
            $email->save();
                        
        }
        return $res;
    }


    public function getAllSuffixes()
    {
        $emailSelect = EmailSuffix::lists('email', 'email');

        return $emailSelect;
    }

}
