<?php namespace App\Repositories;

use App\Models\User;
use App\Models\ExamRecord;
use App\Models\Exam;
use App\Models\Course;
use App\Models\ExamDistribution;
use App\Models\CourseClassRecord;
use App\Repositories\UserRepository;
use App\Repositories\ExamRepository;

use App\Repositories\ExamDistributionRepository;
use App\Repositories\CatalogRepository;
use Auth;

class ExamRecordRepository extends BaseRepository
{

    /**
     * The Role instance.
     *
     * @var App\Models\ExamRecord
     */
    protected $examRecord;
    protected $exam_gestion;
    protected $catalog_gestion;
    protected $distribution_gestion;

    public function __construct(
        ExamRepository $exam_gestion,
        UserRepository $user_gestion,
        CatalogRepository $catalog_gestion,
        ExamDistributionRepository $distribution_gestion,
        ExamRecord $examRecord )
    {
        $this->model = $examRecord;
        $this->exam_gestion = $exam_gestion;
        $this->user_gestion = $user_gestion;
        $this->catalog_gestion = $catalog_gestion;
        $this->distribution_gestion = $distribution_gestion;
    }

     public function getRecordByDistributeId($id)
    {
        $record = $this->model->where('distribution_id',$id)->where('user_id',Auth::user()->id)->first();
        return $record;
    }


     public function findRecordIdByCatalogId( $id )
    {
        switch ( true )
        {
            case (empty($id)):
            case ($id == '0'):
                $records = ExamRecord::all()->lists('id');

                break;
            case ($id != '0'):

                $records = ExamRecord::with('exam')->WhereHas('exam', function ( $query ) use ( $id ) {
                    $query->where('courseCatalog_id',$id );
                })->lists('id');

                break;
            default:
                $records = ExamRecord::all()->lists('id');
                break;
        }
        return $records;
    }

    public function getExamRecords($n,$inputs,$type=false,$manage=true)
    {
        $userList =  $this->user_gestion->getUserIdByInputs( $inputs,$manage );
         if(!empty($inputs['class_id']))
        {
            $userList = CourseClassRecord::where('class_id',$inputs['class_id'])->whereIn('user_id',$userList)->lists('user_id')->all();
        }

        $examList = $this->exam_gestion->getListByInput(isset($inputs['findByExamName'])?$inputs['findByExamName']:"",$type,$manage);
        if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);
            $catalogArray = $this->catalog_gestion->getUnderArray($catalogArray);

            $examList = Exam::whereIn('id',$examList)
                         ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }

        if(!empty($inputs['course_id']) && !empty($inputs['exam_id']))
        {
            $distributionList = ExamDistribution::where('exam_id',$inputs['exam_id'])->where('model_type',Course::class)->where('model_id',$inputs['course_id'])->lists('id')->all();
        }
        else
        {
            $distributionList = ExamDistribution::whereIn('exam_id',$examList)->lists('id')->all();
        }

        $recordList = $this->model->whereIn('distribution_id',$distributionList)
                    ->whereIn('user_id',$userList)

                    ->leftjoin('users','exam_records.user_id','=','users.id')
                    ->orderBy('users.name','asc')
                    ->select('exam_records.*')->paginate($n);
        return $recordList;

    }
}