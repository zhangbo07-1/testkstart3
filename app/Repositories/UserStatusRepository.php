<?php namespace App\Repositories;

use App\Models\UserStatus;

class UserStatusRepository {

	/**
	 * The Role instance.
	 *
	 * @var App\Models\Role
	 */
	protected $userStatus;

	/**
	 * Create a new RolegRepository instance.
	 *
	 * @param  App\Models\UserStatus $userStatus
	 * @return void
	 */
	public function __construct(UserStatus $userStatus)
	{
		$this->userStatus = $userStatus;
	}

	/**
	 * Get all roles.
	 *
	 * @return Illuminate\Support\Collection
	 */
	public function all()
	{
		return $this->userStatus->all();
	}



	/**
	 * Get roles collection.
	 *
	 * @param  App\Models\User
	 * @return Array
	 */
	public function getAllSelect()
	{
		$userStatusSelects = $this->userStatus->all()->lists('title', 'id');

		return $userStatusSelects;
	}

}
