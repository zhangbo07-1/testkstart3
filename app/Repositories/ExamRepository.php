<?php namespace App\Repositories;

use App\Models\QuestionBank;
use App\Models\User;
use App\Models\Exam;
use App\Models\CourseRecord;
use App\Models\ExamResult;
use App\Models\ExamManage;
use App\Models\ExamRecord;
use App\Models\ExamDistribution;
use App\Models\ExamMaintain;
use App\Models\Question;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;

use Input,Auth;

class ExamRepository extends BaseRepository
{


    public function __construct(
        Exam $exam,
        ExamManage $exam_manage,
        ExamMaintain $exam_maintain,
        UserRepository $user_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $exam;
        $this->manage_model = $exam_manage;
        $this->maintain_model = $exam_maintain;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }

    public function index($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {

        $keyword = isset($inputs['findByExamName'])?$inputs['findByExamName']:"";
        $list = $this->getListByInput($keyword);
        
        if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);
            
            $list = $this->model->whereIn('id',$list)
                         ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }
        
        $exams = $this->model->whereIn('id',$list)
                      ->orderBy($orderby,$direction)->paginate($n);
        return $exams;
        
    }



    
    private function saveExam($exam, $inputs, $user_id = null)
    {
        $res = array('status'=>1,'error'=>'','id'=>'');
        $exam->exam_id = $inputs['exam_id'];
        $exam->title = $inputs['title'];
        $exam->description = $inputs['description'];
        if($user_id)
        {
            $exam->user_id = $user_id;
        }
        $exam->exam_time = $inputs['exam_time'];
        $exam->limit_times = $inputs['limit_times'];
        $exam->limit_days = $inputs['limit_days'];
        $exam->pass_score = $inputs['pass_score'];
        $exam->catalog_id = $inputs['catalog_id'];
        $exam->emailed = isset($inputs['emailed']);
        $exam->if_link = isset($inputs['if_link']);
        $exam->save();
        $this->setManageId($exam,$inputs['manage_id'],$user_id);

        $res['id'] = $exam->exam_id;
        return $res;
    }



    public function store($inputs, $user_id)
    {
        $exam = new $this->model;
        $exam = $this->saveExam($exam, $inputs, $user_id);
        return $exam;
    }

    public function update($inputs, $id)
    {
        $exam = $this->getById($id);
        if($exam->if_link != isset($inputs['if_link']))
        {
            foreach($exam->distributions as $distribution)
            {
                $distribution->delete();
            }
        }
        $exam = $this->saveExam($exam, $inputs);
        return $exam;
    }


    public function getListByInput( $input_data ,$type = false,$manage = true)
    {
        if($manage)
        {
            $list = $this->getModifyModelList();
        }
        else
        {
            $list = $this->model->lists('id')->all();
        }
        $user = Auth::user();
        $list = $this->model->where(function($query) use ($user,$list,$manage)
                                    {
                                        $query->where('user_id',$user->id)
                                        ->orWhereHas('maintains',function($q) use ($user)
                                        {
                                            $q->where('maintain_id',$user->id);
                                        });
                                        if($user->role_id > 2 || !$manage)
                                        {
                                            $query->orwhereIn('id',$list);
                                        }
                                    })
              ->where(function($query) use ($input_data)
                      {
                          $query->where('exam_id', 'LIKE', '%' . $input_data . '%')
                          ->orWhere('title', 'LIKE', '%' . $input_data . '%')
                          ->orWhere('description', 'LIKE', '%' . $input_data . '%');
                      })
              ->lists('id')->all();
        if($type)
            return Exam::whereIn('id',$list)
                ->where('if_link',0)
                ->lists('id')->all();
        else
            return $list;
        
    }

    public function filterChosenCombination( $id )
    {
        return ExamCombination::where('exam_id', $id)->get();
    }

    public function filterNotChosenCombination( $id )
    {
        $chosenIdArray = ExamCombination::where('exam_id', $id)->lists('question_bank_id');
        return QuestionBank::whereNotIn('id', $chosenIdArray)->get();
    }

    public function getExamByExamId($exam_id)
    {
        $exam = $this->model->where('exam_id',$exam_id)->first();
        return $exam;
    }

    public function preview($id)
    {
        $exam = $this->getById($id);
        $num = 0;
        $questions = array();
        foreach($exam->combinations as $combination)
        {
            if($combination->numbers > 0)
            {
                $id = Question::where('bank_id',$combination->bank_id)->lists('id')->toArray();

                if($combination->numbers>1)
                {
                    $key = array_rand($id,$combination->numbers);
                    for($i = 0;$i < $combination->numbers;$i++)
                    {
                        $questions[$num] = Question::findOrFail($id[$key[$i]]);
                        $list[$num] = $id[$key[$i]];
                        $num++;
                    }
                }
                else
                {
                    $key = array_rand($id);
                    $questions[$num] = Question::findOrFail($id[$key]);
                    $list[$num] = $id[$key];
                    $num++;

                }
            }
        }
        return $questions;
    }


    public function courseAnswer($course_id,$exam_id)
    {
        $res = array('status'=>0,'message'=>'');
        $courseRecord = CourseRecord::where('course_id',$course_id)->where('user_id',Auth::user()->id)->first();
        if(!$courseRecord)
        {
            $res['status'] = 1;
            $res['message'] = trans('error.00078');
            return $res;
        }

        $distribution = ExamDistribution::where('exam_id',$exam_id)->where('model_id',$course_id)->first();
        $examRecord = ExamRecord::where('distribution_id',$distribution->id)->where('user_id',Auth::user()->id)->first();
        $overed = 0;
        if($examRecord)
        {
            foreach($examRecord->results as $result)
            {
                if($result->overed == 2)
                {
                    $overed ++;
                }
            }
        }
        if($overed>0)
            $res['if_not_marked'] = 1;
        else
            $res['if_not_marked'] = 0;
        $res['distribution'] = $distribution;
        $num = 0;
        foreach($distribution->exam->combinations as $combination)
        {
            $num+=$combination->numbers;
        }
        $res['number'] = $num;
        return $res;

    }
    public function examAnswer($exam_id)
    {
        $res = array('status'=>0,'message'=>'');
        $distribution = ExamDistribution::where('exam_id',$exam_id)->where('model_id',Auth::user()->id)->first();
        if(!$distribution)
        {
            $res['status'] = 1;
            $res['message'] = trans('error.00082');
            return $res;
        }
        $examRecord = ExamRecord::where('distribution_id',$distribution->id)->where('user_id',Auth::user()->id)->first();
        $overed = 0;
        if($examRecord)
        {
            foreach($examRecord->results as $result)
            {
                if($result->overed == 2)
                {
                    $overed ++;
                }
            }
        }
        if($overed>0)
            $res['if_not_marked'] = 1;
        else
            $res['if_not_marked'] = 0;
        $res['distribution'] = $distribution;
        $num = 0;
        foreach($distribution->exam->combinations as $combination)
        {
            $num+=$combination->numbers;
        }
        $res['number'] = $num;
        return $res;

    }


    public function answer($distribution_id)
    {
        $distribution = ExamDistribution::find($distribution_id);
        $res = array('status'=>0,'message'=>'');
        $exam = $distribution->exam;
        if($exam->if_link)
        {
            $courseRecord = CourseRecord::where('course_id',$distribution->model_id)->where('user_id',Auth::user()->id)->first();
            if(!$courseRecord)
            {
                $res['status'] = 1;
                $res['message'] = array('fail'=>trans('error.00078'));
                return $res;
            }
            if($courseRecord->course->if_order == 1)
            {
                if($courseRecord->process < 3)
                {
                    $res['status'] = 1;
                    $res['message'] = array('fail'=>trans('error.00025'));
                    return $res;
                }
            }
            if($courseRecord->process == 5 && !$courseRecord->course->if_open_voerdue )
            {
                $res['status'] = 1;
                $res['message'] = array('fail'=>trans('error.00031'));
                return $res;
            }
        }
        
        
        $num = 0;
        $examRecord = ExamRecord::where('distribution_id',$distribution->id)->where('user_id',Auth::user()->id)->first();

        if($examRecord && $examRecord->process == 5)
        {
            $res['status'] = 1;
            $res['message'] = array('fail'=>trans('error.00031'));
            return $res;
        }
        
        if($exam->limit_times == 0 ||!$examRecord || ($examRecord && count($examRecord->results) < $exam->limit_times))
        {
            
            foreach($exam->combinations as $combination)
            {
                $key = array();
                $key_ID = array();
                $key_FL = array();
                $Pid = Question::where('bank_id',$combination->bank_id)->where('priority',1)->lists('id')->toArray();
                $Nid = Question::where('bank_id',$combination->bank_id)->where('priority',0)->lists('id')->toArray();
                if(count($Pid) >= $combination->numbers)
                {
                    $key_ID = array_rand($Pid,$combination->numbers);
                    if(!is_array($key_ID))
                    {
                        $key_ID = explode(",",$key_ID);
                    }
                    foreach($key_ID as $id)
                    {
                        $key[] = $Pid[$id];
                    }
                }
                else
                {
                    $key = array_values($Pid);
                    $key_ID = array_rand($Nid,$combination->numbers-count($Pid));
                    if(!is_array($key_ID))
                    {
                        $key_ID = explode(",",$key_ID);
                    }
                    
                    foreach($key_ID as $id)
                    {
                        $key_FL[] = $Nid[$id];
                    }
                    $key = array_merge($key,$key_FL);
                }
                $score = $combination->weight/$combination->numbers;
                for($i = 0;$i < $combination->numbers;$i++)
                {
                    $questions[$num] = Question::findOrFail($key[$i]);
                    $list[$key[$i]] =$score;
                    $num++;
                }

            }
            if($num == 0)
            {
                $res['status'] = 1;
                $res['message'] = array('fail'=>trans('error.00032'));
                return $res;
            }
            //$list = implode(',',$list);
            $res['questions'] = $questions;
            $res['list'] = json_encode($list);
            $res['distribution'] = $distribution;
            return $res;
            
        }
        else
        {
            $res['status'] = 1;
            $res['message'] = array('fail'=>trans('error.00033'));
            return $res;
        }
        
    }




    
}
