<?php namespace App\Repositories;

use App\Models\Course;
use App\Models\User;
use App\Models\UserManage;
use App\Models\CourseClass;
use App\Models\CourseClassRecord;
use App\Models\CourseLesson;
use App\Models\CourseLessonSign;
use App\Repositories\CourseRepository;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use Auth;
class CourseClassRepository extends BaseRepository
{

    protected $course_gestion;

    public function __construct(
        CourseClass $class,
        UserRepository $user_gestion,
        CourseRepository $course_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $class;
        $this->course_gestion = $course_gestion;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }

    public function getLessonById($id)
    {
        return CourseLesson::find($id);
    }

    public function lst($course_id,$n,$orderby = 'created_at', $direction = 'desc')
    {
        $classes = $this->model->where('course_id',$course_id)
                      ->orderBy($orderby,$direction)->paginate($n);
        return $classes;

    }
    public function store($inputs,$user_id)
    {
        $class = new $this->model;
        $class->course_id = $inputs['course_id'];
        $res = $this->save($class,$inputs,$user_id);
        return $res;
    }

    public function update($inputs, $id)
    {
        $class = $this->getById($id);
        $res = $this->save($class, $inputs);
        return $res;
    }

    public function save($class,$inputs,$user_id=null)
    {
        $res = array('status'=>1,'message'=>['success'=>trans('error.10000')]);
        $class->title = $inputs['title'];
        $class->teacher = $inputs['teacher'];
        $class->late = $inputs['late'];
         if($user_id)
        {
            $class->user_id = $user_id;
        }
        $class->save();
        foreach($class->lessons as $lesson)
        {
            $lesson->delete();
        }
        for($i =1;$i <= $inputs['lessonNUM'];$i++)
        {
            $lesson = new CourseLesson;
            $lesson->user_id = $class->user_id;
            $lesson->class_id = $class->id;
            $lesson->start_at = $inputs['start_time'.$i];
            $lesson->end_at = $inputs['end_time'.$i];
            $lesson->address = $inputs['address'.$i];
            $lesson->save();
        }

        return $res;

    }

    public function destroy($id)
    {
        $class = $this->getById($id);

        $class->delete();

    }



    public function getAllSelect($course_id)
    {
        $typeSelect = $this->model->where('course_id',$course_id)->lists('title', 'id')->all();

        $typeSelect['0'] = trans('course.class_select');
        ksort($typeSelect);
        return $typeSelect;
    }

     public function getClassSigns($course_id)
     {
         $signs = array();
         $course = Course::find($course_id);
         $classId = $course->classes()->lists('id')->all();
         $lessonId = CourseLesson::whereIn('class_id',$classId)->lists('id')->all();

         $signs = CourseLessonSign::where('user_id',Auth::user()->id)->whereIn('lesson_id',$lessonId)->lists('process','lesson_id')->all();
         return $signs;

     }




    public function getRecords($course_id)
    {
        $record = array();
         $course = Course::find($course_id);
         $classId = $course->classes()->lists('id')->all();
        $record = CourseClassRecord::where('user_id',Auth::user()->id)->whereIn('class_id',$classId)->lists('process','class_id')->all();
        return $record;
    }




}
