<?php namespace App\Repositories;

use App\Models\RecordType;

class RecordTypeRepository extends BaseRepository
{

	/**
	 * The Role instance.
	 *
	 * @var App\Models\Role
	 */


	/**
	 * Create a new RolegRepository instance.
	 *
	 * @param  App\Models\Role $role
	 * @return void
	 */
	public function __construct(RecordType $type)
	{
		$this->model = $type;
	}

	/**
	 * Get all roles.
	 *
	 * @return Illuminate\Support\Collection
	 */
	public function all()
	{
		return $this->model->all();
	}

	/**
	 * Update roles.
	 *
	 * @param  array  $inputs
	 * @return void
	 */
	

	/**
	 * Get roles collection.
	 *
	 * @param  App\Models\User
	 * @return Array
	 */
	public function getAllSelect()
	{
		$types = $this->model->lists('title', 'id')->all();
        foreach($types as $key=>$type)
        {
            $typeSelect[$key] = trans($type);
        }
        $typeSelect['0'] = trans('record.all');
        ksort($typeSelect);
		return $typeSelect;
	}

}
