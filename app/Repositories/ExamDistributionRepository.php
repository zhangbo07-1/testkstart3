<?php namespace App\Repositories;

use App\Models\QuestionBank;
use App\Models\User;
use App\Models\Exam;
use App\Models\Course;
use App\Models\Record;
use App\Models\CourseRecord;
use App\Models\ExamDistribution;
use App\Models\ExamRecord;
use App\Repositories\UserRepository;
use App\Repositories\ExamRepository;
use App\Repositories\CatalogRepository;
use App\Repositories\OrganizationRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\SendEmail;
use App\Models\EmailTemplet;
use Input,Auth;

class ExamDistributionRepository extends BaseRepository
{
    use DispatchesJobs;
    protected $exam_gestion;
    protected $catalog_gestion;
    public function __construct(
        ExamDistribution $distribution,
        ExamRepository $exam_gestion,
        UserRepository $user_gestion,
        CatalogRepository $catalog_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $distribution;
        $this->exam_gestion = $exam_gestion;
        $this->user_gestion = $user_gestion;
        $this->catalog_gestion = $catalog_gestion;
        $this->organization_gestion = $organization_gestion;
    }

    public function getDistributeByExamId($id)
    {
        $distribute = $this->model->where('exam_id',$id)->where('model_id',Auth::user()->id)->first();
        return $distribute;
    }

    public function getAssignList($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $examname = isset($inputs['search_word'])?$inputs['search_word']:"";
        $course = Course::find($inputs['course_id']);
        $findList = $this->exam_gestion->getListByInput( $examname);
        $distributed = '';
        if($course->examDistribution)
        {
            $distributed = $course->examDistribution->exam_id;
        }
        $exams = Exam::whereIn('id',$findList)->where('id','<>',$distributed)->where('if_link',1)->orderBy($orderby,$direction)->paginate($n);
        return $exams;
    }

    public function assignCourse($course_id,$id)
    {
        $course = Course::find($course_id);
        if($course->examDistribution)
        {
            $course->examDistribution->delete();
        }
        $distribute = new $this->model;
        $distribute->exam_id = $id;
        $distribute->model_id = $course_id;
        $distribute->model_type = Course::class;
        $distribute->save();
        return $distribute->exam;
    }
    public function deallocateCourse($course_id)
    {
        $course = Course::find($course_id);
        if($course->examDistribution)
        {
            $course->examDistribution->delete();
        }
        return true;
    }





    public function getUnDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getUnDistributeUserList($inputs,$id);
        $unDistributeUsers = User::with('profile')->with('department')
                                                  ->with('profile.attribute1')
                                                  ->with('profile.attribute2')
                                                  ->with('profile.attribute3')
                                                  ->with('profile.attribute4')
                                                  ->with('profile.attribute5')->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $unDistributeUsers;
    }

    public function getUnDistributeUserList($inputs,$id)
    {
        $exam = Exam::find($id);
        $userList = $this->getApplyUserList($exam);
        $list = $this->user_gestion->getUserIdByInputs($inputs);
        
        $choseIdArray = array();
        foreach($exam->distributions as $distribution)
        {
            $choseIdArray[] = $distribution->model_id;
        }
        $unDistributeUsers = User::whereIn('id',$list)->whereIn('id',$userList)->whereNotIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $unDistributeUsers;

    }




    public function getDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getDistributeUserList($inputs,$id);
        $DistributeUsers = User::with('profile')->with('department')
                                                ->with('profile.attribute1')
                                                ->with('profile.attribute2')
                                                ->with('profile.attribute3')
                                                ->with('profile.attribute4')
                                                ->with('profile.attribute5')->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $DistributeUsers;
    }

    public function getDistributeUserList($inputs,$id)
    {
        
        $exam = Exam::find($id);
        $list = $this->user_gestion->getUserIdByInputs($inputs);
        
        $choseIdArray = array();
        foreach($exam->distributions as $distribution)
        {
            $choseIdArray[] = $distribution->model_id;
        }
        $DistributeUsers = User::whereIn('id',$list)->whereIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $DistributeUsers;

    }




    
    public function adddistributions($inputs,$id)
    {
        $res = array('result'=>true,'message'=>trans('record.distribute_success'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getUnDistributeUserList($inputs,$id);
            
        }
        else
        {
            $list = $inputs['user_id'];
        }
        foreach($list as $user_id)
        {
            
            $distribute = new $this->model;
            $distribute->exam_id = $id;
            $distribute->model_id = $user_id;
            $distribute->model_type = User::class;
            $distribute->save();
            $list = new Record;
            $list->type = 2;
            $list->model_id = $distribute->id;
            $list->model_type = ExamDistribution::class;
            $list->user_id = $user_id;
            $list->save();
            $user = User::find($user_id);
            if ( $distribute->exam->emailed > 0 )
            {
                $url = $_SERVER['HTTP_HOST'];
                $email = EmailTemplet::whereType(1)->first();

                $email->body = str_replace('{{用户名}}',$user->name,$email->body);
                $email->body = str_replace('{{课程标题}}',$distribute->exam->title,$email->body);
                $email->body = str_replace('{{用户网址}}',$url,$email->body);
                $this->dispatch(new SendEmail($email,$user));
                


            }
        }
        return $res;
    }

    public function deleteDistributions($inputs,$id)
    {
        $res = array('result'=>true,'message'=>trans('record.undistribute_success'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getDistributeUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }
        $distributes = $this->model->where('exam_id',$id)->whereIn('model_id',$list)->get();

        foreach($distributes as $distribute)
        {
            foreach($distribute->records as $record)
                $record->delete();
            foreach($distribute->record as $record)
                $record->delete();
            $distribute->delete();
        }
        return $res;
    }


    public function ifOver()
    {
        $records = ExamRecord::where('user_id',Auth::user()->id)->whereIn('process', array(1,2))->get();
        foreach($records as $record)
        {
            if(($record->distribution->exam->limit_times > 0)&&(count($record->results) >= $record->distribution->exam->limit_times))
            {
                $record->process = 4;
                $record->save();
            }
            if(($record->process == 4) &&( ($record->distribution->exam->limit_times == 0) || (count($record->results) < $record->distribution->exam->limit_times)))
            {
                if(count($record->results))
                    $record->process = 2;
                else
                    $record->process = 1;
                $record->save();
            }
        }
        
        $exams = Exam::where('if_link',0)->lists('id')->all();
        $distributions = ExamDistribution::whereIn('exam_id',$exams)->where('model_id',Auth::user()->id)->get();
        foreach($distributions as $distribution)
        {
            if($distribution->exam->limit_days > 0)
            {
                $limitDay = ((strtotime($distribution->created_at))
                    +$distribution->exam->limit_days*24*60*60);

                $limittime =($limitDay-time());
                if($limittime < 0)
                {
                    if(count($distribution->records))
                    {
                        if($distribution->records[0]->process != 3)
                        {
                            $distribution->records[0]->process = 5;
                            $distribution->records[0]->save();
                        }
                    }
                    else
                    {
                        $record = new ExamRecord;
                        $record->user_id = Auth::user()->id;
                        $record->distribution_id =  $distribution->id;
                        $record->process = 5;
                        $record->save();
                    }
                }
                else
                {
                    if(count($distribution->records) && $distribution->records[0]->process == 5)
                    {
                        $distribution->records[0]->process = 2;
                        $distribution->records[0]->save();
                    }
                }
            }
            else
            {
                if(count($distribution->records) && $distribution->records[0]->process == 5)
                {
                    $distribution->records[0]->process = 2;
                    $distribution->records[0]->save();
                }
            }
        }
        
    }

    public function exams($inputs)
    {
        $examsList = $this->getDistributionList($inputs,true,false);
        $listDone = ExamRecord::where('user_id',Auth::user()->id)->whereIN('process',[3,4,5])->lists('distribution_id')->all();
        $distributions = ExamDistribution::whereIn('id',$examsList)->whereNotIn('id',$listDone)->where('model_id',Auth::user()->id)->lists('id')->all();
        return $distributions;
        
    }

    public function examsDone($inputs)
    {
        $listDone = ExamRecord::where('user_id',Auth::user()->id)->whereIn('process',[3,4])->lists('distribution_id')->all();
        $examsList = $this->getDistributionList($inputs,true,false);
        $distributions = ExamDistribution::whereIn('id',$examsList)->whereIn('id',$listDone)->where('model_id',Auth::user()->id)->lists('id')->all();
        
        return $distributions;
        
    }

    
       public function examsOverdue($inputs)
       {
       $listOverdue = ExamRecord::where('user_id',Auth::user()->id)->where('process',5)->lists('distribution_id')->all();
       $examsList = $this->getDistributionList($inputs,true,false);
       $distributions = ExamDistribution::whereIn('id',$examsList)->whereIn('id',$listOverdue)->where('model_id',Auth::user()->id)->lists('id')->all();
       
       return $distributions;
       
       }

     
    
    public function getDistribution($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $userList =  $this->user_gestion->getUserIdByInput( isset($inputs['findByUserName'])?$inputs['findByUserName']:"" );
        $examList = $this->exam_gestion->getListByInput(isset($inputs['findByCourseName'])?$inputs['findByCourseName']:"",true);
        if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);

            $examList = Exam::whereIn('id',$examList)
                         ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }
        
        $examDistributions = $this->model->whereIn('exam_id',$examList)->whereIn('model_id',$userList)->orderBy($orderby,$direction)->paginate($n);
        return $examDistributions;

    }


    public function getDistributionList($inputs,$type=true,$manage=true)
    {
        $userList =  $this->user_gestion->getUserIdByInput( isset($inputs['findByUserName'])?$inputs['findByUserName']:"" );
        $examList = $this->exam_gestion->getListByInput(isset($inputs['findByCourseName'])?$inputs['findByCourseName']:"",$type,$manage);
        if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);
            $catalogArray = $this->catalog_gestion->getUnderArray($catalogArray);

            $examList = Exam::whereIn('id',$examList)
                         ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }
        $dateLimit = isset($inputs['dateLimit'])?$inputs['dateLimit']:0;
        $start_time = !empty($inputs['startDate'])?$inputs['startDate']:date("Y-m-d H:i:s",0);
        $end_time = !empty($inputs['endDate'])?$inputs['endDate']:date("Y-m-d H:i:s",time());
        if($dateLimit == 1)
        {
            $recordList = $this->model->whereIn('exam_id',$examList)
                               ->whereIn('model_id',$userList)
                               ->WhereHas('records',function($q) use ($start_time ,$end_time)
                                   {
                                       $q->whereBetween('created_at',[$start_time,$end_time]);
                                   })->lists('id')->all();
        }
        elseif($dateLimit == 2)
        {
            $recordList = $this->model->whereIn('exam_id',$examList)
                               ->whereIn('model_id',$userList)
                               ->WhereHas('records',function($q) use ($start_time ,$end_time)
                                   {
                                       $q->whereBetween('end_at',[$start_time,$end_time]);
                                   })->lists('id')->all();
        }
        else
        {
            $recordList = $this->model->whereIn('exam_id',$examList)->whereIn('model_id',$userList)->lists('id')->all();
        }
        return $recordList;
    }



    public function returnURL($distribute_id)
    {
        $distribute = $this->getById($distribute_id);
        if($distribute->model_type == Course::class)
        {
            $record = CourseRecord::where('course_id',$distribute->model_id)->where('user_id',Auth::user()->id)->first();
            if($record)
            {
                if($record->process == 5)
                    return 3;
                elseif($record->process > 3)
                    return 2;
            }
            return 1;
        }
        else
        {
            $record = ExamRecord::where('distribution_id',$distribute->id)->where('user_id',Auth::user()->id)->first();
            if($record)
            {
                if($record->process < 3)
                    return 1;
                elseif($record->process < 5)
                    return 2;
                elseif($record->process == 5)
                    return 3;
            }
            return 1;
        }

        
        
    }



}
