<?php namespace App\Repositories;

use App\Models\SurveyResult;
use App\Models\SurveyRecord;
use App\Models\ItemSelectResult;
use App\Models\ItemGradeResult;
use App\Models\CourseClassRecord;
use App\Repositories\SurveyRepository;
use Auth;
class SurveyResultRepository extends BaseRepository
{
    protected $survey_gestion;
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
	SurveyResult $surveyResult,
        SurveyRepository $survey_gestion
    )
    {
        $this->model = $surveyResult;
        $this->survey_gestion = $survey_gestion;
    }
    public function score($record_id,$inputs)
    {
        $surveyResult = new $this->model;
        $surveyResult->record_id = $record_id;
        $surveyResult->user_id = Auth::user()->id;
        $surveyResult->save();
        $record = SurveyRecord::find($record_id);
        $survey = $record->survey;
        foreach($survey->items as $item)
        {
            if($item->type == 1)
            {
                $results = $inputs[$item->id];
                foreach($results as $result)
                {
                    $selectResult = new ItemSelectResult;
                    $selectResult->select_id = $result;
                    $selectResult->result_id = $surveyResult->id;
                    $selectResult->save();
                }
            }
            else
            {
                $gradeResult = new ItemGradeResult;
                $gradeResult->grade_id = $item->grade->id;
                $gradeResult->result_id = $surveyResult->id;
                $gradeResult->choose = $inputs[$item->id];
                $gradeResult->save();
            }
        }
        return $surveyResult;
       
    }


    public function result($id,$inputs=null)
    {
        $record = SurveyRecord::find($id);
        $survey = $record->survey;
        $resultList = SurveyResult::where('record_id',$record->id)->lists('id')->all();
        if(!empty($inputs['class_id']))
        {
            $userList = CourseClassRecord::where('class_id',$inputs['class_id'])->lists('user_id')->all();
            $resultList = SurveyResult::whereIn('id',$resultList)->whereIn('user_id',$userList)->lists('id')->all();
                
        }
        $result['resultNumber'] = count($resultList);
        if($result['resultNumber'])
        {
            foreach($survey->items as $item)
            {
                if($item->type == 1)
                {
                    foreach($item->selects as $select)
                    {
                        $resultNum = ItemSelectResult::whereIn('result_id',$resultList)->where('select_id',$select->id)->count();
                        $result['number'][$item->id][$select->id] = $resultNum;
                    }
                }
                else
                {
                    $grade = $item->grade;
                    for($i = 1;$i<=5;$i++)
                    {
                        $result['number'][$item->id][$i] = ItemGradeResult::whereIn('result_id',$resultList)->where('grade_id',$grade->id)->where('choose',$i)->count();
                        $sum[$i] = $result['number'][$item->id][$i]*$i;
                    }
                    $result['avg'][$item->id] = round(array_sum($sum)/$result['resultNumber'],2);
                }
            
            }
        }
        return $result;
    }

    public function getResultsByRecordId($record_id)
    {
        $results = $this->model->where('record_id',$record_id)->where('user_id',Auth::user()->id)->get();
        return $results;
    }


    public function excel($result,$survey)
    {
        $prog[] = array('', trans('table.content'),trans('table.choice_number'), trans('table.percent'));
        foreach($survey->items as $index=>$item)
        {
            $prog[] = array(trans('question.title').($index+1),$item->title,'','');
            if($item->type ==1)
            {
                foreach($item->selects as $select)
                {
                    $prog[] = array('',$select->title,$result['number'][$item->id][$select->id],round($result['number'][$item->id][$select->id]/$result['resultNumber']*100,2).'%');
                }
            }
            else
            {
                $grade = $item->grade;
                $prog[] = array(trans('table.ps'),'1:'.$grade->lowtitle.'～～5:'.$grade->hightitle,'','');
                for($j = 1;$j <= 5;$j++)
                {
                    $prog[] = array('',$j,$result['number'][$item->id][$j],round($result['number'][$item->id][$j]/$result['resultNumber']*100,2).'%');
                }
                $prog[] = array(trans('table.avg'),$result['avg'][$item->id],'','');
            }
        }
        return $prog;
    }

}
