<?php namespace App\Repositories;

use App\Models\Course;
use App\Models\PostArea;
use App\Models\ResourceArea;
use App\Models\CourseManage;
use App\Models\SendLimit;
use App\Models\CourseMaintain;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\CourseWareRepository;
use App\Repositories\CatalogRepository;
use Input,Storage,Auth;
class CourseRepository extends BaseRepository
{

    /**
     * Create a new UserRepository instance.
     *
     * @param  App\Models\Course $course
     * @param  App\Models\Record $record
     * @return void
     */


    protected $ware_gestion;
    public function __construct(
        Course $course,
        CourseManage $course_manage,
        CourseMaintain $course_maintain,
        UserRepository $user_gestion,
        CourseWareRepository $ware_gestion,
        CatalogRepository $catalog_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $course;
        $this->manage_model = $course_manage;
        $this->catalog_gestion = $catalog_gestion;
        $this->maintain_model = $course_maintain;
        $this->user_gestion = $user_gestion;
        $this->ware_gestion = $ware_gestion;
        $this->organization_gestion = $organization_gestion;
    }




    public function index($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $keyword = isset($inputs['findByCourseName'])?$inputs['findByCourseName']:"";
        $list = $this->getListByInput($keyword);
        if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);
 $catalogArray = $this->catalog_gestion->getUnderArray($catalogArray);
            $list = $this->model->whereIn('id',$list)
                  ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }
        
        $courses = $this->model->whereIn('id',$list)
                      ->orderBy($orderby,$direction)->paginate($n);
        return $courses;

    }


    private function saveCourse($course, $inputs, $user_id = null)
    {
        $res = array('status'=>1,'message'=>['success'=>trans('error.10000')],'id'=>'');
        $course->course_id = $inputs['course_id'];
        $course->language_id = $inputs['language_id'];
        $course->catalog_id = $inputs['catalog_id'];
        if($user_id)
        {
            $course->user_id = $user_id;
        }
        $course->hours = isset($inputs['hours'])?$inputs['hours']:"0";
        $course->minutes = isset($inputs['minutes'])?$inputs['minutes']:"0";
        $course->title = $inputs['title'];
        $course->target = $inputs['target'];
        $course->description = $inputs['description'];
        $course->limitdays = isset($inputs['limitdays'])?$inputs['limitdays']:"0";
        $course->emailed = isset($inputs['emailed']);
        $course->if_order= isset($inputs['if_order']);
        $course->if_send_new_user = isset($inputs['if_send_new_user']);
        $course->if_open_overdue = isset($inputs['if_open_overdue']);
        $course->save();
        $this->setManageId($course,$inputs['manage_id'],$user_id);
        $sendLimit = $course->sendLimit;
        if(!$sendLimit)
        {
            $sendLimit = new SendLimit;
            $sendLimit->course_id = $course->id;
        }
        $sendLimit->attribute1_list = isset($inputs['attribute1_list'])?$inputs['attribute1_list']:"";
        $sendLimit->attribute2_list = isset($inputs['attribute2_list'])?$inputs['attribute2_list']:"";
        $sendLimit->attribute3_list = isset($inputs['attribute3_list'])?$inputs['attribute3_list']:"";
        $sendLimit->attribute4_list = isset($inputs['attribute4_list'])?$inputs['attribute4_list']:"";
        $sendLimit->attribute5_list = isset($inputs['attribute5_list'])?$inputs['attribute5_list']:"";
        $sendLimit->save();
        

        $res['id'] = $course->course_id;
        return $res;
    }



    public function store($inputs, $user_id)
    {
        $course = new $this->model;
        $postArea = new PostArea;
        $postArea->save();
        $resourceArea = new ResourceArea;
        $resourceArea->save();
        $course->area_id = $postArea->id;
        $course->resource_id = $resourceArea->id;
        $course->type = $inputs['type'];
        $res = $this->saveCourse($course, $inputs, $user_id);

        
        return $res;
    }

    public function update($inputs, $id)
    {
        $course = $this->getById($id);
        $course = $this->saveCourse($course, $inputs);
        return $course;
    }


    public function destroy($id)
    {
        $course = $this->getById($id);
        if($course->postArea)
        {
            $course->postArea->delete();
        }
        if($course->resourceArea)
        {
            $course->resourceArea->delete();
        }
        if($course->examDistribution)
        {
            $course->examDistribution->delete();
        }
        foreach($course->wares as $ware)
        {
            $this->ware_gestion->destroy($ware->id);
        }
        $this->ware_gestion->cleanFiles($id);
        $course->delete();

    }





    public function countAllScore($course = null)
    {
        $score = 0;
        if($course)
        {
            $records =  $this->record
                             ->where('course_id',$course->id)->get();

            foreach($records as $r){
                if ( $r->score !== null ) {
                    $score = $score + $r->$score;
                }
            }
        }
        return $score;
    }
    public function getListByInput($input,$manage=true)
    {
        if($manage)
        {
            $list = $this->getModifyModelList();

        }
        else
        {
            $list = $this->model->lists('id')->all();
        }
        $user = Auth::user();
        $list = $this->model->where(function($query) use ($user,$list,$manage)
                                    {
                                        $query->where('user_id',$user->id)
                                        ->orWhereHas('maintains',function($q) use ($user)
                                        {
                                            $q->where('maintain_id',$user->id);
                                        });
                                        if($user->role_id > 2 || !$manage)
                                        {
                                            $query->orwhereIn('id',$list);
                                        }
                                    })
              ->where(function($query) use ($input)
                      {
                          $query->where('title', 'LIKE', '%' . $input . '%')
                          ->orWhere('course_id', 'LIKE', '%' . $input . '%')
                          ->orWhere('description', 'LIKE', '%' . $input . '%');
                      })
              ->lists('id')->all();
        return $list;
    }




    public function getCourseByCourseId($course_id)
    {
        $course = $this->model->where('course_id',$course_id)->firstOrFail();
        return $course;
    }


    public function assignResource($course_id)
    {
        $course = $this->model->where('course_id',$course_id)->first();
        if(!$course->resource_id)
        {
            $resourceArea = new ResourceArea;
            $resourceArea->save();
            $course->resource_id = $resourceArea->id;
            $course->save();
        }
    }




}
