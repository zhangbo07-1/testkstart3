<?php namespace App\Repositories;


use App\Models\Attribute;
use App\Models\FirstAttribute;
use App\Models\SecondAttribute;
use App\Models\ThirdAttribute;
use App\Models\FourthAttribute;
use App\Models\FifthAttribute;
use App\Models\Sex;
use Excel;
class AttributeRepository extends BaseRepository
{


    protected $firstAttribute;
    protected $secondAttribute;
    protected $thirdAttribute;
    protected $fourthAttribute;
    protected $fifthAttribute;

	
	public function __construct(
        FirstAttribute $firstAttribute,
        SecondAttribute $secondAttribute,
        ThirdAttribute $thirdAttribute,
        FourthAttribute $fourthAttribute,
        FifthAttribute $fifthAttribute
    )
	{
        $this->firstAttribute = $firstAttribute;
        $this->secondAttribute = $secondAttribute;
        $this->thirdAttribute = $thirdAttribute;
        $this->fourthAttribute = $fourthAttribute;
        $this->fifthAttribute = $fifthAttribute;
	}



     public function getAllSexSelect()
	{
		$sextypes = Sex::get();
        foreach($sextypes as $sex)
        {
            $sexSelects[$sex->id] = trans($sex->title);
        }
        $sexSelects['0'] = trans('attribute.select');
        ksort($sexSelects);

		return $sexSelects;
	}

    

    public function getAttribute()
    {
        $attribute = Attribute::first();
        return $attribute;
    }


    

    public function getAllFirstAttributeSelect()
	{
		$firstAttributeSelects = $this->firstAttribute->lists('title', 'id')->all();
        $attribute = $this->getAttribute();
        $firstAttributeSelects['0'] = trans('attribute.select').$attribute->first_title;
        ksort($firstAttributeSelects);

		return $firstAttributeSelects;
	}


     public function firstAttributeRename($inputs)
     {
          $res = array('message'=>array('success'=>trans('error.10000')));
         $attribute = $this->getAttribute();
         $attribute->first_title = $inputs['attribute_title'];
         $attribute->save();
         return $res;
     }

    public function firstAttributeAdd($input)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
         $attribute = $this->firstAttribute->whereTitle($input)->first();
        if($attribute)
        {
            $res = array('message'=>array('fail'=>trans('error.00056')));
            return $res;
        }
        $firstAttribute = new $this->firstAttribute;
        $firstAttribute->title = $input;
        $firstAttribute->save();
        return $res;
    }

    public function firstAttributeUpdate($inputs)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
        $attribute = $this->firstAttribute->find($inputs['attribute_id']);
        if($attribute)
        {
            $attribute->title = $inputs['rename_attribute'];
            $attribute->save();
        }
        else
        {
           $res['message'] = array('fail'=>trans('error.00039')); 
        }
        return $res;
    }

    public function firstAttributeDelete($id)
	{
        $res = array('message'=>array('success'=>trans('error.10000')));
        if($id != 0 )
        {
            $firstAttribute = $this->firstAttribute->find($id);
            if(count($firstAttribute->profile))
            {
                $res['message'] = array('fail'=>trans('error.00038'));
            }
            else
            {
                $firstAttribute->delete();
            }
        }
        else
        {
             $res['message'] = array('fail'=>trans('error.00039'));
        }
		return $res;
	}

    public function firstAttributeImport($file)
    {
        $result = array('message'=>array('success'=>trans('error.10000')));
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            if(!empty($prog[array_search('操作',$tag)]) && !empty($prog[array_search('属性名',$tag)]))
            {
                $acts[] = $prog[array_search('操作',$tag)];
                $title[] = $prog[array_search('属性名',$tag)];
            }
        }

        $error = $this->FetchRepeatMemberInArray($title);
        if(!empty($error))
        {
            $result['message'] = array('file'=>trans('error.00056').implode(",",$error));
            return $result;
        }
        foreach($acts as $key=>$act)
        {
            switch($act)
            {
            case 'A':
                $attribute = $this->firstAttribute->whereTitle($title[$key])->first();
                if(!$attribute)
                {
                    $this->firstAttributeAdd($title[$key]);
                }
                break;
            case 'D':
                $attribute = $this->firstAttribute->whereTitle($title[$key])->first();
                if($attribute)
                    $this->firstAttributeDelete($attribute->id);
                break;
                
            }
        }
        return $result;
        
    }


     

    public function getAllSecondAttributeSelect()
	{
		$secondAttributeSelects = $this->secondAttribute->lists('title', 'id')->all();
        $attribute = $this->getAttribute();
        $secondAttributeSelects['0'] = trans('attribute.select').$attribute->second_title;
        ksort($secondAttributeSelects);

		return $secondAttributeSelects;
	}

    public function manageSecondAttributeSelect()
	{
		$secondAttributes = $this->secondAttribute->all();
        $secondAttributeSelects = array();
        foreach($secondAttributes as $secondAttribute)
        {
            $secondAttributeSelects += array($secondAttribute->id=>$secondAttribute->title.'('.$secondAttribute->tag.')');
        }
		return $secondAttributeSelects;
	}

     public function secondAttributeRename($inputs)
     {
          $res = array('message'=>array('success'=>trans('error.10000')));
         $attribute = $this->getAttribute();
         $attribute->second_title = $inputs['attribute_title'];
         $attribute->save();
         return $res;
     }

    public function secondAttributeAdd($input)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
         $attribute = $this->secondAttribute->whereTitle($input)->first();
        if($attribute)
        {
            $res = array('message'=>array('fail'=>trans('error.00056')));
            return $res;
        }
        $secondAttribute = new $this->secondAttribute;
        $secondAttribute->title = $input;
        $secondAttribute->save();
        return $res;
    }

    public function secondAttributeUpdate($inputs)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
        $attribute = $this->secondAttribute->find($inputs['attribute_id']);
        if($attribute)
        {
            $attribute->title = $inputs['rename_attribute'];
            $attribute->save();
        }
        else
        {
           $res['message'] = array('fail'=>trans('error.00039')); 
        }
        return $res;
    }

    public function secondAttributeDelete($id)
	{
        $res = array('message'=>array('success'=>trans('error.10000')));
        if($id != 0 )
        {
            $secondAttribute = $this->secondAttribute->find($id);
            if(count($secondAttribute->profile))
            {
                $res['message'] = array('fail'=>trans('error.00038'));
            }
            else
            {
                $secondAttribute->delete();
            }
        }
        else
        {
             $res['message'] = array('fail'=>trans('error.00039'));
        }
		return $res;
	}

    public function secondAttributeImport($file)
    {
        $result = array('message'=>array('success'=>trans('error.10000')));
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            if(!empty($prog[array_search('操作',$tag)]) && !empty($prog[array_search('属性名',$tag)]))
            {
                $acts[] = $prog[array_search('操作',$tag)];
                $title[] = $prog[array_search('属性名',$tag)];
            }
        }

        $error = $this->FetchRepeatMemberInArray($title);
        if(!empty($error))
        {
            $result['message'] = array('file'=>trans('error.00056').implode(",",$error));
            return $result;
        }
        foreach($acts as $key=>$act)
        {
            switch($act)
            {
            case 'A':
                $attribute = $this->secondAttribute->whereTitle($title[$key])->first();
                if(!$attribute)
                {
                    $this->secondAttributeAdd($title[$key]);
                }
                break;
            case 'D':
                $attribute = $this->secondAttribute->whereTitle($title[$key])->first();
                if($attribute)
                    $this->secondAttributeDelete($attribute->id);
                break;
                
            }
        }
        return $result;
        
    }


     

    public function getAllThirdAttributeSelect()
	{
		$thirdAttributeSelects = $this->thirdAttribute->lists('title', 'id')->all();
        $attribute = $this->getAttribute();
        $thirdAttributeSelects['0'] = trans('attribute.select').$attribute->third_title;
        ksort($thirdAttributeSelects);

		return $thirdAttributeSelects;
	}

    public function manageThirdAttributeSelect()
	{
		$thirdAttributes = $this->thirdAttribute->all();
        $thirdAttributeSelects = array();
        foreach($thirdAttributes as $thirdAttribute)
        {
            $thirdAttributeSelects += array($thirdAttribute->id=>$thirdAttribute->title.'('.$thirdAttribute->tag.')');
        }
		return $thirdAttributeSelects;
	}

     public function thirdAttributeRename($inputs)
     {
          $res = array('message'=>array('success'=>trans('error.10000')));
         $attribute = $this->getAttribute();
         $attribute->third_title = $inputs['attribute_title'];
         $attribute->save();
         return $res;
     }

    public function thirdAttributeAdd($input)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
         $attribute = $this->thirdAttribute->whereTitle($input)->first();
        if($attribute)
        {
            $res = array('message'=>array('fail'=>trans('error.00056')));
            return $res;
        }
        $thirdAttribute = new $this->thirdAttribute;
        $thirdAttribute->title = $input;
        $thirdAttribute->save();
        return $res;
    }


    public function thirdAttributeUpdate($inputs)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
        $attribute = $this->thirdAttribute->find($inputs['attribute_id']);
        if($attribute)
        {
            $attribute->title = $inputs['rename_attribute'];
            $attribute->save();
        }
        else
        {
           $res['message'] = array('fail'=>trans('error.00039')); 
        }
        return $res;
    }

    public function thirdAttributeDelete($id)
	{
        $res = array('message'=>array('success'=>trans('error.10000')));
        if($id!= 0 )
        {
            $thirdAttribute = $this->thirdAttribute->find($id);
            if(count($thirdAttribute->profile))
            {
                $res['message'] = array('fail'=>trans('error.00038'));
            }
            else
            {
                $thirdAttribute->delete();
            }
        }
        else
        {
             $res['message'] = array('fail'=>trans('error.00039'));
        }
		return $res;
	}

    public function thirdAttributeImport($file)
    {
        $result = array('message'=>array('success'=>trans('error.10000')));
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            if(!empty($prog[array_search('操作',$tag)]) && !empty($prog[array_search('属性名',$tag)]))
            {
                $acts[] = $prog[array_search('操作',$tag)];
                $title[] = $prog[array_search('属性名',$tag)];
            }
        }

        $error = $this->FetchRepeatMemberInArray($title);
        if(!empty($error))
        {
            $result['message'] = array('file'=>trans('error.00056').implode(",",$error));
            return $result;
        }
        foreach($acts as $key=>$act)
        {
            switch($act)
            {
            case 'A':
                $attribute = $this->thirdAttribute->whereTitle($title[$key])->first();
                if(!$attribute)
                {
                    $this->thirdAttributeAdd($title[$key]);
                }
                break;
            case 'D':
                $attribute = $this->thirdAttribute->whereTitle($title[$key])->first();
                if($attribute)
                    $this->thirdAttributeDelete($attribute->id);
                break;
                
            }
        }
        return $result;
        
    }


    
    public function getAllFourthAttributeSelect()
	{
		$fourthAttributeSelects = $this->fourthAttribute->lists('title', 'id')->all();
        $attribute = $this->getAttribute();
        $fourthAttributeSelects['0'] = trans('attribute.select').$attribute->fourth_title;
        ksort($fourthAttributeSelects);

		return $fourthAttributeSelects;
	}


     public function fourthAttributeRename($inputs)
     {
          $res = array('message'=>array('success'=>trans('error.10000')));
         $attribute = $this->getAttribute();
         $attribute->fourth_title = $inputs['attribute_title'];
         $attribute->save();
         return $res;
     }

    public function fourthAttributeAdd($input)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
         $attribute = $this->fourthAttribute->whereTitle($input)->first();
        if($attribute)
        {
            $res = array('message'=>array('fail'=>trans('error.00056')));
            return $res;
        }
        $fourthAttribute = new $this->fourthAttribute;
        $fourthAttribute->title = $input;
        $fourthAttribute->save();
        return $res;
    }

    public function fourthAttributeUpdate($inputs)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
        $attribute = $this->fourthAttribute->find($inputs['attribute_id']);
        if($attribute)
        {
            $attribute->title = $inputs['rename_attribute'];
            $attribute->save();
        }
        else
        {
           $res['message'] = array('fail'=>trans('error.00039')); 
        }
        return $res;
    }

    public function fourthAttributeDelete($id)
	{
        $res = array('message'=>array('success'=>trans('error.10000')));
        if($id != 0 )
        {
            $fourthAttribute = $this->fourthAttribute->find($id);
            if(count($fourthAttribute->profile))
            {
                $res['message'] = array('fail'=>trans('error.00038'));
            }
            else
            {
                $fourthAttribute->delete();
            }
        }
        else
        {
             $res['message'] = array('fail'=>trans('error.00039'));
        }
		return $res;
	}

    public function fourthAttributeImport($file)
    {
        $result = array('message'=>array('success'=>trans('error.10000')));
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            if(!empty($prog[array_search('操作',$tag)]) && !empty($prog[array_search('属性名',$tag)]))
            {
                $acts[] = $prog[array_search('操作',$tag)];
                $title[] = $prog[array_search('属性名',$tag)];
            }
        }

        $error = $this->FetchRepeatMemberInArray($title);
        if(!empty($error))
        {
            $result['message'] = array('file'=>trans('error.00056').implode(",",$error));
            return $result;
        }
        foreach($acts as $key=>$act)
        {
            switch($act)
            {
            case 'A':
                $attribute = $this->fourthAttribute->whereTitle($title[$key])->first();
                if(!$attribute)
                {
                    $this->fourthAttributeAdd($title[$key]);
                }
                break;
            case 'D':
                $attribute = $this->fourthAttribute->whereTitle($title[$key])->first();
                if($attribute)
                    $this->fourthAttributeDelete($attribute->id);
                break;
                
            }
        }
        return $result;
        
    }



    public function getAllFifthAttributeSelect()
	{
		$fifthAttributeSelects = $this->fifthAttribute->lists('title', 'id')->all();
        $attribute = $this->getAttribute();
        $fifthAttributeSelects['0'] = trans('attribute.select').$attribute->fifth_title;
        ksort($fifthAttributeSelects);

		return $fifthAttributeSelects;
	}


     public function fifthAttributeRename($inputs)
     {
          $res = array('message'=>array('success'=>trans('error.10000')));
         $attribute = $this->getAttribute();
         $attribute->fifth_title = $inputs['attribute_title'];
         $attribute->save();
         return $res;
     }

    public function fifthAttributeAdd($input)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
        $attribute = $this->fifthAttribute->whereTitle($input)->first();
        if($attribute)
        {
            $res = array('message'=>array('fail'=>trans('error.00056')));
            return $res;
        }
        $fifthAttribute = new $this->fifthAttribute;
        $fifthAttribute->title = $input;
        $fifthAttribute->save();
        return $res;
    }


    public function fifthAttributeUpdate($inputs)
    {
        $res = array('message'=>array('success'=>trans('error.10000')));
        $attribute = $this->fifthAttribute->find($inputs['attribute_id']);
        if($attribute)
        {
            $attribute->title = $inputs['rename_attribute'];
            $attribute->save();
        }
        else
        {
           $res['message'] = array('fail'=>trans('error.00039')); 
        }
        return $res;
    }
    public function fifthAttributeDelete($id)
	{
        $res = array('message'=>array('success'=>trans('error.10000')));
        if($id != 0 )
        {
            $fifthAttribute = $this->fifthAttribute->find($id);
            if(count($fifthAttribute->profile))
            {
                $res['message'] = array('fail'=>trans('error.00038'));
            }
            else
            {
                $fifthAttribute->delete();
            }
        }
        else
        {
             $res['message'] = array('fail'=>trans('error.00039'));
        }
		return $res;
	}

    public function fifthAttributeImport($file)
    {
        $result = array('message'=>array('success'=>trans('error.10000')));
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            if(!empty($prog[array_search('操作',$tag)]) && !empty($prog[array_search('属性名',$tag)]))
            {
                $acts[] = $prog[array_search('操作',$tag)];
                $title[] = $prog[array_search('属性名',$tag)];
            }
        }

        $error = $this->FetchRepeatMemberInArray($title);
        if(!empty($error))
        {
            $result['message'] = array('file'=>trans('error.00056').implode(",",$error));
            return $result;
        }
        foreach($acts as $key=>$act)
        {
            switch($act)
            {
            case 'A':
                $attribute = $this->fifthAttribute->whereTitle($title[$key])->first();
                if(!$attribute)
                {
                    $this->fifthAttributeAdd($title[$key]);
                }
                break;
            case 'D':
                $attribute = $this->fifthAttribute->whereTitle($title[$key])->first();
                if($attribute)
                    $this->fifthAttributeDelete($attribute->id);
                break;
                
            }
        }
        return $result;
        
    }

    public function FetchRepeatMemberInArray($array)
    {
        // 获取去掉重复数据的数组
        $unique_arr = array_unique ( $array );
        // 获取重复数据的数组
        $repeat_arr = array_diff_assoc ( $array, $unique_arr );
        return $repeat_arr;
    }



}
