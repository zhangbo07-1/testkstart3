<?php namespace App\Repositories;

use App\Models\Survey;
use App\Models\Course;
use App\Models\CourseRecord;
use App\Models\ExamRecord;
use App\Models\SurveyManage;
use App\Models\SurveyRecord;
use App\Models\SurveyResult;
use App\Repositories\UserRepository;
use App\Repositories\CourseRepository;
use App\Repositories\OrganizationRepository;
use Auth;
class SurveyRepository extends BaseRepository
{
    protected $course_gestion;
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
	Survey $survey,
        SurveyManage $survey_manage,
        UserRepository $user_gestion,
    CourseRepository $course_gestion,
        OrganizationRepository $organization_gestion
    )
    {
	$this->model = $survey;
        $this->manage_model = $survey_manage;
        $this->user_gestion = $user_gestion;
        $this->course_gestion = $course_gestion;
        $this->organization_gestion = $organization_gestion;
    }
    
    public function index($n,$type,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $surveyname = isset($inputs['findBySurveyname'])?$inputs['findBySurveyname']:"";
        $list = $this->getModifyModelList();
        $surveys = $this->model->whereIn('id',$list)->whereType($type)->where('title','LIKE','%' . $surveyname . '%')->orderBy($orderby,$direction)->paginate($n);
        return $surveys;
    }

    public function lst($n,$type,$inputs="",$orderby = 'created_at', $direction = 'desc')
    {
        $surveyname = isset($inputs['findBySurveyname'])?$inputs['findBySurveyname']:"";
        $list = $this->getApplyModelList();
        $surveys = $this->model->whereIn('id',$list)->whereType($type)->wherePushed(1)->where('title','LIKE','%' . $surveyname . '%')->orderBy($orderby,$direction)->paginate($n);
        return $surveys;
    }

    public function getDistributeList($n,$inputs,$type = 2,$orderby = 'created_at', $direction = 'desc')
    {
        $surveyname = isset($inputs['search_word'])?$inputs['search_word']:"";
        $course_id = isset($inputs['course_id'])?$inputs['course_id']:0;
        $distributed = SurveyRecord::where('course_id',$course_id)->where('type',$type)->lists('survey_id')->all();
        $surveys = $this->model->with('items')->whereNotIn('id',$distributed)->whereType($type)->wherePushed(1)->where('title','LIKE','%' . $surveyname . '%')->orderBy($orderby,$direction)->paginate($n);
        return $surveys;
    }
    
    /**
     * Create or update a post.
     *
     * @param  App\Models\Post $post
     * @param  array  $inputs
     * @param  bool   $user_id
     * @return App\Models\Post
     */
    private function saveSurvey($survey, $inputs, $user_id = null)
    {
        $res = array('status'=>1,'error'=>'','id'=>'');
	$survey->title = $inputs['title'];
	if($user_id)
        {
            $survey->user_id = $user_id;
        }
        $survey->save();
        $this->setManageId($survey,$inputs['manage_id'],$user_id);
        $res['id'] = $survey->id;
	return $res;
    }

    /**
     * Update a post.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function update($inputs, $id)
    {
	$survey = $this->getById($id);
	$res = $this->saveSurvey($survey, $inputs);
	return $res;
    }

    /**
     * Create a survey.
     *
     * @param  array  $inputs
     * @param  int    $user_id
     * @return void
     */
    public function store($inputs,$type, $user_id)
    {
	$survey = new $this->model;
        $survey->type = $type;
	$res = $this->saveSurvey($survey, $inputs, $user_id);
        if($type == 1)
        {
            $this->distribute($survey->id);
        }
	return $res;
    }

    public function push($id)
    {
        $survey = $this->getById($id);
        $survey->pushed = 1;
        $survey->save();
        return $survey;
    }

    public function distribute($id,$course_id=null)
    {
        $record = new SurveyRecord;
        $survey = $this->getById($id);
        if($course_id)
        {
            $old_records = SurveyRecord::where('course_id',$course_id)->where('type',$survey->type)->get();
            foreach($old_records as $old_record)
            {
                $old_record->delete();
            }
            
            $course = Course::find($course_id);
            
            $record->title = $course->title."_".$survey->title;
            $record->course_id = $course_id;
        }
        else
        {
            $record->title = $survey->title;
            $record->course_id = 0;
        }
        $record->survey_id = $id;
        $record->type = $survey->type;
        $record->save();
        return $record;
    }

    public function deallocate($course_id,$type)
    {
        $old_records = SurveyRecord::where('course_id',$course_id)->where('type',$type)->get();
        foreach($old_records as $old_record)
        {
            $old_record->delete();
        }
        return true;
    }




    public function answer($record_id,$course_id = null)
    {
        $res = array('status' => 1,'message' => '');
        $record = $this->getRecordById($record_id);

        $resultNum = SurveyResult::where('record_id',$record_id)->where('user_id',Auth::user()->id)->count();
        if($resultNum >= 1)
        {
            $res['status'] = 0;
            if($record->survey->type == 2)
                $res['message'] = array('fail'=>trans('error.00024'));
            else
                $res['message'] = array('fail'=>trans('error.00083'));
        }
        if($course_id)
        {
            $course = Course::find($course_id);
            $courseRecord = $courseRecord = CourseRecord::where('course_id',$course->id)->where('user_id',Auth::user()->id)->first();
            if($courseRecord->course->if_order == 1)
            {
                if($courseRecord->process < 3 )
                {
                    $res['status'] = 0;
                    $res['message'] = array('fail'=>trans('error.00025'));
                    return $res;
                }

                if($courseRecord->process == 5 && !$courseRecord->course->if_open_voerdue )
            {
                $res['status'] = 0;
                $res['message'] = array('fail'=>trans('error.00031'));
                return $res;
            }

                if($course->examDistribution)
                {
                    $examRecord = ExamRecord::where('distribution_id',$course->examDistribution->id)->where('user_id',Auth::user()->id)->first();
                    if((!$examRecord)||($examRecord && $examRecord->process < 3))
                    {
                        $res['status'] = 0;
                        $res['message'] = array('fail'=>trans('error.00025'));
                        return $res;
                    }

                }
            }
            
        }
        return $res;
    }

    public function getRecordBySurveyId($survey_id,$course_id=null)
    {
        if($course_id)
        {
            $record = SurveyRecord::where('course_id',$course_id)->where('survey_id',$survey_id)->first();
        }
        else
        {
            $record = SurveyRecord::where('survey_id',$survey_id)->first();
        }
        return $record;
    }

    public function getRecordById($record_id)
    {
        $record = SurveyRecord::find($record_id);
        return $record;
    }


    public function getRecord($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $courseList = $this->course_gestion->getListByInput("");
        if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);

            $courseList = Course::whereIn('id',$courseList)
                         ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }
        $name =isset($inputs['findByName'])?$inputs['findByName']:"";
        $evaluationRecords = SurveyRecord::whereIn('course_id',$courseList)->where('title','LIKE','%'.$name.'%')->orderBy($orderby,$direction)->paginate($n);
        return $evaluationRecords;
    }



}
