<?php namespace App\Repositories;

use App\Models\Post;
use App\Repositories\UserRepository;
use Storage,Auth;
class PostRepository extends BaseRepository
{

protected $user_gestion;
	/**
	 * Create a new BlogRepository instance.
	 *
	 * @param  App\Models\Post $post
	 * @param  App\Models\Tag $tag
	 * @param  App\Models\Comment $comment
	 * @return void
	 */
    protected $PATH = "";
	public function __construct(
		Post $post,
        UserRepository $user_gestion
    )
	{
		$this->model = $post;
        $this->user_gestion = $user_gestion;
        $this->PATH = env('OSS_PATH','test');
	}

    
    public function index($n,$area_id = 1,$orderby = 'created_at', $direction = 'desc')
    {
        $posts = $this->model->where('area_id',$area_id)->orderBy($orderby,$direction)->paginate($n);
        return $posts;
    }
    
	/**
	 * Create or update a post.
	 *
	 * @param  App\Models\Post $post
	 * @param  array  $inputs
	 * @param  bool   $user_id
	 * @return App\Models\Post
	 */
  	private function savePost($post, $inputs, $user_id = null)
	{
		$post->title = $inputs['title'];
		$post->summary = $inputs['summary'];
		$post->body = $inputs['body'];
		if($user_id)
        {
            $post->user_id = $user_id;
            $user = $this->user_gestion->getById($user_id);
            $post->manage_id = $user->manage_id;
        }
        if(array_key_exists('manage_id',$inputs))
        {
            $post->manage_id = $inputs['manage_id'];
        }
		$post->save();
        if(array_key_exists('image',$inputs))
        {
            $res = Storage::put(
                $this->PATH.'/posts/'.$post->id.'/title.jpg',
                file_get_contents($inputs['image'])
            );
        }
        elseif(!Storage::has($this->PATH.'/posts/'.$post->id.'/title.jpg'))
        {
            Storage::put($this->PATH.'/posts/'.$post->id.'/title.jpg',file_get_contents('img/slides/0.jpg'));
        }
		return $post;
	}

	/**
	 * Update a post.
	 *
	 * @param  array  $inputs
	 * @param  int    $id
	 * @return void
	 */
	public function update($inputs, $id)
	{
		$post = $this->getById($id);
		$post = $this->savePost($post, $inputs);
		return $post;
	}

	/**
	 * Create a post.
	 *
	 * @param  array  $inputs
	 * @param  int    $user_id
	 * @return void
	 */
	public function store($inputs, $user_id)
	{
		$post = new $this->model;
        $post->area_id = $inputs['area_id'];
		$post = $this->savePost($post, $inputs, $user_id);
		return $post;
	}


    public function destroy($id)
    {
        $post = $this->getById($id);
        Storage::delete($this->PATH.'/posts/'.$post->id.'/title.jpg');
        $post->delete();
        
    }

    public function modify($data_id)
    {
        $post = $this->getById($data_id);
        if(Auth::user()->role_id >= $post->user->role_id)
        {
            return true;
        }
        return false;
    }

}
