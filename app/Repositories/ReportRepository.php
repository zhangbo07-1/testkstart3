<?php namespace App\Repositories;

use App\Models\Course;
use App\Models\User;
use App\Models\Question;
use App\Models\SurveyResult;
use App\Models\CourseRecord;
use App\Models\ExamRecord;
use App\Models\ExamResult;
use App\Models\QuestionResult;
use App\Models\QuestionSelect;
use App\Models\Record;
use App\Models\CourseSpan;
use App\Repositories\CourseRepository;
use App\Repositories\CourseRecordRepository;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\ExamRecordRepository;
use App\Repositories\ExamDistributionRepository;
use App\Repositories\AttributeRepository;
use Auth,DateInterval,Carbon\Carbon;
class ReportRepository extends BaseRepository
{

    protected $course_gestion;
    protected $courseRecord_gestion;
    protected $examRecord_gestion;
    protected $examDistribution_gestion;
     protected $attribute_gestion;

    public function __construct(
        UserRepository $user_gestion,
        CourseRepository $course_gestion,
        CourseRecordRepository $courseRecord_gestion,
        OrganizationRepository $organization_gestion,
        ExamRecordRepository $examRecord_gestion,
        ExamDistributionRepository $examDistribution_gestion,
         AttributeRepository $attribute_gestion
    )
    {
        $this->course_gestion = $course_gestion;
        $this->user_gestion = $user_gestion;
        $this->courseRecord_gestion = $courseRecord_gestion;
        $this->organization_gestion = $organization_gestion;
        $this->examRecord_gestion = $examRecord_gestion;
        $this->examDistribution_gestion = $examDistribution_gestion;
         $this->attribute_gestion = $attribute_gestion;
    }

    public function examExcel($inputs)
    {
        $attribute = $this->attribute_gestion->getAttribute();
        $prog[] = array(
               trans('user.real_name') , trans('table.user_name') ,trans('table.email') , trans('user.department') ,$attribute->first_title,$attribute->second_title, $attribute->third_title, $attribute->fourth_title,$attribute->fifth_title,trans('table.status'), trans('exam.title') , trans('table.catalog') , trans('exam.status') , trans('table.create_time') , trans('table.start_time') , trans('table.end_time') , trans('table.exam_times') , trans('table.exam_time') ,trans('table.exam_score') ,trans('question.id') , trans('question.title') , trans('bank.name') , trans('question.true_select') , trans('question.answer') , trans('question.if_true') , trans('question.select_title')."1", trans('question.select_title')."2", trans('question.select_title')."3", trans('question.select_title')."4",  trans('question.select_title')."5", trans('question.select_title')."6", trans('question.select_title')."7", trans('question.select_title')."8", trans('question.select_title')."9", trans('question.select_title')."10"
        );
        $res = $this->getExam(99999,$inputs);
        $records = $res['records'];
        $examResults = $res['examResults'];
        $trueSelect = $res['trueSelect'];
        $selectResults = $res['selectResults'];
        $questionSelects = $res['questionSelects'];
        foreach($records as $record)
        {
            $data = array(
                $record->user->profile->realname,
                $record->user->name,
                $record->user->email ,
                $record->user->department->name,
                $record->user->profile->attribute1?$record->model->user->profile->attribute1->title:"",
                $record->user->profile->attribute2?$record->model->user->profile->attribute2->title:"",
                $record->user->profile->attribute3?$record->model->user->profile->attribute3->title:"",
                $record->user->profile->attribute4?$record->model->user->profile->attribute4->title:"",
                $record->user->profile->attribute5?$record->model->user->profile->attribute5->title:"",
                $record->user->userStatus->title,
                $record->distribution->exam->title,
                $record->distribution->exam->catalog->name,
                trans("exam.".$record->status->title),
                $record->distribution->created_at,
                $record->created_at,
                $record->end_at,
                count($record->results),
                isset($examResults[$record->id])?$examResults[$record->id]->created_at:"",
                $record->max_score
            );
            if(isset($examResults[$record->id]))
            {
                foreach($examResults[$record->id]->questionResult as $index=>$result)
                {
                    if($result->question->type == 4)
                    {
                        $question = array(
                            $result->question->question_id,
                            $result->question->title,
                            $result->question->bank->title,
                            $trueSelect[$result->question->id],
                            isset($selectResults[$result->id])?$selectResults[$result->id]:""
                        );
                    }
                    else
                    {
                        $question = array(
                            $result->question->question_id,
                            $result->question->title,
                            $result->question->bank->title,
                            $trueSelect[$result->question->id],
                            $selectResults[$result->id],
                            $result->if_true == 1?trans('table.yes'):trans('table.no')
                        );
                        $question = array_merge($question,$questionSelects[$result->question->id]);

                    }

                    if($index == 0)
                    {
                         $prog[] = array_merge($data,$question);
                    }
                    else
                    {
                        $empty = array("","","","","","","","","","","","","","","","","","","");
                        $prog[] = array_merge($empty,$question);

                    }
                }
            }
            else
            {
                 $prog[] = $data;
            }
        }
        return $prog;
    }

    public function courseExcel($inputs)
    {
         $attribute = $this->attribute_gestion->getAttribute();
         $prog[] =array(trans('user.real_name'),trans('table.user_name'),trans('table.email'),trans('user.department'),trans('record.name'),trans('record.type'),trans('table.catalog'),trans('table.status'),trans('course.status'),trans('exam.status'),trans('evaluation.status'),trans('table.create_time'),trans('table.start_time'),trans('table.end_time'),trans('table.study_times'),trans('table.study_span')."(".trans('table.minute').")",trans('table.course_score'),trans('table.exam_score'),$attribute->first_title,$attribute->second_title,$attribute->third_title,$attribute->fourth_title,$attribute->fifth_title,trans('table.status'));
        $res = $this->getCourse(99999,$inputs);
        $records = $res['records'];
        $examRecord = $res['examRecord'];
        $evaResult = $res['evaResult'];
        foreach($records as $record)
        {
            if($record->type == 1 || $record->type == 3)
            {
                if($record->model->process == 1)
                {
                    if(($record->model->course->examDistribution && !empty($examRecord[$record->id])) || (!empty($evaluationRecord[$record->id] ) &&  $evaResult[$record->id]>0))
                    {
                        $Status = trans('course.under_way');
                    }
                    else
                    {
                        $Status = trans('course.not_start');
                    }
                }
                elseif($record->model->process == 3)
                {
                    $Status = trans('course.under_way');
                }
                elseif($record->model->process < 6)
                {
                    $Status = trans('course.'.$record->model->status->title);
                }
                $Status = $Status.($record->model->overdue?'('.trans('table.overdue').')':'');
                $courseStatus = $record->model->process < 6?trans('course.'.$record->model->status->title):"";

                if(!$record->model->course->examDistribution)
                    $examStatus = "";
                elseif(!$examRecord[$record->id])
                    $examStatus = trans('exam.not_start');
                else
                    $examStatus = trans("exam.".$examRecord[$record->id]->status->title);

                if(empty($evaluationRecord[$record->id]))
                    $evaluationStatus = "";
                elseif($evaResult[$record->id]<1)
                    $evaluationStatus = trans('table.not_start');
                else
                    $evaluationStatus = trans('table.done');

                $prog[] = array($record->model->user->profile->realname,
                                $record->model->user->name,
                                $record->model->user->email,
                                $record->model->user->department->name,
                                $record->model->course->title,
                                trans($record->recordType->title),
                                $record->model->course->catalog->name,
                                $Status,
                                $courseStatus,
                                $examStatus,
                                $evaluationStatus,
                                $record->model->created_at,
                                $record->model->started_at,
                                $record->model->end_at?:"",
                                $record->model->study_times,
                                $record->model->study_span?$record->model->study_span:0,
                                $record->model->max_score,
                                $record->model->exam_max_score,
                                $record->model->user->profile->attribute1?$record->model->user->profile->attribute1->title:"",
                                $record->model->user->profile->attribute2?$record->model->user->profile->attribute2->title:"",
                                $record->model->user->profile->attribute3?$record->model->user->profile->attribute3->title:"" ,
                                $record->model->user->profile->attribute4?$record->model->user->profile->attribute4->title:"" ,
                                $record->model->user->profile->attribute5?$record->model->user->profile->attribute5->title:"" ,
                                $record->model->user->userStatus->title,
                );
            }
            if($record->type == 2)
            {
                $prog[] = array($record->model->model->profile->realname,
                                $record->model->model->name,
                                $record->model->model->email,
                                $record->model->model->department->name,
                                $record->model->exam->title,
                                trans($record->recordType->title),
                                $record->model->exam->catalog->name,
                                count($record->model->records)?trans("exam.".$record->model->records[0]->status->title):trans('table.not_start'),
                                "",
                                count($record->model->records)?trans("exam.".$record->model->records[0]->status->title):trans('table.not_start'),
                                "",
                                $record->model->created_at,
                                count($record->model->records)?$record->model->records[0]->created_at:"",
                                "",
                                count($record->model->records)?count($record->model->records[0]->results):"",
                                "",
                                "",
                                count($record->model->records)?$record->model->records[0]->max_score:"" ,
                                $record->model->model->profile->attribute1?$record->model->model->profile->attribute1->title:"",
                                $record->model->model->profile->attribute2?$record->model->model->profile->attribute2->title:"",
                                $record->model->model->profile->attribute3?$record->model->model->profile->attribute3->title:"",
                                $record->model->model->profile->attribute4?$record->model->model->profile->attribute4->title:"",
                                $record->model->model->profile->attribute5?$record->model->model->profile->attribute5->title:"",
                                $record->model->model->userStatus->title,
                );
            }
        }
        return $prog;
    }


    public function itemExcel($result,$record)
    {
        $prog[] = array("",trans('evaluation.1'),trans('evaluation.2'),trans('evaluation.3'),trans('evaluation.4'),trans('evaluation.5'),trans('table.avg'),trans('table.answer_number'));
        foreach($record->survey->items as $item)
        {
            if($item->type == 2)
            {
                $prog[] = array($item->title,
                                round($result['number'][$item->id][1]/$result['resultNumber']*100,2)."%",
                                round($result['number'][$item->id][2]/$result['resultNumber']*100,2)."%",
                                round($result['number'][$item->id][3]/$result['resultNumber']*100,2)."%",
                                round($result['number'][$item->id][4]/$result['resultNumber']*100,2)."%",
                                round($result['number'][$item->id][5]/$result['resultNumber']*100,2)."%",
                                $result['avg'][$item->id],
                                $result['resultNumber']);
            }
        }

        $prog[] = array(trans('question.mulit'),"",trans('table.answer_number'));
        foreach($record->survey->items as $item)
        {
            if($item->type == 1)
            {
                $prog[] = array($item->title,"",$result['resultNumber']);
                foreach($item->selects as $select)
                {
                    $prog[] = array($select->title,
                                    round($result['number'][$item->id][$select->id]/$result['resultNumber']*100,2).'%',"");
                }
            }
        }
        return $prog;
    }


    public function getExam($n,$inputs)
    {
        $examResults = array();
        $resultList = array();
        $trueSelect = array();
        $selectResults = array();
        $questionSelects = array(array());
        $records = $this->examRecord_gestion->getExamRecords($n,$inputs);
        foreach($records as $record)
        {

            /* $examResults[$record->id] = ExamResult::where('record_id',$record->id)
                                      ->where('overed',1)
                                      ->where('score',$record->max_score)
                                      ->first();
            */
            $max = ExamResult::where('record_id',$record->id)
                 ->where('overed',1)
                 ->max('score');

            if($record->max_score != $max)
            {

                $record->max_score = $max;
                $record->save();
            }
            $examResults[$record->id] = ExamResult::where('record_id',$record->id)
                                      ->where('overed',1)
                                      ->where('score',$max)
                                      ->first();
            if( $examResults[$record->id])
            {

                $resultList[] = $examResults[$record->id]->id;
                foreach($examResults[$record->id]->questionResult as $questionResult)
                {
                    $selected = array();
                    foreach($questionResult->selectResults as $selectResult)
                    {
                        $selected[] = $selectResult->select->title;

                    }

                    $selectResults[$questionResult->id] = implode("||",$selected);


                }
            }
        }
        $questionList = QuestionResult::whereIn('result_id',$resultList)
                      ->lists('question_id')->all();
        foreach($questionList as $questionId)
        {
            $true = QuestionSelect::where('if_true',1)
                  ->where('question_id',$questionId)
                  ->lists('title')->all();
            $trueSelect[$questionId] = implode("||",$true);
            $question = Question::find($questionId);
            for($i=0;$i<10;$i++)
            {
                $questionSelects[$questionId][$i] = isset($question->selects[$i])?$question->selects[$i]->title:"";
            }

        }

        $res['records'] = $records;
        $res['examResults'] = $examResults;
        $res['trueSelect'] = $trueSelect;
        $res['selectResults'] = $selectResults;
        $res['questionSelects'] = $questionSelects;

        return $res;
    }


    public function getCourse($n,$inputs)
    {
        $type = isset($inputs['type'])?$inputs['type']:0;
        if($type == 2)
        {
            $courseList = array();
        }
        elseif($type ==1)
        {
            $courseList = $this->courseRecord_gestion->getRecordList($inputs,true);
            $courseList = CourseRecord::whereIn('id', $courseList)
                        ->whereHas('course',function($q)
                        {
                            $q->where('type',1);
                        })
                        ->lists('id')->all();
        }
        elseif($type ==3)
        {
            $courseList = $this->courseRecord_gestion->getRecordList($inputs,true);
            $courseList = CourseRecord::whereIn('id', $courseList)
                        ->whereHas('course',function($q)
                        {
                            $q->where('type',2);
                        })
                        ->lists('id')->all();
        }
        else
        {
            $courseList = $this->courseRecord_gestion->getRecordList($inputs,true);
        }
        if($type == 1 || $type == 3)
        {
            $examList = array();
        }
        else
        {
            $examList = $this->examDistribution_gestion->getDistributionList($inputs,true,true);
        }
        $userList = $this->user_gestion->getUserIdByInputs($inputs);
        $list = Record::whereIn('user_id',$userList)
              ->where(function($query) use ($courseList,$examList)
                      {
                          $query->where(function($q) use ($courseList)
        {
            $q->whereIn('type',[1,3])
            ->whereIn('model_id',$courseList);
        })
                          ->orWhere(function($q) use ($examList)
        {
            $q->where('type',2)
            ->whereIn('model_id',$examList);
        });
                      })
              ->join('users','records.user_id','=','users.id')
              ->orderBy('users.name','asc')->paginate($n);
        $examRecord = array();
        $evaluationRecord = array();
        $evaResult = array();
        $surveyRecord = array();
        $surveyResult = array();

        foreach($list as $record)
        {
            if($record->type == 1 || $record->type == 3)
            {
                if($record->model->course->examDistribution)
                {
                    $examRecord[$record->id] = ExamRecord::where('user_id',$record->user_id)->where('distribution_id',$record->model->course->examDistribution->id)->first();
                }
                 $evaluationRecord[$record->id] = $record->model->course->surveyRecords()->where('type',2)->first();
                if($evaluationRecord[$record->id])
                {

                    $evaResult[$record->id] = SurveyResult::where('user_id',$record->user_id)->where('record_id',$evaluationRecord[$record->id]->id)->count();
                }

                 $surveyRecord[$record->id] = $record->model->course->surveyRecords()->where('type',3)->first();
                if($surveyRecord[$record->id])
                {
                    $surveyResult[$record->id] = SurveyResult::where('user_id',$record->user_id)->where('record_id',$surveyRecord[$record->id]->id)->count();
                }
            }

        }
        $res['records'] = $list;
        $res['examRecord'] = $examRecord;
        $res['evaluationRecord'] = $evaluationRecord;
        $res['evaResult'] = $evaResult;
        $res['surveyRecord'] = $surveyRecord;
        $res['surveyResult'] = $surveyResult;

        return $res;
    }



}
