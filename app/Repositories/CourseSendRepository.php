<?php namespace App\Repositories;

use App\Models\Course;
use App\Models\CourseRecord;
use App\Models\Record;
use App\Models\User;
use App\Jobs\SendEmail;
use App\Models\EmailTemplet;
use App\Repositories\OrganizationRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
class CourseSendRepository extends BaseRepository
{
    use DispatchesJobs;

     public function __construct(

        OrganizationRepository $organization_gestion
    )
    {
        $this->organization_gestion = $organization_gestion;
    }

    public function checkRecord($user_id)
    {
        $courses = Course::get();
        foreach($courses as $course)
        {
            $this->ifNeedRecord($user_id,$course->id);
        }
    }

    public function ifNeedRecord($user_id,$course_id)
    {
        $user = User::find($user_id);
        $course = Course::find($course_id);
        if($course->if_send_new_user)
        {
            foreach($course->manages as $manage)
            {
                $course_manage[] = $manage->manage_id;
            }
            foreach($user->manages as $manage)
            {
                $user_manage[] = $manage->manage_id;
            }
            if($this->organization_gestion->belongOrNot($user_manage,$course_manage))
            {


                $res = 0;
                $attribute1 = explode(',',$course->sendLimit->attribute1_list);
                $attribute2 = explode(',',$course->sendLimit->attribute2_list);
                $attribute3 = explode(',',$course->sendLimit->attribute3_list);
                $attribute4 = explode(',',$course->sendLimit->attribute4_list);
                $attribute5 = explode(',',$course->sendLimit->attribute5_list);
                $attribute1 = array_filter($attribute1);
                $attribute2 = array_filter($attribute2);
                $attribute3 = array_filter($attribute3);
                $attribute4 = array_filter($attribute4);
                $attribute5 = array_filter($attribute5);
                if(empty($attribute1))
                {
                    $res++;
                }
                else
                {
                    if($user->profile->attribute1 && in_array($user->profile->attribute1->title,$attribute1) )
                    {
                        $res++;
                    }
                    else
                    {
                        return false;
                    }

                }
                if(!empty($attribute2))
                {
                    if($user->profile->attribute2 && in_array($user->profile->attribute2->title,$attribute2) )
                    {
                        $res++;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    $res++;
                }
                if(!empty($attribute3))
                {
                    if($user->profile->attribute3 && in_array($user->profile->attribute3->title,$attribute3) )
                    {
                        $res++;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    $res++;
                }

                if(!empty($attribute4))
                {
                    if($user->profile->attribute4 && in_array($user->profile->attribute4->title,$attribute4) )
                    {
                        $res++;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    $res++;
                }

                if(!empty($attribute5))
                {
                    if($user->profile->attribute5 && in_array($user->profile->attribute5->title,$attribute5) )
                    {
                        $res++;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    $res++;
                }
                if($res == 5)
                {
                    $this->addRecord($course_id,$user_id);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

     public function addRecord($course_id,$user_id)
    {
        $record = CourseRecord::where('user_id',$user_id)->where('course_id',$course_id)->first();
        if(!$record)
        {
            $record = new CourseRecord;
            $record->course_id = $course_id;
            $record->user_id = $user_id;
            $record->save();
            $list = new Record;
            $list->model_id = $record->id;
            $list->type = 1;
            $list->model_type = CourseRecord::class;
            $list->user_id = $user_id;
            $list->save();
            $user = User::find($record->user_id);
            if ( $record->course->emailed > 0 )
            {
                $url = $_SERVER['HTTP_HOST'];
                $email = EmailTemplet::whereType(1)->first();

                $email->body = str_replace('{{用户名}}',$user->name,$email->body);
                $email->body = str_replace('{{课程标题}}',$record->course->title,$email->body);
                $email->body = str_replace('{{用户网址}}',$url,$email->body);
                error_log("in mail");
                $this->dispatch(new SendEmail($email,$user));


            }
        }
    }

}
