<?php namespace App\Repositories;

use App\Models\User;
use App\Models\Role;
use App\Models\Profile;
use App\Models\UserStatus;
use App\Models\UserManage;
use App\Models\Organization;
use App\Models\Attribute;
use App\Models\Language;
use App\Models\SystemInfo;
use App\Models\Position;
use App\Models\Rank;
use App\Models\FirstAttribute;
use App\Models\SecondAttribute;
use App\Models\ThirdAttribute;
use App\Models\FourthAttribute;
use App\Models\FifthAttribute;
use App\Models\EmailTemplet;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Repositories\OrganizationRepository;
use App\Repositories\ImportLogRepository;
use App\Repositories\GuidRepository;
use App\Repositories\CourseSendRepository;
use App\Jobs\SendEmail;
use Excel,Auth,Hash;

class UserRepository extends BaseRepository
{
    use DispatchesJobs;
    /**
     * The Role instance.
     *
     * @var App\Models\Role
     */
    protected $role;
    protected $log_gestion;
    protected $guid_gestion;
    protected $table = array('');
    protected $courseSend_gestion;
    /**
     * Create a new UserRepository instance.
     *
     * @param  App\Models\User $user
     * @param  App\Models\Role $role
     * @return void
     */
    public function __construct(
        User $user,
        UserManage $user_manage,
        Role $role,
        GuidRepository $guid_gestion,
        OrganizationRepository $organization_gestion,
        CourseSendRepository $courseSend_gestion,
        ImportLogRepository $log_gestion
    )
    {
        $this->model = $user;
        $this->manage_model = $user_manage;
        $this->role = $role;
        $this->guid_gestion = $guid_gestion;
        $this->log_gestion = $log_gestion;
        $this->courseSend_gestion = $courseSend_gestion;
        $this->organization_gestion = $organization_gestion;
    }


    public function FetchRepeatMemberInArray($array)
    {
        $array = array_filter($array);
        // 获取去掉重复数据的数组
        $unique_arr = array_unique( $array );
        // 获取重复数据的数组
        $repeat_arr = array_diff_assoc( $array, $unique_arr );
        return $repeat_arr;
    }

    public function checkDepartment($departments)
    {
        $error = array();
        foreach($departments as $department)
        {
            $result = Organization::where('tag',$department)->first();
            if(!$result)
            {
                $error[] = $department;
            }
        }
        return $error;
    }

    public function getUserNumber()
    {
        return $this->model->count();
    }


    public function checkApprove($approves)
    {
        $error = array();
        if(is_array($approves))
        {
            foreach($approves as $approve)
            {
                if($approve)
                {
                    $result = User::where('name',$approve)->first();
                    if(!$result)
                    {
                        $error[] = $approve;
                    }
                }
            }
        }
        else
        {
            if($approves)
            {
                $result = User::where('name',$approves)->first();
                if(!$result)
                {
                    $error[] = $approves;
                }
            }
        }
        return $error;
    }

    public function checkName($names,$email)
    {
        $res = array('type'=>0,'error'=>"");
        foreach($names as $key=>$name)
        {
            if($name)
            {
                $user = User::where('name',$name)->first();
                if($user && $user->email != $email[$key])
                {
                    $res['type'] = 1;
                    $res['error'][] = $name;
                }
            }
            else
            {
                $res['type'] = 2;
            }
        }
        return $res;
    }

    public function checkEmail($emails,$name)
    {
        $res = array('type'=>0,'error'=>"");
        foreach($emails as $key=>$email)
        {
            if($email)
            {
                $user = User::where('email',$email)->first();
                if($user && $user->name != $name[$key])
                {
                    $res['type'] = 1;
                    $res['error'][] = $email;
                }
            }
            else
            {
                $res['type'] = 2;
            }
        }
        return $res;
    }


    public function checkRole($roles)
    {
        $error = array();
        foreach($roles as $role)
        {
            $result = Role::where('tag',$role)->first();
            if(!$result)
            {
                $error[] = $role;
            }
        }
        return $error;
    }


    public function checkAttribute1($attribute1)
    {
        $error = array();
        foreach($attribute1 as $tag)
        {
            if($tag)
            {
                $result = FirstAttribute::where('title',$tag)->first();
                if(!$result)
                {
                    $error[] = $tag;
                }
            }
        }
        return $error;
    }
    public function checkAttribute2($attribute2)
    {
        $error = array();
        foreach($attribute2 as $tag)
        {
            if($tag)
            {
                $result = SecondAttribute::where('title',$tag)->first();
                if(!$result)
                {
                    $error[] = $tag;
                }
            }
        }
        return $error;
    }
    public function checkAttribute3($attribute3)
    {
        $error = array();
        foreach($attribute3 as $tag)
        {
            if($tag)
            {
                $result = ThirdAttribute::where('title',$tag)->first();
                if(!$result)
                {
                    $error[] = $tag;
                }
            }
        }
        return $error;
    }

    public function checkAttribute4($attribute4)
    {
        $error = array();
        foreach($attribute4 as $tag)
        {
            if($tag)
            {
                $result = FourthAttribute::where('title',$tag)->first();
                if(!$result)
                {
                    $error[] = $tag;
                }
            }
        }
        return $error;
    }
    public function checkAttribute5($attribute5)
    {
        $error = array();
        foreach($attribute5 as $tag)
        {
            if($tag)
            {
                $result = FifthAttribute::where('title',$tag)->first();
                if(!$result)
                {
                    $error[] = $tag;
                }
            }
        }
        return $error;
    }
    public function getUserByName($name)
    {
        $user = $this->model->whereName($name)->first();
        return $user;
    }

    public function index($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        foreach(Auth::user()->manages as $manage)
        {
            $manageList[] = $manage->manage_id;
        }
        $manageArray = $this->organization_gestion->getUnderArray($manageList);
        $list = $this->model->whereIn('department_id',$manageArray)->lists('id')->all();
        if(!empty($inputs['manage_id']))
        {
            $department_ids = explode(',',$inputs['manage_id']);
            $departmentArray = $this->organization_gestion->getUnderArray($department_ids);
            $list = $this->model->whereIn('id',$list)
                         ->whereIn('department_id',$departmentArray)
                         ->lists('id')->all();
        }

        if(isset($inputs['userStatus']))
        {
            if($inputs['userStatus'] != 0)
            {
                $list = $this->model->whereIn('id',$list)
                             ->where('status_id',$inputs['userStatus'])
                             ->lists('id')->all();
            }
        }
        else
        {
            $list = $this->model->whereIn('id',$list)
                         ->where('status_id',2)
                         ->lists('id')->all();
        }


        $list = $this->model->whereIn('id',$list)
                     ->whereHas('profile',function($query) use ($inputs)
                         {
                             if(isset($inputs['attribute1']) && $inputs['attribute1'] != 0)
                             {
                                 $query->where('attribute1_id',$inputs['attribute1']);
                             }
                             if(isset($inputs['attribute2']) && $inputs['attribute2'] != 0)
                             {
                                 $query->where('attribute2_id',$inputs['attribute2']);
                             }
                             if(isset($inputs['attribute3']) && $inputs['attribute3'] != 0)
                             {
                                 $query->where('attribute3_id',$inputs['attribute3']);
                             }
                             if(isset($inputs['attribute4']) && $inputs['attribute4'] != 0)
                             {
                                 $query->where('attribute4_id',$inputs['attribute4']);
                             }
                             if(isset($inputs['attribute5']) && $inputs['attribute5'] != 0)
                             {
                                 $query->where('attribute5_id',$inputs['attribute5']);
                             }
                         })
                     ->lists('id')->all();
        $username = isset($inputs['findByUsername'])?$inputs['findByUsername']:"";

        $users = $this->model->whereIn('id',$list)
                      ->where(function($query) use ($username)
                          {
                              $query->WhereHas('profile', function ( $q ) use ( $username )
                                  {
                                      $q->where('realname', 'LIKE', '%' . $username . '%');
                                  })
                                    ->orWhereHas('role',function($q) use ($username)
                                        {
                                            $q->where('name','LIKE','%'.$username.'%');
                                        })
                                    ->orWhere('name', 'LIKE', '%' . $username . '%')
                                    ->orWhere('email', 'LIKE', '%' . $username . '%');
                          })
                      ->lists('id')->all();
        $res['number'] = count($users);
        $res['user'] = $this->model->whereIn('id',$users)
                            ->orderBy($orderby,$direction)->paginate($n);
        return $res;
    }





    public function saveUser($user,$inputs,$user_id=null)
    {
        $info = SystemInfo::first();
        $res = array('status'=>1,'error'=>'');
        if(isset($inputs['name']))
        {
            $user->name=$inputs['name'];
        }
        if(isset($inputs['userStatus']))
        {
            if(!$user_id)
            {
                $this->changeStatus($user->id,$inputs['userStatus']);
            }
        }
        elseif(isset($inputs['status']))
        {
            if(!$user_id)
            {
                $status = UserStatus::where('tag','LIKE',$inputs['status'])->first();
                $this->changeStatus($user->id,$status->id);
            }
        }
        if(isset($inputs['email']))
        {
            $user->email=$inputs['email'];
        }
        if(isset($inputs['password']) && !empty($inputs['password']))
        {
            $user->password=bcrypt($inputs['password']);
        }
        if(isset($inputs['pwd'])&&!empty($inputs['pwd'])&&$user_id)
        {
            $user->password=bcrypt($inputs['pwd']);
        }
        if(isset($inputs['role_id']))
        {
            $user->role_id = $inputs['role_id'];
        }
        elseif(isset($inputs['role']))
        {
            $role = Role::whereTag($inputs['role'])->first();
            $user->role_id = $role->id;
        }
        else
        {
            $user->role_id = Auth::check()?Auth::user()->role_id:1;
        }

        if(isset($inputs['department_id'])&&!empty($inputs['department_id']))
        {
            $user->department_id= $inputs['department_id'];
        }
        elseif(isset($inputs['department']))
        {
            $department = Organization::whereTag($inputs['department'])->first();
            $user->department_id= $department->id;
        }
        else
        {
            $user->department_id= Auth::check()?Auth::user()->department_id:$info->department_id;
        }




        if(isset($inputs['approve_id'])&&!empty($inputs['approve_id']))
        {
            $user->department_id= $inputs['approve_id'];
        }
        elseif(isset($inputs['approve'])&&!empty($inputs['approve']))
        {
            $approve = User::whereName($inputs['approve'])->first();
            $user->approve_id= $approve->id;
        }


        if(isset($inputs['language_id'])&&!empty($inputs['language_id']))
        {
            $user->language_id= $inputs['language_id'];
        }
        elseif(isset($inputs['language']))
        {
            $language = Language::where('tag','LIKE',$inputs['language'])->first();
            if($language)
            {
                $user->language_id= $language->id;
            }
        }
        $user->save();

        if(isset($inputs['manage_id'])&&!empty($inputs['manage_id'])&&($user->role_id > 1) && ($user->role_id < 4))
        {
            $this->setManageId($user,$inputs['manage_id'],$user_id);
        }
        else
        {
            if($user->role_id == 1 || $user->role_id == 4)
            {
                foreach($user->manages as $manage)
                {
                    $manage->delete();
                }
            }
            if(($user_id || $user->role_id == 1) && $user->role_id != 4)
            {
                $manage = new UserManage;
                $manage->model_id = $user->id;
                $manage->manage_id = $user->department_id;
                $manage->save();
            }
            if($user->role_id == 4)
            {
                $manage = new UserManage;
                $manage->model_id = $user->id;
                $manage->manage_id = 1;
                $manage->save();
            }

        }

        if($user->profile)
        {
            $profile = $user->profile;
        }
        else
        {
            $profile = new Profile;
            $profile->user_id=intval($user->id);
        }
        $profile->realname=isset($inputs['realname'])?$inputs['realname']:"";
        $profile->tel=isset($inputs['tel'])?$inputs['tel']:"";
        $profile->address=isset($inputs['address'])?$inputs['address']:"";


        if(isset($inputs['attribute1_id']))
        {
            $profile->attribute1_id = $inputs['attribute1_id'];
        }
        elseif(isset($inputs['attribute1']))
        {
            $attribute1 = FirstAttribute::where('title',$inputs['attribute1'])->first();
            $profile->attribute1_id= $attribute1->id;
        }

        if(isset($inputs['attribute2_id']))
        {
            $profile->attribute2_id = $inputs['attribute2_id'];
        }
        elseif(isset($inputs['attribute2']))
        {
            $attribute2 = SecondAttribute::where('title',$inputs['attribute2'])->first();
            $profile->attribute2_id= $attribute2->id;
        }

        if(isset($inputs['attribute3_id']))
        {
            $profile->attribute3_id = $inputs['attribute3_id'];
        }
        elseif(isset($inputs['attribute3']))
        {
            $attribute3 = ThirdAttribute::where('title',$inputs['attribute3'])->first();
            $profile->attribute3_id= $attribute3->id;
        }

        if(isset($inputs['attribute4_id']))
        {
            $profile->attribute4_id = $inputs['attribute4_id'];
        }
        elseif(isset($inputs['attribute4']))
        {
            $attribute4 = FourthAttribute::where('title',$inputs['attribute4'])->first();
            $profile->attribute4_id= $attribute4->id;
        }
        if(isset($inputs['attribute5_id']))
        {
            $profile->attribute5_id = $inputs['attribute5_id'];
        }
        elseif(isset($inputs['attribute5']))
        {
            $attribute5 = FifthAttribute::where('title',$inputs['attribute5'])->first();
            $profile->attribute5_id= $attribute5->id;
        }
        if(isset($inputs['company']))
        {
            $profile->company = $inputs['company'];
        }
        if(isset($inputs['favorite']))
        {
            $profile->remark = $inputs['favorite'];
        }


        if(isset($inputs['sex_id']))
        {
            $profile->sex_id = $inputs['sex_id'];
        }
        elseif(isset($inputs['sex']))
        {
            switch($inputs['sex'])
            {
                case 'F':
                $profile->sex_id= 1;
                break;
                case 'M':
                $profile->sex_id= 2;
                break;
            }
        }
        $profile->save();
        return $res;
    }
    public function create($inputs)
    {
        $user = new User;
        $user_id = Auth::user()->id;
        $res = $this->saveUser($user, $inputs, $user_id);
        $this->changeStatus($user->id,2);
        return $res;

    }
    public function update($id,$inputs)
    {
        $user = $this->getById($id);
        $res = $this->saveUser($user, $inputs);
        return $res;

    }
    public function changeStatus($id,$status)
    {
        error_log("change status");
        $user = $this->getById($id);
        if($status != $user->status_id)
        {
            $user->status_id = $status;
            $user->save();
            if($user->status_id == 2)
            {
                $this->courseSend_gestion->checkRecord($user->id);
            }
        }

    }

    public function closeUser($id)
    {
        $status = UserStatus::whereTitle('关闭')->first();
        $this->changeStatus($id,$status->id);
    }

    /**
     * import a user list.
     *
     * @param  xlsfile $file
     * @return array
     */
    public function import($file)
    {
        $result = array('type'=>0,'error'=>trans('error.10000'));
        $log = $this->log_gestion->createLog($file,1);
        // $this->log_gestion->addLog($log,trans('error.10002'));
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            if(!empty($prog[array_search('操作',$tag)]))
            {
                $name[] = $prog[array_search('用户名',$tag)];
                $act[] = $prog[array_search('操作',$tag)];
                $email[] = $prog[array_search('邮箱',$tag)];
                $department[] = $prog[array_search('组织ID',$tag)];
                $role[] = $prog[array_search('角色',$tag)];
                $approve[] = $prog[array_search('审批人',$tag)];
                $attribute1[]= $prog[array_search('属性1',$tag)];
                $attribute2[]= $prog[array_search('属性2',$tag)];
                $attribute3[]= $prog[array_search('属性3',$tag)];
                $attribute4[]= $prog[array_search('属性4',$tag)];
                $attribute5[]= $prog[array_search('属性5',$tag)];
                $data[] = array('act'=>$prog[array_search('操作',$tag)],
                                'name'=>$prog[array_search('用户名',$tag)],
                                'email'=>$prog[array_search('邮箱',$tag)],
                                'pwd'=>$prog[array_search('密码',$tag)],
                                'realname'=> $prog[array_search('姓名',$tag)],
                                'role'=> $prog[array_search('角色',$tag)],
                                'department'=> $prog[array_search('组织ID',$tag)],
                                'status'=> $prog[array_search('状态',$tag)],
                                'approve'=> $prog[array_search('审批人',$tag)],
                                'language'=> $prog[array_search('语言',$tag)],
                                'tel'=> $prog[array_search('电话',$tag)],
                                'address'=> $prog[array_search('地址',$tag)],
                                'attribute1'=> $prog[array_search('属性1',$tag)],
                                'attribute2'=> $prog[array_search('属性2',$tag)],
                                'attribute3'=> $prog[array_search('属性3',$tag)],
                                'attribute4'=> $prog[array_search('属性4',$tag)],
                                'attribute5'=> $prog[array_search('属性5',$tag)],
                                'sex'=> $prog[array_search('性别',$tag)],
                );
            }

        }
        if((empty($name)) || (empty($department)))
        {
            $result['type'] = 1;
            $error = trans('error.00058');
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->FetchRepeatMemberInArray($name);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00008');
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->FetchRepeatMemberInArray($email);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00009');
            $this->log_gestion->addLog($log,$error);
        }

        if( SystemInfo::first()->user_email)
        {
            $res = $this->checkName($name,$email);
            if($res['type'])
            {

                $result['type'] = 1;
                $error = $res['type']==1?implode(",",$res['error']).trans('error.00048'):trans('error.00050');
                $this->log_gestion->addLog($log,$error);
            }

            $res = $this->checkEmail($email,$name);
            if($res['type'])
            {

                $result['type'] = 1;
                $error = $res['type']==1?trans('error.00049').implode(",",$res['error']):trans('error.00051');
                $this->log_gestion->addLog($log,$error);
            }
        }
        $error = $this->checkDepartment($department);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00010');
            $this->log_gestion->addLog($log,$error);
        }

        $error = $this->checkApprove($approve);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00055');
            $this->log_gestion->addLog($log,$error);
        }

        $error = $this->checkattribute1($attribute1);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00043');
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->checkattribute2($attribute2);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00044');
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->checkattribute3($attribute3);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00045');
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->checkattribute4($attribute4);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00041');
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->checkattribute5($attribute5);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00042');
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->checkRole($role);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = implode(",",$error).trans('error.00018');
            $this->log_gestion->addLog($log,$error);
        }
        if($result['type'])
        {
            $result['error'] =  trans('error.00012');
            return $result;
        }
        $success = 0;
        foreach($data as $info)
        {
            switch($info['act'])
            {
                case 'A':
                $user = $this->model->whereName($info['name'])->first();
                if($user)
                    $this->update($user->id,$info);
                else
                {
                    $res = $this->create($info);

                }
                $error = trans('error.00015').$info['name'];
                $this->log_gestion->addLog($log,$error);
                break;
                case 'D':

                case 'C':
                $user = $this->model->whereName($info['name'])->first();
                if($user)
                {
                    $this->closeUser($user->id);
                    $error = trans('error.00071').$info['name'];
                    $this->log_gestion->addLog($log,$error);
                }
            }
            $success++;
        }
        $error = $success.trans('table.people').trans('error.10000');
         $this->log_gestion->addLog($log,$error);
        return $result;
    }


    public function searchByDepatment($n,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->organization_gestion->getUnderArray($id);
        $users =  $this->model->with('role')->with('profile')->whereIn('department_id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $users;

    }


    public function ownerOrNot($Alist,$Blist)
    {
        foreach($Alist as $Aid)
        {
            foreach($Blist as $Bid)
            {
                if($this->organization_gestion->ownerOrNot($Aid,$Bid))
                {
                    return true;
                }
            }
        }
        return false;

    }

    public function belongOrNot($Alist,$Blist)
    {
        foreach($Alist as $Aid)
        {
            foreach($Blist as $Bid)
            {
                if($this->organization_gestion->ownerOrNot($Bid,$Aid))
                    return true;
            }
        }
        return false;
    }



    public function getUserIdByInput( $input_data ,$manage=true)
    {
        if($manage)
        {
            foreach(Auth::user()->manages as $manage)
            {
                $manageList[] = $manage->manage_id;
            }
            $manageArray = $this->organization_gestion->getUnderArray($manageList);
            $list = $this->model->whereIn('department_id',$manageArray)->lists('id')->all();
        }
        else
        {
            $list = $this->model->lists('id')->all();
        }
        $users = $this->model->whereIn('id',$list)
                     ->where(function($query) use ($input_data)
                         {
                             $query->whereHas('profile', function ( $q ) use ( $input_data ) {
                                 $q->where('realname', 'LIKE', '%' . $input_data . '%');})
                     ->orWhere('name', 'LIKE', '%' . $input_data . '%')
                     ->orWhere('email', 'LIKE', '%' . $input_data . '%');
                         })
                                        ->lists('id')->all();

        return $users;
    }



    public function getUserIdByInputs( $inputs,$manage=true)
    {
        if($manage)
        {
            foreach(Auth::user()->manages as $manage)
            {
                $manageList[] = $manage->manage_id;
            }
            $manageArray = $this->organization_gestion->getUnderArray($manageList);
            $list = $this->model->whereIn('department_id',$manageArray)->lists('id')->all();
        }
        else
        {
            $list = $this->model->lists('id')->all();
        }
        if(!empty($inputs['manage_id']))
        {
            $department_ids = explode(',',$inputs['manage_id']);
            $departmentArray = $this->organization_gestion->getUnderArray($department_ids);
            $list = $this->model->whereIn('id',$list)
                         ->whereIn('department_id',$departmentArray)
                         ->lists('id')->all();
        }

        if(isset($inputs['userStatus']))
        {
            if($inputs['userStatus'] != 0)
            {
                $list = $this->model->whereIn('id',$list)
                             ->where('status_id',$inputs['userStatus'])
                             ->lists('id')->all();
            }
        }
        else
        {
            $list = $this->model->whereIn('id',$list)
                         ->where('status_id',2)
                         ->lists('id')->all();
        }


        $list = $this->model->whereIn('id',$list)
                     ->whereHas('profile',function($query) use ($inputs)
                         {
                             if(isset($inputs['attribute1']) && $inputs['attribute1'] != 0)
                             {
                                 $query->where('attribute1_id',$inputs['attribute1']);
                             }
                             if(isset($inputs['attribute2']) && $inputs['attribute2'] != 0)
                             {
                                 $query->where('attribute2_id',$inputs['attribute2']);
                             }
                             if(isset($inputs['attribute3']) && $inputs['attribute3'] != 0)
                             {
                                 $query->where('attribute3_id',$inputs['attribute3']);
                             }
                             if(isset($inputs['attribute4']) && $inputs['attribute4'] != 0)
                             {
                                 $query->where('attribute4_id',$inputs['attribute4']);
                             }
                             if(isset($inputs['attribute5']) && $inputs['attribute5'] != 0)
                             {
                                 $query->where('attribute5_id',$inputs['attribute5']);
                             }
                         })
                     ->lists('id')->all();
        $username = isset($inputs['findByUsername'])?$inputs['findByUsername']:"";
        $start_time = !empty($inputs['startRegister'])?$inputs['startRegister']:date("Y-m-d H:i:s",0);
        $end_time = !empty($inputs['endRegister'])?$inputs['endRegister']:date("Y-m-d H:i:s",time());
        $users = $this->model->whereIn('id',$list)
                      ->where(function($query) use ($username)
                          {
                              $query->WhereHas('profile', function ( $q ) use ( $username )
                                  {
                                      $q->where('realname', 'LIKE', '%' . $username . '%');
                                  })
                                    ->orWhereHas('role',function($q) use ($username)
                                        {
                                            $q->where('name','LIKE','%'.$username.'%');
                                        })
                                    ->orWhere('name', 'LIKE', '%' . $username . '%')
                                    ->orWhere('email', 'LIKE', '%' . $username . '%');
                          })
                      ->whereBetween('created_at',[$start_time,$end_time])
                      ->lists('id')->all();
        return $users;
    }

    public function exampleExcel()
    {
        $prog[] = array('操作','用户名','邮箱','密码','姓名','角色',
                        '组织','审批人','语言','属性1','属性2','属性3',
                        '属性4','属性5','性别');
        $prog[] = array('A代表增加或更新','','','更新将不改变原密码',
                        '','S 学员','组织名称必须对应组织结构中现有组织',
                        '','','','CN 代表中文','','','');
        $prog[] = array('D代表删除','','','',
                        '','I 教师','',
                        '','','','EN 代表英文','','','');
        $prog[] = array('C代表关闭','','','',
                        '','A 普通管理员','',
                        '','','','','','','');
        $prog[] = array('','','','',
                        '','SA 超级管理员','',
                        '','','','','','','');
        return $prog;

    }

    public function excel($users)
    {

        $attribute = Attribute::first();
        $prog[] = array('操作','用户名','邮箱','密码','姓名','角色',
                        '组织ID','状态','审批人','语言','电话','地址',
                        '属性1','属性2','属性3',
                        '属性4','属性5','性别','创建日期' , '最后访问', '登入次数');
        //  $prog[] = array( trans('table.user_name'), trans('table.email'), trans('user.real_name') , trans('user.role') , trans('table.status') , trans('user.department') , $attribute->first_title,$attribute->second_title, $attribute->third_title,$attribute->fourth_title,$attribute->fifth_title,trans('table.create_time') , trans('user.login_last'), trans('user.login_times') );
        foreach($users as $user)
        {
            if($user->loginMessage)
            {
                $lastLogin = $user->loginMessage->updated_at;
                $loginTimes =  $user->loginMessage->login_times;
            }
            else
            {
                $lastLogin = '';
                $loginTimes =  '';
            }
            $prog[] = array('A', $user->name,$user->email,'', $user->profile->realname ,$user->role->tag,$user->department->tag,$user->userStatus->tag,$user->approve?$user->approve->name:"",$user->language->tag,$user->profile->tel,$user->profile->address,$user->profile->attribute1?$user->profile->attribute1->title:"",$user->profile->attribute2?$user->profile->attribute2->title:"",$user->profile->attribute3?$user->profile->attribute3->title:"",$user->profile->attribute4?$user->profile->attribute4->title:"",$user->profile->attribute5?$user->profile->attribute5->title:"",$user->profile->sex?$user->profile->sex->tag:"",$user->created_at,$lastLogin,$loginTimes );

        }


        return $prog;
    }

    public function resetPassword($inputs)
    {
        $res = array('status'=>1,'message'=>trans('error.10001'));
        $user = $this->model->where('email',$inputs['email'])->first();
        if(!$user)
        {
            $res['status'] = 0;
            $res['message'] = trans('error.00029');
            return $res;
        }
        $password = $this->guid_gestion->guid();
        $password = substr($password,-6);
        $user->password=bcrypt($password);
        $user->save();

        $email = EmailTemplet::whereType(10)->first();
        $url = $_SERVER['HTTP_HOST'];

        $email->body = str_replace('{{用户名}}',$user->name,$email->body);
        $email->body = str_replace('{{密码}}',$password,$email->body);
        $email->body = str_replace('{{用户网址}}',$url,$email->body);
        $this->dispatch(new SendEmail($email,$user));
        return $res;

    }



    public function changePassword( $id, $inputs)
    {

        if ( strlen($inputs['new_password']) > 0 )
        {
            $user = $this->getById($id);
            $new_password = $inputs['new_password'];
            $user->password = Hash::make($new_password);
            $user->save();
        }

    }


    public function needValidate($id)
    {
        $user = $this->getById($id);
        $confirmation_code = str_random(30);
        if( SystemInfo::first()->validate)
        {
            $user->status_id = 1;
            $user->confirmation_code = $confirmation_code;
            $user->save();
            $email = EmailTemplet::whereType(6)->first();
            $url = $_SERVER['HTTP_HOST'] . '/register/verify/' . $confirmation_code;
            $email->body = str_replace('{{用户名}}', $user->name, $email->body);
            $email->body = str_replace('{{认证网址}}', $url, $email->body);
            $this->dispatch(new SendEmail($email,$user));
            return true;
        }
        else
            return false;
    }

    public function needCensor($id)
    {
        $user = $this->getById($id);
        if( SystemInfo::first()->censor)
        {
            $user->status_id = 3;
            $user->save();
            $admins = User::where('role_id',4)->get();
            $info = $user->name;
            $info = $user->profile->realname?$info.",姓名:".$user->profile->realname:$info;
            $info = $user->profile->company?$info.",公司:".$user->profile->company:$info;
            $info = $user->profile->tel?$info.",电话:".$user->profile->tel:$info;
            $info = $user->profile->remark?$info.",感兴趣功能:".$user->profile->remark:$info;
            foreach($admins as $admin)
            {
                $url = $_SERVER['HTTP_HOST'].'/users';
                $email = EmailTemplet::whereType(8)->first();

                $email->body = str_replace('{{用户名}}',$admin->name,$email->body);
                $email->body = str_replace('{{申请用户名}}',$info,$email->body);
                $email->body = str_replace('{{申请邮箱}}',$user->email,$email->body);
                $email->body = str_replace('{{用户网址}}',$url,$email->body);
                $this->dispatch(new SendEmail($email,$admin));
            }

            return true;
        }
        else
            return false;

    }

    public function register($inputs)
    {
        $res = array('type'=>0,'message'=>'');
        $user = new User;
        $res = $this->saveUser($user, $inputs);
        if($this->needValidate($user->id))
        {
            $res['message'] = array('fail'=>trans('error.00052'));
        }
        elseif($this->needCensor($user->id))
        {
            $res['message'] = array('fail'=>trans('error.00053'));
        }
        else
        {
            $this->changeStatus($user->id,2);
            $res['message'] = array('success'=>trans('error.00062'));
        }
        return $res;


    }



    public function checkStatus($key)
    {
        $res = array('type'=>0,'message'=>'');
        if( SystemInfo::first()->user_email)
            $checkIfConfirm = $this->model->where('email', $key)->first();
        else
            $checkIfConfirm = $this->model->where('name', $key)->first();
        if ( $checkIfConfirm )
        {
            if ( $checkIfConfirm->status_id == 1 ) {
                $res['type'] = 2;

                $res['message'] = array('fail'=>trans('error.00052'));
                $res['user_id'] = $checkIfConfirm->id;


            } elseif ( $checkIfConfirm->status_id == 3 ) {
                $res['type'] = 1;
                $res['message'] = array('fail'=>trans('error.00053'));
                $res['user_id'] = $checkIfConfirm->id;
            } elseif ( $checkIfConfirm->status_id == 4 ) {
                $res['type'] = 1;
                $res['message'] = array('fail'=>trans('error.00054'));
                $res['user_id'] = $checkIfConfirm->id;

            }
        }
        return $res;

    }


    public function validate($confirmation_code)
    {
        $res = array('type'=>0,'message'=>array('success'=>trans('error.00061')));
        $user = User::whereConfirmationCode($confirmation_code)->first();
        if ( !$user )
        {
            $res['type'] = 1;
            $res['message'] = array('fail'=>trans('00060'));
            return $res;
        }
        if($user->status_id == 1)
        {
            if($this->needCensor($user->id))
            {
                $res['type'] = 1;
                $res['message'] = array('fail'=>trans('00053'));
                return $res;
            }
            else
            {
                $this->changeStatus($user->id,2);

            }
        }

        return $res;


    }


    public function resend($user_id)
    {
        $user = User::whereId($user_id)->first();
        $email = EmailTemplet::whereType(7)->first();
        $url = $_SERVER['HTTP_HOST'] . '/register/verify/' . $user->confirmation_code;
        $email->body = str_replace('{{用户名}}', $user->name, $email->body);
        $email->body = str_replace('{{认证网址}}', $url, $email->body);
        error_log($url);
        $this->dispatch(new SendEmail($email,$user));

    }

    public function getByEmail($email)
    {
        $user = $this->model->where('email',$email)->first();
        return $user;
    }

    public function getByName($name)
    {
        $user = $this->model->where('name',$name)->first();
        return $user;
    }


    public function getByOpenID($open_ID)
    {
        $user = $this->model->where('open_id',$open_ID)->first();
        return $user;
    }




}
