<?php namespace App\Repositories;

use App\Models\ExamResult;
use App\Models\Question;
use App\Models\QuestionSelect;
use App\Models\QuestionSelectResult;
use App\Models\QuestionResult;
use App\Models\ExamRecord;
use App\Models\CourseRecord;
use App\Models\ExamDistribution;
use App\Repositories\ExamRepository;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\SendEmail;
use App\Models\EmailTemplet;
use Auth,Carbon\Carbon;
class ExamResultRepository extends BaseRepository
{
    use DispatchesJobs;
    protected $exam_gestion;
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
	ExamResult $examResult,
    ExamRepository $exam_gestion,
    UserRepository $user_gestion
    )
    {
        $this->model = $examResult;
        $this->exam_gestion = $exam_gestion;
        $this->user_gestion = $user_gestion;
    }
    public function score($distribution_id,$inputs)
    {
        $distribution = ExamDistribution::find($distribution_id);
        $exam = $distribution->exam;
        $user = Auth::user();
       
        $record = ExamRecord::where('distribution_id',$distribution->id)->where('user_id',$user->id)->first();
        if(!$record)
        {
            $record = new ExamRecord;
            $record->distribution_id = $distribution->id;
            $record->user_id = $user->id;
            $record->save();
        }

        $result = new $this->model;
        $result->user_id = $user->id;
        $result->record_id = $record->id;
        $result->save();
        $list = $inputs['lists'];
        $list = json_decode($list);
        $totalScore = 0;
        foreach($list as $question_id=>$score)
        {
            $error = 0;
            $question = Question::find($question_id);
            $questionResult = new QuestionResult;
            
            $questionResult->question_id = $question->id;
            $questionResult->result_id = $result->id;
            $questionResult->save();
            if(isset($inputs[$question->id]) )
            {
                if($question->type == 2)
                {
                    $true = array();
                    foreach($question->selects as $select)
                    {
                        if($select->if_true == 1)
                        {
                            $true[] = $select->id;
                        }
                    }
                    $res = $inputs[$question->id];
                    foreach($res as $chose)
                    {
                        $selectResult = new QuestionSelectResult;
                        $selectResult->select_id = $chose;
                        $selectResult->result_id = $questionResult->id;
                        $selectResult->save();
                    }
                    $resA = array_diff($true,$res);
                    $resB = array_diff($res,$true);
                    if(empty($resA) && empty($resB))
                    {
                        $questionResult->if_true = 1;
                        $questionResult->score = $score;
                        $totalScore += $score;
                    }
                }
                elseif($question->type == 4)
                {
                    $questionResult->answer = $inputs[$question->id];
                    $questionResult->score = $score;
                    $questionResult->overed = 0;
                    $result->overed = 2;
                }
                else
                {
                    $select = QuestionSelect::find($inputs[$question->id]);
                    $selectResult = new QuestionSelectResult;
                    $selectResult->select_id = $select->id;
                    $selectResult->result_id = $questionResult->id;
                    $selectResult->save();
                    if($select->if_true == 1)
                    {
                        $questionResult->if_true = 1;
                        $questionResult->score = $score;
                        $totalScore += $score;
                    }
                }
            }
            $questionResult->save();
        }
        $result->score = round($totalScore,1);
        if($result->overed == 0)
        {
            $result->overed = 1;
            if($result->score >= $exam->pass_score)
                $result->passed = 1;
        }
        $result->save();


         $resultNUM = ExamResult::where('user_id',$user->id)->where('record_id',$record->id)->count();
         if($result->overed == 1)
         {
             if($record->max_score < $result->score)
             {
                 $record->max_score = $result->score;
             }
             if($record->max_score >= $exam->pass_score)
             {
                 $record->process = 3;
                 if(!$record->end_at)
                 {
                     $record->end_at = Carbon::now();
                 }
             }
         }
        if($record->process < 3)
        {
            if($exam->limit_times == 0 ||  $resultNUM < $exam->limit_times)
            {
                $record->process = 2;
            }
            else
            {
                $record->process = 4;
            }
        }
        if($exam->if_link)
        {
            $courseRecord = CourseRecord::where('user_id',$user->id)->where('course_id',$distribution->model_id)->first();
            $courseRecord->exam_max_score = $record->max_score;
            $courseRecord->save();
        }

        $record->save();
        return true;
    }

    public function mark($n,$id,$inputs)
    {
        $exam = $this->exam_gestion->getById($id);
        $list = array();
        foreach($exam->distributions as $distribution)
        {
            foreach($distribution->records as $record)
            {
                $list[] = $record->id;
            }
        }
        $userList = $this->user_gestion->getUserIdByInput(isset($inputs['findByUserName'])?$inputs['findByUserName']:"");
        $results = $this->model->whereIn('user_id',$userList)->whereIn('record_id',$list)->lists('id')->all();
        if(isset($inputs['type']) && $inputs['type'] == 2)
        {
            $questionList = QuestionResult::whereIn('result_id',$results)->where('answer','<>','')->where('overed',1)->lists('id')->all();
        }
        else
        {
            $questionList = QuestionResult::whereIn('result_id',$results)->where('answer','<>','')->where('overed',0)->lists('id')->all();
        }
        
        $questionResults = QuestionResult::whereIn('question_results.id',$questionList)
                         
                         ->join('exam_results','exam_results.id','=','question_results.result_id')
                         ->select('question_results.*')
                         ->orderBy('exam_results.user_id','desc')
                         ->orderBy('exam_results.created_at','desc')
                         ->paginate($n);
        return $questionResults;
    }

    public function getQuestionResult($id)
    {
        $result = QuestionResult::find($id);
        return $result;
    }

    public function scoreQuestion($exam_id,$id,$inputs)
    {
        $exam = $this->exam_gestion->getById($exam_id);
        $result = QuestionResult::find($id);
        if($result->examResult->overed == 2)
        {
            $score = round($result->score/5*$inputs['mark'],1);
            $result->examResult->score += $score;
            $result->score = $score;
            $result->overed = 1;
            $result->save();
            $not_over = 0;
            foreach($result->examResult->questionResult as $questionResult)
            {
                if(!$questionResult->overed)
                {
                    $not_over++;
                }
            }
            if($not_over == 0)
            {
                $result->examResult->overed = 1;
            }
            $result->examResult->save();
        }
        if($result->examResult->overed == 1)
        {
            if($result->examResult->score >= $exam->pass_score)
                $result->examResult->passed = 1;
            $result->examResult->save();
            $record = $result->examResult->record;
            if($record->max_score < $result->examResult->score)
            {
                $record->max_score = $result->examResult->score;
            }
        
            if($record->max_score >= $exam->pass_score)
            {
                $record->process = 3;
                if(!$record->end_at)
                {
                    $record->end_at = Carbon::now();
                }
            }
            $record->save();
            if($exam->if_link)
            {
                $courseRecord = CourseRecord::where('user_id',Auth::user()->id)->where('course_id',$record->distribution->model_id)->first();
                $courseRecord->exam_max_score = $record->max_score;
                $courseRecord->save();
            }
              $user = $this->user_gestion->getById($result->examResult->user_id);
              $url = $_SERVER['HTTP_HOST'];
              $email = EmailTemplet::whereType(12)->first();

              $email->body = str_replace('{{用户名}}',$user->name,$email->body);
              $email->body = str_replace('{{考试标题}}',$exam->title,$email->body);
              
              $this->dispatch(new SendEmail($email,$user));
        }
        return true;
    }

    public function getResultsByDistributionId($id)
    {
        $record = ExamRecord::where('distribution_id',$id)->where('user_id',Auth::user()->id)->first();
        $results = $this->model->where('record_id',$record->id)->where('user_id',Auth::user()->id)->get();
        return $results;
    }

}
