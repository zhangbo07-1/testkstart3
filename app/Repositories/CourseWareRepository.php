<?php namespace App\Repositories;

use App\Models\CourseWare;
use App\Models\Course;
use App\Models\CourseRecord;
use App\Models\StudyLog;
use App\Repositories\GuidRepository;
use App\Repositories\UserRepository;
use Storage,Auth,help;
class CourseWareRepository extends BaseRepository
{
    protected $ZipPath = '/resourcefile/coursezip/';
    protected $PATH = "";
    protected $WarePath = 'resourcefile/courses/';
    protected $guid_gestion;
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
        CourseWare $ware,
        GuidRepository $guid_gestion,
        UserRepository $user_gestion
    )
    {
        $this->model = $ware;
        $this->guid_gestion = $guid_gestion;
        $this->user_gestion = $user_gestion;
        $this->PATH = env('OSS_PATH','test');
    }
    public function index($n,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $wares = $this->model->where('course_id',$id)->orderBy($orderby,$direction)->paginate($n);
        return $wares;
    }



    private function saveWare($ware,$inputs,$user_id = null)
    {
        $res = array('status'=>1,'error'=>'');
        if($user_id)
        {
            $ware->user_id = $user_id;
        }
        
        if(array_key_exists('file',$inputs))
        {
            $filename = $inputs['file']->getClientOriginalName();
            $extension = $inputs['file']->getClientOriginalExtension();
             if($extension != 'zip')
            {
                $res['status'] = 0;
                $res['error'] = array('fail'=>trans('error.00023'));
                return $res;
            }
            $ware->filename = $filename;
            $ware->path = $this->guid_gestion->guid();
            $ware->storagename = $ware->path.'.'.$extension;
            Storage::disk('local')->put($this->ZipPath.$ware->course_id.'/'.$ware->storagename, file_get_contents($inputs['file']));
            $file = public_path().'/'.$this->WarePath . $ware->course_id;
            if(!file_exists($file))
            {
                mkdir($file);
            }
            
            $zip = new \ZipArchiveEx;
            if ( $zip->open( storage_path()."/app".$this->ZipPath.$ware->course_id.'/'.$ware->storagename) === TRUE )
            {
                $zip->extractTo(public_path().'/'.$this->WarePath.$ware->course_id.'/'.$ware->path);
            }
            else
            {
                $res['status'] = 0;
                $res['error'] = array('fail'=>trans('error.00023'));
                return $res;
            }
            $result =  Storage::put(
                $this->PATH.$this->ZipPath.$ware->course_id.'/'.$ware->storagename,
                file_get_contents($inputs['file'])
            );
            if($result)
            {
                if($ware->save())
                {
                    if($this->useWare($ware->id))
                    {
                        return $res;
                    }
                    else
                    {
                        $res['status'] = 0;
                        $res['error'] = array('fail'=>trans('error.00023'));
                        return $res;
                    }
                }
            }
            else
            {
                $res['status'] = 0;
                $res['error'] = array('fail'=>trans('error.00019'));
                return $res;
            }
        }
        $res['status'] = 0;
        $res['error'] = array('fail'=>trans('error.00021'));
        return $res;
    }


    public function useWare($id)
    {
        $ware = $this->getById($id);
        $coursePath = public_path().'/'.$this->WarePath.$ware->course_id.'/'.$ware->path;
        if(!file_exists($coursePath))
        {
            $file = Storage::get($this->PATH.$this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename);
            Storage::disk('local')->put($this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename, $file);
            $file = public_path().'/'.$this->WarePath . $ware->course_id;
            if(!file_exists($file))
            {
                mkdir($file);
            }
            $zip = new \ZipArchiveEx;
            if ( $zip->open(storage_path()."/app".$this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename) === TRUE )
            {
                $zip->extractTo($coursePath);
            }
            else
            {
                $this->destroy($ware->id);
                return false;
            }
        }


         $filepath = public_path().'/'.$this->WarePath . $ware->course_id . '/'.$ware->path.'/';
            $infofile = $filepath.'imsmanifest.xml';
            

            if(file_exists($infofile))
            {
                $xml = simplexml_load_file($infofile);
                $resources = $xml->resources->resource;
                $file = '';
                foreach($resources as $resource)
                {
                    if(isset($resource['href']))
                    {
                        $file = $resource['href'];
                    }
                }
                if($file)
                {
                    return true;
                }
                else
                {
                    $this->destroy($ware->id);
                    return false;
                }
            }
            else
            {
                $this->destroy($ware->id);
                    return false;
            }
        return true;
    }

    

    public function store($course_id,$inputs,$user_id)
    {
        $ware = new $this->model;
        $ware->course_id = $course_id;
        $res = $this->saveWare($ware,$inputs,$user_id);
        if($res['status'])
        {
            $course = Course::find($course_id);
            $course->pushed = 2;
            $course->save();
            /*
               $course->ware_id = $ware->id;
               $course->save();
               foreach($course->studyLogs as $log)
               {
               $log->delete();
               }
             */
        }
        return $res;
    }

    public function destroy($id)
    {
	$ware = $this->getById($id);
        Storage::delete($this->PATH.$this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename);
        $ware->delete();
    }

    public function downLoad($id)
    {
        $ware = $this->getById($id);
        $zipPath = storage_path()."/app".$this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename;
        if(!file_exists($zipPath))
        {
            $file = Storage::get($this->PATH.$this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename);
            Storage::disk('local')->put($this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename, $file);
        }
        $url = $zipPath;
        return $url;
    }


    public function cleanFiles($id)
    {
        $basepath = storage_path()."/app";
        $file = $basepath.$this->ZipPath . $id;
        if(file_exists($file))
        {
            $this->deldir($file);
        }
        $file = public_path() . '/'.$this->WarePath . $id;
        if(file_exists($file))
        {
            $this->deldir($file);
        }
    }

    public function deldir($dir,$main = true)
    {

        $dh=opendir($dir);
        while ($file=readdir($dh)) {
            if($file!="." && $file!="..")
            {
                $fullpath=$dir."/".$file;
                if(!is_dir($fullpath))
                {
                    unlink($fullpath);
                }
                else
                {
                    $this->deldir($fullpath,false);
                }
            }
        }
        closedir($dh);
        if(!$main)
        {
            if(rmdir($dir))
            {
                return true;
                    
            }
            else
            {
                return false;
            }
        }
        else
            return true;
    }

    public function preview($id)
    {
        $res = array('status'=>1,'data'=>'no');
        $ware = $this->getLastWare($id);
        if($ware)
        {
            $suspend_data = '';
            $cmi_location = '';
            $cmi_mode = '';
            $completion_status = '';
            $this->useWare($ware->id);
            $filepath = $this->WarePath . $id . '/'.$ware->path.'/';
            $infofile = $filepath.'imsmanifest.xml';
            

            if(file_exists($infofile))
            {
                $xml = simplexml_load_file($infofile);
                $resources = $xml->resources->resource;
                $file = '';
                foreach($resources as $resource)
                {
                    if(isset($resource['href']))
                    {
                        $file = $resource['href'];
                    }
                }
                if($file)
                {
                    
                    $firstfile = $filepath . $file;
                    if ( file_exists($firstfile) )
                    {
                        $filename =$firstfile;
                        $res['data'] = array(
                            'course_id' => $id,
                            'user_id'   => Auth::user()->id,
                            'filename'  => $filename,
                            'suspend_data'  => $suspend_data,
                            'cmi_location'  => $cmi_location,
                            'completion_status'=>$completion_status,
                            'cmi_mode'     => $cmi_mode
                        );
                        return $res;
                    }
                }
            }
        }
        $res['status'] = 0;
        $res['data'] = trans('error.00030');
        
        return $res;
    }


    
    public function startWare($id)
    {
        $res = array('status'=>1,'data'=>'no');
        $course = Course::find($id);
        $ware = $this->getById($course->ware_id);
        if($ware)
        {
            $user_id = Auth::user()->id;
            $record = CourseRecord::where('user_id',$user_id)->where('course_id',$course->id)->first();
            if($record->process == 5 && !$course->if_open_voerdue )
            {
                $res['status'] = 0;
                $res['data'] = trans('error.00031');
                return $res;
            }

            
            $studyLog = StudyLog::where('course_id',$id)->where('user_id',$user_id)->first();
            if($studyLog)
            {
                $suspend_data = $studyLog->suspend_data;
                $suspend_data = str_replace(array("\r\n", "\r", "\n"), "", $suspend_data);
                $cmi_location = $studyLog->location;
                $cmi_mode = $studyLog->mode;
                $completion_status = $studyLog->completion_status;
            }
            else
            {
                
                $suspend_data = '';
                $cmi_location = '';
                $cmi_mode = '';
                $completion_status = '';
            }
            $this->useWare($course->ware_id);
            $filepath = $this->WarePath . $id . '/'.$ware->path.'/';
            
            $infofile = $filepath.'imsmanifest.xml';
            

            if(file_exists($infofile))
            {
                $xml = simplexml_load_file($infofile);
                $resources = $xml->resources->resource;
                $file = '';
                foreach($resources as $resource)
                {
                    if(isset($resource['href']))
                    {
                        $file = $resource['href'];
                    }
                }
                if($file)
                {
                    
                    $firstfile = $filepath . $file;
                    if ( file_exists($firstfile) )
                    {
                        $filename =$firstfile;
                        $res['data'] = array(
                            'course_id' => $id,
                            'user_id'   => $user_id,
                            'filename'  => $filename,
                            'suspend_data'  => $suspend_data,
                            'cmi_location'  => $cmi_location,
                            'completion_status'=>$completion_status,
                            'cmi_mode'     => $cmi_mode
                        );
                        return $res;
                    }
                }
            }
        }
        
        $res['status'] = 0;
        $res['data'] = trans('error.00030');
        
        return $res;
    }

    public function getLastWare($id)
    {
        $ware = $this->model->where('course_id',$id)->orderBy('created_at','desc')->first();
        return $ware;
    }

    public function push($id)
    {
        $res = array('status'=>1,'message'=>array('success'=>trans('course.push_success')));
        $course = Course::find($id);
        if($course->type != 2)
        {
            $ware = $this->getLastWare($id);
            $this->useWare($ware->id);
            $course->ware_id = $ware->id;
        }
        $course->pushed = 1;
        $course->save();
        foreach($course->studyLogs as $log)
        {
            $log->delete();
        }
        /*
        $file =public_path().'/'. $this->WarePath . $ware->course_id;
        if(file_exists($file))
        {
            $this->deldir($file);
        }
        */
       
        return $res;
    }
    


}
