<?php namespace App\Repositories;

use App\Models\Course;
use App\Models\User;
use App\Models\Record;
use App\Models\StudyLog;

use App\Models\Rank;
use App\Models\SurveyResult;
use App\Models\CourseRecord;
use App\Models\CourseClass;
use App\Models\CourseLesson;
use App\Models\CourseLessonSign;
use App\Models\CourseClassRecord;
use App\Models\ExamRecord;
use App\Models\CourseSpan;
use App\Models\ExamDistribution;
use App\Repositories\CourseRepository;
use App\Repositories\UserRepository;
use App\Repositories\CatalogRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\ExamDistributionRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\SendEmail;
use App\Models\EmailTemplet;
use Auth,DateInterval,Carbon\Carbon;
class CourseRecordRepository extends BaseRepository
{
    use DispatchesJobs;
    protected $course_gestion;
    protected $examDistribution_gestion;
    protected $catalog_gestion;

    public function __construct(
        CourseRecord $record,
        UserRepository $user_gestion,
        CourseRepository $course_gestion,
        OrganizationRepository $organization_gestion,
        CatalogRepository $catalog_gestion,
        ExamDistributionRepository $examDistribution_gestion
    )
    {
        $this->model = $record;
        $this->course_gestion = $course_gestion;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
        $this->catalog_gestion = $catalog_gestion;
        $this->examDistribution_gestion = $examDistribution_gestion;
    }



    public function getClassRecords($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getClassDistributeUserList($inputs,$id);
        $records = CourseClassRecord::where('class_id',$id)->whereIn('user_id',$list)->with('user')->with('user.profile')->with('user.department')
                 ->with('user.profile.attribute1')
                 ->with('user.profile.attribute2')
                 ->with('user.profile.attribute3')
                 ->with('user.profile.attribute4')
                 ->with('user.profile.attribute5')
                 ->with('classSigns')
                 ->with('courseClass.lessons')                 
                 ->orderBy($orderby, $direction)->paginate($n);
        return $records;
    }



    public function getClassUnDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getClassUnDistributeUserList($inputs,$id);
        $unDistributeUsers = User::with('profile')->with('department')
                           ->with('profile.attribute1')
                           ->with('profile.attribute2')
                           ->with('profile.attribute3')
                           ->with('profile.attribute4')
                           ->with('profile.attribute5')
                           ->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $unDistributeUsers;
    }


    public function getClassUnDistributeUserList($inputs,$id)
    {
        $class = CourseClass::find($id);
        $userList = $this->getApplyUserList($class->course);
        $list = $this->user_gestion->getUserIdByInputs($inputs);
        $overedIdArray = $this->model->where('course_id',$class->course->id)->whereIn('process',[3,4])->lists('user_id')->all();
        $checkedIdArray = CourseClassRecord::where('class_id',$id)->lists('user_id')->all();
        $unDistributeUsers = User::whereIn('id',$list)->whereIn('id',$userList)->whereNotIn('id',$overedIdArray)->whereNotIn('id',$checkedIdArray)->where('status_id',2)->lists('id')->all();
        return $unDistributeUsers;
    }



    public function getClassDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        
        $list = $this->getClassDistributeUserList($inputs,$id);
        $DistributeUsers = User::with('profile')->with('department')
                         ->with('profile.attribute1')
                         ->with('profile.attribute2')
                         ->with('profile.attribute3')
                         ->with('profile.attribute4')
                         ->with('profile.attribute5')
                         ->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $DistributeUsers;
    }


    public function getClassDistributeUserList($inputs,$id)
    {
        
        $list = $this->user_gestion->getUserIdByInputs($inputs);
        $choseIdArray = CourseClassRecord::where('class_id',$id)->lists('user_id')->all();
        $DistributeUsers = User::whereIn('id',$list)->whereIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $DistributeUsers;

    }

    public function getUnDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getUnDistributeUserList($inputs,$id);
        $unDistributeUsers = User::with('profile')->with('department')
                           ->with('profile.attribute1')
                           ->with('profile.attribute2')
                           ->with('profile.attribute3')
                           ->with('profile.attribute4')
                           ->with('profile.attribute5')
                           ->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $unDistributeUsers;
    }

    public function getUnDistributeUserList($inputs,$id)
    {
        
        $course = Course::find($id);
        $userList = $this->getApplyUserList($course);
        $list = $this->user_gestion->getUserIdByInputs($inputs);
        $choseIdArray = $this->model->where('course_id',$id)->lists('user_id')->all();
        $unDistributeUsers = User::whereIn('id',$list)->whereIn('id',$userList)->whereNotIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $unDistributeUsers;

    }




    public function getDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        
        $list = $this->getDistributeUserList($inputs,$id);
        $DistributeUsers = User::with('profile')->with('department')
                         ->with('profile.attribute1')
                         ->with('profile.attribute2')
                         ->with('profile.attribute3')
                         ->with('profile.attribute4')
                         ->with('profile.attribute5')
                         ->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $DistributeUsers;
    }

    public function getDistributeUserList($inputs,$id)
    {
        
        $list = $this->user_gestion->getUserIdByInputs($inputs);
        $choseIdArray = $this->model->where('course_id',$id)->lists('user_id')->all();
        $DistributeUsers = User::whereIn('id',$list)->whereIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $DistributeUsers;

    }


    public function getOverdueUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        
        $list = $this->getOverdueUserList($inputs,$id);
        $DistributeUsers = User::with('profile')->with('department')
                         ->with('profile.attribute1')
                         ->with('profile.attribute2')
                         ->with('profile.attribute3')
                         ->with('profile.attribute4')
                         ->with('profile.attribute5')
                         ->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $DistributeUsers;
    }


    public function getOverdueUserList($inputs,$id)
    {
               
        $list = $this->user_gestion->getUserIdByInputs($inputs);
        $oldRecords = $this->model->where('course_id',$id)->whereIn('process', array(1,2,3))->get();
        foreach($oldRecords as $record)
        {
            if($record->course->limitdays > 0)
            {
                $limitDay = ((strtotime($record->created_at))
                             +$record->course->limitdays*24*60*60);

                $limittime =($limitDay-time());
                if($limittime < 0)
                {
                    $record->process = 5;
                    $record->save();
                }
            }
        }

        
        $choseIdArray = $this->model->where('course_id',$id)->where('process',5)->lists('user_id')->all();
        $DistributeUsers = User::whereIn('id',$list)->whereIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $DistributeUsers;

    }


    public function addClassRecords($inputs,$id)
    {
        $res = array('result'=>true,'message'=>trans('record.distribute_success'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getClassUnDistributeUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }

        foreach($list as $user_id)
        {
            $this->addClassRecord($id,$user_id);
        }
        return $res;
    }


    public function addClassRecord($id,$user_id)
    {
        $class = CourseClass::find($id);
        $classRecord = new CourseClassRecord;
        $classRecord->class_id = $id;
        $classRecord->user_id = $user_id;
        $classRecord->save();
        $record = $this->model->where('user_id',$user_id)->where('course_id',$class->course->id)->first();
        if(!$record)
        {
            $this->addRecord($class->course->id,$user_id);
        }
    }


    public function classSign($course_id,$class_id,$lesson_id)
    {
        $res = array('result'=>true,'message'=>trans('course.sign_success'));
        $user = Auth::user();
        $classRecord = CourseClassRecord::where('class_id',$class_id)->where('user_id',$user->id)->first();
        if(!$classRecord)
        {
            $res['result'] = false;
            $res['message'] = trans('error.00076');
            return $res;
        }
        $lessonSign = CourseLessonSign::where('user_id',$user->id)->where('lesson_id',$lesson_id)->first();
        if($lessonSign)
        {
            $res['result'] = false;
            $res['message'] = trans('error.00079');
            return $res;
        }
        $lessonSign = new CourseLessonSign;
        $lessonSign->classRecord_id = $classRecord->id;
        $lessonSign->user_id = $user->id;
        $lessonSign->lesson_id = $lesson_id;
        $lessonSign->save();
        $this->ifClassLate($lesson_id,$lessonSign->id);
        $this->ifClassOver($class_id,$user->id);
        return $res;
        
    }

    public function ifClassLate($lesson_id,$sign_id)
    {
        $lesson = CourseLesson::find($lesson_id);

        $lessonSign = CourseLessonSign::find($sign_id);
        $limit = (strtotime($lesson->start_at));
        $signTime = (time()-$limit);
        error_log("sign".$signTime);
        if($signTime <= $lesson->courseClass->late*60)
        {
            $lessonSign->process = 2;
            if($signTime <= 0)
            {
                $lessonSign->process = 3;
            }
        }
        $lessonSign->save();
    }

    public function ifClassOver($class_id,$user_id)
    {
        $classRecord = CourseClassRecord::where('class_id',$class_id)->where('user_id',$user_id)->first();
        $courseRecord = $this->model->where('user_id',$user_id)->where('course_id',$classRecord->courseClass->course->id)->first();
        if(!$courseRecord->started_at)
        {
            $courseRecord->started_at = Carbon::now();
        }
        if(count($classRecord->classSigns) == count($classRecord->courseClass->lessons))
        {
            $status = 0;
            foreach($classRecord->classSigns as $sign)
            {
                if($sign->process < 2)
                {
                    $status++;
                }
            }
            if($status == 0)
            {
                $classRecord->process = 2;
                $classRecord->save();
                $courseRecord->process = 3;
                $courseRecord->end_at = Carbon::now();
            }
        }
        else
        {
            if($courseRecord->process < 3)
            {
                $courseRecord->process = 2;
                
            }
        }
        $courseRecord->save();
    }


    public function classEnroll($course_id,$id)
    {
        $res = array('result'=>true,'message'=>trans('course.enroll_success'));
        $user = Auth::user();
        $courseRecord = $this->model->where('user_id',$user->id)->where('course_id',$course_id)->first();
        if($courseRecord)
        {
            if($courseRecord->process >= 3)
            {
                $res['result'] = false;
                $res['message'] = trans('error.00076');
                return $res;
            }
            $classRecord = CourseClassRecord::where('class_id',$id)->where('user_id',$user->id)->first();
            if($classRecord)
            {
                $res['result'] = false;
                $res['message'] = trans('error.00077');
                return $res;
            }
        }
        $this->addClassRecord($id,$user->id);
        return $res;
    }


    public function beOver($inputs,$id)
    {
        $res = array('result'=>true,'message'=>trans('error.10000'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getClassDistributeUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
            
        }
        $records = CourseClassRecord::where('class_id',$id)->whereIn('user_id',$list)->get();
        foreach($records as $record)
        {
            if($record->process != 2)
            {
                $record->process = 2;
                $record->save();
                $courseRecord = $this->model->where('course_id',$record->courseClass->course_id)->where('user_id',$record->user_id)->first();
                if($courseRecord->process < 3 || $courseRecord->process == 5)
                {
                    $courseRecord->process = 3;
                    $courseRecord->end_at = Carbon::now();
                    $courseRecord->save();
                }
                
            }
        }
        return $res;
    }

    public function beNotOver($inputs,$id)
    {
        $res = array('result'=>true,'message'=>trans('error.10000'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getClassDistributeUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }

        $records = CourseClassRecord::where('class_id',$id)->whereIn('user_id',$list)->get();
        foreach($records as $record)
        {
            if($record->process == 2)
            {
                $record->process = 1;
                $record->save();
                $courseRecord = $this->model->where('course_id',$record->courseClass->course_id)->where('user_id',$record->user_id)->first();
                if($courseRecord->process == 3 || $courseRecord->process == 4 )
                {
                    $courseRecord->process = 2;
                    $courseRecord->end_at = null;
                    $courseRecord->save();
                }
                
            }
        }
        return $res;
    }


    public function addRecords($inputs,$id)
    {
        $res = array('result'=>true,'message'=>trans('record.distribute_success'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getUnDistributeUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }

        foreach($list as $user_id)
        {
            $this->addRecord($id,$user_id);
        }
        return $res;
    }

    public function addRecord($course_id,$user_id)
    {
        $course = Course::find($course_id);
        $record = $this->model->where('user_id',$user_id)->where('course_id',$course_id)->first();
        if(!$record)
        {
            $record = new $this->model;
            $record->course_id = $course_id;
            $record->user_id = $user_id;
            $record->save();
            $list = new Record;
            $list->model_id = $record->id;
            if($course->type == 1)
            {
                $list->type = 1;
            }
            else
            {
                $list->type = 3;
            }
            $list->model_type = CourseRecord::class;
            $list->user_id = $user_id;
            $list->save();
            $user = User::find($record->user_id);
            if ( $record->course->emailed > 0 )
            {
                $url = $_SERVER['HTTP_HOST'];
                $email = EmailTemplet::whereType(1)->first();

                $email->body = str_replace('{{用户名}}',$user->name,$email->body);
                $email->body = str_replace('{{课程标题}}',$record->course->title,$email->body);
                $email->body = str_replace('{{用户网址}}',$url,$email->body);
                $this->dispatch(new SendEmail($email,$user));
                


            }
        }
    }

    public function redistribute($inputs,$id)
    {
        $res = array('result'=>true,'message'=>trans('record.redistribute_success'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getOverdueUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }
        $records = $this->model->where('course_id',$id)->whereIn('user_id',$list)->get();
        foreach($records as $record)
        {
            $record->process = 2;
            $record->overdue = 1;
            $record->created_at = Carbon::now();
            $record->save();
        }
        return $res;
    }



    public function classRedistribute($inputs,$course_id,$id)
    {
        
        $res = array('result'=>true,'message'=>trans('record.redistribute_success'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getOverdueUserList($inputs,$course_id);
        }
        else
        {
            $list = $inputs['user_id'];
        }
        $records = $this->model->where('course_id',$course_id)->whereIn('user_id',$list)->get();
        foreach($records as $record)
        {
            $record->process = 2;
            $record->overdue = 1;
            $record->created_at = Carbon::now();
            $record->save();
            $this->addClassRecord($id,$record->user_id);
        }
        return $res;
    }


    public function deleteClassRecords($inputs,$id)
    {
        $res = array('result'=>true,'message'=>trans('record.undistribute_success'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getClassDistributeUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }
        $classRecords = CourseClassRecord::where('class_id',$id)->whereIn('user_id',$list)->get();

        foreach($classRecords as $classRecord)
        {
            
            $classList = $classRecord->courseClass->course->classes()->lists('id')->all();
            if(CourseClassRecord::whereIN('class_id',$classList)->where('user_id',$classRecord->user_id)->count() == 1)
            {
                $record = $this->model->where('course_id',$classRecord->courseClass->course_id)->where('user_id',$classRecord->user_id)->first();
                foreach($record->record as $list)
                    $list->delete();
                if($record->course->examDistribution)
                {
                    foreach($record->course->examDistribution->records as $examrecord)
                        $examrecord->delete();
                }
                $studyLog = StudyLog::where('course_id',$id)->where('user_id',$record->user_id)->first();
                if($studyLog)
                {
                    $studyLog->delete();
                }
                $record->delete();
            }
            $classRecord->delete();
        }
        return $res;
    }

    public function deleteRecords($inputs,$id)
    {
        $res = array('result'=>true,'message'=>trans('record.undistribute_success'));
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getDistributeUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }
        $records = $this->model->where('course_id',$id)->whereIn('user_id',$list)->get();

        foreach($records as $record)
        {
            foreach($record->record as $list)
                $list->delete();
            if($record->course->examDistribution)
            {
                foreach($record->course->examDistribution->records as $examrecord)
                    $examrecord->delete();
            }
            $studyLog = StudyLog::where('course_id',$id)->where('user_id',$record->user_id)->first();
            if($studyLog)
            {
                $studyLog->delete();
            }
            $record->delete();
        }
        return $res;
    }




    public function saveRecordMessage($id,$inputs)
    {
        $variable = $inputs["variable"];
        $value = $inputs["value"];
        $scormtype = $inputs["scormtype"];
        if ( $variable == "initialize" and $value == "0" )
        {
            $record = $this->model->where('user_id', Auth::user()->id)->where('course_id', $id)->firstOrFail();
            if(!$record->started_at)
            {
                $record->started_at = Carbon::now();
                $record->process = 2;
            }

            $record->study_times++;
            $record->save();
        }
        //If completed

        if( ( $variable == "cmi.completion_status" and $value == "completed" )||( $variable == "cmi.success_status" and $value == "1")||( $variable == "cmi.core.lesson_status" and $value == "passed"))
        {
            $record = $this->model->where('user_id', Auth::user()->id)->where('course_id', $id)->firstOrFail();
            if($record->process == 2)
            {
                $record->end_at = Carbon::now();
                $record->process = 99;

                $record->save();

                $spans = CourseSpan::where('course_id',$id)->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
                if($spans)
                {
                    $record->study_span += $spans[0]->study_span/60;
                    $record->save();
                    foreach($spans as $span)
                    {
                        $span->delete();
                    }
                }
                
            }


        }

        if($variable == "cmi.score.raw")
        {
            $record = $this->model->where('user_id', Auth::user()->id)->where('course_id', $id)->firstOrFail();
            if($record->process < 3)
            {
                if($record->max_score < $value)
                {
                    $record->max_score = $value;
                }
                $record->save();
            }
            
        }
        if($variable == "cmi.core.score.raw")
        {
            $record = $this->model->where('user_id', Auth::user()->id)->where('course_id', $id)->firstOrFail();
            if($record->process < 3)
            {
                if($record->max_score < $value)
                {
                    $record->max_score = $value;
                }
                $record->save();
            }
        }
        if ( $variable == "cmi.session_time" )
        {
            $span = new CourseSpan;
            $span->course_id = $id;
            $span->user_id = Auth::user()->id;

            //$record->study_span += substr($value,2,-1);

            if(strstr($value,':'))
            {
                $timespan = explode(":",$value);
                $span->study_span = $timespan[0]*3600+$timespan[1]*60+$timespan[2];
            }
            else
            {
                $timespan = strstr($value,'.',TRUE);
                if($timespan)
                {
                    $dv = new DateInterval($timespan.'S');
                }
                else
                {
                    $dv = new DateInterval($value);
                }
                $span->study_span = $dv->h * 3600 + $dv->i * 60 + $dv->s;
           
            }
            
            $span->save();
        }

        if ( $variable == "cmi.core.session_time" )
        {
            $span = new CourseSpan;
            $span->course_id = $id;
            $span->user_id = Auth::user()->id;
            $timespan = explode(":",$value);
            $span->study_span = $timespan[0]*3600+$timespan[1]*60+$timespan[2];
            $span->save();
        }
        if($variable == "terminate" && $value == "start")
        {
            $record = $this->model->where('user_id', Auth::user()->id)->where('course_id', $id)->firstOrFail();
            
            $spans = CourseSpan::where('course_id',$id)->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
            
            if(count($spans))
            {
                if($record->process < 3 )
                {
                    $record->study_span += $spans[0]->study_span/60;

                    $record->save();
                }

                if($record->process == 99 )
                {
                    $record->study_span += $spans[0]->study_span/60;
                    $record->process = 3;
                    $record->save();
                }


                foreach($spans as $span)
                {
                    $span->delete();
                }
            }
            if($record->process == 99 )
            {
                $record->process = 3;
                $record->save();
            }

        }
        if($variable == "cmi.suspend_data")
        {
            $studyLog = StudyLog::where('course_id',$id)->where('user_id',Auth::user()->id)->first();
            if(!$studyLog)
            {
                $studyLog = new StudyLog;
                $studyLog->course_id = $id;
                $studyLog->user_id = Auth::user()->id;
                $studyLog->save();
            }
            $suspend_data = str_replace(array("\r\n", "\r", "\n"), "", $value);
            $studyLog->suspend_data = $suspend_data;
            $studyLog->save();
        }

        if($variable == "cmi.location")
        {
            $studyLog = StudyLog::where('course_id',$id)->where('user_id',Auth::user()->id)->first();
            if(!$studyLog)
            {
                $studyLog = new StudyLog;
                $studyLog->course_id = $id;
                $studyLog->user_id = Auth::user()->id;
                $studyLog->save();
            }
            $studyLog->location  = $value;
            $studyLog->save();
        }
        if($variable == "cmi.mode")
        {
            $studyLog = StudyLog::where('course_id',$id)->where('user_id',Auth::user()->id)->first();
            if(!$studyLog)
            {
                $studyLog = new StudyLog;
                $studyLog->course_id = $id;
                $studyLog->user_id = Auth::user()->id;
                $studyLog->save();
            }
            $studyLog->mode  = $value;
            $studyLog->save();
        }

        if($variable == "cmi.completion_status")
        {
            $studyLog = StudyLog::where('course_id',$id)->where('user_id',Auth::user()->id)->first();
            if(!$studyLog)
            {
                $studyLog = new StudyLog;
                $studyLog->course_id = $id;
                $studyLog->user_id = Auth::user()->id;
                $studyLog->save();
            }
            $studyLog->completion_status  = $value;
            $studyLog->save();
        } 

        return response()->json([ 'result' => true ]);
    }
    public function ifOver($record_id)
    {
        $record = $this->getById($record_id);
        if($record->process == 3 || $record->process == 4)
        {
            $status = 0;
            if($record->course->examDistribution)
            {
                $examRecord = ExamRecord::where('user_id',Auth::user()->id)->where('distribution_id',$record->course->examDistribution->id)->first();
                if($examRecord &&$examRecord->process == 3)
                {
                    $status = 1;

                }
            }
            else
            {
                $status = 1;
            }
            $evaluationRecord = $record->course->surveyRecords()->where('type',2)->first();
            if($evaluationRecord)
            {
                if(SurveyResult::where('record_id',$evaluationRecord->id)->where('user_id',Auth::user()->id)->count())
                {
                    $status++;
                }
            }
            else
            {
                $status++;
            }
            if($status == 2)
            {
                $record->process = 4;
            }
            else
            {
                $record->process = 3;
            }
            $record->save();
            
        }

        if($record->course->limitdays > 0 && $record->process != 4 )
        {
            $limitDay = ((strtotime($record->created_at))
                         +$record->course->limitdays*24*60*60);

            $limittime =($limitDay-time());
            if($limittime < 0)
            {
                $record->process = 5;
                $record->save();
            }
            elseif($record->process == 5)
            {
                $record->process = 2;
                $record->save();
            }
        }
        if($record->course->type == 1 && $record->course->limitdays == 0 && $record->process == 5)
        {
            $record->process = 2;
            $record->save();
        }

         if($record->course->type == 2 && $record->process != 4)
        {
            $this->ifClassOverdue($record->id);
        }

        
    }


    public function ifClassOverdue($record_id)
    {
        $record = $this->getById($record_id);
        if($record->course->type != 2 || $record->process == 4)
        {
            return false;
        }
        $classId = $record->course->classes()->lists('id')->all();
        $classId = CourseClassRecord::where('user_id',Auth::user()->id)->whereIn('class_id',$classId)->lists('class_id')->all();
        $limitTime = CourseLesson::whereIn('class_id',$classId)->max('end_at');
        $limitTime = strtotime($limitTime)+24*60*60*7;
        $limitTime =($limitTime-time());
        if($limitTime < 0)
        {
            $record->process = 5;
            $record->save();
        }
        elseif($record->process == 5)
        {
            $record->process = 2;
            $record->save();
        }
    }



    public function lessons($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $oldRecords = $this->model->with("course")->where('user_id',Auth::user()->id)->get();

        foreach($oldRecords as $record)
        {
            $this->ifOver($record->id);
        }

        $this->examDistribution_gestion->ifOver();
        $type = isset($inputs['type'])?$inputs['type']:0;
        if($type == 2)
        {
            $courseList = array();
        }
        elseif($type ==1)
        {
            $courseList = $this->getRecordList($inputs,false);
            $courseList = $this->model->whereIn('id', $courseList)
                        ->whereHas('course',function($q)
                        {
                            $q->where('type',1);
                        })
                        ->where('user_id',Auth::user()->id)
                        ->whereIn('process', array(1,2,3))
                        ->lists('id')->all();
        }
        elseif($type ==3)
        {
            $courseList = $this->getRecordList($inputs,false);
            $courseList = $this->model->whereIn('id', $courseList)
                        ->whereHas('course',function($q)
                        {
                            $q->where('type',2);
                        })
                        ->where('user_id',Auth::user()->id)
                        ->whereIn('process', array(1,2,3))
                        ->lists('id')->all();
        }
        else
        {
            $courseList = $this->getRecordList($inputs,false);
            $courseList = $this->model->whereIn('id', $courseList)->where('user_id',Auth::user()->id)->whereIn('process', array(1,2,3))->lists('id')->all();
        }
        if($type == 1 || $type == 3)
        {
            $examList = array();
        }
        else
        {
            $examList = $this->examDistribution_gestion->exams($inputs);
        }
        $list = Record::where('user_id',Auth::user()->id)
              ->where(function($q) use ($courseList)
                      {
                          $q->whereIN('type',[1,3])
                          ->whereIn('model_id',$courseList);
                      })
              ->orWhere(function($q) use ($examList)
                        {
                            $q->where('type',2)
                            ->whereIn('model_id',$examList);
                        })
              ->orderBy($orderby,$direction)->paginate($n);

        return $list;
    }



    public function lessonsDone($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $oldRecords = $this->model->with("course")->where('user_id',Auth::user()->id)->get();

        foreach($oldRecords as $record)
        {

            $this->ifOver($record->id);
        }
        $this->examDistribution_gestion->ifOver();
        $type = isset($inputs['type'])?$inputs['type']:0;
        if($type == 2)
        {
            $courseList = array();
        }
        elseif($type ==1)
        {
            $courseList = $this->getRecordList($inputs,false);
            $courseList = $this->model->whereIn('id', $courseList)
                        ->whereHas('course',function($q)
                        {
                            $q->where('type',1);
                        })
                        ->where('user_id',Auth::user()->id)
                        ->where('process', 4)
                        ->lists('id')->all();
        }
        elseif($type ==3)
        {
            $courseList = $this->getRecordList($inputs,false);
            $courseList = $this->model->whereIn('id', $courseList)
                        ->whereHas('course',function($q)
                        {
                            $q->where('type',2);
                        })
                        ->where('user_id',Auth::user()->id)
                        ->where('process', 4)
                        ->lists('id')->all();
        }
        else
        {
            $courseList = $this->getRecordList($inputs,false);
            $courseList = $this->model->whereIn('id', $courseList)->where('user_id',Auth::user()->id)->where('process', 4)->lists('id')->all();
        }
        if($type == 1 || $type == 3)
        {
            $examList = array();
        }
        else
        {
            $examList = $this->examDistribution_gestion->examsDone($inputs);
        }




        
        $list = Record::where('user_id',Auth::user()->id)
              ->where(function($q) use ($courseList)
                      {
                          $q->whereIn('type',[1,3])
                          ->whereIn('model_id',$courseList);
                      })
              ->orWhere(function($q) use ($examList)
                        {
                            $q->where('type',2)
                            ->whereIn('model_id',$examList);
                        })
              ->orderBy($orderby,$direction)->paginate($n);

        return $list;
    }


    public function lessonsOverdue($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $oldRecords = $this->model->with("course")->where('user_id',Auth::user()->id)->get();

        foreach($oldRecords as $record)
        {
            $this->ifOver($record->id); 
        }
        $this->examDistribution_gestion->ifOver();
        $type = isset($inputs['type'])?$inputs['type']:0;
        if($type == 2)
        {
            $courseList = array();
        }
        elseif($type ==1)
        {
            $courseList = $this->getRecordList($inputs,false);
            $courseList = $this->model->whereIn('id', $courseList)
                        ->whereHas('course',function($q)
                        {
                            $q->where('type',1);
                        })
                        ->where('user_id',Auth::user()->id)
                        ->where('process', 5)
                        ->lists('id')->all();
        }
        elseif($type ==3)
        {
            $courseList = $this->getRecordList($inputs,false);
            $courseList = $this->model->whereIn('id', $courseList)
                        ->whereHas('course',function($q)
                        {
                            $q->where('type',2);
                        })
                        ->where('user_id',Auth::user()->id)
                        ->where('process', 5)
                        ->lists('id')->all();
        }
        else
        {
            $courseList = $this->getRecordList($inputs,false);
            $courseList = $this->model->whereIn('id', $courseList)->where('user_id',Auth::user()->id)->where('process', 5)->lists('id')->all();
        }
        if($type == 1 || $type == 3)
        {
            $examList = array();
        }
        else
        {
            $examList = $this->examDistribution_gestion->examsOverdue($inputs);
        }
        $list = Record::where('user_id',Auth::user()->id)
              ->where(function($q) use ($courseList)
                      {
                          $q->whereIn('type',[1,3])
                          ->whereIn('model_id',$courseList);
                      })
              ->orWhere(function($q) use ($examList)
                        {
                            $q->where('type',2)
                            ->whereIn('model_id',$examList);
                        })
              ->orderBy($orderby,$direction)->paginate($n);

        return $list;
    }





    public function getRecordByCourseId( $id )
    {
        $course = Course::findOrFail($id);
        $record = $this->model->where('course_id',$id)
                ->where('user_id',Auth::user()->id)->first();
        return $record;

    }

    public function updateStar($id, $inputs )
    {
        $record = $this->getById($id);
        $record->star = $inputs['score'];
        
        $record->save();
        if ( $record )
        {
            $res[ 'result'] =true ;
        } else
        {
            $res[ 'result'] =false ;
        }
        return $res;


    }

    public function getRecord($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getRecordList($inputs);
        $courseRecords = $this->model->whereIn('id',$list)->orderBy($orderby,$direction)->paginate($n);
        return $courseRecords;

    }


    public function getRecordList($inputs,$manage=true)
    {
        $userList =  $this->user_gestion->getUserIdByInput( isset($inputs['findByUserName'])?$inputs['findByUserName']:"" ,$manage);
        $courseList = $this->course_gestion->getListByInput(isset($inputs['findByCourseName'])?$inputs['findByCourseName']:"",$manage);
        if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);

            $catalogArray = $this->catalog_gestion->getUnderArray($catalogArray);
            $courseList = Course::whereIn('id',$courseList)
                        ->whereIn('catalog_id',$catalogArray)
                        ->lists('id')->all();
        }
        $dateLimit = isset($inputs['dateLimit'])?$inputs['dateLimit']:0;
        $start_time = !empty($inputs['startDate'])?$inputs['startDate']:date("Y-m-d H:i:s",0);
        $end_time = !empty($inputs['endDate'])?$inputs['endDate']:date("Y-m-d H:i:s",time());
        if($dateLimit == 1)
        {
            $recordList = $this->model->whereIn('course_id',$courseList)->whereIn('user_id',$userList)->whereBetween('started_at',[$start_time,$end_time])->lists('id')->all();
        }
        elseif($dateLimit == 2)
        {
            $recordList = $this->model->whereIn('course_id',$courseList)->whereIn('user_id',$userList)->whereBetween('end_at',[$start_time,$end_time])->lists('id')->all();
        }
        else
        {
            $recordList = $this->model->whereIn('course_id',$courseList)->whereIn('user_id',$userList)->lists('id')->all();
        }
        return $recordList;
    }


    public function getRanks($n)
    {
        $ranks = Rank::orderBy('study_span','desc')->orderBy('user_id')->paginate($n);
        return $ranks;
    }


    
    public function report($n,$inputs,$course_id)
    {
        $courseList = $this->getRecordList($inputs,true);
        $courseList = CourseRecord::whereIn('id', $courseList)->where('course_id',$course_id)->lists('id')->all();

        $class_id = isset($inputs['class_id'])?$inputs['class_id']:0;
        if($class_id)
        {
            $userList = CourseClassRecord::where('class_id',$class_id)->lists('user_id')->all();
        }
        else
        {
            $userList = $this->user_gestion->getUserIdByInputs($inputs);
        }
        $list = Record::whereIn('user_id',$userList)
              ->where(function($query) use ($courseList)
                      {
                          $query->where(function($q) use ($courseList)
        {
            $q->whereIn('type',[1,3])
            ->whereIn('model_id',$courseList);
        });
                          
                      })
              ->join('users','records.user_id','=','users.id')
              ->orderBy('users.name','asc')->paginate($n);

        $examRecord = array();
        $evaluationRecord = array();
        $evaResult = array();
        $surveyRecord = array();
        $surveyResult = array();

        foreach($list as $record)
        {

            if($record->model->course->examDistribution)
            {
                $examRecord[$record->id] = ExamRecord::where('user_id',$record->user_id)->where('distribution_id',$record->model->course->examDistribution->id)->first();
            }
            $evaluationRecord[$record->id] = $record->model->course->surveyRecords()->where('type',2)->first();
            if($evaluationRecord[$record->id])
            {

                $evaResult[$record->id] = SurveyResult::where('user_id',$record->user_id)->where('record_id',$evaluationRecord[$record->id]->id)->count();
            }

            $surveyRecord[$record->id] = $record->model->course->surveyRecords()->where('type',3)->first();
            if($surveyRecord[$record->id])
            {
                $surveyResult[$record->id] = SurveyResult::where('user_id',$record->user_id)->where('record_id',$surveyRecord[$record->id]->id)->count();
            }

        }
        $res['records'] = $list;
        $res['examRecord'] = $examRecord;
        $res['evaluationRecord'] = $evaluationRecord;
        $res['evaResult'] = $evaResult;
        $res['surveyRecord'] = $surveyRecord;
        $res['surveyResult'] = $surveyResult;

        return $res;
    }






    
    public function reportExcel($inputs,$course_id)
    {
        
        $prog[] =array(trans('user.real_name'),trans('table.user_name'),trans('table.email'),trans('user.department'),trans('record.name'),trans('record.type'),trans('table.catalog'),trans('table.status'),trans('course.status'),trans('exam.status'),trans('evaluation.status'),trans('table.create_time'),trans('table.start_time'),trans('table.end_time'),trans('table.study_times'),trans('table.study_span')."(".trans('table.minute').")",trans('table.course_score'));
        $res = $this->report(99999,$inputs,$course_id);
        
        $records = $res['records'];
        $examRecord = $res['examRecord'];
        $evaluationRecord = $res['evaluationRecord'];
        $evaResult = $res['evaResult'];

        $surveyRecord = $res['surveyRecord'];
        $surveyResult = $res['surveyResult'];
        foreach($records as $record)
        {
            if($record->type == 1 || $record->type == 3)
            {
                if($record->model->process == 1)
                {
                    if(($record->model->course->examDistribution && !empty($examRecord[$record->id])) || (!empty($evaluationRecord[$record->id] ) &&  $evaResult[$record->id]>0))
                    {
                        $Status = trans('course.under_way');
                    }
                    else
                    {
                        $Status = trans('course.not_start');
                    }
                }
                elseif($record->model->process == 3)
                {
                    $Status = trans('course.under_way');
                }
                else
                {
                    $Status = trans('course.'.$record->model->status->title);
                }
                $Status = $Status.($record->model->overdue?'('.trans('table.overdue').')':'');
                $couseStatus = trans('course.'.$record->model->status->title);
                if(!$record->model->course->examDistribution)
                    $examStatus = "";
                elseif(!$examRecord[$record->id])
                    $examStatus = trans('exam.not_start');
                else
                    $examStatus = trans("exam.".$examRecord[$record->id]->status->title);

                if(empty($evaluationRecord[$record->id]))
                    $evaluationStatus = "";
                elseif($evaResult[$record->id]<1)
                    $evaluationStatus = trans('table.not_start');
                else
                    $evaluationStatus = trans('table.done');

                $prog[] = array($record->model->user->profile->realname,
                                $record->model->user->name,
                                $record->model->user->email,
                                $record->model->user->department->name,
                                $record->model->course->title,
                                trans($record->recordType->title),
                                $record->model->course->catalog->name,
                                $Status,
                                $couseStatus,
                                $examStatus,
                                $evaluationStatus,
                                $record->model->created_at,
                                $record->model->started_at,
                                $record->model->end_at?:"",
                                $record->model->study_times,
                                $record->model->study_span?$record->model->study_span:0,
                                $record->model->max_score,
                                $record->model->exam_max_score,
                               
                );
            }
           
        }
        return $prog;
    }


}
