<?php namespace App\Repositories;

use App\Models\Question;
use App\Models\QuestionResult;
use App\Models\QuestionBank;
use App\Repositories\BankRepository;
use App\Models\QuestionSelect;
use App\Repositories\ImportLogRepository;
use Excel;
class QuestionRepository extends BaseRepository
{
    protected $bank_gestion;

    protected $log_gestion;
    /**
     * Create a new CommentRepository instance.
     *
     * @param  App\Models\Question $question
     * @return void
     */
    public function __construct(
        Question $question,
        BankRepository $bank_gestion,
        ImportLogRepository $log_gestion
    )
    {
        $this->model = $question;
        $this->bank_gestion = $bank_gestion;
        $this->log_gestion = $log_gestion;
    }


    public function index($id,$n,$orderby = 'question_id', $direction = 'asc')
    {
        $questions = $this->model->where('bank_id',$id)->orderBy($orderby,$direction)->paginate($n);
        return $questions;
    }


    /**
     * Save a item.
     *
     * @param  array $inputs

     * @return void
     */
    public function saveQuestion($question, $inputs, $bank_id)
    {
        if(array_key_exists('Qtype', $inputs))
        {
            switch( $inputs['Qtype'])
            {
                case 'radio':
                case 'SC':
                $question->type = 1;
                break;
                case 'multiple':
                case 'MC':
                $question->type = 2;
                break;
                case 'false':
                case 'TF':
                $question->type = 3;
                break;
                case 'essay':
                case 'ES':
                $question->type = 4;
                break;
            }
        }
        $question->question_id = $inputs['question_id'];
        $question->title = $inputs['title'];
        $question->bank_id = $bank_id;
        $question->priority = $inputs['priority'];
        $question->save();
        foreach($question->selects as $select)
        {
            $select->delete();
        }

        if($question->type == 1)
        {
            $radioNum = $inputs['radioNUM'];
            for($i = 1;$i<=$radioNum; $i++)
            {
                
                $select = new QuestionSelect;
                $select->title = $inputs['radioselect'.$i];
                $select->question_id = $question->id;
                if(array_key_exists('radioture', $inputs))
                {
                    if($inputs['radioture']== $i)
                    {
                        $select->if_true = 1;
                    }
                }
                else
                {
                    $question->delete();
                    return false;
                }
                $select->save();
            }
        }



        if($question->type==2)
        {
            $mulitNum = $inputs['mulitNUM'];
            $mulitTrue = $inputs['multipleture'];
            for($i = 1;$i<=$mulitNum; $i++)
            {
                $select = new QuestionSelect;
                $select->title = $inputs['mulitselect'.$i];
                $select->question_id = $question->id;
                if(in_array($i, $mulitTrue))
                {
                    $select->if_true = 1;
                }
                
                $select->save();
            }
        }
        if($question->type == 3)
        {
            for($i = 1;$i<3; $i++)
            {
                $select = new QuestionSelect;
                if($i == 1)
                {
                    $select->title = "对";
                }
                else
                {
                    $select->title = "错";
                }
                $select->question_id = $question->id;
                if(array_key_exists('falseture', $inputs))
                {
                    if($inputs['falseture']==$i)
                    {
                        $select->if_true = 1;
                    }
                }
                else
                {
                    $question->delete();
                    return false;
                }
                $select->save();
            }
        }
        return true;

    }

    /**
     * Store a question.
     *
     * @param  array $inputs

     * @return void
     */
    public function store($inputs, $bank_id)
    {
        $result = 1;
        $question = new $this->model;
        $result = $this->saveQuestion($question, $inputs, $bank_id);
        return $result;
    }


    /**
     * update a item.
     *
     * @param  array $inputs

     * @return void
     */
    public function update($inputs, $bank_id, $question_id)
    {
        $result = 1;
        $question = $this->getById($question_id);
        $result = $this->saveQuestion($question, $inputs, $bank_id);
        return $result;
    }

    public function getReport($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $bankList = $this->bank_gestion->getList();
        
        $res['bankSelect'] = QuestionBank::whereIn('id',$bankList)->lists('title','id')->all();
        $res['bankSelect']['0'] = trans('table.input_select_bank');
        ksort($res['bankSelect']);
        if(isset($inputs['bank_id']))
        {
            $bankList = QuestionBank::whereIn('id',$bankList)->where('id',$inputs['bank_id'])->lists('id')->all();
        }
        
        $res['questions'] = $this->model->whereIn('bank_id',$bankList)->orderBy($orderby,$direction)->paginate($n);
        foreach($res['questions'] as $question)
        {
            if($question->type != 4)
            {
                $sum = count($question->questionResults);
                $right = QuestionResult::where('question_id',$question->id)
                                                                      ->where('if_true',1)->count();
                if($sum == 0 )
                {
                    $res['true'][$question->id] = 0;
                    $res['false'][$question->id] = 0;
                }
                elseif($right == $sum)
                {
                    $res['true'][$question->id] = 100;
                    $res['false'][$question->id] = 0;
                }
                elseif($right == 0)
                {
                    $res['true'][$question->id] = 0;
                    $res['false'][$question->id] = 100;
                }
                else
                {
                    $res['true'][$question->id] = round($right/$sum*100,2);
                    $res['false'][$question->id] = 100- $res['true'][$question->id];
                }
            }
        }
        return $res;
        
    }


    public function excel($inputs)
    {
        $prog[] = array(trans('question.title'),trans('bank.title'),trans('table.type'),trans('table.answer_number'),'%'.trans('table.true'),'%'.trans('table.false'));
        foreach($inputs['questions'] as $question)
        {
            switch ($question->type)
            {
                case 1:
                $type = trans('question.radio');
                break;
                case 2:
                $type = trans('question.mulit');
                break;
                case 3:
                $type = trans('question.false');
                break;
                case 4:
                $type = trans('question.essay');
                break;
                default:
                $type = trans('table.unknow');
            }
            if($question->type !=4)
            {
                $true =  $inputs['true'][$question->id];
                $false =  $inputs['false'][$question->id];
            }
            else
            {
                $true =  "";
                $false =  "";
            }
            $prog[] = array($question->title,
                            $question->bank->title,
                            $type,
                            count($question->questionResults),
                            $true,
                            $false);
        }
        return $prog;
    }
     public function FetchRepeatMemberInArray($array)
    {
        // 获取去掉重复数据的数组
        $unique_arr = array_unique ( $array );
        // 获取重复数据的数组
        $repeat_arr = array_diff_assoc ( $array, $unique_arr );
        return $repeat_arr;
    }



    public function getResultById($Rid)
    {
        return QuestionResult::find($Rid);
    }
    

    public function import($bank_id,$file)
    {
        $result = array('message'=>array('success'=>trans('error.10000')));
        //$log = $this->log_gestion->createLog($file,3);
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            $select = array();
            $selectNum = 0;
            if(!empty($prog[array_search('操作',$tag)]) && !empty($prog[array_search('题目类型',$tag)]) && !empty($prog[array_search('题目',$tag)])&& !empty($prog[array_search('题目ID',$tag)]))
            {
                $ID[] = $prog[array_search('题目ID',$tag)];
                $type = $prog[array_search('题目类型',$tag)];
                switch($prog[array_search('强制',$tag)])
                {
                    case 'TRUE':
                    $priority = 1;
                    break;
                    default:
                    $priority = 0;
                }
                
                for($i=1;$i<=10;$i++)
                {
                    if(!empty($prog[array_search('选项'.$i,$tag)]))
                    {
                        $select[] = $prog[array_search('选项'.$i,$tag)];
                        $selectNum++;
                    }
                }

                switch($type)
                {
                    case 'SC':
                    if($selectNum == 0)
                    {
                        $result['message'] = array('fail'=>trans('error.00002'));
                        return $result;
                    }
                    $true = $prog[array_search('正确答案',$tag)];
                    $question = array('act'=>$prog[array_search('操作',$tag)],
                                      'Qtype'=>$type,
                                      'title'=>$prog[array_search('题目',$tag)],
                                      'question_id' => $prog[array_search('题目ID',$tag)],
                                      'priority'=>$priority,
                                      'radioture'=>$true,
                                      'radioNUM'=>$selectNum
                    );
                    for($i = 1;$i <= $selectNum;$i++)
                    {
                        $question += array("radioselect".$i=>$select[$i-1]);
                    }
                    break;
                    case 'MC':
                    
                    if($selectNum == 0)
                    {
                        $result['message'] = array('fail'=>trans('error.00002'));
                        return $result;
                    }
                    $true = explode(',',$prog[array_search('正确答案',$tag)]);
                    $question = array('act'=>$prog[array_search('操作',$tag)],
                                      'Qtype'=>$type,
                                      'title'=>$prog[array_search('题目',$tag)],
                                      'question_id' => $prog[array_search('题目ID',$tag)],
                                      'priority'=>$priority,
                                      'multipleture'=>$true,
                                      'mulitNUM'=>$selectNum
                    );
                    for($i = 1;$i <= $selectNum;$i++)
                    {
                        $question += array('mulitselect'.$i=>$select[$i-1]);
                    }
                    break;
                    case 'TF':
                    if($prog[array_search('正确答案',$tag)] == 'TRUE')
                        $true = 1;
                    else
                        $true = 2;
                    $question = array('act'=>$prog[array_search('操作',$tag)],
                                      'Qtype'=>$type,
                                      'title'=>$prog[array_search('题目',$tag)],
                                      'question_id' => $prog[array_search('题目ID',$tag)],
                                      'priority'=>$priority,
                                      'falseture'=>$true
                    );
                    break;
                    case 'ES':
                    $question = array('act'=>$prog[array_search('操作',$tag)],
                                      'Qtype'=>$type,
                                      'title'=>$prog[array_search('题目',$tag)],
                                      'question_id' => $prog[array_search('题目ID',$tag)],
                                      'priority'=>$priority
                    );
                    break;
                    
                }
                $data[] = $question;
            }
            else
            {
                $result['message'] = array('fail'=>trans('error.00040'));
                return $result;
            }
        }
        $error = $this->FetchRepeatMemberInArray($ID);
        if(!empty($error))
        {
            $result['message'] = array('fail'=>implode(",",$error).trans('error.00013'));
            return $result;
        }

       
        $success = 0;
        foreach($data as $info)
        {
            switch($info['act'])
            {
                case 'A':
                $question = $this->model->where('question_id',$info['question_id'])->first();
                if($question)
                {
                    if($question->bank_id == $bank_id)
                    {
                        $res = $this->saveQuestion($question, $info, $bank_id);
                    }
                    else
                    {
                        $result['message'] = array('fail'=>$info['question_id'].trans('error.00013').";".$success.trans('question.title').trans('error.10000'));
                        return $result;
                    }
                }
                else
                {
                    $question = new $this->model;
                    $res = $this->saveQuestion($question, $info, $bank_id);
                }
                if($res = true)
                {
                    $success++;
                }
                break;
                case 'D':
                $question = $this->model->where('question_id',$info['question_id'])->first();
                if($question && $question->bank_id == $bank_id)
                {
                    $this->destroy($question->id);
                    $success++;
                }
                break;
            }
            
        }
        $result['message'] = array('success'=>$success.trans('question.title').trans('error.10000'));
        return $result;
    }

    public function questionExcel($bank_id)
    {
        $bank = $this->bank_gestion->getById($bank_id);
        $prog[] = array('操作','题目ID','题目类型','强制','题目','正确答案','选项1','选项2','选项3','选项4','选项5','选项6','选项7','选项8','选项9','选项10');
        foreach($bank->questions as $question)
        {
            $priority = $question->priority?'TRUE':'FALSE';
            $true = array();
            $chose = array();
            foreach($question->selects as $key=>$select)
            {
                $chose[] = $select->title;
                if($select->if_true)
                    $true[] = $key+1;
            }
            $true = implode(',',$true);
            switch($question->type)
            {
                case 1:
                $type = 'SC';
                
                $info = array('A',$question->question_id,$type,$priority,$question->title,$true);
                $info = array_merge($info,$chose);
                break;
                case 2:
                $type = 'MC';
                $info = array('A',$question->question_id,$type,$priority,$question->title,$true);
                $info = array_merge($info,$chose);
                break;
                case 3:
                $type = 'TF';
                foreach($question->selects as $key=>$select)
                {
                    if($select->if_true && $select->title =="对")
                        $true="TRUE";
                    else
                        $true = "FALSE";
                }
                $info = array('A',$question->question_id,$type,$priority,$question->title,$true);
                break;
                case 4:
                $type = 'ES';
                $info = array('A',$question->question_id,$type,$priority,$question->title);
                break;
            }
            $prog[] = $info;
        }
        return $prog;
    }

}
