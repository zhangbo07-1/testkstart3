<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\EmailTemplet;
use App\Models\User;
use Mail;
class SendEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $username,$useremail, $emailTitle,$emailBody,$hostname;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(EmailTemplet $email, User $user)
    {
        $this->username = $user->name;
        $this->useremail= $user->email;
        $this->emailTitle = $email->title;
        $this->emailBody = $email->body;
        $this->hostname = $_SERVER['HTTP_HOST'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
