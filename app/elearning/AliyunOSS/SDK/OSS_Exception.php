<?php

namespace App\elearning\AliyunOSS\SDK;
use App\elearning\AliyunOSS\SDK\XML2Array;
use Exception;

// EXCEPTIONS
/**
 * OSS异常类，继承自基类
 */
class OSS_Exception extends Exception {}
